#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "SDL2.lib")
#pragma comment(lib, "SDL2main.lib")
#pragma comment(lib, "SDL2_ttf.lib")
#pragma comment(lib, "SDL2_image.lib")

#pragma comment(linker, "/SUBSYSTEM:windows /entry:mainCRTStartup")

#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#ifdef __APPLE__
	#include <SDL2_image/SDL_image.h>
	#include <SDL2_ttf/SDL_ttf.h>
#else
	#include <SDL2/SDL_ttf.h>
	#include <SDL2/SDL_image.h>
#endif

#define GAMEPAD_MAX 2
#define GAMEPAD_BUTTON_A 0
#define GAMEPAD_BUTTON_B 1
#define GAMEPAD_BUTTON_X 2
#define GAMEPAD_BUTTON_Y 3
#define GAMEPAD_BUTTON_LS 4
#define GAMEPAD_BUTTON_RS 5
#define GAMEPAD_BUTTON_START 6
#define GAMEPAD_LAXIS 7
#define GAMEPAD_LT 8
#define GAMEPAD_RT 9
#define GAMEPAD_LTDOWN 10
#define GAMEPAD_RTDOWN 11

void openGamePads() {
	SDL_GameController* controller = NULL;
	for (int i = 0; i < SDL_NumJoysticks(); ++i) {
		if (SDL_IsGameController(i)) {
			controller = SDL_GameControllerOpen(i);
		}
	}
}

int principal(int argc, char* argv[]) {
	SDL_Init(SDL_INIT_EVERYTHING);

	const unsigned int gamePadCount = SDL_NumJoysticks();
	if (gamePadCount) {	// S'il y des manettes connect�es.
		SDL_GameController* gamePads[GAMEPAD_MAX];	// Pointeur vers les manettes.
		short gamePadValues[] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

		// Connexion des manettes...
		for (unsigned int i = 0; i < gamePadCount; i++)
			gamePads[i] = SDL_GameControllerOpen(i);

		// Cr�ation de la fen�tre...		
		SDL_Window* sdlWindow = SDL_CreateWindow("Exemple de gestion de manette", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 550, 250, 0);
		SDL_Renderer* sdlRenderer = SDL_CreateRenderer(sdlWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

		SDL_Rect rects[] = { { 350, 200, 50, 50 }, { 400, 150, 50, 50 }, { 300, 150, 50, 50 }, { 350, 100, 50, 50 }, { 0, 0, 250, 50 }, { 300, 0, 250, 50 }, { 0, 0, 0, 0 }, { 100, 100, 150, 150 }, { 0, 100, 50, 150 }, { 500, 100, 50, 150 }, { 0, 100, 50, 0 }, { 500, 100, 50, 0 }};
		SDL_Point leftXAxis = { 175, 175 };

		SDL_Event sdlEvent;
		bool isOpen = true;
		while (isOpen) {	// Boucle de jeux...
			// Gestion des �v�nements...
			while (SDL_PollEvent(&sdlEvent)) {
				switch (sdlEvent.type) {
					case SDL_QUIT:
						isOpen = false;
						break;

					case SDL_CONTROLLERAXISMOTION: {
						unsigned int gamePadId = sdlEvent.cbutton.which;
						short gamePadAxisValue = sdlEvent.caxis.value;
						switch (sdlEvent.caxis.axis) {
							case SDL_CONTROLLER_AXIS_LEFTX:
								leftXAxis.x = gamePadAxisValue * 74 / 32765 + 174;
								break;

							case SDL_CONTROLLER_AXIS_LEFTY:
								leftXAxis.y = gamePadAxisValue * 74 / 32765 + 174;
								break;

							case SDL_CONTROLLER_AXIS_TRIGGERLEFT:
								rects[GAMEPAD_LTDOWN].h = gamePadAxisValue * 150 / 32767;
								break;

							case SDL_CONTROLLER_AXIS_TRIGGERRIGHT:
								rects[GAMEPAD_RTDOWN].h = gamePadAxisValue * 150 / 32767;
								break;
						}
					}
					break;

					case SDL_CONTROLLERBUTTONDOWN: {
						unsigned int gamePadId = sdlEvent.cbutton.which;
						switch (sdlEvent.cbutton.button) {
							case SDL_CONTROLLER_BUTTON_A:
								gamePadValues[GAMEPAD_BUTTON_A] = 1;
								break;

							case SDL_CONTROLLER_BUTTON_B:
								gamePadValues[GAMEPAD_BUTTON_B] = 1;
								break;

							case SDL_CONTROLLER_BUTTON_X:
								gamePadValues[GAMEPAD_BUTTON_X] = 1;

								break;

							case SDL_CONTROLLER_BUTTON_Y:
								gamePadValues[GAMEPAD_BUTTON_Y] = 1;

								break;

							case SDL_CONTROLLER_BUTTON_LEFTSHOULDER:
								gamePadValues[GAMEPAD_BUTTON_LS] = 1;
								break;

							case SDL_CONTROLLER_BUTTON_RIGHTSHOULDER:
								gamePadValues[GAMEPAD_BUTTON_RS] = 1;
								break;

							case SDL_CONTROLLER_BUTTON_START:
								gamePadValues[GAMEPAD_BUTTON_START] = 1;
								break;
						}
					}
					break;

					case SDL_CONTROLLERBUTTONUP: {
						unsigned int gamePadId = sdlEvent.cbutton.which;
						switch (sdlEvent.cbutton.button) {
							case SDL_CONTROLLER_BUTTON_A:
								gamePadValues[GAMEPAD_BUTTON_A] = 0;
								break;

							case SDL_CONTROLLER_BUTTON_B:
								gamePadValues[GAMEPAD_BUTTON_B] = 0;
								break;

							case SDL_CONTROLLER_BUTTON_X:
								gamePadValues[GAMEPAD_BUTTON_X] = 0;
								break;

							case SDL_CONTROLLER_BUTTON_Y:
								gamePadValues[GAMEPAD_BUTTON_Y] = 0;
								break;

							case SDL_CONTROLLER_BUTTON_LEFTSHOULDER:
								gamePadValues[GAMEPAD_BUTTON_LS] = 0;
								break;

							case SDL_CONTROLLER_BUTTON_RIGHTSHOULDER:
								gamePadValues[GAMEPAD_BUTTON_RS] = 0;
								break;

							case SDL_CONTROLLER_BUTTON_START:
								gamePadValues[GAMEPAD_BUTTON_START] = 0;
								break;
						}
						break;
					}
				}
			}

			// Affichage...
			SDL_SetRenderDrawColor(sdlRenderer, 0, 0, 0, 255);
			SDL_RenderClear(sdlRenderer);

			SDL_SetRenderDrawColor(sdlRenderer, 255, 255, 255, 255);

			SDL_RenderDrawRect(sdlRenderer, &rects[GAMEPAD_LAXIS]);
			SDL_RenderDrawPoint(sdlRenderer, leftXAxis.x, leftXAxis.y);

			SDL_RenderDrawRect(sdlRenderer, &rects[GAMEPAD_LT]);
			SDL_RenderFillRect(sdlRenderer, &rects[GAMEPAD_LTDOWN]);
			SDL_RenderDrawRect(sdlRenderer, &rects[GAMEPAD_RT]);
			SDL_RenderFillRect(sdlRenderer, &rects[GAMEPAD_RTDOWN]);

			(gamePadValues[GAMEPAD_BUTTON_LS]) ? SDL_RenderFillRect(sdlRenderer, &rects[GAMEPAD_BUTTON_LS]) : SDL_RenderDrawRect(sdlRenderer, &rects[GAMEPAD_BUTTON_LS]);
			(gamePadValues[GAMEPAD_BUTTON_RS]) ? SDL_RenderFillRect(sdlRenderer, &rects[GAMEPAD_BUTTON_RS]) : SDL_RenderDrawRect(sdlRenderer, &rects[GAMEPAD_BUTTON_RS]);
			(gamePadValues[GAMEPAD_BUTTON_A]) ? SDL_RenderFillRect(sdlRenderer, &rects[GAMEPAD_BUTTON_A]) : SDL_RenderDrawRect(sdlRenderer, &rects[GAMEPAD_BUTTON_A]);
			(gamePadValues[GAMEPAD_BUTTON_B]) ? SDL_RenderFillRect(sdlRenderer, &rects[GAMEPAD_BUTTON_B]) : SDL_RenderDrawRect(sdlRenderer, &rects[GAMEPAD_BUTTON_B]);
			(gamePadValues[GAMEPAD_BUTTON_X]) ? SDL_RenderFillRect(sdlRenderer, &rects[GAMEPAD_BUTTON_X]) : SDL_RenderDrawRect(sdlRenderer, &rects[GAMEPAD_BUTTON_X]);
			(gamePadValues[GAMEPAD_BUTTON_Y]) ? SDL_RenderFillRect(sdlRenderer, &rects[GAMEPAD_BUTTON_Y]) : SDL_RenderDrawRect(sdlRenderer, &rects[GAMEPAD_BUTTON_Y]);

			SDL_RenderPresent(sdlRenderer);
		}

		// Lib�ration de la fen�tre...
		SDL_DestroyRenderer(sdlRenderer);
		SDL_DestroyWindow(sdlWindow);

		// D�connexion des manettes...
		for (unsigned int i = 0; i < gamePadCount; i++)
			SDL_GameControllerClose(gamePads[i]);
	}

	SDL_Quit();
	return 0;
}
