#ifndef ORBITAL_H
#define ORBITAL_H

#define PI 3.14159265358979324

#include "Camera3V.h"
#include "Vector3d.h"

using namespace std;

class Orbital : public Camera3v {
private:
	double rotatedAround;
	double rotatedOver;
	double rayonOnGround;
	double rayon;
	double rayonMax;
	double rayonMin;
public:
	Orbital(Vector3d position, Vector3d target, Vector3d up) : Camera3v(position, target, up) {
		rayonMax = 40;
		rayonMin = 2;
		rayonOnGround = sqrt(((position.z - target.z) * (position.z - target.z)) + ((position.x - target.x) * (position.x - target.x)));
		rayon = sqrt(((position.y - target.y) * (position.y - target.y)) + (rayonOnGround * rayonOnGround));
		rotatedAround = atan((position.x - target.x) / rayonOnGround); // Permet de trouver l'angle de rotation actuelle au sol de la cam�ra en foncton de la cible
		rotatedOver = acos(rayonOnGround / rayon); // Permet de trouver l'angle de rotation actuelle dans les airs de la cam�ra en fonction de la cible
		if (rayon > rayonMax) {
			rayon = rayonMax;
		}
	}

	void rotateAroundTarget2(double anglex, double angley) {
		Vector3d normal = position - target; // Le vecteur normal est le vecteur ORIENTATION au point de la cible

		normal.normalize();

		rotatedAround += anglex;
		rotatedOver += angley;

		normal.x = sin(rotatedAround);
		normal.z = cos(rotatedAround);

		normal.normalize();

		if (rotatedOver > (PI / 2) || rotatedOver < -(PI / 2)) { // v�rifie qu'on ne regarde pas de trop haut
			rotatedOver -= angley;
		}

		normal.y = sin(rotatedOver);
		normal.normalize();

		normal = normal * rayon;

		Vector3d temp = normal + target;

		if (!(sqrt((abs(temp.x - target.x) * abs(temp.x - target.x)) + (abs(temp.y - target.y) * abs(temp.y - target.y)) + (abs(temp.z - target.z) * abs(temp.z - target.z))) > rayonMax)) { // S'assure qu'on ne d�zoome jamais plus que le rayon maximal
			if (!(sqrt((abs(temp.x - target.x) * abs(temp.x - target.x)) + (abs(temp.y - target.y) * abs(temp.y - target.y)) + (abs(temp.z - target.z) * abs(temp.z - target.z))) < rayonMin)) { // S'assure qu'on ne zoom jamais plus que le rayon minimal
				changePosition(temp);

				rayonOnGround = sqrt(((position.z - target.z) * (position.z - target.z)) + ((position.x - target.x) * (position.x - target.x)));
				rayon = sqrt(((position.y - target.y) * (position.y - target.y)) + (rayonOnGround * rayonOnGround));
			}
		}
	}

	void zoom(double amount) {
		Vector3d normal = target - position;

		normal.normalize();

		normal = normal * amount;

		Vector3d temp = normal + position;
		if (!(sqrt((abs(temp.x - target.x) * abs(temp.x - target.x)) + (abs(temp.y - target.y) * abs(temp.y - target.y)) + (abs(temp.z - target.z) * abs(temp.z - target.z))) > rayonMax)) { // S'assure qu'on ne d�zoome jamais plus que le rayon maximal
				if(!(sqrt((abs(temp.x - target.x) * abs(temp.x - target.x)) + (abs(temp.y - target.y) * abs(temp.y - target.y)) + (abs(temp.z - target.z) * abs(temp.z - target.z))) < rayonMin)) { // S'assure qu'on ne zoom jamais plus que le rayon minimal
					changePosition(temp);

					rayonOnGround = sqrt(((position.z - target.z) * (position.z - target.z)) + ((position.x - target.x) * (position.x - target.x)));
					rayon = sqrt(((position.y - target.y) * (position.y - target.y)) + (rayonOnGround * rayonOnGround));
				}

		}
	}


	void notification() {

	}

};

#endif