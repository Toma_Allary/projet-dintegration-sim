/// \file RobotTestingSpace.h
/// \brief ecran de test de v�hicule avant de commencer
#ifndef ROBOTTESTINGSPACE_H
#define ROBOTTESTINGSPACE_H

#define GAMEPAD_MAX 2
#define GAMEPAD_BUTTON_A 0
#define GAMEPAD_BUTTON_B 1
#define GAMEPAD_BUTTON_X 2
#define GAMEPAD_BUTTON_Y 3
#define GAMEPAD_BUTTON_LS 4
#define GAMEPAD_BUTTON_RS 5
#define GAMEPAD_BUTTON_START 6
#define GAMEPAD_LAXIS 7
#define GAMEPAD_LT 8
#define GAMEPAD_RT 9
#define GAMEPAD_LTDOWN 10
#define GAMEPAD_RTDOWN 11

#include "Scene3d.h"
#include "Image.h"
#include "DoubleLabel.h"
#include "LabelImageButton.h"
#include "libSDL2.h"
#include "Component.h"
#include "FrameBloc.h"
#include "Robot.h"
#include "Orbital.h"
#include "Chrono.h"
#include "Observable.h"
#include "Ground.h"
#include "DynamicCamera.h"


void garagePrototype();

/// \class RobotTestingSpace
/// \brief ecran de test de v�hicule avant de commencer
///
/// ecran de test de v�hicule avant de commencer
///
class RobotTestingSpace : public Scene3d {
private:
	unsigned int buttonBackgroundId,
	menuBackgroundId,
	overGarageBackgroundId,
	betonId,
	iceId,
    earthId;
	FrameBloc* frame;
	FlameThrower* testFront;
	Saw* testSaw;
	Robot* robot1;
	bool rightMouseButtonDown;
	bool upkey, downkey, rightkey, leftkey, spacebar, catapult, F;
	Chrono* chrono;
	DoubleLabel* frontTest;
	Orbital* camera;
	//Player* playerTest;
	//DynamicCamera* camera;

public:

	RobotTestingSpace() {
		betonId = SDLGLContext::getInstance()->loadTexture("Textures/Beton.png");
        iceId = SDLGLContext::getInstance()->loadTexture("Textures/Glace.png");
        earthId = SDLGLContext::getInstance()->loadTexture("Textures/Sable.png");
		arena = new Environment();

		arena->addGround(new Ground(1, true, betonId, 0, -1, -25.0, "Objets/MapBase.obj"));
        arena->addGround(new Ground(0.3, false, iceId, 0, -0.9, -34.0, "Objets/flaque.obj"));
        arena->addGround(new Ground(1.5, false, earthId, -5, -0.9, -15.0, "Objets/flaque.obj"));

		buttonBackgroundId = SDLGLContext::getInstance()->loadTexture("Textures/buttonBackground.png");
		overGarageBackgroundId = SDLGLContext::getInstance()->loadTexture("Textures/_buttonBackground.png");
		
		robot1 = new Robot("Paul");
		 frame = new FrameBloc( new Material(SDLGLContext::getInstance()->loadTexture("Textures/usedMetal.png"), 2.00, 2.00, 2.00, 2.00, 2.00, 2.00, 2.00), -3.0, 0.0, -25.0);
		 robot1->addComponent(frame);
		 testSaw = new Saw(new Material(0, 1, 1, 1, 1, 1, 1, 1), 1, 1, -25);
		 testFront = new FlameThrower(new Material(SDLGLContext::getInstance()->loadTexture("Textures/Beton.png"), 2.00, 2.00, 2.00, 2.00, 2.00, 2.00, 2.00), -3.0, 0.0, -25.0);
		 robot1->addComponent(testFront);
		 robot1->addComponent(testSaw);
		 		

		 rightMouseButtonDown = rightkey = leftkey = downkey = upkey = spacebar = catapult = F = false;
		 camera = new Orbital(Vector3d(0.0, 10.0, 0.0), Vector3d(0.0, 0.0, -25.0), Vector3d(0.0, 1.0, 0.0));
		 //camera = new DynamicCamera(Vector3d(-3.0, 10.0, -50.0), Vector3d(-10.0, 5.0, -25.0), Vector3d(0.0, 1.0, 0.0), robot1,nullptr);

		 drawables["garageButton"] = new LabelImageButton(buttonBackgroundId, ResourceManager::get<Font*>("font12"), { 255, 255, 255, 255 }, "Retour au Garage", 0, 0, 149, 30, 0.1, overGarageBackgroundId);
		 ((Button*)drawables["garageButton"])->bindOnClick(garagePrototype);
		 events[SDL_MOUSEBUTTONDOWN] = new Observable();
		 events[SDL_MOUSEBUTTONDOWN]->subscribe(drawables["garageButton"]);
		 events[SDL_MOUSEMOTION] = new Observable();
		 events[SDL_MOUSEMOTION]->subscribe(drawables["garageButton"]);

		 chrono = new Chrono();

		//playerTest= new Player();

		 //frontTest = new DoubleLabel(170, 635.0, ResourceManager::get<Font*>("font12"), to_string(robot1->getComponentList()->back()->getFront().x), { 0, 0, 255, 255 }, 0.2, 2.0);
		 //drawables["test"] = new LabelImageButton(buttonBackgroundId, ResourceManager::get<Font*>("font12"), { 255, 255, 255, 255 }, to_string(robot1->getComponentList()->back()->getFront().x), 200, 0, 200, 30, 0.1, overGarageBackgroundId);
		 //drawables["testy"] = new LabelImageButton(buttonBackgroundId, ResourceManager::get<Font*>("font12"), { 255, 255, 255, 255 }, to_string(robot1->getComponentList()->back()->getFront().y), 400, 0, 200, 30, 0.1, overGarageBackgroundId);

	}

	~RobotTestingSpace() {
		glDeleteTextures(1, &buttonBackgroundId);
		glDeleteTextures(1, &menuBackgroundId);
		glDeleteTextures(1, &overGarageBackgroundId);
		glDeleteTextures(1, &betonId);
        glDeleteTextures(1, &iceId);
        glDeleteTextures(1, &earthId);
		delete camera;
		//delete playerTest;

		while (meshes.size()) {
			Mesh3a* toDelete = meshes.back();
			meshes.pop_back();
			delete toDelete;
		}

		for (auto it : events)
			delete it.second;

		for (auto it : drawables)
			delete it.second;
	}

	void draw() {
		// 3D
		//cout<<SDLEvent::getInstance()->sdlEvent.caxis.value * 100 / 32765 << endl;
		const unsigned int gamePadNum = SDL_NumJoysticks();
		if (gamePadNum) {
			SDL_GameController* gamePads[GAMEPAD_MAX];

			short gamePadInput[] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

			for (unsigned int i = 0; i < gamePadNum; i++)
				gamePads[i] = SDL_GameControllerOpen(i);
		}

		glEnable(GL_LIGHTING);

		SDLGLContext::getInstance()->setPerspective(false);

		double sensibility = 0.001;
		double sensibilityCont = 0.0001;
		if (SDLEvent::getInstance()->sdlEvent.type == SDL_MOUSEMOTION && rightMouseButtonDown) { // v�rifie si la sourie bouge ET qu'un bouton de la sourie est appuy�
			camera->rotateAroundTarget2(-(SDLEvent::getInstance()->sdlEvent.motion.xrel) * sensibility, SDLEvent::getInstance()->sdlEvent.motion.yrel * sensibility);
		}
		if (SDLEvent::getInstance()->sdlEvent.caxis.axis == SDL_CONTROLLER_AXIS_RIGHTX || SDLEvent::getInstance()->sdlEvent.caxis.axis == SDL_CONTROLLER_AXIS_RIGHTX) {
			SDL_Point contMouv = {0, 0};
			if (SDLEvent::getInstance()->sdlEvent.caxis.axis == SDL_CONTROLLER_AXIS_RIGHTX)
				contMouv.x = SDLEvent::getInstance()->sdlEvent.caxis.value * 74 / 32765;
			if (SDLEvent::getInstance()->sdlEvent.caxis.axis == SDL_CONTROLLER_AXIS_RIGHTY)
				contMouv.y = SDLEvent::getInstance()->sdlEvent.caxis.value * 74 / 32765;
			camera->rotateAroundTarget2(-(contMouv.x)  * sensibilityCont, (contMouv.y) * sensibilityCont);
		}


		if (SDLEvent::getInstance()->sdlEvent.type == SDL_MOUSEWHEEL) {
			camera->zoom(SDLEvent::getInstance()->sdlEvent.wheel.y * 0.05);
		}

		camera->applyView();
		
			
		if (chrono->getElapsedTime() > 0.01) {
			if (robot1->getComponentList()->front()->getPosition().y > 0.0) 
				robot1->applyGravityOnRobot(chrono->getElapsedTime(), robot1->getWheelsAdherence());
			if (robot1->getComponentList()->front()->getPosition().y < 0.0) {
				robot1->resetY();
			}
		
			if ((spacebar) and (robot1->getJumpBool())) {
				//if(robot1->getComponentList()->back()->getPosition().y <= 0.005)
				robot1->jump(chrono->getElapsedTime());
				robot1->setJumpBool(false);
				//robot1->free(chrono->getElapsedTime());
			}
			else
				if (F) {
					robot1->fly(chrono->getElapsedTime());
				}
			
			else
				if (upkey)
					robot1->acceleration(chrono->getElapsedTime());
				else
					if (downkey)
						robot1->deceleration(chrono->getElapsedTime());
				else
					robot1->free(chrono->getElapsedTime());
				if (rightkey)
					robot1->direction(true, robot1->getLength(), robot1->getWidth(), 1, chrono->getElapsedTime()); //angle en radiant
				else
					if (leftkey)
						robot1->direction(false, robot1->getLength(), robot1->getWidth(), 1, chrono->getElapsedTime());
			chrono->reset();
		}
		arena->draw();
//		((LabelImageButton*)drawables["test"])->getLabel()->setText("X : " + to_string(robot1->getComponentList()->back()->getFront().x), {255, 255, 0, 255});
//		((LabelImageButton*)drawables["testy"])->getLabel()->setText("Z : " + to_string(robot1->getComponentList()->back()->getFront().z), { 255, 250, 0, 255 });

		robot1->draw();

		// 2D
		SDLGLContext::getInstance()->setPerspective(true);
		glDisable(GL_LIGHTING);

		for (auto it : drawables)
			it.second->draw();

		glEnable(GL_LIGHTING);
	}

	void notification(){
		switch (SDLEvent::getInstance()->sdlEvent.type) {
		case SDL_MOUSEBUTTONDOWN:
			events[SDL_MOUSEBUTTONDOWN]->notify();
			if (SDLEvent::getInstance()->sdlEvent.button.button == SDL_BUTTON_RIGHT) {
				rightMouseButtonDown = true; // IMPORTANT sinon le mouvement de cam�ra avec la souris de fonctionne pas
			}
			break;
		case SDL_MOUSEBUTTONUP:
			if (SDLEvent::getInstance()->sdlEvent.button.button == SDL_BUTTON_RIGHT) {
				rightMouseButtonDown = false; // IMPORTANT sinon le mouvement de cam�ra avec la souris de fonctionne pas
			}
	
			break;
		case SDL_MOUSEMOTION:
			events[SDL_MOUSEMOTION]->notify();
			break;
		
		}
		switch (SDLEvent::getInstance()->sdlEvent.type) {
		case SDL_KEYDOWN:
			switch (SDLEvent::getInstance()->sdlEvent.key.keysym.sym) {
			case SDLK_f:
				F = true;
				break;
			case SDLK_UP:
				upkey = true;
				break;

			case SDLK_SPACE:
				spacebar = true;
				break;

			case SDLK_DOWN:
				downkey = true;
				break;

			case SDLK_RIGHT:
				rightkey = true;
				break;
			case SDLK_LEFT:
				leftkey = true;
				break;
			
			case SDLK_0:
				catapult = true;
				break;
			case SDLK_q:
				testSaw->activate();
				break;
			}
			break;
		case SDL_KEYUP:
			switch (SDLEvent::getInstance()->sdlEvent.key.keysym.sym) {
			case SDLK_UP:
				upkey = false;
				break;
			case SDLK_f:
				F = false;
				break;
			case SDLK_DOWN:
				downkey = false;
				break;

			case SDLK_RIGHT:
				rightkey = false;
				break;

			case SDLK_LEFT:
				leftkey = false;
				break;

			case SDLK_SPACE:
				spacebar = false;
				robot1->setJumpBool(true);
				break;
			case SDLK_q:
				testSaw->disable();
				break;
			}
			break;
		case SDL_CONTROLLERAXISMOTION:
			//playerTest->notification();
			short gamePadAxisValue = SDLEvent::getInstance()->sdlEvent.caxis.value;
			switch (SDLEvent::getInstance()->sdlEvent.caxis.axis) {
				case SDL_CONTROLLER_AXIS_RIGHTX:
					//TODO
					break;
				case SDL_CONTROLLER_AXIS_RIGHTY:
					//TODO
					break;

				case SDL_CONTROLLER_AXIS_TRIGGERLEFT:
					downkey = true;
					break;

				case SDL_CONTROLLER_AXIS_TRIGGERRIGHT:
					upkey = true;
					break;
			}
				
			break;
		}
		/*chrono->reset();
		if(upkey)
			robot1->acceleration(chrono->getElapsedTime());
		else
		if (downkey)
			robot1->deceleration(chrono->getElapsedTime());
		if (rightkey)
			robot1->direction(true, robot1->getLength(), robot1->getWidth(), 90, chrono->getElapsedTime()); //robot1->getLength(), robot1->getWidth()
		else
		if (leftkey)
			robot1->direction(false, robot1->getLength(), robot1->getWidth(), 90, chrono->getElapsedTime());
		//if (!upkey and !downkey)
			//robot1->free(chrono->getElapsedTime());
		robot1->draw();*/

		// 2D
		SDLGLContext::getInstance()->setPerspective(true);
		glDisable(GL_LIGHTING);

		for (auto it : drawables)
			it.second->draw();

			
		


	}

	string getType() {
		return "RobotTestingSpace";
		
	}

	};

/// \brief change de scene pour aller vers le garage
void garagePrototype() {
	Scene::setScene(GARAGE);
}
#endif