/// \file FrameBloc.h
/// \brief Bloc de structure
#ifndef FRAMEBLOC_H
#define FRAMEBLOC_H

#include "Component.h"
#include "Mesh3a.h"

/// \class FrameBloc
/// \brief Bloc de structure
///
/// Bloc composant la structure generale du vehicule
///
class FrameBloc : public Component {
private:
	unsigned int conTextID;
    unsigned int conSelTextID;
public:
    /// \brief constructe
    FrameBloc(Material* material, const double& x, const double& y, const double& z, const unsigned int& selecId = 0) : Component(material, SDLGLContext::getInstance()->loadTexture("Textures/usedMetal.png"), x, y ,z, "Objets/chassis4W.obj"){
		textID = SDLGLContext::getInstance()->loadTexture("Textures/usedMetal.png");
		conTextID = SDLGLContext::getInstance()->loadTexture("Textures/snapPoint.png");
        conSelTextID = SDLGLContext::getInstance()->loadTexture("Textures/snapPointSelect.png");
    type = FRAMEBLOC;
    name = "Structures";
		price = 50;
		mass = 100;
		health = 500 * material->getPriceMutltiplicator();
		maxHealth = 500 * material->getPriceMutltiplicator();
		energieCost = 0;
		
		connectors[0] = (new Mesh3a(conTextID, x, y, z, "Objets/crate.obj", conSelTextID));
		connectors[0]->getTransformationMatrix()->setScale(0.3);
		connectors[0]->transform();

		connectors[1] = (new Mesh3a(conTextID, x, y, this->getMaxZ(), "Objets/crate.obj", conSelTextID));
		connectors[1]->getTransformationMatrix()->setScale(0.3);
		connectors[1]->transform();
		
		connectors[2] = (new Mesh3a(conTextID, this->getMinX(), y, this->getMaxZ(), "Objets/crate.obj", conSelTextID));
		connectors[2]->getTransformationMatrix()->setScale(0.3);
		connectors[2]->transform();

		connectors[3] = (new Mesh3a(conTextID, this->getMaxX(), y, this->getMaxZ(), "Objets/crate.obj", conSelTextID));
		connectors[3]->getTransformationMatrix()->setScale(0.3);
		connectors[3]->transform();
		
		connectors[4] = (new Mesh3a(conTextID, this->getMinX() , y, this->getMinZ(), "Objets/crate.obj", conSelTextID));
		connectors[4]->getTransformationMatrix()->setScale(0.3);
		connectors[4]->transform();
		 
		connectors[5] = (new Mesh3a(conTextID, this->getMaxX(), y, this->getMinZ(), "Objets/crate.obj", conSelTextID));
		connectors[5]->getTransformationMatrix()->setScale(0.3);
		connectors[5]->transform();

		hitboxes.push_back(new HitBox(this->position, -3, getMinX(), getMaxY(), getMinY(), getMaxZ(), -25));
		hitboxes.push_back(new HitBox(this->position, -3, getMinX(), getMaxY(), getMinY(), -25, getMinZ()));
		hitboxes.push_back(new HitBox(this->position, -3, -1, getMaxY(), getMinY(), 1.02 + (this->position.z), -1.03 + (this->position.z)));
		hitboxes.push_back(new HitBox(this->position, 1, -1, getMaxY(), getMinY(), 1.96 + (this->position.z), -1.97 + (this->position.z)));
		hitboxes.push_back(new HitBox(this->position, 3, 1, getMaxY(), getMinY(), 1.02 + (this->position.z), -1.03 + (this->position.z)));
		hitboxes.push_back(new HitBox(this->position, getMaxX(), 3, getMaxY(), getMinY(), getMaxZ(), -25));
		hitboxes.push_back(new HitBox(this->position, getMaxX(), 3, getMaxY(), getMinY(), -25, getMinZ()));
    }
    /// \brief constructeur de copie
    FrameBloc(FrameBloc* frameBloc) : Component(frameBloc->getMaterial(), 0, frameBloc->getPosition().x, frameBloc->getPosition().y, frameBloc->getPosition().z, frameBloc->fileName){
		  textID = SDLGLContext::getInstance()->loadTexture("Textures/usedMetal.png");
		  conTextID = SDLGLContext::getInstance()->loadTexture("Textures/snapPoint.png");
        type = FRAMEBLOC;
        name = "Structures";
        price = 50;
		mass = 100;
		health = 500 * material->getPriceMutltiplicator();
		maxHealth = 500 * material->getPriceMutltiplicator();
		energieCost = 0;
		this->connectors = frameBloc->getConnectors();

		hitboxes.push_back(new HitBox(this->position, -3, getMinX(), getMaxY(), getMinY(), getMaxZ(), -25));
		hitboxes.push_back(new HitBox(this->position, -3, getMinX(), getMaxY(), getMinY(), -25, getMinZ()));
		hitboxes.push_back(new HitBox(this->position, -3, -1, getMaxY(), getMinY(), 1.02 + (this->position.z), -1.03 + (this->position.z)));
		hitboxes.push_back(new HitBox(this->position, 1, -1, getMaxY(), getMinY(), 1.96 + (this->position.z), -1.97 + (this->position.z)));
		hitboxes.push_back(new HitBox(this->position, 3, 1, getMaxY(), getMinY(), 1.02 + (this->position.z), -1.03 + (this->position.z)));
		hitboxes.push_back(new HitBox(this->position, getMaxX(), 3, getMaxY(), getMinY(), getMaxZ(), -25));
		hitboxes.push_back(new HitBox(this->position, getMaxX(), 3, getMaxY(), getMinY(), -25, getMinZ()));
	}
    ~FrameBloc(){
		
    }

	void levelUp(){
		
	}

    //Afiichage
    void draw(){
        Mesh3a::draw();
		for (int i = 0; i < connectors.size(); i++) connectors[i]->draw();
    }
	/// \brief Obtenir Description
	/// \return retourne la description
		list<string>* getDescrition(){
			list<string>* description = new list<string>;
			description->push_back("Descrition:");
			description->push_back("Bloc permettant de former la structue");
			description->push_back("Prix: " + to_string((int)getPrice()));
			description->push_back("Masse: " + to_string((int)getMass()));
			description->push_back("Vie: " + to_string((int)getHealth()) + "/" + to_string((int)getMaxHealth()));
			return description;
		  }
	/// \brief Notification
    virtual void notification(){}

};

#endif