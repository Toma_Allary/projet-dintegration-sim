	#/// \file Ntree.h
/// \brief arbre ennaire
#ifndef NTREE_H
#define NTREE_H

#include <map>
#include "Node.h"
#include "NodeIterator.h"
#include <list>

using namespace std;

template <typename T>
class Ntree {
private:
	unsigned int size;
	Node<T>* root; ///< noeud de depart de l'arbre
	///\brief recherche d'element
	Node<T>* searchRecursive(T data, Node<T>* runner) {
		if (data == runner->data)
			return runner;

		for (int i = 0; i < runner->linksCount; i++) {
			if (runner->next[i]) {
				Node<T>* returnValue = searchRecursive(data, runner->next[i]);
				if (returnValue)
					return returnValue;
			}
		}

		return nullptr;

	}

	///\brief path des elements de l'arbre
	void pathRecursive(list<T> path, Node<T>* runner) {
		if (runner) {
			path.push_back(runner->data);

			for (int i = 0; i < runner->linksCount; i++) {
				if (runner->next[i])
					pathRecursive(path, runner->next[i]);
			}
		}
	}

public:
	///\brief constructeur
	Ntree() {
		size = 0;
		root = nullptr;
	}
	///\brief destructeur
	~Ntree() {
		remove(root->data);
	}

	unsigned int getSize(){
		return size;
	}
	///\brief ajout d'un noued a l'arbre
	void add(T data, unsigned int size, T parent, unsigned int parentId, unsigned IdToParent) {
		if (root) {
			Node<T>* addTo = searchRecursive(parent);
			if (addTo) {
				for (int i = 0; i < addTo->linksCount; i++) {
					if (!addTo->next[i]) {
						addTo->next[i] = new Node<T>(data, size, addTo, parentId, IdToParent);
						addTo->linksCount++;
						i = addTo->linksCount;
					}
				}
			}
		}
		else {
			root = new Node<T>(data, size, nullptr, 0, 0);
		}
		this->size++;

	}

	///\brief recherche d'elements
	Node<T>* searchRecursive(T data) {
		if (root) {
			return searchRecursive(data, root);
		}
		else {
			return nullptr;
		}
	}

	///\brief path des elements de l'arbre
	list<T> pathRecursive() {
		list<T> path;
		if (root) {
			path.push_back(root->data);

			for (int i = 0; i < root->linksCount; i++) {
				if (root->next[i])
					pathRecursive(path,root ->next[i]);
			}
		}

		return path;
		
	}

	///\brief retrait d'elements
	void remove(T data) {
		if (searchRecursive(data)) {
			size--;

			Node<T> *toDelete = searchRecursive(data);
			//retrait des enfants
			for (int i = 0; i < toDelete->linksCount; i++) {
				if (toDelete->next[i])
					remove(toDelete->next[i]->data);
			}

			//mettre le pointeur du parent vers la data � nullptr
			if (toDelete->parent) {
				for (int i = 0; i < toDelete->parent->linksCount; i++) {
					if (toDelete->parent->next[i] == toDelete) {
						toDelete->parent->next[i] = nullptr;
					}
				}
			}
			toDelete->parent = nullptr;
			if (toDelete == root)
				root = nullptr;
			delete toDelete;
			toDelete = nullptr;
		}
	}

	T getRootData() {
		if (root) {
			return root->data;
		}
		return nullptr;
	}

};



#endif
