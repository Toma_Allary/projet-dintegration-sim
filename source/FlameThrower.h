/// \file FlameThrower.h
/// \brief Lance flamme
#ifndef FLAMETHROWER_H
#define FLAMETHROWER_H

#include <list>

#include "Weapon.h"
#include "Mesh3a.h"
#include "Chrono.h"
#include <time.h>
#include <cmath>

/// \class FlameThrower
/// \brief Lance flamme
///
/// Lance flamme
///
class FlameThrower: public Weapon {
private:
	unsigned int conTextID;
    unsigned int conSelTextID;

    double flameThrowerSize;///< Distance entre la position du lance flammes et le bout ou sortent les flammes
    list<Mesh3a*> flames;///< Flammes sortant du lance flammes
    map<unsigned int, double> lastScaleOfFlame;///< Dernier scaling de la flamme identifier par sa position dans la liste flames
    double range;///< Coefficient de grandeur du cone de flame
    map<unsigned int, unsigned int> flameTexture;///< Textures des flammes
public:
    FlameThrower(Material* material, const double& x, const double& y, const double& z, const unsigned int& selecId = 0) : Weapon(material, x, y ,z, "Objets/lanceFlamme.obj" ){
        textID = SDLGLContext::getInstance()->loadTexture("Textures/lanceFlamme.png");
		conTextID = SDLGLContext::getInstance()->loadTexture("Textures/snapPoint.png");
        conSelTextID = SDLGLContext::getInstance()->loadTexture("Textures/snapPointSelect.png");

        flameTexture[0] = SDLGLContext::getInstance()->loadTexture("Textures/feu1.png");
        flameTexture[1] = SDLGLContext::getInstance()->loadTexture("Textures/feu2.png");
        flameTexture[2] = SDLGLContext::getInstance()->loadTexture("Textures/feu3.png");
        for(int i = 0; i < 2 ; i++){
            flames.push_back(new Mesh3a(flameTexture[i % 3], 0, 4, -25.0, "Objets/crate.obj"));
			lastScaleOfFlame[i] = 2.5;
        }

        range = 0.85;
        type = FLAMETHROWER;
        name = "Lance flamme";
		price = 600;
		mass = 400;
		health = 400 * material->getPriceMutltiplicator();
        maxHealth = 400 * material->getPriceMutltiplicator();
        energieCost = 1;
		damage = 3;
		flameThrowerSize = 3.24296; //calculer avec le point le plus eloigne du centre (l'avant du lance flamme)dans blender

		connectors[0] = (new Mesh3a(conTextID, x, y, this->getMaxZ(), "Objets/crate.obj", conSelTextID));
		connectors[0]->getTransformationMatrix()->setScale(0.3);
		connectors[0]->transform();

		hitboxes.push_back(new HitBox(this->position, getMaxX(), -0.68, 0.01 + getMaxY(), 0.1 + (this->position.y), -.2 + (this->position.z), -0.01 + getMinZ()));
		hitboxes.push_back(new HitBox(this->position, getMaxX(), -1.85, 0.25 + (this->position.y), -0.25 + (this->position.y), 0.04 + (this->position.z), -0.45 + (this->position.z)));
		hitboxes.push_back(new HitBox(this->position, -2.86, -2.44, 0.3 + (this->position.y), -0.44 + (this->position.y), -0.58 + (this->position.z), 0.17 + (this->position.z)));
		hitboxes.push_back(new HitBox(this->position, -3.26, -2.86, 0.14 + (this->position.y), -0.36 + (this->position.y), 0.04 + (this->position.z), -0.45 + (this->position.z)));
		hitboxes.push_back(new HitBox(this->position, -2.54, -1.84, 0.25 + (this->position.y), -.3 + (this->position.y), 0.04 + (this->position.z), -0.46 + (this->position.z)));

	}
    FlameThrower(FlameThrower* flameThrower) : Weapon(flameThrower->getMaterial(), flameThrower->position.x, flameThrower->position.y ,flameThrower->position.z, flameThrower->fileName){
        textID = SDLGLContext::getInstance()->loadTexture("Textures/lanceFlamme.png");
		conTextID = SDLGLContext::getInstance()->loadTexture("Textures/snapPoint.png");
        conSelTextID = SDLGLContext::getInstance()->loadTexture("Textures/snapPointSelect.png");

        flameTexture[0] = SDLGLContext::getInstance()->loadTexture("Textures/feu1.png");
        flameTexture[1] = SDLGLContext::getInstance()->loadTexture("Textures/feu2.png");
        flameTexture[2] = SDLGLContext::getInstance()->loadTexture("Textures/feu3.png");
        for(int i = 0; i < 2 ; i++){
            flames.push_back(new Mesh3a(flameTexture[i % 3], 0, 4, -25.0, "Objets/crate.obj"));
			lastScaleOfFlame[i] = 2.5;
        }


        range = flameThrower->range;
        type = FLAMETHROWER;
        name = "Lance flamme";
		price = 600;
		mass = 400;
		health = 400 * material->getPriceMutltiplicator();
        maxHealth = 400 * material->getPriceMutltiplicator();
        energieCost = 1;
        damage = 55;
		flameThrowerSize = 3.24296; //calculer avec le point le plus eloigne du centre (l'avant du lance flamme)dans blender

		connectors[0] = (new Mesh3a(conTextID, flameThrower->position.x, flameThrower->position.y, this->getMaxZ(), "Objets/crate.obj", conSelTextID));
		connectors[0]->getTransformationMatrix()->setScale(0.3);
		connectors[0]->transform();

		hitboxes.push_back(new HitBox(this->position, getMaxX(), -0.68, 0.01 + getMaxY(), 0.1 + (this->position.y), -.2 + (this->position.z), -0.01 + getMinZ()));
		hitboxes.push_back(new HitBox(this->position, getMaxX(), -1.85, 0.25 + (this->position.y), -0.25 + (this->position.y),  0.04 + (this->position.z), -0.45 + (this->position.z)));
		hitboxes.push_back(new HitBox(this->position, -2.86, -2.44, 0.3 + (this->position.y), -0.44 + (this->position.y), -0.58 + (this->position.z), 0.17 + (this->position.z)));
		hitboxes.push_back(new HitBox(this->position, -3.26, -2.86, 0.14 + (this->position.y), -0.36 + (this->position.y), 0.04 + (this->position.z), -0.45 + (this->position.z)));
		hitboxes.push_back(new HitBox(this->position, -2.54, -1.84, 0.25 + (this->position.y), -.3 + (this->position.y), 0.04 + (this->position.z), -0.46 + (this->position.z)));

    }
    /// \brief Destructeur
    ~FlameThrower(){
        for(auto it : flames)
            delete it;
    }

    /// \brief Activer l'effet de la composante
    virtual void activate(){
		isActivated = true;
		overTimeUse = true;
    }

	inline bool isItActivated() {
		return isActivated;
	}
    /// \brief Desactiver l'effet de la composante
    virtual void disable(){
		isActivated = false;
		overTimeUse = false;
    }

	list<Mesh3a*> getFlameList() {
		return flames;
	}

    void levelUp(){
        level++;
		range += 0.15;
        damage *= 1.5;
		maxHealth *= 1.5;
		health *= 1.5;
	}

    /// \brief Affichage
    void draw(){
		if(overTimeUse){
            int i = 0;
            for(auto it : flames){
                //remettre a son scaling d'origine pour eviter des problemes de detection de collision
                it->getTransformationMatrix()->setScale(1 / lastScaleOfFlame[i]);
                it->transform();

                double howFarTheFlameIs = ((double)rand() / RAND_MAX) * 5.85 * range;
                lastScaleOfFlame[i] = howFarTheFlameIs * range;
                it->getTransformationMatrix()->setScale(howFarTheFlameIs * range);
                it->transform();
                it->setPosition(front.x * (flameThrowerSize + howFarTheFlameIs) + position.x, front.y * (flameThrowerSize + howFarTheFlameIs) + position.y, front.z * (flameThrowerSize + howFarTheFlameIs) + position.z);
                it->draw(); 
                i++;             
            }
		}
        Mesh3a::draw();
		for (int i = 0; i < connectors.size(); i++) connectors[i]->draw();
    }

    /// \brief Notification
    void notification(){
        
    }

};

#endif