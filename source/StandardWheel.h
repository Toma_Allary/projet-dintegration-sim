/// \file StandardWheel.h
/// \brief Roues standards
#ifndef STANDARDWHEEL_H
#define STANDARDWHEEL_H

#include "Locomotion.h"

/// \class StandardWheel
/// \brief roues standards
///
/// roues standards
///
class StandardWheel : public Locomotion {
private:
	unsigned int conTextID;
    unsigned int conSelTextID;
	Matrix44d fowradRotation;
	Chrono fowradRotationChrono;
public:
    StandardWheel(Material* material, const double& x, const double& y, const double& z, const unsigned int& selecId = 0) : Locomotion(material, x, y , z, "Objets/weel.obj"){
			textID = SDLGLContext::getInstance()->loadTexture("Textures/weel.png");
			conTextID = SDLGLContext::getInstance()->loadTexture("Textures/snapPoint.png");
        conSelTextID = SDLGLContext::getInstance()->loadTexture("Textures/snapPointSelect.png");
			type = STANDARDWHEEL;
			name = "Roue standard";
			price = 100;
			mass = 100;
			health = 400 * material->getPriceMutltiplicator();
			maxHealth = 400 * material->getPriceMutltiplicator();
			energieCost = 3;
			adherence = 3;

			connectors[0] = (new Mesh3a(conTextID, x, this->getMinY(), z, "Objets/crate.obj", conSelTextID));
			connectors[0]->getTransformationMatrix()->setScale(0.3);
			connectors[0]->transform();
			hitboxes.push_back(new HitBox(this->position, getMaxX(), getMinX(), getMaxY(), getMinY(), getMaxZ(), getMinZ()));
	}

    StandardWheel(StandardWheel* standardWheel) : Locomotion(standardWheel->getMaterial(), standardWheel->position.x, standardWheel->position.y , standardWheel->position.z, standardWheel->fileName){
        textID = SDLGLContext::getInstance()->loadTexture("Textures/weel.png");
		conTextID = SDLGLContext::getInstance()->loadTexture("Textures/snapPoint.png");
        conSelTextID = SDLGLContext::getInstance()->loadTexture("Textures/snapPointSelect.png");
        type = STANDARDWHEEL;
        name = "Roue standard";
		price = 100;
		mass = 100;
		health = 400 * material->getPriceMutltiplicator();
		maxHealth = 400 * material->getPriceMutltiplicator();
		energieCost = 3;
        adherence = 3;

		connectors[0] = (new Mesh3a(conTextID, standardWheel->position.x, standardWheel->getMinY(), standardWheel->position.z, "Objets/crate.obj", conSelTextID));
		connectors[0]->getTransformationMatrix()->setScale(0.3);
		connectors[0]->transform();
        hitboxes.push_back(new HitBox(this->position, getMaxX(), getMinX(), getMaxY(), getMinY(), getMaxZ(), getMinZ()));
	}
    ~StandardWheel(){

    }

    /// \brief Affichage
    void draw(){
		/*fowradRotation.loadRotation(speed->getNorm() * fowradRotationChrono.getElapsedTime() / 2.45, up);
		setTransformationMatrix(fowradRotation);
		transform();*/

        Mesh3a::draw();
		for (int i = 0; i < connectors.size(); i++) connectors[i]->draw();
		fowradRotationChrono.reset();
    }

    /// \brief Notification
    void notification() {}

	inline bool isItActivated() {
		return isActivated;
	}

	double getAdherence(){
		return adherence;
	}
};

#endif
