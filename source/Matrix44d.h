/// \file Matrix44d.h
/// \brief fichier contenant la classe Matrix44d

#ifndef MATRIX44D_H
#define MATRIX44D_H

#include <cmath>
#include <algorithm>
#include "Vector3d.h"

#define m11 matrix[0]
#define m12 matrix[1]
#define m13 matrix[2]
#define m14 matrix[3]
#define m21 matrix[4]
#define m22 matrix[5]
#define m23 matrix[6]
#define m24 matrix[7]
#define m31 matrix[8]
#define m32 matrix[9]
#define m33 matrix[10]
#define m34 matrix[11]
#define m41 matrix[12]
#define m42 matrix[13]
#define m43 matrix[14]
#define m44 matrix[15]

/// \class Matrix44d
/// \brief Matrice 4X4 de réels
///
/// Matrice 4x4 de réels
///
class Matrix44d {
public:
	double* matrix;///< Matrice.
	double rotAngle;

	/// \brief Constructeur.
	Matrix44d() {
		matrix = new double[16];
		double rotAngle = 0.0;
		//création d'une matrix par défault
		resetMatrix();
	}

	/// \brief Destructeur.
	~Matrix44d() {
	//	delete[] matrix;
	}

	/// \sets matrix
	void resetMatrix() {
		m11 = 1.0;	m12 = 0.0;  m13 = 0.0; m14 = 0.0;
		m21 = 0.0;  m22 = 1.0;  m23 = 0.0; m24 = 0.0;
		m31 = 0.0;  m32 = 0.0;  m33 = 1.0; m34 = 0.0;
		m41 = 0.0;  m42 = 0.0;  m43 = 0.0; m44 = 0.0;
	}

	/// \brief Chargement de la matrice identité.
	void loadIdentity() {
		std::fill(&m11, &m44, 0.0);
		m11 = m22 = m33 = m44 = 1.0;
	}

	/// \brief Chargement de la matrice orthogonale.
	void loadOrthogonal(double r, double b) {
		m11 = 2.0 / r; m12 = 0.0;      m13 =  0.0; m14 = 0.0;
		m21 = 0.0;     m22 = 2.0 / -b; m23 =  0.0; m24 = 0.0;
		m31 = 0.0;     m32 = 0.0;      m33 = -2.0; m34 = 0.0;
		m41 = -1.0;    m42 = 1.0;      m43 =  0.0; m44 = 1.0;
	}

	/// \brief Chargement de la matrice de projection.
	/// \param r Bordure droite
	/// \param t Bordure supérieure.
	/// \param n Plan rapproché.
	/// \param f plan éloigné.
	void loadProjection(double r, double t, double n, double f) {
		m11 = n / r; m12 = 0.0;   m13 =  0.0;                      m14 =  0.0;
		m21 = 0.0;   m22 = n / t; m23 =  0.0;                      m24 =  0.0;
		m31 = 0.0;   m32 = 0.0;   m33 = -(f + n) / (f - n);        m34 = -1.0;
		m41 = 0.0;   m42 = 0.0;   m43 =  (-2.0 * f * n) / (f - n); m44 =  0.0;
	}

	/// \brief Chargement de la matrice de vue.
	/// \param pos Position.
	/// \param target Cible.
	/// \param up Haut
	void loadView(Vector3d pos, Vector3d target, Vector3d up) {
		Vector3d front = target - pos; front.normalize();
		Vector3d side = front % up; side.normalize();
		Vector3d nup = side % front; nup.normalize();		

		m11 = side.x; m12 = nup.x; m13 = -front.x; m14 = 0.0;
		m21 = side.y; m22 = nup.y; m23 = -front.y; m24 = 0.0;
		m31 = side.z; m32 = nup.z; m33 = -front.z; m34 = 0.0;
		m41 = 0.0;    m42 = 0.0;   m43 =  0.0;     m44 = 1.0;
	}

	/// \brief Chargement de la matrice de rotation sur l'axe des x.
	/// \param angle Angle.
	void loadXRotation(double angle) {
		rotAngle = angle;
		m11 = 1.0; m12 = 0.0;        m13 =  0.0;          m14 = 0.0;
		m21 = 0.0; m22 = cos(rotAngle); m23 = -(sin(rotAngle)); m24 = 0.0;
		m31 = 0.0; m32 = sin(rotAngle); m33 =  cos(rotAngle);   m34 = 0.0;
		m41 = 0.0; m42 = 0.0;        m43 =  0.0;          m44 = 1.0;
	}

	/// \brief Chargement de la matrice de rotation sur l'axe des y.
    /// \param angle Angle.
	void loadYRotation(double angle) {
		rotAngle = angle;
		m11 = cos(rotAngle);    m12 = 0.0;        m13 = sin(rotAngle);          m14 = 0.0;
		m21 = 0.0;           m22 = 1.0;        m23 = 0.0;          m24 = 0.0;
		m31 = -(sin(rotAngle)); m32 = 0.0;        m33 = cos(rotAngle);   m34 = 0.0;
		m41 = 0.0;           m42 = 0.0;        m43 = 0.0;          m44 = 1.0;
	}

	/// \brief Chargement de la matrice de rotation sur l'axe des z.
	/// \param angle Angle.
	void loadZRotation(double angle) {
		rotAngle = angle;
		m11 = cos(rotAngle);  m12 = -(sin(rotAngle)); m13 =0.0 ;          m14 = 0.0;
		m21 = sin(rotAngle);  m22 = cos(rotAngle);    m23 = 0.0;          m24 = 0.0;
		m31 = 0.0;         m32 = 0.0;           m33 = 1.0;          m34 = 0.0;
		m41 = 0.0;         m42 = 0.0;           m43 = 0.0;          m44 = 1.0;
	}

	/// \brief Chargement de la matrice de rotation.
	/// \param angle Angle.
	/// \param axe Axe.
	void loadRotation(const double& angle, Vector3d axe) {
		rotAngle = angle;
		axe.normalize();
		double s = sin(rotAngle);
		double c = cos(rotAngle);
		double nc = 1 - c;

		m11 = (axe.x * axe.x * nc) + c;           m12 = (axe.x * axe.y * nc) - (axe.z * s); m13 = (axe.x * axe.z * nc) + (axe.y * s); m14 = 0.0;
		m21 = (axe.x * axe.y * nc) + (axe.z * s); m22 = (axe.y * axe.y * nc) + c;           m23 = (axe.y * axe.z * nc) - (axe.x * s); m24 = 0.0;
		m31 = (axe.x * axe.z * nc) - (axe.y * s); m32 = (axe.y * axe.z * nc) + (axe.x * s); m33 = (axe.z * axe.z * nc) + c;           m34 = 0.0;
		m41 = 0.0;                              m42 = 0.0;                              m43 = 0.0;                                m44 = 1.0;
	}
	/// \brief Chargement de la matrice de translation
	inline void setTranslation(const double& x, const double& y, const double& z) {
		m41 = x; m42 = y; m43 = z;
	}
	/// \brief Chargement de la matrice de translation
	inline void setScale(const double&  scaleOrder) {
		m14 = m24 = m34 = scaleOrder;

	}

	double getRotAngle() {
		return rotAngle;
	}

	void setRotAngle(double rotAngle) {
		this->rotAngle = rotAngle;
	}
/// \brief Multiplication par un vecteur.
/// \param v Vecteur.
/// \return Vecteur résultant.
	Vector3d operator*(Vector3d v) {
		return Vector3d((v.x * m11) + (v.y * m12) + (v.z * m13), (v.x * m21) + (v.y * m22) + (v.z * m23), (v.x * m31) + (v.y * m32) + (v.z * m33));
	}

	void operator*(const double& scalar) {
		m11 *= scalar; m12 *= scalar; m13 *= scalar; m14 *= scalar;
		m21 *= scalar; m22 *= scalar; m23 *= scalar; m24 *= scalar;
		m31 *= scalar; m32 *= scalar; m33 *= scalar; m34 *= scalar;
		m41 *= scalar; m42 *= scalar; m43 *= scalar; m44 *= scalar;
		
	}

	void invert33Matrix() {
		double det = (m11 * ((m22 * m33) - (m23 * m32))) - (m12 * ((m12 * m33) - (m13 * m32))) + (m13 * ((m12 * m23) - (m13 * m22)));

		if (det) {
			m11 *= 1 / det; m12 *= 1 / det; m13 *= 1 / det;
			m21 *= 1 / det; m22 *= 1 / det; m23 *= 1 / det;
			m31 *= 1 / det; m32 *= 1 / det; m33 *= 1 / det;
		}

	}
		
};

#endif
