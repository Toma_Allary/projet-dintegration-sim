/// \file Observable.h
/// \brief Observable

#ifndef OBSERVABLE_H
#define OBSERVABLE_H

#include <list>
#include <cstdarg>
#include "Observer.h"

/// \class Observable
/// \brief Observable
///
/// Diffuseur d'observations.
///
class Observable {
private:
	std::list<Observer*> observers; ///< Observateurs.

public:
	/// \brief Notification d'observation.
	void notify() { 
		for (auto it : observers)
			it->notification(); 
	}

	/// \brief Ajout d'un observateur.
	/// \param observeur Observateur.
	inline void subscribe(Observer* observer) {
		observers.push_back(observer);
	}

	/// \brief Retrait d'un observateur.
	/// \param observeur Observateur
	inline void unsubscribe(Observer* observer) {
		observers.remove(observer); 
	}
};

#endif
