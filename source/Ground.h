/// \file Ground.h
/// \brief type de terrain
#ifndef PI_GROUND_H
#define PI_GROUND_H

#include "Mesh3a.h"
#include "Locomotion.h"

/// \class Ground
/// \brief type de terrain
///
/// type de terrain
///
class Ground: public Mesh3a{
private:
    double adherenceMultiplicator; ///< aderence
    bool concrete; ///< si il est present physiquement dans l'environement
public:
    Ground(double adherenceMultiplicator, bool concrete, unsigned int textureID, const double& x, const double& y, const double& z, string fileName): Mesh3a(textureID, x, y, z, fileName){
        this->adherenceMultiplicator = adherenceMultiplicator;
        this->concrete = concrete;
    };

    /// \brief obtenir un mu entre le terrain et la roue
    /// \param typeOfLocomotion roue qui touche le terrain
    double getMu(Locomotion* typeOfLocomotion){
        return typeOfLocomotion->getAdherence() * adherenceMultiplicator;
    };

    /// \brief obtenir un bool qui indique si il est present physiquement
    bool getConcrete(){
        return concrete;
    }
};

#endif

