/// \file ComponentButton.h
/// \brief contient la classe ComponentButton.h

#ifndef PI_COMPONENTBUTTON_H
#define PI_COMPONENTBUTTON_H

#include <list>
#include <string>
#include "Component.h"

using namespace std;

/// \class ComponentButton
/// \brief Bouton de composante
///
/// contient un pointeur de composante
///
class ComponentButton : public LabelImageButton{
private:
    Component* buttonComponent;///< composante relier au bouton
public:
    ComponentButton(Component* buttonComponent,const unsigned int& textureId, Font* font, const SDL_Color& fontColor, const string& title, const double& x = 0.0, const double& y = 0.0, const unsigned int& width = 0.0, const unsigned int& height = 0.0, const double& layer = 0.0, const unsigned int overTextureId = 0): LabelImageButton(textureId, font, fontColor, title, x, y, width, height, layer, overTextureId){
        this->buttonComponent= buttonComponent;
    }

    inline Component* getComponent(){
        return buttonComponent;
    }

    /// \brief obtenir la description de la composante
    /// \return Retour de la description
    list<string>* getDescription(){
        return buttonComponent->getDescrition();
    }
    string getType(){
        return "ComponentButton";
    }

};

#endif

