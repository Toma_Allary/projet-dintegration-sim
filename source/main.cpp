#define LOADINGSCREEN 0
#define MAINMENU 1
#define GARAGE 3
#define OPTIONMENU 4
#define PAUSE 5
#define ROBOTTESTINGSPACE 6
#define CAMERATESTINGSPACE 7
#define INGAME 8
#define ENDOFGAME 9

#include "Engine.h"
#include"FPSCounter.h"

int main(int argc, char* argv[]) {
	initSDL();

	Engine* engine = new Engine;
	engine->start();
	delete engine;

	ResourceManager::deleteAll();

	quitSDL();

	return 0;
}