/// \file Catapult.h
/// \brief Catapulte
#ifndef CATAPULT_H
#define CATAPULT_H

#include "Weapon.h"
#include "CannonBall.h"

/// \class Catapult
/// \brief Catapulte
///
/// Catapulte
///
class Catapult: public Weapon {
private:
	unsigned int conTextID;
    unsigned int conSelTextID;
	unsigned int cannonBallID;
	CannonBall* cannonBall;
	Chrono* catapultChrono;
	bool wasUse;
	double catapultPower;
public:
    Catapult(Material* material, const double& x, const double& y, const double& z, const unsigned int& selecId = 0) : Weapon(material, x, y ,z, "Objets/catapult.obj"){
        textID = SDLGLContext::getInstance()->loadTexture("Textures/catapult.png");
		conTextID = SDLGLContext::getInstance()->loadTexture("Textures/snapPoint.png");
        conSelTextID = SDLGLContext::getInstance()->loadTexture("Textures/snapPointSelect.png");
		cannonBallID = SDLGLContext::getInstance()->loadTexture("Textures/feu1.png");
		cannonBall = new CannonBall(cannonBallID, x, 6.3, z);
		
		cannonBall->resetFrontAndUp(front, up);
		catapultChrono = new Chrono();
		wasUse = false;
		catapultPower = 0.0;
		type = CATAPULT;
        name = "Catapulte";
		price = 250;
		mass = 500;
		health = 400 * material->getPriceMutltiplicator();
		maxHealth = 400 * material->getPriceMutltiplicator();
		energieCost = 1;
        damage = 150;
		overTimeUse = false;
		up = Vector3d(0.0, 1.0, 0.0);
		connectors[0] = (new Mesh3a(conTextID, x, y, this->getMinZ(), "Objets/crate.obj", conSelTextID));
		connectors[0]->getTransformationMatrix()->setScale(0.3);
		connectors[0]->transform();

		hitboxes.push_back(new HitBox(this->position, 0.49, -0.49, 4.84 + (this->position.y), 3.85 + (this->position.y), -0.2 + (this->position.z), -0.69 + (this->position.z)));
		hitboxes.push_back(new HitBox(this->position, 1.5, -1.5, 0.33 + (this->position.y), -0.65 + (this->position.y), .18 + (this->position.z), -0.85 + (this->position.z)));
		hitboxes.push_back(new HitBox(this->position, 0.23, -0.23, 3.94 + (this->position.y), 0.30 + (this->position.y), -0.11 + (this->position.z), -0.58 + (this->position.z)));

    }
    Catapult(Catapult* catapult) : Weapon(catapult->getMaterial(), catapult->position.x, catapult->position.y ,catapult->position.z, catapult->fileName){
        textID = SDLGLContext::getInstance()->loadTexture("Textures/catapult.png");
		conTextID = SDLGLContext::getInstance()->loadTexture("Textures/snapPoint.png");
        conSelTextID = SDLGLContext::getInstance()->loadTexture("Textures/snapPointSelect.png");
		catapultChrono = catapult->catapultChrono;
        type = CATAPULT;
        name = "Catapulte";
		price = 250;
		mass = 500;
		health = 400 * material->getPriceMutltiplicator();
		maxHealth = 400 * material->getPriceMutltiplicator();
		energieCost = 1;
        damage = 150;
		up = Vector3d(0.0, 1.0, 0.0);
		cannonBall = catapult->cannonBall;
		connectors[0] = (new Mesh3a(conTextID, catapult->position.x, catapult->position.y, this->getMinZ(), "Objets/crate.obj", conSelTextID));
		connectors[0]->getTransformationMatrix()->setScale(0.3);
		connectors[0]->transform();

		hitboxes.push_back(new HitBox(this->position, 0.49, -0.49, 4.84 + (this->position.y), 3.85 + (this->position.y), -0.2 + (this->position.z), -0.69 + (this->position.z)));
		hitboxes.push_back(new HitBox(this->position, 1.5, -1.5, 0.33 + (this->position.y), -0.65 + (this->position.y), .18 + (this->position.z), -0.85 + (this->position.z)));
		hitboxes.push_back(new HitBox(this->position, 0.23, -0.23, 3.94 + (this->position.y), 0.30 + (this->position.y), -0.11 + (this->position.z), -0.58 + (this->position.z)));
    }
	/// \brief Destructeur
    ~Catapult(){
		//delete cannonBall;
    }

	void levelUp(){
		level++;
        damage *= 1.5;
		maxHealth *= 1.5;
		health *= 1.5;
	}

	void launchCannonBall(double power) {
		if (cannonBall) {
			cannonBall->launch(power);
		}
	}

    /// \brief Activer l'effet de la composante
    virtual void activate(){
		overTimeUse = true;
	}
    /// \brief Desactiver l'effet de la composante
    virtual void disable(){
		overTimeUse = false;
    }

	inline bool isItActivated() {
		return isActivated;
	}

	CannonBall* getCannonBall() {
		return cannonBall;
	}

    /// \brief Affichage
    void draw(){
		bool stillInRotation = false;
		bool readyToLaunch = false;
		//if(Scene3d::getScene())
		double deltaT = catapultChrono->getElapsedTime();
		if (deltaT > 0.01) {
			catapultChrono->reset();
			if (overTimeUse) {
				if (this->getUp().y > 0.5) {
					rotationMatrix.loadRotation(0.004, front);
					catapultPower = (0.9 - up.y) * 5;
					if (catapultPower < 0.1)
						catapultPower = 0.1001 * 5;
					wasUse = false;
				}
				cannonBall->resetFrontAndUp(front, up);
			}
			else {
				if (this->getUp().y < 0.9) {
					rotationMatrix.loadRotation(-0.08, front);
					stillInRotation = true;
					wasUse = true;

					//cannonBall->resetSpeed;
				}
				else if (wasUse) {
					readyToLaunch = true;
					wasUse = false;
					//stillInRotation = false;
				}
				cannonBall->resetFrontAndUp(front, up);
			}

			if (cannonBall->isInTheAir()) {
				if (cannonBall->getPosition().y < 0) {//collision?
					cannonBall->changeInTheAirBool();
				}
				else {
					cannonBall->setWindAndGravity(-0.240);
					cannonBall->setSpeed(cannonBall->applyGravityAndWind(cannonBall->getSpeed(), deltaT, 0)->x, cannonBall->applyGravityAndWind(cannonBall->getSpeed(), deltaT, 0.0)->y, cannonBall->applyGravityAndWind(cannonBall->getSpeed(), deltaT, 0)->z);
					cannonBall->setPosition(cannonBall->getPosition().x + cannonBall->getSpeed()->x, cannonBall->getPosition().y + cannonBall->getSpeed()->y, cannonBall->getPosition().z + cannonBall->getSpeed()->z);
				}
			}
			else if (readyToLaunch) {
				cannonBall->launch(catapultPower);
			}
			else {
				cannonBall->setPosition(hitboxes.front()->getMinX() + (hitboxes.front()->getMaxX() - hitboxes.front()->getMinX())/2,
					hitboxes.front()->getMinY() + (hitboxes.front()->getMaxY() - hitboxes.front()->getMinY()) / 2,
					hitboxes.front()->getMinZ());
			}
		}
		this->setTransformationMatrix(rotationMatrix);
		this->transform();
        Mesh3a::draw();
		for (int i = 0; i < connectors.size(); i++) connectors[i]->draw();
		rotationMatrix.loadIdentity();
		if (cannonBall) {
			cannonBall->draw();
		}
		
    }

    /// \brief Notification
    void notification(){

    }

};


#endif