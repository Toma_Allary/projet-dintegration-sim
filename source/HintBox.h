/// \file HintBox.h
/// \brief contient la classe HintBox

#ifndef PI_HINTBOX_H
#define PI_HINTBOX_H


#include <string>
#include <list>
#include <string>
#include <SDL2/SDL_opengl.h>
#include "Font.h"
#include "VisualComponent.h"
#include "Label.h"

using namespace std;

/// \class HintBox
/// \brief Boite à indices
///
/// Boite avec un label à l'intérieur
///

class HintBox : public VisualComponent {
private:

    list<string>* stringList;///< Liste de string que l'ont veut afficher.
    Font* font;///< La police de caratère dans l'aquelle la description est affiché.
    list<Label*> labelList;///< Liste de Label.
    double widthNeeded;///< La largeur qu'il faut pour boite.
    double heightNeeded;///< La hauteur qu'il faut pour la boite.
    SDL_Color color;
public:

    HintBox(unsigned int textureId ,list<string>* stringList,SDL_Color color, double x = 0.0, double y = 0.0, double width = 0.0, double height = 0.0, const double& layer = 0.0): VisualComponent(x,y,width,height,layer){
        this->stringList = stringList;
        this->textureId = textureId;
        this->color = color;

        heightNeeded = 0.0;
        widthNeeded = 0.0;

        font= new Font("Fonts/button.ttf", 10);

        int i=0;

        for (list<string>::iterator it = stringList->begin(); it != stringList->end() ; it++){
            labelList.push_back(new Label( x + 5, y + (10*i), font, *it , color, layer+0.01)) ;
            heightNeeded += labelList.back()->getHeight();
            if(widthNeeded < labelList.back()->getWidth()){
                widthNeeded = labelList.back()->getWidth();
            }

            i++;
        }

        widthNeeded += 10.0;
        changeDimensions(widthNeeded, heightNeeded);


}

~HintBox(){
        delete font;
        for (auto it : labelList){
            delete it;
        }
    }

    /// \brief changer Le texte à l'intérieur de la boite
    void changeText(list<string>* newStringList){
        for (auto it : labelList){
            delete it;
        }

        labelList.clear();

        heightNeeded = 0.0;
        widthNeeded = 0.0;

        int i=0;


        for (list<string>::iterator it = newStringList->begin(); it != newStringList->end() ; it++){
            labelList.push_back(new Label(position.x + 5, position.y + (10*i), font, *it , color, layer+0.01)) ;
            heightNeeded += labelList.back()->getHeight();
            if(widthNeeded < labelList.back()->getWidth()){
                widthNeeded = labelList.back()->getWidth();
            }

            i++;
        }

        widthNeeded += 10.0;
        changeDimensions(widthNeeded, heightNeeded);
    }

    /// \brief Affichage
    void draw() {
        int i=0;
        for (list<Label*>::iterator it = labelList.begin(); it != labelList.end() ; it++){
            (*it)->setPosition(position.x+5, position.y+(10*i));
            i++;
        }
        if(visible) {
            glBindTexture(GL_TEXTURE_2D, textureId);

            glEnableClientState(GL_VERTEX_ARRAY);
            glEnableClientState(GL_TEXTURE_COORD_ARRAY);

            glVertexPointer(3, GL_DOUBLE, 0, vertices);
            glTexCoordPointer(2, GL_DOUBLE, 0, texCoords);

            glDrawArrays(GL_QUADS, 0, 4);

            glDisableClientState(GL_TEXTURE_COORD_ARRAY);

            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            glDrawArrays(GL_QUADS, 0, 4);
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

            glDisableClientState(GL_VERTEX_ARRAY);

            for (auto it : labelList) {
                it->draw();
            }
        }
    }

    /// \brief notification
    void notification(){

 }
    string getType(){
        return "HintBox";
    }

};

#endif //PI_HINTBOX_H
