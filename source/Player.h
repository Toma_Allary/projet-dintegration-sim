/// \file Player.h
/// \brief Joueur
#ifndef PLAYER_H
#define PLAYER_H

#include <list>
#include <cmath>
#include <map>
#include "Component.h"
#include "Robot.h"
#include "Inventory.h"
#include "Observer.h"


/// \class Player
/// \brief Joueur
///
/// Joueur
///
class Player : public Observer {
private:
	double money;///< Argent du joueur
	map<string, short> scalingActionsAmount;///< Nombre lier a un evenement de jauge (de base)
	map<string, Controlable*> componentsAction;///< Composantes controlable a actionner
	map<unsigned int, string> actions;///< Actions assigner aux evenements par le joueur
	list<Component*> inventory;///< Composante que le joueur a acheter
	Controlable* itemInUse;
	Robot* robot;///< Robot du joueur
	unsigned int gamePadId;///< Identificateur de la manette du joueur
	Inventory* inventoryScene;///< Inventaire contenant les informations du joueur
	Chrono deltaT;
	SDL_Point leftAxis;
	EnergyBlock* energyBlock;
	bool isToolActivatable;
	double deltaTime;
	unsigned int victories;
	double healthBeforeGame;
public:
	double pickedUpScrapValue = 0.0;
	Player() {
		healthBeforeGame = 0;
		money = 4000.0;
		isToolActivatable = true;
		victories = 0;
		scalingActionsAmount["turn"] = 0;
		scalingActionsAmount["turnOpposite"]=0;
        scalingActionsAmount["goFoward"] = 0;
        scalingActionsAmount["goBackward"] = 0;
		scalingActionsAmount["launchCannonBall"] = 0;
		itemInUse = nullptr;

		robot = new Robot("Paul");
		inventoryScene = nullptr;
		energyBlock = new EnergyBlock(0.0, 3.0, 0.0);
		//robot->addComponent(energyBlock);
	}
	//todo: destructeur...


	/// \brief Obtenir le robot
	/// \return Retourne le robot du joueur
	Robot* getRobot() {
		return robot;
	}
	/// \brief Changer l'argent disponible au joueur
	void setMoney(double money) {
		this->money = money;
	}
	/// \brief Obtenir l'argent restante au joueur
	/// \return Argent du joueur
	double getMoney() {
		return money;
	}
	/// \brief Obtenir l'identificateur de la manette
	/// \return L'identificateur de la manette
	unsigned int getGamePadId(){
		return gamePadId;
	}
	/// \brief Initialiser l'identificateur de la manette
	/// \param id Identificateur
	void setGamePadId(unsigned int id){
		gamePadId = id;
	}

	void addVictory(){
		victories++;
	}
	inline unsigned int getVictories(){
		return victories;
	}

	double getDamageDealt(){
		double overAllDamage = 0.0;
		for(auto it : *robot->getComponentList()){
			if(it->getType() == SAW or it->getType() == FLAMETHROWER or it->getType() == CATAPULT){
				overAllDamage += ((Weapon*)it)->getDamageDealt();
			}
		}
		return overAllDamage;
	}

	void resetDamageDealt(){
		for(auto it : *robot->getComponentList()){
			if(it->getType() == SAW or it->getType() == FLAMETHROWER or it->getType() == CATAPULT){
				((Weapon*)it)->resetDamageDealt();
			}
		}
	}

	inline void initializeHealthBeforeGame(){
		healthBeforeGame = robot->getLife();
	}
	inline double getHealthBeforeGame(){
		return healthBeforeGame;
	}
	/// \breif Obtenir l'evenement lanchant l'activation de la composante (autre que la locomotion)
	/// \param binded Controlable declenchable par l'evenement que l'on veut trouver
	/// \return L'evenement auquel le controlable est assigner 
	unsigned int getEventBindedToComponent(Controlable* binded){
		for(auto it : componentsAction){
			if(it.second == binded){
				string toFind = it.first;
				for(auto it2 : actions){
					if(it2.second == toFind)
						return it2.first;
				}
			}
		}
	}
	/// \breif Obtenir l'evenement lier a un action
	/// \param action Action lier a l'evenement
	/// \return L'evenement lier a l'action
	unsigned int getEventFromAction(string action){
		for(auto it : actions){
			if(it.second == action)
				return it.first;
		}
	}
	/// \brief Assigner un action a une touche/evenement **ne pas mettre d'action si ce n'est pas une locomotion**
	/// \param event Evenement du gamePad
	/// \param action Nom de l'action a faire lors de l'evenement
	void keyBind(unsigned int event, Controlable* toBind, string action = ""){
		if(actions.find(event) == actions.end()){//si l'evenement n'est pas deja pris
			if(scalingActionsAmount.find(action) == scalingActionsAmount.end()){//si l'action est une action secondaire (pas jaugeable)
				string id = "componentAction" + to_string(componentsAction.size());
				componentsAction[id] = toBind;
				actions[event] = id;
			}else
				actions[event] = action;
		}
	}
	/// \brief Retirer un assignement d'action
	/// \param event Evenement auquel l'action est assigner
	void keyUnbind(unsigned int event){
		if(scalingActionsAmount.find(actions[event]) == scalingActionsAmount.end()){//si l'action est une action secondaire (pas jaugeable)
			componentsAction.erase(actions[event]);
		}
		actions.erase(event);
	}
	/// \brief Obtenir l'inventaire du joueur
	Inventory* getInventory() {
		return inventoryScene;
	}
	/// \brief Ajouter un inventaire au joueur
	void addInventory() {
		inventoryScene = new Inventory();
	}
	/// \brief Affichage
	void draw() {
		robot->draw();
		deltaTime = deltaT.getElapsedTime();
		if (deltaTime > 0.01) {
			deltaT.reset();
			if (scalingActionsAmount["goFoward"] != 0) {
				//faire avancer le robot
				robot->acceleration(deltaTime, scalingActionsAmount["goFoward"] / (32767.0));
			}
			else {
				if (scalingActionsAmount["goBackward"] != 0) {
					//faire reculer le robot
					robot->deceleration(deltaTime, scalingActionsAmount["goBackward"] / (32767.0));
				}
				else {
					robot->free(deltaTime);
				}
			}
			if (scalingActionsAmount["turn"] < -0.0001 or scalingActionsAmount["turn"] > 0.0001) {
				//faire tourner le robot
				robot->direction(scalingActionsAmount["turn"] < 0.0, robot->getLength(), robot->getWidth(), (-1.0 * abs(scalingActionsAmount["turn"])) * 0.7 / 32767.0, deltaTime);
				if ((sqrt(robot->getSpeed()->x * robot->getSpeed()->x + robot->getSpeed()->z * robot->getSpeed()->z)) > 0.3)
					robot->setRobotSpeed(*(robot->getSpeed())*0.9);
			}
			if (scalingActionsAmount["turnOpposite"] != 0) {
				robot->direction(true, robot->getLength(), robot->getWidth(), -0.7, deltaTime);
				if ((sqrt(robot->getSpeed()->x * robot->getSpeed()->x + robot->getSpeed()->z * robot->getSpeed()->z)) > 0.3)
					robot->setRobotSpeed(*(robot->getSpeed())*0.9);
			}
			for (auto it : *robot->getComponentList()) {
				if (it->getType() == ENERGYBLOCK) {
					if (itemInUse) {
						if (((EnergyBlock*)it)->isEnergyRemovable(100.0, deltaTime)) {
							((EnergyBlock*)it)->removeEnergy(100.0, deltaTime);
						}
						else {
							itemInUse->disable();
							itemInUse = nullptr;
						}
					}
				}
			}
		}
	}
	/// \brief Notification
	void notification(){
		switch (SDLEvent::getInstance()->sdlEvent.type) {
			case SDL_CONTROLLERAXISMOTION: {
				if(actions.find(SDLEvent::getInstance()->sdlEvent.caxis.axis) != actions.end()){
					if(componentsAction.find(actions[SDLEvent::getInstance()->sdlEvent.cbutton.button]) == componentsAction.end()){
						short gamePadAxisValue = SDLEvent::getInstance()->sdlEvent.caxis.value;
						scalingActionsAmount[actions[SDLEvent::getInstance()->sdlEvent.caxis.axis]] = gamePadAxisValue;
					}
				}
				break;
			}
			case SDL_CONTROLLERBUTTONDOWN:{
				if(actions.find(SDLEvent::getInstance()->sdlEvent.cbutton.button) != actions.end()){
					if(scalingActionsAmount.find(actions[SDLEvent::getInstance()->sdlEvent.cbutton.button]) == scalingActionsAmount.end()){ //protection au cas ou deux event de type different aurait le meme unsigned int
						unsigned int a = SDLEvent::getInstance()->sdlEvent.cbutton.button;
						unsigned int d = SDLEvent::getInstance()->sdlEvent.caxis.axis;
						unsigned int c = SDLEvent::getInstance()->sdlEvent.key.keysym.scancode;

						string b = actions[a];
						

						for (auto it : *robot->getComponentList()) {
							if (it->getType() == ENERGYBLOCK) {
								if (((EnergyBlock*)it)->isEnergyRemovable(100.0, deltaTime)) {
									if (!(componentsAction[actions[SDLEvent::getInstance()->sdlEvent.cbutton.button]]->isItUsedsInstantly())) {
										if (!(componentsAction[actions[SDLEvent::getInstance()->sdlEvent.cbutton.button]]->isItActivated()))
											componentsAction[actions[SDLEvent::getInstance()->sdlEvent.cbutton.button]]->activate();
										itemInUse = componentsAction[actions[SDLEvent::getInstance()->sdlEvent.cbutton.button]];
									}
									else {
										for (auto it : *robot->getComponentList()) {
											if (it->getType() == ENERGYBLOCK) {
												if (isToolActivatable)
													((EnergyBlock*)it)->removeEnergy(100.0, 1.0);
											}
										}
										isToolActivatable = false;
										if(!(itemInUse->isItActivated()))
											componentsAction[actions[SDLEvent::getInstance()->sdlEvent.cbutton.button]]->activate();
									}
								}
								else {
									componentsAction[actions[SDLEvent::getInstance()->sdlEvent.cbutton.button]]->disable();
									itemInUse = nullptr;
								}
							}
						}
					}
				}
			}
				break;
			case SDL_CONTROLLERBUTTONUP:
				if(actions.find(SDLEvent::getInstance()->sdlEvent.cbutton.button) != actions.end()){
					if(scalingActionsAmount.find(actions[SDLEvent::getInstance()->sdlEvent.cbutton.button]) == scalingActionsAmount.end()){ //protection au cas ou deux event de type different aurait le meme unsigned int
						itemInUse = nullptr;;
						isToolActivatable = true;
						componentsAction[actions[SDLEvent::getInstance()->sdlEvent.cbutton.button]]->disable();
					}
				}
				break;
			case SDL_KEYDOWN:
				if(actions.find(SDLEvent::getInstance()->sdlEvent.key.keysym.scancode) != actions.end()){
					if(componentsAction.find(actions[SDLEvent::getInstance()->sdlEvent.key.keysym.scancode]) == componentsAction.end()){
						scalingActionsAmount[actions[SDLEvent::getInstance()->sdlEvent.key.keysym.scancode]] = 32767;
					}else{
						for (auto it : *robot->getComponentList()) {
							if (it->getType() == ENERGYBLOCK) {
								if (((EnergyBlock*)it)->isEnergyRemovable(100.0, deltaTime)) {
									if (!(componentsAction[actions[SDLEvent::getInstance()->sdlEvent.key.keysym.scancode]]->isItUsedsInstantly())) {
										if (!(componentsAction[actions[SDLEvent::getInstance()->sdlEvent.key.keysym.scancode]]->isItActivated()))
											componentsAction[actions[SDLEvent::getInstance()->sdlEvent.key.keysym.scancode]]->activate();
										itemInUse = componentsAction[actions[SDLEvent::getInstance()->sdlEvent.key.keysym.scancode]];
									}
									else {
											for (auto it : *robot->getComponentList()) {
												if (it->getType() == ENERGYBLOCK) {
													if (isToolActivatable)
														((EnergyBlock*)it)->removeEnergy(100.0, 1.0);
												}
											}
											isToolActivatable = false;
											if (!(itemInUse->isItActivated()))
												componentsAction[actions[SDLEvent::getInstance()->sdlEvent.key.keysym.scancode]]->activate();
									}
								
								}
								else {
									componentsAction[actions[SDLEvent::getInstance()->sdlEvent.key.keysym.scancode]]->disable();
									itemInUse = nullptr;;
								}
							}
						}
					}
				}
				break;
			case SDL_KEYUP:
				if(actions.find(SDLEvent::getInstance()->sdlEvent.key.keysym.scancode) != actions.end()){
					if(componentsAction.find(actions[SDLEvent::getInstance()->sdlEvent.key.keysym.scancode]) == componentsAction.end()){
						scalingActionsAmount[actions[SDLEvent::getInstance()->sdlEvent.key.keysym.scancode]] = 0;
						
						if(actions[SDLEvent::getInstance()->sdlEvent.key.keysym.scancode] == "turn" or actions[SDLEvent::getInstance()->sdlEvent.key.keysym.scancode] == "turnOpposite"){
							for (auto it : *(this->robot->getComponentList())) {//tourner les roues pour qu'elles reviennent droite
								if (it->getType() == STANDARDWHEEL or it->getType() == NAILEDWHEEL) {
									if(((Locomotion*)it)->getIsFrontLocomotion()){
										((Locomotion*)it)->turnLocomotionForDirection(0.0);
									}
								}
							}
						}
					}else{
						itemInUse = nullptr;;
						isToolActivatable = true;
						componentsAction[actions[SDLEvent::getInstance()->sdlEvent.key.keysym.scancode]]->disable();
					}
				}
				break;
		}
	}
	
};

#endif