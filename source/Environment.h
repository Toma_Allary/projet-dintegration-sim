/// \file Environment.h
/// \brief regroupe les terrains
#ifndef PI_ENVIRONMENT_H
#define PI_ENVIRONMENT_H

#include <list>
#include "Ground.h"
#include "Drawable.h"

/// \class Environment
/// \brief regroupe les terrains
///
/// regroupe les terrains
///
class Environment : public Drawable{
private:
    list<Ground*>* grounds;
    Mesh3a* atmoSphere;
	map<string, Weapon*> environmentWeapons;

public:
    Environment(){
        grounds = new list<Ground*>();

        atmoSphere = new Mesh3a(SDLGLContext::getInstance()->loadTexture("Textures/bluesky.png"), 0.0, -50.0, 0.0, "Objets/AtmoSphere.obj");
    }

    ~Environment(){
        for(list<Ground*>::iterator it = grounds->begin(); it != grounds->end(); it)
            delete (*it);
        delete grounds;
        
        delete atmoSphere;
		environmentWeapons.erase(environmentWeapons.begin(), environmentWeapons.end());
    }

    /// \brief ajouter un terrain
    /// \param newGround nouveau terrain
    void addGround(Ground* newGround){
        grounds->push_back(newGround);
    }

	inline void addWeapon(Weapon* weapon, string id) {
		environmentWeapons[id] = weapon;
	}

	inline Weapon* getWeapon(string id) {
		return environmentWeapons[id];
	}

	inline map<string, Weapon*> getEnvironmentWeapons() {
		return environmentWeapons;
	}

    /// \brief obtenir tout les terrains
    /// \return liste des terrains
    inline list<Ground*>* getGrounds(){
        return grounds;
    }

    void draw(){
        atmoSphere->draw();
		for (auto it : environmentWeapons) {
			it.second->draw();
		}

        for(list<Ground*>::iterator it = grounds->begin(); it != grounds->end(); it++)
            (*it)->draw();
    }

    void notification(){

    }

    string getType(){
		return "Environnement";
    }
};

#endif //PI_ENVIRONMENT_H
