#ifndef RESOURCEMANAGER_H
#define RESOURCEMANAGER_H

#include <map>
#include <string>
#include "Resource.h"

using namespace std;

class ResourceManager {
private:
    static map<string, Resource*> resources;

public:
    template <typename T>
    inline static T get(const string& name) {
      return (T)resources[name];
    }

    inline static bool notSet(const string& name) {
      return (resources[name]) ? false : true;
    }

    inline static void set(const string& name, Resource* resource) {
      resources[name] = resource;
    }

    static void deleteAll() {
      for (auto it : resources)
        delete it.second;
    }
};

map<string, Resource*> ResourceManager::resources;

#endif
