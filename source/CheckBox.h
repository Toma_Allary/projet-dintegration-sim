/// \file CheckBox.h
/// \brief Case à cocher.

#ifndef SOURCE_CHECKBOX_H
#define SOURCE_CHECKBOX_H

#include "VisualComponent.h"

/// \class CheckBox
/// \brief Case à cocher.
///
/// Case à cocher.
///
class CheckBox : public VisualComponent {
private:
    bool check; ///< Cochée.
    unsigned int textUncheckId; ///<  Deuxieme identificateur de texture.
public:
    CheckBox(unsigned int textUncheckId, unsigned int textCheckId, double x = 0.0, double y = 0.0, double width = 0.0, double height = 0.0, const double& layer = 0.0) : VisualComponent(x, y, width, height, layer) {
        check = false;
        this->textUncheckId = textUncheckId;
        textureId = textCheckId;
    }

    /// \brief Affichage
    void draw() {
        glBindTexture(GL_TEXTURE_2D, (check) ? textureId : textUncheckId);        

        glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);

		glVertexPointer(3, GL_DOUBLE, 0, vertices);
		glTexCoordPointer(2, GL_DOUBLE, 0, texCoords);
		
		glDrawArrays(GL_QUADS, 0, 4);

		glDisableClientState(GL_VERTEX_ARRAY);
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    }

	inline bool getChecked() {
		return check;
	}

    /// \brief Notification d'un événement
    void notification(){
        SDL_Event sdlEvent = SDLEvent::getInstance()->sdlEvent;
        switch (sdlEvent.type) {
        case SDL_MOUSEBUTTONDOWN:
            SDL_Point cursor = { sdlEvent.button.x, sdlEvent.button.y };
            SDL_Rect rect = { (int)position.x, (int)position.y, (int)width, (int)height };
            if (SDL_PointInRect(&cursor, &rect))
                check = !check;
            break;
        }
    }
    string getType(){
        return "CheckBox";
    }

};

#endif
