#ifndef PI_HITBOX_H
#define PI_HITBOX_H

#include "Matrix44d.h"


class HitBox {
private:
	double* vertices; ///< Sommets.
	double* normals; ///< Normales.
	Vector3d objCenter;
	Matrix44d transformMatrix;
	Vector3d front;
	Vector3d up;
	double maxX;
	double minX;
	double maxY;
	double minY;
	double maxZ;
	double minZ;
	double ray;
public:
	HitBox(Vector3d objCenter, double maxX, double minX, double maxY, double minY, double maxZ, double minZ) {
		this->maxX = maxX - objCenter.x;
		this->maxY = maxY - objCenter.y;
		this->maxZ = maxZ - objCenter.z;
		this->minX = this->minY = this->minZ = 0.0;
		if (maxX >= maxY && maxX >= maxZ) {
			ray = maxX;
		}
		else if (maxY >= maxX && maxY >= maxZ) {
			ray = maxY;
		}
		else if (maxZ >= maxY && maxZ >= maxX) {
			ray = maxZ;
		}
		front = (0.0, 0.0, -1.0);
		up = (0.0, 1.0, 0.0);
		vertices = new double[108];
		normals = new double[36];
		this->objCenter = objCenter;
		transformMatrix.resetMatrix();

		//triangle de c�t� droite vers nous
		vertices[0] = maxX;
		vertices[1] = maxY;
		vertices[2] = maxZ;
		vertices[3] = maxX;
		vertices[4] = minY;
		vertices[5] = maxZ;
		vertices[6] = maxX;
		vertices[7] = minY;
		vertices[8] = minZ;
		normals[0] = 1;
		normals[1] = 0;
		normals[2] = 0;

		//triangle de c�t� gauche vers nous
		vertices[9] = minX;
		vertices[10] = maxY;
		vertices[11] = maxZ;
		vertices[12] = minX;
		vertices[13] = minY;
		vertices[14] = maxZ;
		vertices[15] = minX;
		vertices[16] = minY;
		vertices[17] = minZ;
		normals[3] = -1;
		normals[4] = 0;
		normals[5] = 0;
			   
		// triangle du dessus de gauche
		vertices[18] = minX;
		vertices[19] = maxY;
		vertices[20] = minZ;
		vertices[21] = maxX;
		vertices[22] = maxY;
		vertices[23] = minZ;
		vertices[24] = minX;
		vertices[25] = maxY;
		vertices[26] = maxZ;
		normals[6] = 0;
		normals[7] = 1;
		normals[8] = 0;

		// triangle de derri�re � gauche
		vertices[27] = minX;
		vertices[28] = maxY;
		vertices[29] = minZ;
		vertices[30] = maxX;
		vertices[31] = maxY;
		vertices[32] = minZ;
		vertices[33] = minX;
		vertices[34] = minY;
		vertices[35] = minZ;
		normals[9] = 0;
		normals[10] = 0;
		normals[11] = -1;


		// triangle du dessus de droite
		vertices[36] = maxX;
		vertices[37] = maxY;
		vertices[38] = maxZ;
		vertices[39] = maxX;
		vertices[40] = maxY;
		vertices[41] = minZ;
		vertices[42] = minX;
		vertices[43] = maxY;
		vertices[44] = maxZ;
		normals[12] = 0;
		normals[13] = 1;
		normals[14] = 0;

		// triangle de face � droite
		vertices[45] = maxX;
		vertices[46] = maxY;
		vertices[47] = maxZ;

		vertices[48] = maxX;
		vertices[49] = minY;
		vertices[50] = maxZ;

		vertices[51] = minX;
		vertices[52] = minY;
		vertices[53] = maxZ;
		normals[15] = 0;
		normals[16] = 0;
		normals[17] = 1;

		// triangle du dessous de droite
		vertices[54] = maxX;
		vertices[55] = minY;
		vertices[56] = maxZ;
		vertices[57] = maxX;
		vertices[58] = minY;
		vertices[59] = minZ;
		vertices[60] = minX;
		vertices[61] = minY;
		vertices[62] = maxZ;
		normals[18] = 0;
		normals[19] = -1;
		normals[20] = 0;

		// triangle du dessous de gauche
		vertices[63] = minX;
		vertices[64] = minY;
		vertices[65] = minZ;
		vertices[66] = maxX;
		vertices[67] = minY;
		vertices[68] = minZ;
		vertices[69] = minX;
		vertices[70] = minY;
		vertices[71] = maxZ;
		normals[21] = 0;
		normals[22] = -1;
		normals[23] = 0;

		// triangle de face � gauche
		vertices[72] = minX;
		vertices[73] = maxY;
		vertices[74] = maxZ;
		vertices[75] = maxX;
		vertices[76] = maxY;
		vertices[77] = maxZ;
		vertices[78] = minX;
		vertices[79] = minY;
		vertices[80] = maxZ;
		normals[24] = 0;
		normals[25] = 0;
		normals[26] = 1;
		
		// triangle de c�t� gauche vers le fond
		vertices[81] = minX;
		vertices[82] = maxY;
		vertices[83] = maxZ;
		vertices[84] = minX;
		vertices[85] = maxY;
		vertices[86] = minZ;
		vertices[87] = minX;
		vertices[88] = minY;
		vertices[89] = minZ;
		normals[27] = -1;
		normals[28] = 0;
		normals[29] = 0;

		//triangle derriere a droite
		vertices[90] = maxX;
		vertices[91] = maxY;
		vertices[92] = minZ;
		vertices[93] = maxX;
		vertices[94] = minY;
		vertices[95] = minZ;
		vertices[96] = minX;
		vertices[97] = minY;
		vertices[98] = minZ;
		normals[30] = 0;
		normals[31] = 0;
		normals[32] = -1;
		

		//triangle de c�t� droite vers le fond
		vertices[99] = maxX;
		vertices[100] = maxY;
		vertices[101] = maxZ;
		vertices[102] = maxX;
		vertices[103] = maxY;
		vertices[104] = minZ;
		vertices[105] = maxX;
		vertices[106] = minY;
		vertices[107] = minZ;
		normals[33] = 1;
		normals[34] = 0;
		normals[35] = 0;

	}

	~HitBox() {
		delete[] vertices;
		delete[] normals;
	}

	/// \brief retour d�n triangle selon le id
	Triangle getTriangles(unsigned int id) {
		Vector3d point1 = Vector3d();
		Vector3d point2 = Vector3d();
		Vector3d point3 = Vector3d();
		Vector3d normal = Vector3d();
		id *= 9;
		point1.x = vertices[id];
		point1.y = vertices[id + 1];
		point1.z = vertices[id + 2];

		point2.x = vertices[id + 3];
		point2.y = vertices[id + 4];
		point2.z = vertices[id + 5];

		point3.x = vertices[id + 6];
		point3.y = vertices[id + 7];
		point3.z = vertices[id + 8];
		id /= 3;
		normal.x = normals[id];
		normal.y = normals[id + 1];
		normal.z = normals[id + 2];

		return Triangle(point1, point2, point3, normal);
	}

	/// \brief obtenir la position du maximum X du modele
	double getMaxX() {
		maxX = vertices[0];
		for (unsigned int i = 0; i < 108; i += 3)
			if (vertices[i] > maxX) maxX = vertices[i];
		return maxX;
	}

	/// \brief obtenir la position du minimum X du modele
	double getMinX() {
		minX = vertices[0];
		for (unsigned int i = 0; i < 108; i += 3)
			if (vertices[i] < minX) minX = vertices[i];
		return minX;
	}

	/// \brief obtenir la position du maximum Y du modele
	double getMaxY() {
		maxY = vertices[1];
		for (unsigned int i = 0; i < 108; i += 3)
			if (vertices[i + 1] > maxY) maxY = vertices[i + 1];
		return maxY;
	}

	/// \brief obtenir la position du minimum Y du modele
	double getMinY() {
		minY = vertices[1];
		for (unsigned int i = 0; i < 108; i += 3)
			if (vertices[i + 1] < minY) minY = vertices[i + 1];
		return minY;
	}

	/// \brief obtenir la position du maximum Z du modele
	double getMaxZ() {
		maxZ = vertices[2];
		for (unsigned int i = 0; i < 108; i += 3)
			if (vertices[i + 2] > maxZ) maxZ = vertices[i + 2];
		return maxZ;
	}

	/// \brief obtenir la position du minimum Z du modele
	double getMinZ() {
		minZ = vertices[2];
		for (unsigned int i = 0; i < 108; i += 3)
			if (vertices[i + 2] < minZ) minZ = vertices[i + 2];
		return minZ;
	}

	inline double getVertice(unsigned int indice) {
		return vertices[indice];
	}

	/// \ Permet d'obtenir les sommets du mod�le 
	inline double* getVertices() {
		return vertices;
	}

	/// \ Permet d'obtenir les normales du mod�le 
	inline double* getNormals() {
		return normals;
	}

	inline Vector3d getFront() {
		return front;
	}

	inline double getRay() {
		return ray;
	}

	inline Vector3d getObjCenter() {
		return objCenter;
	}
	void translate(Vector3d translation = Vector3d(0.0, 0.0, 0.0)) {
		Vector3d newPosition = objCenter + translation;
		setPosition(newPosition.x, newPosition.y, newPosition.z);

	}

	/// \brief Enregistre la matrix
	void setTransformationMatrix(Matrix44d transformMatrix) {
		this->transformMatrix = transformMatrix;
	}

	void applyCurrentMatrix() {
		Vector3d vertex(0.0, 0.0, 0.0);
		Vector3d norm(0.0, 0.0, 0.0);

		for (unsigned int i = 0; i < 108; i += 3) {
			vertex.x = vertices[i];
			vertex.y = vertices[i + 1];
			vertex.z = vertices[i + 2];

			vertex = this->transformMatrix * vertex;

			vertices[i] = vertex.x;
			vertices[i + 1] = vertex.y;
			vertices[i + 2] = vertex.z;
		}
		for (unsigned int i = 0; i < 36; i += 3) {
			norm.x = normals[i];
			norm.y = normals[i + 1];
			norm.z = normals[i + 2];

			norm = this->transformMatrix * norm;

			normals[i] = norm.x;
			normals[i + 1] = norm.y;
			normals[i + 2] = norm.z;
		}

		front = this->transformMatrix * front;
		front.normalize();

		objCenter = this->transformMatrix * objCenter;

		up = this->transformMatrix * up;
		up.normalize();

	}

	/// \brief Effectue la rotation autour d'un vecteur 3D fourni avec les trois double. l'axe est en fait la position et non l'axe de rotation
	void transform(double radius = 0.0, Vector3d radiusAxe = (0.0, 0.0, 0.0)) {
		
		Vector3d vertex(0.0, 0.0, 0.0);
		Vector3d norm(0.0, 0.0, 0.0);

		transformMatrix.setTranslation(objCenter.x, objCenter.y, objCenter.z);
		if ((radius < -0.000000005) or (radius > 0.000000005)) {
			double positionNorm = objCenter.getNorm();
			double xFraction = objCenter.x / positionNorm;
			double yFraction = objCenter.y / positionNorm;
			double zFraction = objCenter.z / positionNorm;
			setPosition(xFraction*radius, yFraction*radius, zFraction*radius);
		}
		if ((radiusAxe.x > 0.002) or (radiusAxe.x < -0.002) or (radiusAxe.y > 0.002) or (radiusAxe.y < -0.002) or (radiusAxe.z > 0.002) or (radiusAxe.z < -0.002)) {
			setPosition(radiusAxe.x, radiusAxe.y, radiusAxe.z);
		}
		else {
			setPosition(0.0, 0.0, 0.0);
		}
		for (unsigned int i = 0; i < 108; i += 3) {
			vertex.x = vertices[i];
			vertex.y = vertices[i + 1];
			vertex.z = vertices[i + 2];

			vertex = this->transformMatrix * vertex;

			if (transformMatrix.matrix[3] && transformMatrix.matrix[7] && transformMatrix.matrix[11]) {
				vertex.x *= transformMatrix.matrix[3];
				vertex.y *= transformMatrix.matrix[7];
				vertex.z *= transformMatrix.matrix[11];
			}

			vertices[i] = vertex.x;
			vertices[i + 1] = vertex.y;
			vertices[i + 2] = vertex.z;
		}
		for (unsigned int i = 0; i < 36; i += 3) {
			norm.x = normals[i];
			norm.y = normals[i + 1];
			norm.z = normals[i + 2];

			norm = this->transformMatrix * norm;

			normals[i] = norm.x;
			normals[i + 1] = norm.y;
			normals[i + 2] = norm.z;
		}

		front = this->transformMatrix * front;

		if (transformMatrix.matrix[3] && transformMatrix.matrix[7] && transformMatrix.matrix[11]) {
			front.x *= transformMatrix.matrix[3];
			front.y *= transformMatrix.matrix[7];
			front.z *= transformMatrix.matrix[11];
		}

		up = this->transformMatrix * up;

		if (transformMatrix.matrix[3] && transformMatrix.matrix[7] && transformMatrix.matrix[11]) {
			up.x *= transformMatrix.matrix[3];
			up.y *= transformMatrix.matrix[7];
			up.z *= transformMatrix.matrix[11];
		}

		setPosition(this->transformMatrix.matrix[12], this->transformMatrix.matrix[13], this->transformMatrix.matrix[14]);
	}

	void setPosition(const double& x, const double& y, const double& z) {
		for (unsigned int i = 0; i < 108; i += 3) {
			vertices[i] += (x - objCenter.x);
			vertices[i + 1] += (y - objCenter.y);
			vertices[i + 2] += (z - objCenter.z);
		}

		objCenter.x = x;
		objCenter.y = y;
		objCenter.z = z;

	}

	void draw() {
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		glEnableClientState(GL_NORMAL_ARRAY);
		glEnableClientState(GL_VERTEX_ARRAY);

		glNormalPointer(GL_DOUBLE, 0, normals);
		glVertexPointer(3, GL_DOUBLE, 0, vertices);

		glDrawArrays(GL_TRIANGLES, 0, 36);

		glDisableClientState(GL_NORMAL_ARRAY);
		glDisableClientState(GL_VERTEX_ARRAY);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}
};

#endif