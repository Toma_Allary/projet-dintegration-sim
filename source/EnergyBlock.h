/// \file FrameBloc.h
/// \brief Bloc de structure
#ifndef ENERGYBLOCK_H
#define ENERGYBLOCK_H

#include "Component.h"
#include "Mesh3a.h"

/// \class FrameBloc
/// \brief Bloc de structure
///
/// Bloc composant la structure generale du vehicule
///
class EnergyBlock : public Component {
private:
	unsigned int conTextID;
    unsigned int conSelTextID;
	double energy;
	double maxEnergy;
public:
	/// \brief constructe
	EnergyBlock(const double& x = 0.0, const double& y =0.0, const double& z=0.0, const unsigned int& selecId = 0) : Component(new Material(SDLGLContext::getInstance()->loadTexture("Textures/usedMetal.png"), 2.00, 2.00, 2.00, 2.00, 2.00, 2.00, 2.00) , 0, x, y, z, "Objets/energyBlock.obj") {
		textID = SDLGLContext::getInstance()->loadTexture("Textures/energyBlock.png");
		conTextID = SDLGLContext::getInstance()->loadTexture("Textures/snapPoint.png");
        conSelTextID = SDLGLContext::getInstance()->loadTexture("Textures/snapPointSelect.png");
		type = ENERGYBLOCK;
		name = "bloc d'energie";
		price = 50;
		mass = 100;
		health = maxHealth = 550 * material->getPriceMutltiplicator();
		energy = maxEnergy = 550;
		energieCost = 0;

		
		
		this->transformMatrix.setScale(0.5);
		this->transform();
		connectors[0] = (new Mesh3a(conTextID, x, this->getMinY(), z, "Objets/crate.obj", conSelTextID));
		connectors[0]->getTransformationMatrix()->setScale(0.3);
		connectors[0]->transform();

		connectors[1] = (new Mesh3a(conTextID, x, y, this->getMaxZ(), "Objets/crate.obj", conSelTextID));
		connectors[1]->getTransformationMatrix()->setScale(0.3);
		connectors[1]->transform();

		hitboxes.push_back(new HitBox(this->position, getMaxX(), getMinX(), getMaxY(), getMinY(), getMaxZ(), getMinZ()));
	}

	/// \brief constructeur de copie
	EnergyBlock(EnergyBlock* energyBloc) : Component(energyBloc->getMaterial(), 0, energyBloc->getPosition().x, energyBloc->getPosition().y, energyBloc->getPosition().z, energyBloc->fileName) {
		textID = SDLGLContext::getInstance()->loadTexture("Textures/energyBlock.png");
		conTextID = SDLGLContext::getInstance()->loadTexture("Textures/snapPoint.png");
        conSelTextID = SDLGLContext::getInstance()->loadTexture("Textures/snapPointSelect.png");
		type = ENERGYBLOCK;
		name = "bloc d'energie";
		price = 50;
		mass = 100;
		health = maxHealth = 550 * material->getPriceMutltiplicator();
		energy = maxEnergy = 550;
		energieCost = 0;
		this->transformMatrix.setScale(0.5);
		this->transform();
		this->connectors = energyBloc->getConnectors();

		hitboxes.push_back(new HitBox(this->position, getMaxX(), getMinX(), getMaxY(), getMinY(), getMaxZ(), getMinZ()));

	}
	~EnergyBlock() {

	}

	inline void refillEnergy() {
		energy = maxEnergy;
	}

	//Afiichage
	void draw() {
		Mesh3a::draw();
		for (int i = 0; i < connectors.size(); i++) connectors[i]->draw();
	}

	/// \brief Obtenir Description
	/// \return retourne la description
	list<string>* getDescrition() {
		list<string> *description = new list<string>;
		description->push_back("Descrition:");
		description->push_back("Réservoir d'energie du vehicule");
		description->push_back("Niveau: " + to_string(level));
		description->push_back("Prix: " + to_string((int)(getPrice() + ((level - 1) * getPrice() / 2.0))));
        description->push_back("Masse: " + to_string((int)getMass()));
        description->push_back("Vie: " + to_string((int)getHealth()) + "/" + to_string((int)getMaxHealth()));
		return description;
	}
	/// \brief Notification
	virtual void notification() {}

	void receiveDamageOnEnergieBlock(double damage, double* weaponDamageDealtToUpdate = nullptr) {
		health -= damage;
		if(health <= 0.0){
			*weaponDamageDealtToUpdate += damage + health;
			energy = 0.0;
		}else{
			*weaponDamageDealtToUpdate += damage;
		}
	}

	void levelUp(){
		level++;
		energy *= 1.5;
		maxEnergy *= 1.5;
		maxHealth *= 1.5;
		health *= 1.5;
	}

	inline void removeEnergy(double energyToRemove, double deltaT) {
		if(energy - (energyToRemove * deltaT) > 0.0)
			energy -= (energyToRemove * deltaT);
	}

	inline void addEnergy(double energyToAdd) {
		energy += energyToAdd;
	}

	bool isEnergyRemovable(double energyToRemove, double deltaT) {
		if ((energy - (energyToRemove * deltaT)) > 0.0)
			return true;
		return false;
	}

	bool isEnergyAddable(double energyToAdd) {
		if ((int)(energy+energyToAdd) > maxEnergy)
			return false;
		return true;
	}

	inline unsigned int getType(){
		return ENERGYBLOCK;
	}

	inline double getEnergy() {
		return energy;
	}

	inline double getMaxEnergy(){
		return maxEnergy;
	}
};

#endif