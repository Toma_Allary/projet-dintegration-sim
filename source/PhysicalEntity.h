/// \file PhysicalEntity.h
/// \brief PhysicalEntity

#ifndef PHYSICALENTITY_H
#define PHYSICALENTITY_H

#include <list>

#include"Vector3d.h"
#include"Vector2d.h"
#include"libSDL2.h"

/// \class PhysicalEntity
/// \brief Entit. physique (classe mère)
///
/// Contient les éléments des objets physiques
///
class PhysicalEntity {
protected:
	static Vector3d gravity;
	static Vector3d wind;

public:
	static void setWindAndGravity(const double& gravity = -2.40, const double& windX = 0.0, const double& windY = 0.0, const double& windZ = 0.0) {
		PhysicalEntity::wind.x = windX;
		PhysicalEntity::wind.y = windY;
		PhysicalEntity::wind.z = windZ;
		PhysicalEntity::gravity.y = gravity;
	}

 	static Vector3d* applyGravityAndWind(Vector3d* speed, const double& deltaT, const double& adherence) {
		if (wind.x > adherence)
			return (wind.z > adherence) ? new Vector3d(speed->x + wind.x * deltaT, speed->y + ((wind.y + gravity.y) * deltaT), speed->z + wind.z * deltaT) : new Vector3d(speed->x + wind.x * deltaT, speed->y + ((wind.y + gravity.y) * deltaT), speed->z);
		else
			return (wind.z > adherence) ? new Vector3d(speed->x, speed->y + ((wind.y + gravity.y) * deltaT), speed->z + wind.z * deltaT) : new Vector3d(speed->x, speed->y + ((wind.y + gravity.y) * deltaT), speed->z);
	}

	static bool sameSide(const Vector3d& p1, const Vector3d& p2, const Vector3d& a, const Vector3d& b) { // V�rifie si les deux points p1 et p2 sont du m�me c�t� du segment ab
		const Vector3d cp1 = (b - a) % (p1 - a);
		const Vector3d cp2 = (b - a) % (p2 - a);

		return (cp1.dotProduct(cp2) >= 0.0) ? true : false;
	}

	static bool PointInTriangle2d(const Vector2d& pt, const Vector2d& v1, const Vector2d& v2, const Vector2d& v3) {
		const double d1 = (pt.x - v2.x) * (v1.y - v2.y) - (v1.x - v2.x) * (pt.y - v2.y);
		const double d2 = (pt.x - v3.x) * (v2.y - v3.y) - (v2.x - v3.x) * (pt.y - v3.y);
		const double d3 = (pt.x - v1.x) * (v3.y - v1.y) - (v3.x - v1.x) * (pt.y - v1.y);

		const bool neg = (d1 < 0.0) || (d2 < 0.0) || (d3 < 0.0);
		const bool pos = (d1 > 0.0) || (d2 > 0.0) || (d3 > 0.0);

		return !(neg && pos);
	}

	static double doesItCollide(const Vector3d& objectPos, const Vector3d& objectDirection, Triangle plan){ // verifie s'il y a collision entre le segment et le plan et retourne le ratio r par lequel le segment doit être multiplié. Retourne x=1 si pas de collision et x<1 si collision.
		Vector3d segment = (objectPos + objectDirection); //Peut etre a changer position le segment doit etre entre la position de l'objet et la face dans la direction de celui-ci
		if (segment.dotProduct(plan.getNormal()) != 0.0) {
			double r = ((plan.getNormal().dotProduct(plan.getT0() - objectPos))) / (plan.getNormal().dotProduct(segment - objectPos));
			if (r > 0.0 ) { //possibilite de collision
				if (r <= 1.0) { //collision 
					Vector3d intersection = ((segment - objectPos) * r) + objectPos;
					
					Vector2d p(0.0, 0.0);
					Vector2d a(0.0, 0.0);
					Vector2d b(0.0, 0.0);
					Vector2d c(0.0, 0.0);

					const double absX = abs(plan.getNormal().x);
					const double absY = abs(plan.getNormal().y);
					const double absZ = abs(plan.getNormal().z);

					if (absX >= absY && absX >= absZ) { // si la composante de la normal la plus grande est le x
						p.set(intersection.y, intersection.z);
						a.set(plan.getT0().y, plan.getT0().z);
						b.set(plan.getT1().y, plan.getT1().z);
						c.set(plan.getT2().y, plan.getT2().z);
					}
					else if (absY >= absX && absY >= absZ) { // si la composante de la normal la plus grande est le y
						p.set(intersection.x, intersection.z);
						a.set(plan.getT0().x, plan.getT0().z);
						b.set(plan.getT1().x, plan.getT1().z);
						c.set(plan.getT2().x, plan.getT2().z);
							
						}
					else if (absZ >= absY && absZ >= absX){ // si la composante de la normal la plus grande est le z
						p.set(intersection.x, intersection.y);
						a.set(plan.getT0().x, plan.getT0().y);
						b.set(plan.getT1().x, plan.getT1().y);
						c.set(plan.getT2().x, plan.getT2().y);
					}
									   
					if (PhysicalEntity::PointInTriangle2d(p, a, b, c)) { //Le point est dans le triangle donc collision
						///TODO : gerer ce qu'on fait s'il y a collision
						return r - 0.001;
					}
				}
			}
		}
		return 1.0; // Pour verifier si il y a collision ou non
	}
};

Vector3d PhysicalEntity::wind;
Vector3d PhysicalEntity::gravity;

#endif
