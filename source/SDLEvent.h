/// \file SDLEvent.h
/// \brief Événement SDL.

#ifndef SDLEVENT_H
#define SDLEVENT_H

#include "Singleton.h"

/// \class SDLEvent
/// \brief Événement SDL.
///
/// Événemnent générée par la librairie SDL2.
///
class SDLEvent : public Singleton<SDLEvent>{
public:
	SDL_Event sdlEvent; ///< Événement.
};

#endif
