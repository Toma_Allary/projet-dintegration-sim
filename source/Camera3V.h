/// \file Camera3V.h
/// \brief Caméra

#ifndef CAMERA3V_H
#define CAMERA3V_H

#include "Matrix44d.h"
#include "Observer.h"

/// \class Camera3V
/// \brief Caméra
///
/// Caméra à 3 vecteurs
///

class Camera3v : public Observer{
protected:
	Vector3d position; ///< Position.
	Vector3d target; ///< Cible.
	Vector3d up; ///< Haut.
	Matrix44d view; ///< Matrice de vue.

public:
/// \brief Constructeur
/// \param position Position.
/// \param target Cible.
/// \param up Haut.
	Camera3v(const Vector3d& position, const Vector3d& target, const Vector3d& up) {
		this->position = position;
		this->target = target;
		this->up = up;

		view.loadView(position, target, up);
	}

	~Camera3v() {

	}

/// \brief Application de la matrice de vue
	void applyView() {
		view.loadView(position, target, up);
		glMultMatrixd(view.matrix);
		glTranslated(-position.x, -position.y, -position.z);
	}

    /// \brief permet d'obtenir la position de la caméra.
    /// \return retourne la position avec un vecteur
	inline Vector3d getPosition() {
		return position;
	}

    /// \brief permet d'avoir le vecteur front de la caméra
    /// \return retourne un vecteur unitaire
	Vector3d getFront() {
		Vector3d front = target - position;
		front.normalize();
		return front;
	}

    /// \brief permet d'avoir le vecteur side de la caméra
    /// \return retourne un vecteur
	Vector3d getSide() {
		Vector3d side = up % getFront();
		return side;
	}

    /// \brief permet d'avoir le vecteur up de la caméra
    /// \return retourne un vecteur
	Vector3d getUp() {
		Vector3d normal = getSide() % getFront();
		return normal;
	}

    /// \brief change la position de la caméra
	inline void changePosition(Vector3d position) {
		this->position = position;
	}

	/// \brief Notification d'événement
	virtual void notification() = 0;
};

#endif
