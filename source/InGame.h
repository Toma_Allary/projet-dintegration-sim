/// \file InGame.h
/// \brief ecran de la partie
#ifndef INGAME_H
#define INGAME_H

#define PI 3.14159265358979324

#include "Scene3d.h"
#include "Image.h"
#include "DoubleLabel.h"
#include "LabelImageButton.h"
#include "DynamicCamera.h"
#include "Environment.h"
#include <time.h>

/// \class InGame
/// \brief ecran de la partie
///
/// la ou se deroulent les combats
///
class InGame : public Scene3d {
private:
	unsigned int betonId,
		iceId,
		earthId,
		asphaltId,
		wallsId,
		walls2Id;
	Component* Bloc1;
	Component* Bloc2;
	unsigned int texturesId[7];
	Chrono chrono;
	Chrono timeBeforeCollectibleDrop;
	Chrono eventChrono;
	Chrono eventChrono2;
	Chrono eventMouvementChrono;
	list<Mesh3a*>collectibles;
	Robot* robot1;
	Robot* robot2;
	bool saw1MovBool;
    bool saw2MovBool;

public:
	InGame() {
		player1->initializeHealthBeforeGame();
		player2->initializeHealthBeforeGame();

        saw1MovBool = true;
        saw2MovBool = false;
		for (auto it : *player1->getRobot()->getComponentList()) {
			if (player1->getRobot()->getComponentTree()->searchRecursive(it) == nullptr) {
				it->setVisibility(false);
			}
		}

		for (auto it : *player2->getRobot()->getComponentList()) {
			if (player2->getRobot()->getComponentTree()->searchRecursive(it) == nullptr) {
				it->setVisibility(false);
			}
		}
		srand(time(0));
		wallsId = SDLGLContext::getInstance()->loadTexture("Textures/walls.png");
        walls2Id = SDLGLContext::getInstance()->loadTexture("Textures/walls2.png");
		asphaltId = SDLGLContext::getInstance()->loadTexture("Textures/asphalt.png");
		betonId = SDLGLContext::getInstance()->loadTexture("Textures/Beton.png");
		iceId = SDLGLContext::getInstance()->loadTexture("Textures/Glace.png");
		earthId = SDLGLContext::getInstance()->loadTexture("Textures/Sable.png");
		texturesId[GARAGE_TEXTURE_ATMOSPHERE] = SDLGLContext::getInstance()->loadTexture("Textures/garageAtmoSphere.png");
		texturesId[GARAGE_TEXTURE_MENU_BACKGROUND] = SDLGLContext::getInstance()->loadTexture("Textures/menuBackground.png");
		texturesId[GARAGE_TEXTURE_BUTTON_BACKGROUND] = SDLGLContext::getInstance()->loadTexture("Textures/buttonBackground.png");
		texturesId[GARAGE_TEXTURE_OVERBUTTON_BACKGROUND] = SDLGLContext::getInstance()->loadTexture("Textures/_buttonBackground.png");
		texturesId[GARAGE_TEXTURE_USED_METAL] = SDLGLContext::getInstance()->loadTexture("Textures/usedMetal.png");
		texturesId[GARAGE_TEXTURE_CHASSIS] = SDLGLContext::getInstance()->loadTexture("Textures/brushedMetal.png");
		texturesId[GARAGE_TEXTURE_ENERGY_BLOCK] = SDLGLContext::getInstance()->loadTexture("Textures/energyBlock.png");
		arena = new Environment();

		// Initialisation des armes environmentales...
        eventMouvementChrono.reset();
		Saw* tempSaw = new Saw(new Material(texturesId[GARAGE_TEXTURE_USED_METAL], 2.00, 2.00, 2.00, 2.00, 2.00, 2.00, 2.00), -10.0, 0.0, 34.0);
		tempSaw->getTransformationMatrix()->loadZRotation(PI / 2);
        tempSaw->getTransformationMatrix()->loadXRotation(PI / 2);
		tempSaw->getTransformationMatrix()->setScale(PI);
		tempSaw->transform();
		tempSaw->getTransformationMatrix()->loadIdentity();
		Saw* tempSaw2 = new Saw(new Material(texturesId[GARAGE_TEXTURE_USED_METAL], 2.00, 2.00, 2.00, 2.00, 2.00, 2.00, 2.00), -10.0, .0, -45.0);
		tempSaw2->getTransformationMatrix()->loadZRotation(PI / 2);
		tempSaw2->getTransformationMatrix()->loadXRotation(PI / 2);
		tempSaw2->getTransformationMatrix()->setScale(PI);
		tempSaw2->transform();
		tempSaw2->getTransformationMatrix()->loadIdentity();
		Catapult* tempCatapult = new Catapult(new Material(texturesId[GARAGE_TEXTURE_USED_METAL], 2.00, 2.00, 2.00, 2.00, 2.00, 2.00, 2.00), 0, 11, 53.0);
		tempCatapult->getTransformationMatrix()->loadYRotation(5*PI/6);
		tempCatapult->getTransformationMatrix()->setScale(PI);
		tempCatapult->getCannonBall()->getTransformationMatrix()->setScale(PI);
		tempCatapult->getCannonBall()->setPosition(tempCatapult->getCannonBall()->getPosition().x, tempCatapult->getCannonBall()->getPosition().y + 7, tempCatapult->getCannonBall()->getPosition().z);
		tempCatapult->getCannonBall()->setInitialPosition(tempCatapult->getCannonBall()->getPosition().x, tempCatapult->getCannonBall()->getPosition().y, tempCatapult->getCannonBall()->getPosition().z);
		tempCatapult->getCannonBall()->transform();
		tempCatapult->transform();
		tempCatapult->getTransformationMatrix()->loadIdentity();

		// Ajout des armes environmentales à la liste d'armes de l'environnement...
		arena->addWeapon(tempCatapult, "Catapult");
		arena->addWeapon(tempSaw, "Saw");
		arena->addWeapon(tempSaw2, "Saw2");

		// Activation des armes environmentales...
		arena->getWeapon("Saw")->activate();
		arena->getWeapon("Saw2")->activate();

		for (unsigned int i = 0; i < 10; i++) {
			collectibles.push_back(new CannonBall(iceId, rand() % 50 - 25, 1.2, rand() % 100 - 50));
		}

		for (auto it : collectibles) {
			//it->getTransformationMatrix()->setScale(0.0085);
			//it->transform();
		}

		Bloc1 = nullptr;
		Bloc2 = nullptr;
		for (auto it : *player1->getRobot()->getComponentList()) {
			healthP1 += it->getHealth();
			if (it->getType() == ENERGYBLOCK) {
				staticHealthP1 = it->getHealth();
				Bloc1 = it;
			}
			it->setConnectorsVisibilty(false);
		}
		for (auto it : *player2->getRobot()->getComponentList()) {
			healthP2 += it->getHealth();
			if (it->getType() == ENERGYBLOCK) {
				staticHealthP2 = it->getHealth();
				Bloc2 = it;
			}
			it->setConnectorsVisibilty(false);
		}
		if (Bloc1)
			drawables["P1Energy"] = new DoubleLabel(50, 10, ResourceManager::get<Font*>("font12"), "P1 Energy : " + to_string((int)((EnergyBlock*)Bloc1)->getEnergy()) + " / 550", { 255, 250, 0, 255 }, 0.4);
		if (Bloc2)
			drawables["P2Energy"] = new DoubleLabel(900, 10, ResourceManager::get<Font*>("font12"), "P2 Energy : " + to_string((int)((EnergyBlock*)Bloc2)->getEnergy()) + " / 550", { 255, 250, 0, 255 }, 0.4);
		if (healthP1)
			drawables["P1Health"] = new DoubleLabel(50, 30, ResourceManager::get<Font*>("font12"), "P1 Health : " + to_string((int)(Bloc1)->getHealth()) + " / " + to_string((int)(staticHealthP1)), { 255, 250, 0, 255 }, 0.4);
		if (healthP2)
			drawables["P2Health"] = new DoubleLabel(900, 30, ResourceManager::get<Font*>("font12"), "P2 Health : " + to_string((int)(Bloc2)->getHealth()) + " / " + to_string((int)(staticHealthP2)), { 255, 250, 0, 255 }, 0.4);

		/*drawables["Robot1Speed"] = new DoubleLabel(800, 50, ResourceManager::get<Font*>("font12"), "P1 speed : X:" + to_string((double)(0.0)) + " Y: " + to_string((double)(0.0)) + " Z: " + to_string((double)(0.0)), { 255,225,10,255 }, 0.4);
		drawables["Robot2Speed"] = new DoubleLabel(800, 70, ResourceManager::get<Font*>("font12"), "P2 speed : X:" + to_string((double)(0.0)) + " Y: " + to_string((double)(0.0)) + " Z: " + to_string((double)(0.0)), { 255,225,10,255 }, 0.4);
		drawables["Robot2collided"] = new DoubleLabel(800, 90, ResourceManager::get<Font*>("font12"), "P2 collided : false", { 255,225,10,255 }, 0.4);
		drawables["Robot1collided"] = new DoubleLabel(800, 110, ResourceManager::get<Font*>("font12"), "P2 collided : false", { 255,225,10,255 }, 0.4);*/
		drawables["player1Money"] = new DoubleLabel(50, 50, ResourceManager::get<Font*>("font12"), "player1 money: " + to_string(player1->getMoney()), { 255, 250, 0, 255 }, 0.4);
		drawables["player2Money"] = new DoubleLabel(900, 50, ResourceManager::get<Font*>("font12"), "player2 money: " + to_string(player2->getMoney()), { 255, 250, 0, 255 }, 0.4);

		arena->addGround(new Ground(0.5, true, asphaltId, 0, 0, 0, "Objets/MapBase.obj"));
		list<Ground*>::iterator it = arena->getGrounds()->begin();
		(*it)->getTransformationMatrix()->setScale(2);
		(*it)->transform();
		arena->addGround(new Ground(3.0, false, iceId, 0, 0.1, -50.0, "Objets/flaque.obj"));

		it++;
		(*it)->getTransformationMatrix()->setScale(3.2);
		(*it)->transform();
		arena->addGround(new Ground(0.5, true, asphaltId, 10, 4.0, -10, "Objets/crate.obj"));
		it++;
		(*it)->getTransformationMatrix()->setScale(4.1);
		(*it)->transform();
		arena->addGround(new Ground(0.5, true, wallsId, 0, 0, 0, "Objets/walls.obj"));//mur du font
		it++;
        (*it)->getTransformationMatrix()->setScale(2.1);
        (*it)->transform();
        arena->addGround(new Ground(0.5, true, wallsId, -11, 0, 0, "Objets/walls.obj"));//mur de droite
        it++;
        (*it)->getTransformationMatrix()->loadRotation(80.1, Vector3d(0,1,0));
        (*it)->getTransformationMatrix()->setScale(2.1);
        (*it)->transform();
        arena->addGround(new Ground(0.5, true, wallsId, 11, 0, 0, "Objets/walls.obj"));//mur de gauche
        it++;
        (*it)->getTransformationMatrix()->loadRotation(-80.1, Vector3d(0,1,0));
        (*it)->getTransformationMatrix()->setScale(2.1);
        (*it)->transform();
        arena->addGround(new Ground(0.5, true, walls2Id, 0, 0.8, -10.5, "Objets/walls2.obj"));
        it++;
        (*it)->getTransformationMatrix()->loadRotation(110.0, Vector3d(0,1,0));
        (*it)->getTransformationMatrix()->setScale(2.1);
        (*it)->transform();
		for (list<Component*>::iterator it = player2->getRobot()->getComponentList()->begin(); it != player2->getRobot()->getComponentList()->end(); it++) {
			(*it)->setPosition((*it)->getPosition().x, (*it)->getPosition().y + 15, (*it)->getPosition().z + 35);
		}
		for (list<Component*>::iterator it = player1->getRobot()->getComponentList()->begin(); it != player1->getRobot()->getComponentList()->end(); it++) {
			(*it)->setPosition((*it)->getPosition().x, (*it)->getPosition().y + 15, (*it)->getPosition().z);
		}

		camera = new DynamicCamera(Vector3d(3.0, 10.0, -50.0), Vector3d(-10.0, 5.0, -25.0), Vector3d(0.0, 1.0, 0.0), player1->getRobot(), player2->getRobot());

		robot1 = player1->getRobot();
		robot2 = player2->getRobot();
		chrono.reset();
		eventChrono.reset();
		eventChrono2.reset();
		player1->pickedUpScrapValue = 0.0;
		player2->pickedUpScrapValue = 0.0;
	}

	~InGame() {
		// Remettre les robots des joueurs au milieu pour pouvoir retourner au garage...
		player1->getRobot()->reposition();
		player2->getRobot()->reposition();
		player1->getRobot()->changeOnGround();
		player2->getRobot()->changeOnGround();
		for (unsigned int i = 0; 1 < collectibles.size(); i++) {
			Mesh3a* todelete = collectibles.back();
			collectibles.pop_back();
			delete todelete;
		}
	}

	void draw() {
		// 3D
		glEnable(GL_LIGHTING);

		SDLGLContext::getInstance()->setPerspective(false);

		((DynamicCamera*)camera)->applyDynamicView();

		auto groundsIt = arena->getGrounds()->begin();
		list<Mesh3a*> concrete;
		concrete.push_back((Mesh3a*)(*groundsIt));
		//concrete.push_back(new Mesh3a(asphaltId, 10, 0, -10, "Objets / crate.obj",0));

		list<Mesh3a*> otherGrounds;
		groundsIt++;
		while (groundsIt != arena->getGrounds()->end()) {
			if ((*groundsIt)->getConcrete())
				concrete.push_back((Mesh3a*)(*groundsIt));
			else
				otherGrounds.push_back((Mesh3a*)(*groundsIt));

			groundsIt++;
		}

		healthP1 = 0;
		healthP2 = 0;
        unsigned int i = eventChrono2.getElapsedTime();
		// Activation des catapultes environmentales...
		if (arena->getWeapon("Catapult")->isUsedPresently()) {
            if (eventChrono2.getElapsedTime() >= 1.4) {
                eventChrono2.reset();
                arena->getWeapon("Catapult")->disable();
            }
        }
		else
            if (eventChrono2.getElapsedTime() >= 5) {
            eventChrono2.reset();
            arena->getWeapon("Catapult")->activate();
        }
		//controlle le parcours des sciers dans l'environnement
		//les bool demouvement indique la direction dans laquelle les scie ce dirigent. Bas : true / Haut : false
		if(arena->getWeapon("Saw")->getPosition().x < -50)
		    saw1MovBool = false;
        else if(arena->getWeapon("Saw")->getPosition().x > 50)
            saw1MovBool = true;

        if(saw1MovBool)
            arena->getWeapon("Saw")->setPosition(arena->getWeapon("Saw")->getPosition().x - 4.5*eventMouvementChrono.getElapsedTime(), arena->getWeapon("Saw")->getPosition().y, arena->getWeapon("Saw")->getPosition().z);
        else
            arena->getWeapon("Saw")->setPosition(arena->getWeapon("Saw")->getPosition().x + 4.5*eventMouvementChrono.getElapsedTime(), arena->getWeapon("Saw")->getPosition().y, arena->getWeapon("Saw")->getPosition().z);

        if(arena->getWeapon("Saw2")->getPosition().x < -50)
            saw2MovBool = false;
        else if(arena->getWeapon("Saw2")->getPosition().x > 50)
            saw2MovBool = true;

        if(saw2MovBool)
            arena->getWeapon("Saw2")->setPosition(arena->getWeapon("Saw2")->getPosition().x - 4.5*eventMouvementChrono.getElapsedTime(), arena->getWeapon("Saw2")->getPosition().y, arena->getWeapon("Saw2")->getPosition().z);
        else
            arena->getWeapon("Saw2")->setPosition(arena->getWeapon("Saw2")->getPosition().x + 4.5*eventMouvementChrono.getElapsedTime(), arena->getWeapon("Saw2")->getPosition().y, arena->getWeapon("Saw2")->getPosition().z);
        eventMouvementChrono.reset();

		for (auto it : arena->getEnvironmentWeapons()) {
			//healthP1 += it.second->getHealth();
			if (it.second->getType() == CATAPULT) {
				for (auto it2 : *player1->getRobot()->getComponentList()) {
					list<Mesh3a*> temp;
					temp.push_back(((Catapult*)it.second)->getCannonBall());
					//if(NULL) //collision avec boulet
					if (Scene3d::ultimateDoesItCollide(it2, temp, (*it2->getSpeed())))
						((Catapult*)it.second)->dealDamage(it2, player1->getRobot()->getComponentTree(), player1->getRobot()->getComponentList());
				}
			}
			else if (it.second->getType() == SAW) {
				if (((Saw*)it.second)->isUsedPresently())
					if (player1->getRobot()->getComponentTree()->getRootData()->getPosition().distanceBetween(((Saw*)it.second)->getPosition()) <= 5) { // vérifie le player 1
						for (auto it2 : *player1->getRobot()->getComponentList()) {
							list<Mesh3a*> temp;
							temp.push_back(it2);
							//if (NULL)// collision entre scie et component de l'autre robot
							if (Scene3d::ultimateDoesItCollide(it.second, temp, Vector3d(0, 1, 0))) {
								((Saw*)it.second)->dealDamage(it2, player1->getRobot()->getComponentTree(), player1->getRobot()->getComponentList());
							}
						}
					}
				if (player2->getRobot()->getComponentTree()->getRootData()->getPosition().distanceBetween(((Saw*)it.second)->getPosition()) <= 5) { // vérifie le player 2
					for (auto it2 : *player2->getRobot()->getComponentList()) {
						list<Mesh3a*> temp;
						temp.push_back(it2);
						//if (NULL)// collision entre scie et component de l'autre robot
						if (Scene3d::ultimateDoesItCollide(it.second, temp, Vector3d(0, 1, 0))) {
							((Saw*)it.second)->dealDamage(it2, player2->getRobot()->getComponentTree(), player2->getRobot()->getComponentList());
						}
					}
				}
			}
			else if (it.second->getType() == FLAMETHROWER) {
				if (((FlameThrower*)it.second)->isUsedPresently()) {
					if (player1->getRobot()->getComponentTree()->getRootData()->getPosition().distanceBetween(((FlameThrower*)it.second)->getPosition()) <= 10) { // vérifie le player 1
						for (auto it2 : *player1->getRobot()->getComponentList()) { // gestion du premier joueur
							list<Mesh3a*> temp;
							temp.push_back(it2);
							//if (((FlameThrower*)it)->getFlameList())// collision entre flame et component de l'autre robot
							for (auto itf : ((FlameThrower*)it.second)->getFlameList()) {
								if (Scene3d::ultimateDoesItCollide(itf, temp, Vector3d(0, -1, 0))) {
									((FlameThrower*)it.second)->dealDamage(it2, player1->getRobot()->getComponentTree(), player1->getRobot()->getComponentList());
								}
							}
						}
					}

					if (player2->getRobot()->getComponentTree()->getRootData()->getPosition().distanceBetween(((FlameThrower*)it.second)->getPosition()) <= 10) { // vérifie le player 2
						for (auto it2 : *player2->getRobot()->getComponentList()) { // gestion du deuxi�me joueur
							list<Mesh3a*> temp;
							temp.push_back(it2);
							//if (((FlameThrower*)it)->getFlameList())// collision entre flame et component de l'autre robot
							for (auto itf : ((FlameThrower*)it.second)->getFlameList()) {
								if (Scene3d::ultimateDoesItCollide(itf, temp, Vector3d(0, -1, 0))) {
									((FlameThrower*)it.second)->dealDamage(it2, player2->getRobot()->getComponentTree(), player2->getRobot()->getComponentList());
								}
							}
						}
					}
				}
			}
		}

		for (auto it : *robot1->getComponentList()) {
			if (it->getSpeed()->x || it->getSpeed()->y || it->getSpeed()->z) {
				CannonBall* collectible = (CannonBall*)Scene3d::ultimateDoesItCollide(it, collectibles, (*it->getSpeed()));
				if (collectible) {
					if (collectible->getTextureID() == earthId) {
						player1->setMoney(player1->getMoney() + collectible->scrapPriceGain);
						player1->pickedUpScrapValue += collectible->scrapPriceGain;
					}
					else {
						for (auto it : *(robot1->getComponentList())) {
							if (it->getType() == ENERGYBLOCK) {
								if (((EnergyBlock*)it)->isEnergyAddable(10)) {
									((EnergyBlock*)it)->addEnergy(10);
								}
							}
						}
					}
					collectibles.remove(collectible);
					delete collectible;
					collectible = nullptr;
				}
			}
		}


		for (auto it : *robot2->getComponentList()) {
			if (it->getSpeed()) {
				CannonBall* collectible = (CannonBall*)Scene3d::ultimateDoesItCollide(it, collectibles, (*it->getSpeed()));
				if (collectible) {
					if (collectible->getTextureID() == earthId) {
						player2->setMoney(player2->getMoney() + collectible->scrapPriceGain);
						player2->pickedUpScrapValue += collectible->scrapPriceGain;
					}
					else {
						for (auto it : *(robot2->getComponentList())) {
							if (it->getType() == ENERGYBLOCK) {
								if (((EnergyBlock*)it)->isEnergyAddable(10)) {
									((EnergyBlock*)it)->addEnergy(10);
								}
							}
						}
					}
					collectibles.remove(collectible);
					delete collectible;
				}
			}
		}

		//Collision entre les deux robots
		Vector3d tempFront;
		list<Mesh3a*> Mesh3a2;
		Vector3d position;
		Vector3d smallestCollision(0, 0, 0);

		list<Mesh3a*> concreteGrounds;
		if ((robot1->getSpeed()->x != 0) or (robot1->getSpeed()->z != 0)) {
			for (auto it : *robot2->getComponentList()) {
				if (it->getVisible()) {
					Mesh3a2.push_back((Mesh3a*)it);
				}
			}
			for (auto it : concrete) {
				concreteGrounds.push_back((Mesh3a*)it);
			}
			Vector3d speed = *robot1->getSpeed();
			for (auto it : *robot1->getComponentList()) {
				if (Scene3d::ultimateDoesItCollide(it, Mesh3a2, (*it->getSpeed()))) {
					robot2->setCollided(true);
					robot1->setCollided(true);
					for (auto it2 : *robot2->getComponentList()) {
						it2->setPosition(it2->getPosition().x + speed.x, it2->getPosition().y, it2->getPosition().z + speed.z);
						it2->setSpeed(speed.x, speed.y, speed.z);
					}
					for (auto it1 : *robot1->getComponentList()) {
						it1->setSpeed(-it1->getSpeed()->x, -it1->getSpeed()->y, -it1->getSpeed()->z);
						it1->setPosition(it1->getPosition().x + it1->getSpeed()->x, it1->getPosition().y, it1->getPosition().z + it1->getSpeed()->z);
					}
					break;
				}
				if (Scene3d::ultimateDoesItCollide(it, concreteGrounds, (*it->getSpeed()))) {
					robot1->setCollided(true);
					for (auto it1 : *robot1->getComponentList()) {
						it1->setSpeed(-it1->getSpeed()->x, 0, -it1->getSpeed()->z);
						it1->setPosition(it1->getPosition().x + it1->getSpeed()->x, it1->getPosition().y, it1->getPosition().z + it1->getSpeed()->z);
					}
					break;
				}

			}
		}
		/*if(robot1->getSpeed()->y < 0){
			for (auto it : concrete) {
				concreteGrounds.push_back((Mesh3a*)it);
			}
			Vector3d speed = *robot1->getSpeed();
			for (auto it : *robot1->getComponentList()) {
				Mesh3a* colidedGround = Scene3d::ultimateDoesItCollide(it, concreteGrounds, (*it->getSpeed()));
				if (colidedGround) {
					robot1->setCollided(true);
					double hightPosition = colidedGround->getMaxY();
					for (auto it1 : *robot1->getComponentList()) {
						it1->setSpeed(-it1->getSpeed()->x, 0, -it1->getSpeed()->z);
						it1->setPosition(it1->getPosition().x + it1->getSpeed()->x, hightPosition , it1->getPosition().z + it1->getSpeed()->z);
						if(!robot1->getOnGround()){
							robot1->changeOnGround();
						}
					}
				}
			}
		}*/
		list<Mesh3a*> Mesh3a1;
		Vector3d position2;
		if ((robot2->getSpeed()->x != 0) or (robot2->getSpeed()->z != 0)) {
			for (auto it : *robot1->getComponentList()) {
				if (it->getVisible()) {
					Mesh3a1.push_back((Mesh3a*)it);
				}
			}
			Vector3d speed = *robot2->getSpeed();
			for (auto it : *robot2->getComponentList()) {
				if (Scene3d::ultimateDoesItCollide(it, Mesh3a1, (*it->getSpeed()))) {
					robot2->setCollided(true);
					robot1->setCollided(true);
					for (auto it2 : *robot1->getComponentList()) {
						it2->setPosition(it2->getPosition().x + speed.x, it2->getPosition().y, it2->getPosition().z + speed.z);
						it2->setSpeed(speed.x, speed.y, speed.z);
					}
					for (auto it1 : *robot2->getComponentList()) {
						it1->setSpeed(-it1->getSpeed()->x, -it1->getSpeed()->y, -it1->getSpeed()->z);
						it1->setPosition(it1->getPosition().x + it1->getSpeed()->x, it1->getPosition().y, it1->getPosition().z + it1->getSpeed()->z);
					}
					break;
				}
				if(Scene3d::ultimateDoesItCollide(it, concreteGrounds, (*it->getSpeed()))) {
					robot2->setCollided(true);
					for (auto it1 : *robot2->getComponentList()) {
						it1->setSpeed(-it1->getSpeed()->x, 0, -it1->getSpeed()->z);
						it1->setPosition(it1->getPosition().x + it1->getSpeed()->x, it1->getPosition().y, it1->getPosition().z + it1->getSpeed()->z);
					}
					break;
				}

			}

		}

		unsigned int P1component = player1->getRobot()->getComponentTree()->getSize();
		unsigned int P2component = player2->getRobot()->getComponentTree()->getSize();

		for (auto it : *player1->getRobot()->getComponentList()) {
			healthP1 += it->getHealth();
			if (it->getType() == CATAPULT) {
				for (auto it2 : *player2->getRobot()->getComponentList()) {
					list<Mesh3a*> temp;
					temp.push_back(((Catapult*)it)->getCannonBall());
					if (Scene3d::ultimateDoesItCollide(it2, temp, (*it->getSpeed())))
						((Catapult*)it)->dealDamage(it2, player2->getRobot()->getComponentTree(), player2->getRobot()->getComponentList());
				}
			}
			else if (it->getType() == SAW) {
				if (((Saw*)it)->isUsedPresently())
					for (auto it2 : *player2->getRobot()->getComponentList()) {
						
						if (((it2->getPosition().x < (it->getPosition().x + 5)) and (it2->getPosition().x > (it->getPosition().x - 5))) and ((it2->getPosition().y < (it->getPosition().y + 3)) and (it2->getPosition().y > (it->getPosition().y - 3))) and ((it2->getPosition().z < (it->getPosition().z + 5)) and (it2->getPosition().z > (it->getPosition().z - 5)))) {
							((Saw*)it)->dealDamage(it2, player2->getRobot()->getComponentTree(), player2->getRobot()->getComponentList());
						}
					}

			}
		

		else if (it->getType() == FLAMETHROWER) {
			if (((FlameThrower*)it)->isUsedPresently()) {
				for (auto it2 : *player2->getRobot()->getComponentList()) {
					list<Mesh3a*> temp;
					temp.push_back(it2);
					for (auto itf : ((FlameThrower*)it)->getFlameList()) {
						if (Scene3d::ultimateDoesItCollide(itf, temp, (*it->getSpeed())))
							((FlameThrower*)it)->dealDamage(it2, player2->getRobot()->getComponentTree(), player2->getRobot()->getComponentList());
					}
				}
			}
		}
		
	}
	for (auto it : *player2->getRobot()->getComponentList()) {
		healthP2 += it->getHealth();
		if (it->getType() == CATAPULT) {
			for (auto it2 : *player1->getRobot()->getComponentList()) {
				list<Mesh3a*> temp;
				temp.push_back(((Catapult*)it)->getCannonBall());
				if (Scene3d::ultimateDoesItCollide(it2, temp, (*it->getSpeed())))
					((Catapult*)it)->dealDamage(it2, player1->getRobot()->getComponentTree(), player1->getRobot()->getComponentList());
			}
		}
		else if (it->getType() == SAW) {
			if (((Saw*)it)->isUsedPresently())
				for (auto it2 : *player1->getRobot()->getComponentList()) {
					list<Mesh3a*> temp;
					temp.push_back(it2);
					if (Scene3d::ultimateDoesItCollide(it, temp, (*it->getSpeed()))) 
					((Saw*)it)->dealDamage(it2, player1->getRobot()->getComponentTree(), player1->getRobot()->getComponentList());
				}

		}

			else if (it->getType() == FLAMETHROWER) {
			if (((FlameThrower*)it)->isUsedPresently()) {
				for (auto it2 : *player1->getRobot()->getComponentList()) {
					list<Mesh3a*> temp;
					temp.push_back(it2);
					for (auto itf : ((FlameThrower*)it)->getFlameList()) {
						if (Scene3d::ultimateDoesItCollide(itf, temp, (*it->getSpeed())))
							((FlameThrower*)it)->dealDamage(it2, player1->getRobot()->getComponentTree(), player1->getRobot()->getComponentList());
					}
				}
			}
			}
		}
		/*if(timeBeforeCollectibleDrop.getElapsedTime() > 10){
			delete (*collectibles.begin());
			collectibles.pop_front();
			Mesh3a* newCollectible = new Mesh3a(iceId, rand()%50-25, 1, rand() % 50 - 25, "Objets/Ball.obj");
			newCollectible->getTransformationMatrix()->setScale(0.0085);
			newCollectible->transform();
			collectibles.push_back(newCollectible);
			timeBeforeCollectibleDrop.reset();
		}*/

		if (P1component > player1->getRobot()->getComponentTree()->getSize()) {
			collectibles.push_back(new CannonBall(earthId, rand()%50-25, 1.2, rand() % 100 - 50));
			((CannonBall*)collectibles.back())->scrapPriceGain *= (P1component - player1->getRobot()->getComponentTree()->getSize());
		}
		if (P2component > player2->getRobot()->getComponentTree()->getSize()) {
			collectibles.push_back(new CannonBall(earthId, rand()%50-25, 1.2, rand() % 100 - 50));
			((CannonBall*)collectibles.back())->scrapPriceGain *= (P2component - player2->getRobot()->getComponentTree()->getSize());
		}

		for (auto it : collectibles) {
			it->draw();
		}

		Mesh3a* object = nullptr;
		bool test1Temporary = false;
		Mesh3a* object2 = nullptr;
		bool test2Temporary = false;

		for (list<Component*>::iterator it = player1->getRobot()->getComponentList()->begin(); it != player1->getRobot()->getComponentList()->end(); it++) {
			if ((*it)->getType() == STANDARDWHEEL) {
				object = (*it);
				test1Temporary = !test1Temporary;
				break;
			}
			object = (*it);
		}

		for (list<Component*>::iterator it = player2->getRobot()->getComponentList()->begin(); it != player2->getRobot()->getComponentList()->end(); it++) {
			if ((*it)->getType() == STANDARDWHEEL) {
				object2 = (*it);
				test2Temporary = !test2Temporary;
				break;
			}
		    object2 = (*it);
		}


		/*double a=0;

		for (int i = 0; i < 12; i++) {
			a=PhysicalEntity::doesItCollide(object->getPosition(), *player1->getRobot()->getSpeed(), (*arena->getGrounds()->begin())->getHitBox()->getTriangles(i));
			if(a!=1){
				a=4;
			}
		if(PhysicalEntity::doesItCollide(object->getPosition(), *player1->getRobot()->getSpeed(), (*arena->getGrounds()->begind())->getHitBox()->getTriangles(i)) >= 1)
		player1->getRobot()->applyGravityOnRobot(chrono.getElapsedTime());
		 if(PhysicalEntity::doesItCollide(object2->getPosition(), *player2->getRobot()->getSpeed(), (*arena->getGrounds()->begin())->getHitBox()->getTriangles(i)) >= 1)
		player2->getRobot()->applyGravityOnRobot(chrono.getElapsedTime());
		}
*/
		if (!player1->getRobot()->getOnGround())
			player1->getRobot()->applyGravityOnRobot(chrono.getElapsedTime());
		if (!player2->getRobot()->getOnGround())
			player2->getRobot()->applyGravityOnRobot(chrono.getElapsedTime());

		if (object) {
			Vector3d bottomWheelPosition = object->getPosition();
			bottomWheelPosition.y = object->getMinY();
			if ((bottomWheelPosition + totalCollisionSegment(object, concrete, *player1->getRobot()->getSpeed())).y <= 0 && !player1->getRobot()->getOnGround()) {
				if (test1Temporary)
					player1->getRobot()->changeMu((*arena->getGrounds()->begin())->getMu((Locomotion*)object));
				player1->getRobot()->changeOnGround();
			}
			if((bottomWheelPosition + totalCollisionSegment(object, concrete, *player1->getRobot()->getSpeed())).y >= 0 && player1->getRobot()->getOnGround()){
				player1->getRobot()->changeOnGround();
			}
		}

		if(player1->getRobot()->getSpeed()->x > 0.00001 or player1->getRobot()->getSpeed()->x < -0.00001 or player1->getRobot()->getSpeed()->z > 0.00001 or player1->getRobot()->getSpeed()->z < -0.00001) {
			for (auto it : *player1->getRobot()->getComponentList()) {
				if ((*it).getType() == STANDARDWHEEL) {
					Ground* collidedGround = (Ground*)ultimateDoesItCollide(it, otherGrounds, (*it->getSpeed()));
					if(collidedGround){
						if(!player1->getRobot()->getIsOnOtherGround()){
							player1->getRobot()->changeMu(collidedGround->getMu((Locomotion*)object));
							break;
						}
						else
							player1->getRobot()->changeMu((*arena->getGrounds()->begin())->getMu((Locomotion*)object));
					}
				}
			}
		}

		if (object2) {
			Vector3d bottomWheelPosition2 = object2->getPosition();
			bottomWheelPosition2.y = object2->getMinY();
			if ((bottomWheelPosition2 + totalCollisionSegment(object2, concrete, *player2->getRobot()->getSpeed())).y <= 0 && !player2->getRobot()->getOnGround()) {
				if (test2Temporary)
					player2->getRobot()->changeMu((*arena->getGrounds()->begin())->getMu((Locomotion*)object2));
				player2->getRobot()->changeOnGround();
			}
			if((bottomWheelPosition2 + totalCollisionSegment(object, concrete, *player2->getRobot()->getSpeed())).y >= 0 && player2->getRobot()->getOnGround()){
				player2->getRobot()->changeOnGround();
			}
		}

		if(player2->getRobot()->getSpeed()->x > 0.00001 or player2->getRobot()->getSpeed()->x < -0.00001 or player2->getRobot()->getSpeed()->z > 0.00001 or player2->getRobot()->getSpeed()->z < -0.00001) {
			for (auto it : *player2->getRobot()->getComponentList()) {
				if ((*it).getType() == STANDARDWHEEL) {
					Ground* collidedGround = (Ground*)ultimateDoesItCollide(it, otherGrounds, (*it->getSpeed()));
					if(collidedGround){
						if(!player2->getRobot()->getIsOnOtherGround()){
							player2->getRobot()->changeMu(collidedGround->getMu((Locomotion*)object));
							break;
						}
						else
							player2->getRobot()->changeMu((*arena->getGrounds()->begin())->getMu((Locomotion*)object));
					}
				}
			}
		}
        player1->draw();
        player2->draw();

        arena->draw();
		if (Bloc1)
			((DoubleLabel*)drawables["P1Energy"])->setText("P1 Energy : " + to_string((int)((EnergyBlock*)Bloc1)->getEnergy()) + " / 550", { 255, 250, 0, 255 });
		if (Bloc2)
			((DoubleLabel*)drawables["P2Energy"])->setText("P2 Energy : " + to_string((int)((EnergyBlock*)Bloc2)->getEnergy()) + " / 550", { 255, 250, 0, 255 });
		if (healthP1)
			((DoubleLabel*)drawables["P1Health"])->setText("P1 Health : " + to_string((int)(Bloc1)->getHealth()) + " / " + to_string((int)(staticHealthP1)), { 255, 250, 0, 255 });
		if (healthP2)
			((DoubleLabel*)drawables["P2Health"])->setText("P2 Health : " + to_string((int)(Bloc2)->getHealth()) + " / " + to_string((int)(staticHealthP2)), { 255, 250, 0, 255 });

		/*((DoubleLabel*)drawables["Robot1Speed"])->setText("P1 speed : X:" + to_string((double)(robot1->getSpeed()->x)) + " Y: " + to_string((double)(robot1->getSpeed()->y)) + " Z: " +  to_string((double)(robot1->getSpeed()->z)), {255,225,10,255});
		((DoubleLabel*)drawables["Robot2Speed"])->setText("P2 speed : X:" + to_string((double)(robot2->getSpeed()->x)) + " Y: " + to_string((double)(robot2->getSpeed()->y)) + " Z: " + to_string((double)(robot2->getSpeed()->z)), { 255,225,10,255 });
		((DoubleLabel*)drawables["Robot1collided"])->setText("P1 collided :" + robot1->getCollided(), { 255,225,10,255 });
		((DoubleLabel*)drawables["Robot2collided"])->setText("P2 collided :" + robot2->getCollided(), { 255,225,10,255 });*/

		((DoubleLabel*)drawables["player1Money"])->setText("player1 money: " + to_string(player1->getMoney()), {255,225,10,255});
		((DoubleLabel*)drawables["player2Money"])->setText("player2 money: " + to_string(player2->getMoney()), {255,225,10,255});

		//((LabelImageButton*)drawables["P2Energy"])->getLabel()->setText("Z : " + to_string(robot1->getComponentList()->back()->getFront().z), { 255, 250, 0, 255 });
		SDLGLContext::getInstance()->setPerspective(true);
		glDisable(GL_LIGHTING);
		for (auto it : drawables)
			it.second->draw();
		Option::getInstance()->showFPS();
		
				
		for (auto it : *player1->getRobot()->getComponentList()) {
			if (it->getType() == ENERGYBLOCK) {
				if (((((EnergyBlock*)it)->getEnergy() < 1) and !collectibles.size()) or (it->getHealth() <= 0)) {
					Scene::setScene(ENDOFGAME);
					player2->addVictory();
					Scene::setWinner(2);
					//while((player1->getRobot()->getComponentList()->front()->getFront()->x < -0.95))
						//player1->getRobot()->direction(true, player1->getRobot()->getLength(), player1->getRobot()->getWidth(), 1, chrono.getElapsedTime());
					//player2->getRobot()->direction(true, player2->getRobot()->getLength(), player2->getRobot()->getWidth(), 60, chrono.getElapsedTime());
				}
			}
		}

		for (auto it : *player2->getRobot()->getComponentList()) {
			if (it->getType() == ENERGYBLOCK) {
				if (((((EnergyBlock*)it)->getEnergy() < 1) and !collectibles.size()) or (it->getHealth() <= 0)) {
					Scene::setScene(ENDOFGAME);
					player1->addVictory();
					Scene::setWinner(1);
				}
			}
		}
		chrono.reset();
	}

    void notification(){
        if(Option::getInstance()->getCheckBoxChecked("player1GamePadCheckBox")) {
            if(SDLEvent::getInstance()->sdlEvent.cbutton.which == player1->getGamePadId()) //retirer pour le control avec clavier
            	player1->notification();
        }
        else{
            player1->notification();
        }
        if(Option::getInstance()->getCheckBoxChecked("player2GamePadCheckBox")) {
            if(SDLEvent::getInstance()->sdlEvent.cbutton.which == player2->getGamePadId())
            	player2->notification();

        }
        else{
            player2->notification();
        }
		switch (SDLEvent::getInstance()->sdlEvent.type) {
		case SDL_KEYDOWN:
			if (SDLEvent::getInstance()->sdlEvent.key.keysym.scancode == SDL_SCANCODE_P) {
				Scene::setScene(PAUSE);
				player1->getRobot()->disableAllComponent();
				player2->getRobot()->disableAllComponent();
			}
			break;
		}
    }
    string getType() {
        return "InGame";
    }
};
#endif