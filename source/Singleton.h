/// \file Singleton.h
/// \brief Singleton.

#ifndef SINGLETON_H
#define SINGLETON_H

template <typename T>

/// \class Singleton
/// \brief Singleton
///
/// Patron de conception Singleton.
///
class Singleton {
private:
	static T* instance; ///< Instance.

public:
	/// \brief Obtention de l'instance.
	/// \return Instance.
	inline static T* getInstance() { 
		return instance;	
	}

	/// \brief Libération de l'instance.
	inline static void deleteInstance() {	
		delete instance; 
	}
};

template <typename T>
T* Singleton<T>::instance = new T(); 

#endif
