/// \file Propeller.h
/// \brief Propeller
#ifndef YSNAP_H
#define YSNAP_H

#include "Component.h"

/// \class YSnap
/// \brief Y pour connecteur
///
/// Sert a obtenir plus de connecteur
///
class YSnap : public Component {
private:
    unsigned int conTextID;///< id des connceteurs non sélectioné
    unsigned int conSelTextID;///< id des connceteurs sélectioné

public:
    YSnap(Material* material, const double& x, const double& y, const double& z) : Component(material, 0, x, y, z,"Objets/helice.obj"){
        type =  YSNAP;
        name = "y snap";
        price = 50;
        mass = 150;
        maxHealth = 200 * material->getPriceMutltiplicator();
        health = 200 * material->getPriceMutltiplicator();
        energieCost = 1;
        Mesh3a::getTransformationMatrix()->setScale(0.1);
        Mesh3a::transform();

        textID =SDLGLContext::getInstance()->loadTexture("Textures/helice.png");
        conTextID = SDLGLContext::getInstance()->loadTexture("Textures/snapPoint.png");
        conSelTextID = SDLGLContext::getInstance()->loadTexture("Textures/snapPointSelect.png");

        connectors[0] = (new Mesh3a(conTextID, position.x, this->getMinY(), position.z, "Objets/crate.obj", conSelTextID));
        connectors[0]->getTransformationMatrix()->setScale(0.3);
        connectors[0]->transform();
        connectors[1] = (new Mesh3a(conTextID, this->getMaxX(), this->getMaxY(), position.z, "Objets/crate.obj", conSelTextID));
        connectors[1]->getTransformationMatrix()->setScale(0.3);
        connectors[1]->transform();
        connectors[2] = (new Mesh3a(conTextID, this->getMinX(), this->getMaxY(), position.z, "Objets/crate.obj", conSelTextID));
        connectors[2]->getTransformationMatrix()->setScale(0.3);
        connectors[2]->transform();
        connectors[3] = (new Mesh3a(conTextID, position.x, this->getMaxY(), this->getMaxZ(), "Objets/crate.obj", conSelTextID));
        connectors[3]->getTransformationMatrix()->setScale(0.3);
        connectors[3]->transform();
        connectors[4] = (new Mesh3a(conTextID, position.x, this->getMaxY(), this->getMinZ(), "Objets/crate.obj", conSelTextID));
        connectors[4]->getTransformationMatrix()->setScale(0.3);
        connectors[4]->transform();
    }

    YSnap(YSnap* ySnap) : Component( ySnap->getMaterial(), 0, ySnap->position.x, ySnap->position.y, ySnap->position.z, ySnap->fileName){
        type =  YSNAP;
        name = "y snap";
        price = 50;
        mass = 150;
        maxHealth = 200 * material->getPriceMutltiplicator();
        health = 200 * material->getPriceMutltiplicator();
        energieCost = 1;
        Mesh3a::getTransformationMatrix()->setScale(0.1);
        Mesh3a::transform();

        textID =SDLGLContext::getInstance()->loadTexture("Textures/helice.png");
        conTextID = SDLGLContext::getInstance()->loadTexture("Textures/snapPoint.png");
        conSelTextID = SDLGLContext::getInstance()->loadTexture("Textures/snapPointSelect.png");

        int i = 0;
        for(auto it : ySnap->getConnectors()){
            connectors[i] = new Mesh3a(conTextID, it.second->getPosition().x, it.second->getPosition().y, it.second->getPosition().z, "Objets/crate.obj", conSelTextID);
            connectors[i]->getTransformationMatrix()->setScale(0.3);
            connectors[i]->transform();
            i++;
        }
    }

    /// \brief Affichage
    void draw(){
        Mesh3a::draw();
        for (int i = 0; i < connectors.size(); i++) connectors[i]->draw();
    }

    // \brief Obtenir Description
    /// \return retourne la description
    list<string>* getDescrition(){
        list<string>* description = new list<string>;
        description->push_back("Descrition:");
        description->push_back("outil permettant d'avoir plus de connecteurs");
		description->push_back("Prix: " + to_string((int)(getPrice() + ((level - 1) * getPrice() / 2.0))));
        description->push_back("Masse: " + to_string((int)getMass()));
        description->push_back("Vie: " + to_string((int)getHealth()) + "/" + to_string((int)getMaxHealth()));
        return description;
    }

    void levelUp(){

    }

    void notification(){

    }
};

#endif