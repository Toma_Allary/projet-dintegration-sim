/// \file NailedWheel.h
/// \brief Roues clouter
#ifndef NAILEDWHEEL_H
#define NAILEDWHEEL_H

#include "Locomotion.h"

/// \class NailedWheel
/// \brief Roues clouter
///
/// Roues clouter
///
class NailedWheel : public Locomotion {
private:
	unsigned int conTextID;
    unsigned int conSelTextID;
public:
    NailedWheel(Material* material, const double& x, const double& y, const double& z, const unsigned int& selecId = 0) : Locomotion(material, x, y ,z, "Objets/nailedWheel.obj "){
        textID = SDLGLContext::getInstance()->loadTexture("Textures/nailedWheel.png");
		conTextID = SDLGLContext::getInstance()->loadTexture("Textures/snapPoint.png");
        conSelTextID = SDLGLContext::getInstance()->loadTexture("Textures/snapPointSelect.png");
        type = NAILEDWHEEL;
        name = "Roue clouter";
		price = 150;
		mass = 150;
		health = 400 * material->getPriceMutltiplicator();
		maxHealth = 400 * material->getPriceMutltiplicator();
		energieCost = 5;
        adherence = 6;

		connectors[0] = (new Mesh3a(conTextID, x, this->getMinZ(), z, "Objets/crate.obj", conSelTextID));
		connectors[0]->getTransformationMatrix()->setScale(0.3);
		connectors[0]->transform();
    }
    NailedWheel(NailedWheel* nailedWheel) : Locomotion(nailedWheel->getMaterial(), nailedWheel->position.x, nailedWheel->position.y , nailedWheel->position.z, nailedWheel->fileName){
        textID = SDLGLContext::getInstance()->loadTexture("Textures/nailedWheel.png");
		conTextID = SDLGLContext::getInstance()->loadTexture("Textures/snapPoint.png");
        conSelTextID = SDLGLContext::getInstance()->loadTexture("Textures/snapPointSelect.png");
        type = NAILEDWHEEL;
        name = "Roue clouter";
		price = 150;
		mass = 150;
		health = 400 * material->getPriceMutltiplicator();
		maxHealth = 400 * material->getPriceMutltiplicator();
		energieCost = 5;
        adherence = 6;

		connectors[0] = (new Mesh3a(conTextID, nailedWheel->position.x, nailedWheel->getMinY(), nailedWheel->position.z, "Objets/crate.obj", conSelTextID));
		connectors[0]->getTransformationMatrix()->setScale(0.3);
		connectors[0]->transform();
    }
    ~NailedWheel(){

    }

    /// \brief Affichage
    void draw(){
        Mesh3a::draw();
		for (int i = 0; i < connectors.size(); i++) connectors[i]->draw();
    }
	/// \brief Obtenir Description
    /// \return retourne la description
	list<string>* getDescrition(){
		list<string>* description = new list<string>;
		description->push_back("Descrition:");
		description->push_back("roue avec plus d'adherance");
		description->push_back("Prix: " + to_string(price));
		description->push_back("Masse: " + to_string(mass));
		description->push_back("Vie: " + to_string(health));
		return description;
	}

    /// \brief Notification
    void notification(){
        
    }

	double getAdherence(){
		return adherence;
	}

	inline bool isItActivated() {
		return isActivated;
	}

};

#endif