/// \file Component.h
/// \brief Composantes
#ifndef COMPONENT_H
#define COMPONENT_H

#define FRAMEBLOC 1
#define STANDARDWHEEL 2
#define NAILEDWHEEL 3
#define FLAMETHROWER 4
#define CATAPULT 5
#define SAW 6
#define ENERGYBLOCK 7
#define SPRING 8
#define PROPELLER 9
#define YSNAP 10


#include <string>
#include <list>
#include <map>
#include "Drawable.h"
#include "Vector3d.h"
#include "PhysicalEntity.h"
#include "Mesh3a.h"
#include "Material.h"
#include "Ntree.h"

using namespace std;
/// \class Component
/// \brief Composantes
///
/// Composantes pour vehicule
///
class Component : public Mesh3a{
protected:
	unsigned int level;/// Niveau d'amelioration de la composante
  unsigned int type;///< Type de la composante
	string name;///< Nom de la composante
  double mass;///< Masse
  double price;///< Prix d'achat
  double maxHealth; ///< Vie maximal
  double health;///< Vie
  int energieCost;///< Consommation d'enerie
	Vector3d* speed;///< vitesse de la composante
	map<unsigned int, Mesh3a*> connectors;
  Material* material;

public:
	bool isAlreadyBought;

  Component(Material* material, unsigned int textureID, const double& x, const double& y, const double& z, string fileName, const unsigned int& selecId = 0):Mesh3a(textureID, x, y, z, fileName, selecId){
    this->material = material;
		level = 1;
    speed = new Vector3d(0.0, 0.0, 0.0);
	health *= material->getPriceMutltiplicator();
		isAlreadyBought = false;
	}

	Component(Component* component, unsigned int textureID, string fileName): Mesh3a(textureID,component->getPosition().x, component->getPosition().y, component->getPosition().z, fileName){
        this->level = component->level;
				this->type = component->type;
		this->mass = component->mass;
		this->price = component->price;
		this->health = component->health;
		this->energieCost = component->energieCost;
		this->speed = component->speed;
		this->name = component->name;
        this->material = component->material;
				this->isAlreadyBought = component->isAlreadyBought;
	}

	inline unsigned int getLevel(){
		return level;
	}

	/// \brief Destructeur
  virtual ~Component() {
	  delete speed;
  }

	/// \brief Obtenir le materiau
	/// \return materiau
	inline Material* getMaterial() {
		return material;
	}

	/// \brief Enregistre la matrix
	void setTransformationMatrix(Matrix44d transformMatrix) {
		for (int i = 0; i < connectors.size(); i++) {
			connectors[i]->setTransformationMatrix(transformMatrix);
		}

		Mesh3a::setTransformationMatrix(transformMatrix);
		
	}

	void resetHealth() {
		health = maxHealth;
	}

	/// \brief permet de changer la position d'une composante et de ses modeles
	void setPosition(const double& x, const double& y, const double& z) {
		double dX, dY, dZ;
		for (int i = 0; i < connectors.size(); i++) {
			dX = connectors[i]->getPosition().x - this->position.x;
			dY = connectors[i]->getPosition().y - this->position.y;
			dZ = connectors[i]->getPosition().z - this->position.z;

			connectors[i]->setPosition((x + dX), (y + dY), (z + dZ));
		}

		Mesh3a::setPosition(x, y, z);
		
	}
	
	inline double getHealth(){
		return health;
	}

	inline double getMaxHealth(){
		return maxHealth;
	}		

	void setHealth(double amount){
		health = amount;
	}

	virtual void levelUp() = 0;

	void receiveDamage(double damage, Ntree<Component*>* componentTree, list<Component*>* robotComponents, double* weaponDamageDealtToUpdate = nullptr) {
		if(name != "Roue standard" and name != "Roue clouter" and name != "Structures"){
			health -= damage;
			if(health <= 0.0){
				*weaponDamageDealtToUpdate += damage + health;
				health = 0.0;
				componentTree->remove(this);
				for(auto it : *robotComponents){
					if(!componentTree->searchRecursive(it)){
						it->visible = false;
						it->setConnectorsVisibilty(false);
					}
				}
			}else{
				*weaponDamageDealtToUpdate += damage;
			}
		}
	}

	/// \brief changement de materiau
	/// \param newMaterial nouveau materiau
  void changeMaterial(Material* newMaterial){
    material = newMaterial;
  }

	void translate(Vector3d translation = Vector3d(0.0, 0.0, 0.0)) {
		for (int i = 0; i < connectors.size(); i++) {
			connectors[i]->translate(translation);
		}
		Mesh3a::translate(translation);
	}

	void applyCurrentMatrix(){
		for (int i = 0; i < connectors.size(); i++) {
			connectors[i]->applyCurrentMatrix();
		}
		Mesh3a::applyCurrentMatrix();
	}

	void transform(double radius = 0.0, Vector3d radiusAxe = (0.0, 0.0, 0.0)) {
		Vector3d translation = position;
		Component::setPosition(0.0 ,0.0, 0.0);
		Mesh3a::transform();

		//Mesh3a::transform(radius, radiusAxe);
		for (int i = 0; i < connectors.size(); i++) {
			connectors[i]->setTransformationMatrix(this->transformMatrix);
			connectors[i]->applyCurrentMatrix();
			/*double deltaX = connectors[i]->getPosition().x - this->position.x , deltaY = connectors[i]->getPosition().y - this->position.y, deltaZ = connectors[i]->getPosition().z - this->position.z;
		
			connectors[i]->setTransformationMatrix(this->transformMatrix);
			if ((!radius) or radiusAxe.x or radiusAxe.y or radiusAxe.z) {
				connectors[i]->transform(radius, Vector3d(deltaX + radiusAxe.x, deltaY + radiusAxe.y, deltaZ + radiusAxe.z));
			}
			else {
				double positionNorm = position.getNorm();
				double xFraction = position.x / positionNorm;
				double yFraction = position.y / positionNorm;
				double zFraction = position.z / positionNorm;
				
				Vector3d tit = (deltaX + (xFraction * radius), deltaY + (yFraction * radius), deltaZ + (zFraction * radius));
				connectors[i]->transform(radius, tit);
			}*/
		}

		Component::setPosition(translation.x, translation.y, translation.z);	
	}

	void setConnectorsVisibilty(bool option) {
		for (int i = 0; i < connectors.size(); i++)
			connectors[i]->setVisibility(option);//l
	}

	void setSpeed(double x, double y, double z) {
		this->speed->x = x;
		this->speed->y = y;
		this->speed->z = z;
	}

	double getAdherence() {
		return this->getAdherence();
	}

	void instantRotationLeft(double angle) {
		//mesh->setTransformationMatrix()
	}

	inline double getMass() {
		return mass;
	}

	inline Vector3d* getSpeed() {
		return speed;
	}
    /// \brief Obtention du nom de la composante
    /// \return Nom de la composante
    inline unsigned int getType(){
		return type;
	}

	/// \brief Obtenir le nom de la composante
	/// \return Le nom de la composante
	inline string getName(){
		return name;
	}

	inline double getPrice(){
		return price * material->getPriceMutltiplicator();
	}

	/// \brief Obtenir la liste des connecteurs
	/// \return La liste des connecteurs
	map<unsigned int, Mesh3a*> getConnectors(){
		return connectors;
	}
    
	/// \brief Notification
    virtual void notification() = 0;

    /// \brief Obtenir description
    virtual list<string>* getDescrition() = 0;

	///// \brief Obtenir le boulet de canon
	//virtual CannonBall* getCannonBall() = 0;

};

#endif