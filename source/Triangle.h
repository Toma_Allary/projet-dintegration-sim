/// \file Triangle.h
/// \brief Contiens trois sommets de chaque triangles des faces d'object
#ifndef TRIANGLE_H
#define TRIANGLE_H

#include "Vector3d.h"

using namespace std;
/// \class Triangle
/// \brief triangles des faces
///
/// Stockage des sommets
///
class Triangle {
private:
	Vector3d vertice1;
	Vector3d vertice2;
	Vector3d vertice3;
	Vector3d normal;

public:
/// \brief Constructeur
	Triangle(Vector3d vertice1, Vector3d vertice2, Vector3d vertice3, Vector3d normal) {
		this->vertice1 = vertice1;
		this->vertice2 = vertice2;
		this->vertice3 = vertice3;
		this->normal = normal;
	}

	/// \brief Destructeur
	~Triangle() {

	}

	inline Vector3d getT0(){
		return vertice1;
	}

	inline Vector3d getT1() {
		return vertice2;
	}

	inline Vector3d getT2() {
		return vertice3;
	}

	inline Vector3d getNormal() {
		return normal;
	}
	
};

#endif
