#ifndef NODEITERATOR_H
#define NODELITERATOR_H

#include "Node.h"

template <typename T>
class NodeIterator {
private:
	Node<T>* it;

public:
	NodeIterator(Node<T>* it) {
		this->it = it;
	}
	NodeIterator(NodeIterator<T>* it) {
		this->it = it->it;
	}

	T getData() {
		if (it) {
			return it->data;
		}
		else {
			return NULL;
		}
	}

	bool goNext(unsigned int ID) {
		if (it->next[ID]) {
			it = it->next[ID];
			return true;
		}
		return false;
	}

	void setData(T data) {
		it->data = data;
	}
};


#endif 

