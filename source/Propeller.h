/// \file Propeller.h
/// \brief Propeller
#ifndef PI_PROPELLER_H
#define PI_PROPELLER_H

#include "Controlable.h"

/// \class Propeller
/// \brief Propeller
///
/// helice
///
class Propeller : public Controlable {
private:
    unsigned int conTextID;///< id des connceteurs non sélectioné
    unsigned int conSelTextID;///< id des connceteurs sélectioné

public:
    Propeller(Material* material, const double& x, const double& y, const double& z, const unsigned int& selecId = 0) : Controlable(material, x, y, z,"Objets/helice.obj"){
        type = PROPELLER;
        name = "Propeller";
        price = 500;
        mass = 150;
        maxHealth = 200 * material->getPriceMutltiplicator();
        health = 200 * material->getPriceMutltiplicator();
        energieCost = 1;
        Mesh3a::getTransformationMatrix()->setScale(0.2);
        Mesh3a::transform();

        textID =SDLGLContext::getInstance()->loadTexture("Textures/helice.png");
        conTextID = SDLGLContext::getInstance()->loadTexture("Textures/snapPoint.png");
        conSelTextID = SDLGLContext::getInstance()->loadTexture("Textures/snapPointSelect.png");

        connectors[0] = (new Mesh3a(conTextID, x, this->getMinY(), z, "Objets/crate.obj", conSelTextID));
        connectors[0]->getTransformationMatrix()->setScale(0.3);
        connectors[0]->transform();
    }

    Propeller(Propeller* propeller) : Controlable( propeller->getMaterial(), propeller->position.x, propeller->position.y, propeller->position.z, propeller->fileName){
        type = PROPELLER;
        name = "Propeller";
        price = 500;
        mass = 150;
        maxHealth = 200 * material->getPriceMutltiplicator();
        health = 200 * material->getPriceMutltiplicator();
        energieCost = 1;
        Mesh3a::getTransformationMatrix()->setScale(0.2);
        Mesh3a::transform();

        textID =SDLGLContext::getInstance()->loadTexture("Textures/helice.png");
        conTextID = SDLGLContext::getInstance()->loadTexture("Textures/snapPoint.png");
        conSelTextID = SDLGLContext::getInstance()->loadTexture("Textures/snapPointSelect.png");

        connectors[0] = (new Mesh3a(conTextID, position.x, this->getMinY(), position.z, "Objets/crate.obj", conSelTextID));
        connectors[0]->getTransformationMatrix()->setScale(0.3);
        connectors[0]->transform();
    }

    /// \brief Activer l'effet de la composante
    void activate() {
        isUsedInstantly = true;
    }

    /// \brief Desactiver l'effet de la composante
    void disable(){
        isUsedInstantly = false;
    }

	inline bool isItActivated() {
		return isActivated;
	}

    /// \brief Affichage
    void draw(){
        Mesh3a::draw();
        for (int i = 0; i < connectors.size(); i++) connectors[i]->draw();
        usingTime.getElapsedTime();
        if(isUsedInstantly){
            rotationMatrix.loadRotation(usingTime.getElapsedTime() * 15, up);
            this->setTransformationMatrix(rotationMatrix);
            this->transform();
        }
        usingTime.reset();
    }

    void levelUp(){
        level++;
		maxHealth *= 1.5;
		health *= 1.5;
	}

    // \brief Obtenir Description
    /// \return retourne la description
    list<string>* getDescrition(){
        list<string>* description = new list<string>;
        description->push_back("Descrition:");
        description->push_back("outil permettant de voler");
		description->push_back("Niveau: " + to_string(level));
		description->push_back("Prix: " + to_string((int)(getPrice() + ((level - 1) * getPrice() / 2.0))));
        description->push_back("Masse: " + to_string((int)getMass()));
        description->push_back("Vie: " + to_string((int)getHealth()) + "/" + to_string((int)getMaxHealth()));
        return description;
    }
    void notification(){

    }
};

#endif
