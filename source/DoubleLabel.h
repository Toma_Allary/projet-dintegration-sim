/// \file Label.h
/// \brief Double étiquette

#ifndef DOUBLELABEL_H
#define DOUBLELABEL_H

#include <string>
#include "Label.h"

using namespace std;

/// \class DoubleLabel
/// \brief Double étiquette
///
/// Étiquettes.
///
class DoubleLabel : public Label {
private:
	Font* font; ///< Police de caractères.
	string text;///< Texte de base.
	Label* label;

public:
	/// \brief Affectation du texte.
	void setText(const string& text, const SDL_Color& color) {
		if (font->getResource()) {
			glDeleteTextures(1, &textureId);
			glGenTextures(1, &textureId);

			glBindTexture(GL_TEXTURE_2D, textureId);

			SDL_Surface* surface = TTF_RenderUTF8_Blended((TTF_Font*)font->getResource(), text.c_str(), color);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, surface->w, surface->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, surface->pixels);
			width = surface->w;	height = surface->h;
			SDL_FreeSurface(surface);

			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		}
	}

	/// \brief Constructeur.
	/// \param x Position sur l'axe des x.
	/// \param y Position sur l'axe des y.
	/// \param width Largeur.
	/// \param height Hauteur.
	/// \param font Police de caractères.
	/// \param text Texte.
	/// \param color Couleur.
	DoubleLabel(const double& x, const double& y, Font* font, const string& text, const SDL_Color& color, const double& layer = 0.0, const double& offset = 1.0) : Label(x, y, font, text, color, layer) {
		this->font = font;
		this->text = text;
		setText(text, color);
		setPosition(x, y);
		label = new Label(x + offset, y + offset, font, text, {0, 0, 0, 255}, layer - 0.1);
	}

	/// \brief Destructeur.
	~DoubleLabel() {
		delete label;
		glDeleteTextures(1, &textureId);
	}

	/// \brief Affichage.
	void draw() {
		label->draw();

		glBindTexture(GL_TEXTURE_2D, textureId);

		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);

		glVertexPointer(3, GL_DOUBLE, 0, vertices);
		glTexCoordPointer(2, GL_DOUBLE, 0, texCoords);
		
		glDrawArrays(GL_QUADS, 0, 4);

		glDisableClientState(GL_VERTEX_ARRAY);
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	}

	void notification() {}
};

#endif