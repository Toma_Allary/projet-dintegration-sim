#pragma once
#include "Component.h"
#include "PhysicalEntity.h"

class CannonBall : public Mesh3a{
private:
	unsigned int conTextID;
	unsigned int conSelTextID;
	unsigned int cannonBallID;
	Vector3d* initialPosition;
	Vector3d* speed;
	bool inTheAir;

public:
	double scrapPriceGain;
	
	CannonBall(unsigned int textureID, const double& x, const double& y, const double& z):  Mesh3a(textureID, x, y, z, "Objets/Ball.obj") {
		initialPosition = new Vector3d(x, y, z);
		speed = new Vector3d(0.0, 0.0, 0.0);
		inTheAir = false;
		getTransformationMatrix()->setScale(0.0085);
		transform();
		scrapPriceGain = 100.0;
		//hitboxes.push_back(new HitBox(this->position, getMaxX(), -0.68, 0.01 + getMaxY(), 0.1 + (this->position.y), -.2 + (this->position.z), -0.01 + getMinZ()));
	}

	void resetFrontAndUp(Vector3d catapultFront, Vector3d catapultUp) {
		front = Vector3d(catapultFront.z, 1.0 - catapultUp.y, -catapultFront.x);
		up = Vector3d(catapultUp.z, catapultUp.y, catapultUp.x);
	}

	void resetSpeed() {
		speed = new Vector3d(0.0, 0.0, 0.0);
	}

	Vector3d* getInitialPosition() {
		return initialPosition;
	}

	void setInitialPosition(double x, double y, double z) {
		initialPosition->x = x;
		initialPosition->y = y;
		initialPosition->z = z;
	}

	void setSpeed(double x, double y, double z) {
		this->speed->x = x;
		this->speed->y = y;
		this->speed->z = z;
	}
		
	bool isInTheAir() {
		return inTheAir;
	}

	void changeInTheAirBool() {
		inTheAir = false;
	}

	void launch(double power) {
		speed->x = 0.2 * front.x * power;
		speed->y = 0.2 * front.y * power * 12.0;
		speed->z = 0.2 * front.z * power;
		inTheAir = true;
	}

	Vector3d* getSpeed() {
		return speed;
	}

	void notification() {

	}

	list<string>* getDescrition() {
		return nullptr;
	}
};