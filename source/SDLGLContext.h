/// \file SDLGLContext.h
/// \brief Contexte OpenGL.

#ifndef SDLGLCONTEXT_H
#define SDLGLCONTEXT_H

#include "SDLWindow.h"
#include "Matrix44d.h"
#include "Singleton.h"
#include"libSDL2.h"

/// \class SDLGLContext
/// \brief Contexte OpenGL.
///
/// Contexte OpenGL.
///
class SDLGLContext : public SDLWindow , public Singleton<SDLGLContext> {
private:
	SDL_GLContext glContext; ///< Contexte OpenGL.
	Matrix44d orthogonal, projection; ///< Matrice de projection.

public:
	/// \brief Constructeur.
	/// \param title Titre de la fenêtre
	/// \param x Position de la fenêtre sur l'axe des x.
	/// \param y Position de la fenêtre sur l'axe des y.
	/// \param width Largeur de la fenêtre
	/// \param height Hauteur de la fenêtre
	/// \param flags Indicateurs
	SDLGLContext(const char* title = "", const int& x = SDL_WINDOWPOS_CENTERED, const int& y = SDL_WINDOWPOS_CENTERED, const int& width = 1200, const int& height = 700, const unsigned int& flags = 0) : SDLWindow(title, x, y, width, height, flags | SDL_WINDOW_OPENGL) {
		glContext = SDL_GL_CreateContext(sdlWindow);
		SDL_GL_SetSwapInterval(0);
		setMatrices();
	}

	/// \brief Destructeur.
	~SDLGLContext() {
		SDL_GL_DeleteContext(glContext);
	}

	void setContextResolution(const int& width, const int& height) {
		setWindowSize(width, height);
		setMatrices();
	}

	const int& getWindowWidth() {
		return width;
	}

	const int& getWindowHeight() {
		return height;
	}

	/// \brief Création d'une surface SDL.
	/// \param width Largeur.
	/// \param height Hauteur.
	/// \return Surface SDL.
	inline SDL_Surface* createSurface(const int& width, const int& height) {
		return SDL_CreateRGBSurface(0, width, height, 32, 255, 65280, 16711680, 4278190080);
	}

	/// \brief Chargement d'une texture.
	/// \param sdlSurface Surface SDL.
	/// \return Identificateur de texture.
	unsigned int loadTexture(SDL_Surface* sdlSurface) {
		unsigned int id;
		glGenTextures(1, &id);
		glBindTexture(GL_TEXTURE_2D, id);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, sdlSurface->w, sdlSurface->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, sdlSurface->pixels);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

		return id;		
	}

	/// \brief Chargement d'une texture.
	/// \param fileName Nom du fichier.
	/// \return Identificateur de texture.
	unsigned int loadTexture(const char* fileName) {
		unsigned int id;
		glGenTextures(1, &id);
		glBindTexture(GL_TEXTURE_2D, id);

		SDL_Surface* surface = IMG_Load(fileName);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, surface->w, surface->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, surface->pixels);
		SDL_FreeSurface(surface); 

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

		return id;
	}

	/// \brief Initialisation des matrices de projection.
	/// \param fov Champ de vision.
	/// \param nearCut Plan rapproché.
	/// \param farCut Plan éloigné.
	void setMatrices(double fov = 89.0, double nearCut = 0.1, double farCut = 1500.0) {
		orthogonal.loadOrthogonal(width, height);

		double r = std::tan(fov / 2.0) * nearCut;
		double t = r * ((double)height / (double)width);
		projection.loadProjection(r, t, nearCut, farCut);
	}

	/// \brief Affectation de la perspective
	/// \param is2D Affichage 2D sinon 3D
	void setPerspective(const bool& is2D) {
		glMatrixMode(GL_PROJECTION); glLoadIdentity();
		(is2D) ? glMultMatrixd(orthogonal.matrix) : glMultMatrixd(projection.matrix);
		glMatrixMode(GL_MODELVIEW); glLoadIdentity();
	}

	/// \brief Vide les tampons d'affichage.
	inline void clear() { 
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); 
	}

	/// \brief Actualise le contenu de la fenêtre.
	inline void refresh() {
		SDL_GL_SwapWindow(sdlWindow); 
	}
};

#endif
