/// \file SDLWindow.h
/// \brief Fenêtre.

#ifndef SDLWINDOW_H
#define SDLWINDOW_H

/// \class SDLWindow
/// \brief Fenêtre.
///
/// Fenêtre du système d'exploitation.
///
class SDLWindow {
protected:
	SDL_Window* sdlWindow; ///< Fenêtre SDL.
	int width, height; ///< Largeur et Hauteur.

public:
	/// \brief Constructeur.
	/// \param title Titre de la fenêtre.
	/// \param x Position de la fenêtre sur l'axe des x.
	/// \param y Position de la fenêtre sur l'axe des y.
	/// \param width Largeur de la fenêtre.
	/// \param height Hauteur de la fenêtre.
	/// \param flags Indicateurs.
	SDLWindow(const char* title, const int& x, const int& y, const int& width, const int& height, const unsigned int& flags) {
		sdlWindow = SDL_CreateWindow(title, x, y, width, height, flags);
		this->width = width; this->height = height;
	}

	/// \brief Destructeur.
	virtual ~SDLWindow() {
		SDL_DestroyWindow(sdlWindow);
	}

	void setWindowSize(const int& width, const int& height) {
		this->width = width; this->height = height;
		SDL_SetWindowSize(sdlWindow, width, height);
		glViewport(0, 0, width, height);
	}

	/// \brief Vider le contenu de la fenêtre.
	virtual void clear() = 0;

	/// \brief Actualiser le contenu de la fenêtre.
	virtual void refresh() = 0;
};

#endif
