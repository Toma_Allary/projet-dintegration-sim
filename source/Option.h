/// \file Option.h
/// \brief Contient la classe Option.h

#ifndef OPTION_H
#define OPTION_H

#define OPTION_TEXTURE_BACKGROUND 0
#define OPTION_TEXTURE_BUTTON_BACKGROUND 1
#define OPTION_TEXTURE_OVERBUTTON_BACKGROUND 2
#define OPTION_TEXTURE_CHECKED 3
#define OPTION_TEXTURE_UNCHECKED 4

#include "Scene.h"
#include "Label.h"
#include "Singleton.h"
#include "Image.h"
#include "DoubleLabel.h"
#include "CheckBox.h"
#include "LabelImageButton.h"
#include "FPSCounter.h"

void backPrototype();

/// \class Option
/// \brief Scène du menu option
///
/// Cette classe permet d'afficher à l'écran le menu option qui, lui, permet à l'utilisateur d'afficher en temps réel les frames par secondes et de changer la résolution du programme.
///
class Option : public Scene, public Singleton<Option>{
private: 
	CheckBox* checkbox;
	FPSCounter* fpscounter;
	unsigned int texturesId[5];

public:

	/// \brief Initialisation des visual components
	void setOption() {		
		SDL_Color red = { 0, 0, 255, 255 }, white = { 255, 255, 255, 255 };

		texturesId[OPTION_TEXTURE_BACKGROUND] = SDLGLContext::getInstance()->loadTexture("Textures/menuBackground.png");
    	texturesId[OPTION_TEXTURE_BUTTON_BACKGROUND] = SDLGLContext::getInstance()->loadTexture("Textures/buttonBackground.png");
    	texturesId[OPTION_TEXTURE_OVERBUTTON_BACKGROUND] = SDLGLContext::getInstance()->loadTexture("Textures/_buttonBackground.png");
    	texturesId[OPTION_TEXTURE_CHECKED] = SDLGLContext::getInstance()->loadTexture("Textures/Checked.png");
    	texturesId[OPTION_TEXTURE_UNCHECKED] = SDLGLContext::getInstance()->loadTexture("Textures/UnChecked.png");

		checkbox = new CheckBox(texturesId[OPTION_TEXTURE_UNCHECKED], texturesId[OPTION_TEXTURE_CHECKED], 700, 300, 50, 50, 0.5);
		drawables["CheckBox"] = checkbox;
		int gamepadCount = SDL_NumJoysticks();
		drawables["gamepadCount"] = new Label(715.0, 200.0, ResourceManager::get<Font*>("font32"), std::to_string(gamepadCount), (gamepadCount > 1) ? white : red, 0.5);
		drawables["background"] = new Image(texturesId[OPTION_TEXTURE_BACKGROUND], 0.0, 0.0, 1200, 700);

        drawables["title"] = new DoubleLabel(475.0, 50.0, ResourceManager::get<Font*>("font60"), "OPTIONS", white, 0.2, 5.0);
        drawables["backButton"] = new LabelImageButton(texturesId[OPTION_TEXTURE_BUTTON_BACKGROUND], ResourceManager::get<Font*>("font32"), white, "Retour", 450, 580, 300, 100, 0.1, texturesId[OPTION_TEXTURE_OVERBUTTON_BACKGROUND]);
        drawables["gamepadLabel"] = new Label(450.0, 200.0, ResourceManager::get<Font*>("font32"), "Manettes", white, 0.5);
        drawables["fpsLabel"] = new Label(450.0, 304.0, ResourceManager::get<Font*>("font32"), "Afficher IPS", white, 0.5);
		drawables["player1GamepadLabel"] = new Label(450.0, 400.0,ResourceManager::get<Font*>("font32"), "Player 1 Gamepad", white, 0.5);
		drawables["player2GamepadLabel"] = new Label(450.0, 500.0, ResourceManager::get<Font*>("font32"), "Player 2 Gamepad", white, 0.5);
		drawables["player1GamePadCheckBox"] = new CheckBox(texturesId[OPTION_TEXTURE_UNCHECKED], texturesId[OPTION_TEXTURE_CHECKED], 800, 400, 50, 50, 0.5);
		drawables["player2GamePadCheckBox"] = new CheckBox(texturesId[OPTION_TEXTURE_UNCHECKED], texturesId[OPTION_TEXTURE_CHECKED], 800, 500, 50, 50, 0.5);


		((Button*)drawables["backButton"])->bindOnClick(backPrototype);

		events[SDL_MOUSEMOTION] = new Observable();
		events[SDL_MOUSEBUTTONDOWN] = new Observable();
		events[SDL_MOUSEBUTTONDOWN]->subscribe(drawables["backButton"]);
		events[SDL_MOUSEBUTTONDOWN]->subscribe(drawables["CheckBox"]);
		events[SDL_MOUSEBUTTONDOWN]->subscribe(drawables["player1GamePadCheckBox"]);
		events[SDL_MOUSEBUTTONDOWN]->subscribe(drawables["player2GamePadCheckBox"]);
		events[SDL_MOUSEMOTION]->subscribe(drawables["backButton"]);

		fpscounter = new FPSCounter();
	}

	/// \brief Destructeur.
	~Option() {
    for (auto it : drawables) delete it.second;
		for (auto it : events) delete it.second;

    glDeleteTextures(5, texturesId);
	}

	inline bool getCheckBoxChecked(string nameOfCheckBox) {
		return ((CheckBox*)drawables[nameOfCheckBox])->getChecked();
	}

	inline string getType(){
		return "Option";
	}

	void showFPS() {
		if (getCheckBoxChecked("CheckBox"))
			fpscounter->draw();
	}

	/// \brief permet d'afficher à l'écran le menu des options
	void draw() {
		SDLGLContext::getInstance()->setPerspective(true);
		glDisable(GL_LIGHTING);
		for (auto it : drawables)
			it.second->draw();
		showFPS();
		glEnable(GL_LIGHTING);
	}

	/// \brief permet de notifier les boutons du menu des options
	virtual void notification() {
		if (events[SDLEvent::getInstance()->sdlEvent.type])
			events[SDLEvent::getInstance()->sdlEvent.type]->notify();
	}
};

inline void backPrototype() {
	Scene::setScene(12345);
}

#endif

