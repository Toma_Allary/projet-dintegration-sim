#ifndef RESOURCE_H
#define RESOURCE_H

class Resource {
public:
  virtual ~Resource() {}
  virtual void* getResource() = 0;
};

#endif
