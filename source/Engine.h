/// \file Engine.h
/// \brief Moteur

#ifndef ENGINE_H
#define ENGINE_H

#include <fstream>
#include <map>
#include <string>
#include <vector> 
#include <time.h>

#include "libSDL2.h"
#include "ResourceManager.h"
#include "Mesh3a.h"
#include "Scene.h"
#include "Label.h"
#include "LoadingScreen.h"
#include "Chrono.h"
#include "CheckBox.h"
#include "MainMenu.h"
#include "Garage.h"
#include "PauseMenu.h"
#include "Option.h"
#include "RobotTestingSpace.h"
#include "CameraTestingSpace.h"
#include "InGame.h"
#include "EndGame.h"

using namespace std;
/// \class Engine
/// \brief moteur
///
/// Gestion de la boucle de jeux.
///
class Engine {
private:
	bool isRunning;
	SDLGLContext glContext; ///< Contexte OpenGL.
	Scene* currentScene; ///< Scène actuelle.
	Scene* inGameSecondaryScene; ///< InGame pas active mais qui doit rester en mémoire
	Scene* pauseSecondaryScene; ///< Pause pas active mais qui doit rester en mémoire

	unsigned int sceneChanger; ///< Identificateur de la prochaine scène.

public:
	Engine() : glContext("420-204-RE : Projet d'Integration") {
		// TODO : creer des constante (#define) pour chaque menu/scene a afficher
		currentScene = new LoadingScreen("", MAINMENU);
		inGameSecondaryScene = pauseSecondaryScene = nullptr;
		sceneChanger = 0;  

		Option::getInstance()->setOption();
		isRunning = false;
	}

	~Engine() {
		delete currentScene;
	}

	/// \brief Démarrage de la boucle de jeux.
	void start() {
		glEnable(GL_TEXTURE_2D);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_LIGHTING);
		glEnable(GL_LIGHT0);

		srand(time(0));

		isRunning = true;
		while (isRunning) {
			while (SDL_PollEvent(&SDLEvent::getInstance()->sdlEvent))
				switch (SDLEvent::getInstance()->sdlEvent.type) {
				case SDL_QUIT:
					isRunning = false;
					break;
				default:
					currentScene->notification();
					break;
				}
			if (sceneChanger != Scene::getScene()) {
				sceneChanger = Scene::getScene();
				// Si l'on veut garder les element d'une partie il faudra ajouter un if/ une condition avant de supprimer la scene actuelle 
				Scene* toDelete = currentScene;
				if(inGameSecondaryScene and Scene::getScene() == INGAME){
					currentScene = inGameSecondaryScene;
					inGameSecondaryScene = nullptr;
					delete toDelete;
				}
				else{
					switch (sceneChanger) {
						// TODO : Ajouter les cas pour passer de menu en menu
						case GARAGE:
							currentScene = new Garage();
							delete toDelete;
							break;
						case MAINMENU:
							currentScene = new MainMenu(&isRunning);
							if (toDelete != Option::getInstance()) {
							delete toDelete;
							}
							break;
						case OPTIONMENU:
							pauseSecondaryScene = currentScene;
							currentScene = Option::getInstance();
							break;
						case PAUSE:
							inGameSecondaryScene = currentScene;
							currentScene = new PauseMenu();
							break;
						case ROBOTTESTINGSPACE:
							currentScene = new RobotTestingSpace();
							delete toDelete;
							break;
						case INGAME:
							currentScene = new InGame();
							delete toDelete;
							break;
						case ENDOFGAME:
							if(inGameSecondaryScene){
								delete inGameSecondaryScene;
								inGameSecondaryScene = nullptr;
							}
							currentScene = new EndGame(&isRunning);
							delete toDelete;
							break;
						default:
							currentScene = pauseSecondaryScene;
							pauseSecondaryScene = nullptr;
							break;
					}
				}
			}

			glLoadIdentity();
			//camera->applyView();

			glContext.clear();
			if (currentScene)
				currentScene->draw();
			glContext.refresh();
		}
	}
};

#endif

