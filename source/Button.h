/// \file Button.h
/// \brief Bouton

#ifndef BUTTON_H
#define BUTTON_H

#include "VisualComponent.h"
#include "Observer.h"

/// \class Button
/// \brief Bouton
///
/// Bouton
///
class Button : public VisualComponent{
protected:
    void (*onClick)(); ///< Fonction à appeler lors d'un click.
    bool clicked;///< S'il est cliquer
    bool mouseOn;///< Si la souris est dessus le bouton.
public:
    /// \brief Constructeur
    /// \param x Position sur l'axe des x.
    /// \param y Position sur l'axe des y.
    /// \param h Hauteur.
    /// \param w Largeur.
    Button(const double& x = 0.0, const double& y = 0.0, const unsigned int& width = 0.0, const unsigned int& height = 0.0, const double& layer = 0.0) : VisualComponent(x, y, width, height, layer) {
        clicked = false;
        mouseOn = false;
	}

	~Button() {

	}

    /// \brief Obtenir s'il est est cliquer ou non
    /// \return Retourne s'il est est cliquer ou non
    inline bool getClicked(){
        return clicked;
    }

    /// \brief Permet de voir si la sourie est sur le bouton
    /// \return bolléen qui indique si la sourie est dessus le bouton ou non.
    inline bool getMouseOn(){
        return mouseOn;
    }

    /// \brief Affectation du pointeur de fonction onClick
    /// \param onClick Pointeur de fonction
    inline void bindOnClick(void (*onClick)()){
        this->onClick = onClick;
    }

    /// \brief permet de voir le type du drawable.
    /// \return retourne une string indiquand  le type du drawables.
    string getType(){
        return "Button";
    }

    virtual void notification() = 0;
    virtual void draw() = 0;
};

#endif
