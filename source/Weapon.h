/// \file Weapon.h
/// \brief Arme
#ifndef WEAPON_H
#define WEAPON_H

#include "Controlable.h"
#include"EnergyBlock.h"
#include "Ntree.h"

/// \class Weapon
/// \brief Arme
///
/// Arme
///
class Weapon : public Controlable{
protected:
    unsigned int damage; ///< dommages fait
	double damageDealt;///< degat fait a l'enemie
public:
    Weapon(Material* material, const double& x, const double& y, const double& z, string fileName, const unsigned int& selecId = 0) : Controlable(material, x, y ,z, fileName){
		damageDealt = 0.0;
    }
    ~Weapon(){

    }

	void dealDamage(Component* component, Ntree<Component*>* componentTree, list<Component*>* robotComponents) {
		if (name != "bloc d'energie") {
			if (name == "Lance flamme"){
				component->receiveDamage(damage * component->getMaterial()->getFireResistance(), componentTree, robotComponents, &damageDealt);// *material->getCutResistance());
			}
			if (name == "Scie"){
				component->receiveDamage(damage * component->getMaterial()->getCutResistance(), componentTree, robotComponents, &damageDealt);// *material->getCutResistance());
			}
			if (name == "Catapulte"){
				component->receiveDamage(damage * component->getMaterial()->getImpactResistance(), componentTree, robotComponents, &damageDealt);
			}
		}
		else
            ((EnergyBlock*)component)->receiveDamageOnEnergieBlock(damage, &damageDealt);
        
	}

	inline double getDamageDealt(){
		return damageDealt;
	}

	inline void resetDamageDealt(){
		damageDealt = 0.0;
	}

	bool isUsedPresently() {
		return overTimeUse;
	}

    	/// \brief Obtenir Description
    /// \return retourne la description
	list<string>* getDescrition(){
		list<string>* description = new list<string>;
		description->push_back("Descrition:");
		description->push_back("Arme");
        if(isUsedInstantly)
    	    description->push_back("Dommage: " + to_string(damage));
        else
    	    description->push_back("Dommage: " + to_string(damage) + "/s");
		description->push_back("Niveau: " + to_string(level));
		description->push_back("Prix: " + to_string((int)(getPrice() + ((level - 1) * getPrice() / 2.0))));
        description->push_back("Masse: " + to_string((int)getMass()));
        description->push_back("Vie: " + to_string((int)getHealth()) + "/" + to_string((int)getMaxHealth()));
		return description;
	}

    virtual void levelUp() = 0;
    /// \brief Activer l'effet de la composante
    virtual void activate() = 0;
    /// \brief Desactiver l'effet de la composante
    virtual void disable() = 0;
    /// \brief Affichage
    virtual void draw() = 0;
    /// \brief Notification
    virtual void notification() = 0;
};

#endif