﻿/// \file Scene.h
/// \brief Scène.

#ifndef SCENE_H
#define SCENE_H

#include <map>
#include <list>
#include "Font.h"
#include "Mesh3a.h"
#include "VisualComponent.h"
#include "Observable.h"
#include "Drawable.h"

using namespace std;
/// \class Scene
/// \brief Scene.
///
/// Scène du moteur.
///
class Scene : public Drawable {
protected:
	map<string, Drawable*> drawables;///< Affichables
	list<Mesh3a*> meshes; ///< Modèles.
	static unsigned int currentScene; ///< Scène actuelle.
	map<unsigned int, Observable*> events;///< gestion des evenements
	static unsigned int lastWinner;
public :
	virtual ~Scene() {}

	/// \brief Affichage.
	virtual void draw() = 0;

	/// \brief Notifications
	virtual void notification() = 0;

	/// \brief Changement de scène.
	/// \param sceneId Identificateur de scène.
	static void setScene(const unsigned int& sceneId) {
		currentScene = sceneId;
	}

	/// \brief Obtention de l'identificateur de scène.
	/// \return Identificateur de scène.
	static unsigned int getScene() {
		return currentScene;
	}

	inline static void setWinner(unsigned int winnerID) {
		lastWinner = winnerID;
	}

	inline static unsigned int getWinner() {
		return lastWinner;
	}
};
unsigned int Scene::lastWinner = 0;
unsigned int Scene::currentScene = 0;

#endif
