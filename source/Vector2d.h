/// \file Vector3d.h
/// \brief contient la classe Vector3d
#ifndef VECTOR2D_H
#define VECTOR2D_H
#include<cmath>

/// \class Vector3d
/// \brief Vecteur � 3 composantes en r�els.
///
/// Vecteur � 3 composantes r�els.
///
class Vector2d {
public:
	double x, y; ///< Composantes.

	/// \brief Constructeur.
	/// \param x Composante x.
	/// \param y Composante y.
	Vector2d(const double& x = 0.0, const double& y = 0.0) {
		this->x = x;
		this->y = y;
	}

	/// \brief Constructeur de copie.
	Vector2d(const Vector2d& v) {
		x = v.x;
		y = v.y;
	}

	/// \brief Obtention de la norme.
	/// \return Norme.
	inline double getNorm() const {
		return sqrt(x * x + y * y);
	}
	/// \brief Changemnt du x et du y
	void set(const double& x, const double & y){
		this->x = x;
		this->y = y;
	}

	/// \brief Rendre le vecteur unitaire.
	void normalize() {
		double norm;
		if (norm = sqrt(x * x + y * y)) {
			x = x / norm;
			y = y / norm;
		}
	}

	/// \brief Soustraction.
	/// \param v Vecteur.
	/// \return Vecteur r�sultant.
	Vector2d operator-(const Vector2d& v) const {
		return Vector2d(x - v.x, y - v.y);
	}

	/// \brief Addition.
	/// \param v Vecteur.
	/// \return Vecteur r�sultant.
	Vector2d operator+(const Vector2d& v) const {
		return Vector2d(x + v.x, y + v.y);
	}

	/// \brief Multiplication par un scalaire.
	/// \param s Scalaire.
	/// \return Vecteur r�sultant.
	Vector2d operator*(const double& s) const {
		return Vector2d(x * s, y * s);
	}

	/// \brief Division par un scalaire.
	/// \param s Scalaire.
	/// \return Vecteur r�sultant.
	Vector2d operator/(const double& s) const {
		return Vector2d(x / s, y / s);
	}

	/// \brief Produit vectoriel.
	/// \param v Vecteur.
	/// \return Vecteur r�sultant.
	/*Vector2d operator%(const Vector2d& v) const {
		return Vector2d((y * v.z) - (z * v.y), (z * v.x) - (x * v.z), (x * v.y) - (y * v.x));
	}*/
	/// \brief Produit scalaire.
	/// \param v Vecteur.
	/// \return scalaire r�sultant.
	double dotProduct(const Vector2d& v) const {
		return (x * v.x) + (y * v.y);
	}
};

#endif
