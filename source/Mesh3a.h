/// \file Mesh3a.h
/// \brief Chargeur de modèle.

#ifndef MESH_H
#define MESH_H

#include "Triangle.h"
#include <fstream>
#include <list>
#include"Vector3d.h"
#include "HitBox.h"
#include "PhysicalEntity.h"

using namespace std;

/// \class Mesh3a
/// \brief Chargeur de modèles
///
/// Chargeur de modèle à 3 tableaux.
///
class Mesh3a : public PhysicalEntity{
protected:
	bool displayAxes; ///< aide visuel
	bool displayHitBoxes; ///< aide visuel
	bool displayFront; ///< aide visuel
	bool selected;
	bool visible;
	double* axesVertices;///< aide visuel
	double* axesNormals;///< aide visuel
	double* FrontVertices;///< aide visuel
	double* vertices; ///< Sommets.
	double* normals; ///< Normales.
	double* coords; ///< Coordonnées de textures.
	unsigned int textID; ///< Identificateur de texture.
	unsigned int selectedTextId;///< Identificateur de texture lorsque le mesh est sélectionné.
	unsigned int actualTextId;///< La texture actuelle du mesh.
	unsigned int verticeArrSize;
	Vector3d position;
	Matrix44d rotationMatrix;
	Matrix44d translationMatrix1;
	Matrix44d translationMatrix2;
	Matrix44d transformMatrix;
	Vector3d front;
	Vector3d staticFront;
	Vector3d up;
	double maxX;
	double minX;
	double maxY;
	double minY;
	double maxZ;
	double minZ;
	string fileName;
	HitBox* hitbox;
	list<HitBox*> hitboxes;
	Mesh3a* collide;///< pointeur de l'objet en collision.
	

public:
/// \brief Constructeur.
/// \param id Identificateur de texture.
	Mesh3a(const unsigned int& id, const double& x, const double& y, const double& z, string fileName, const unsigned int& selecId = 0) {
		displayAxes = false;///< aide visuelle
		displayHitBoxes = true;///< aide visuelle
		displayFront = true;///< aide visuelle
		visible = true;
		selected = false;
		selectedTextId = selecId;
		textID = id;
		actualTextId = textID;
		axesVertices = new double[27];///< aide visuelle
		axesNormals = new double[9];///< aide visuelle
		FrontVertices = new double[9];///< aide visuelle
		position = { 0, 0, 0 };
		this->fileName = fileName;
		front = staticFront = Vector3d(-1.0, 0.0, 0.0);
		up = Vector3d(0.0, 1.0, 0.0);
		loadOBJ(fileName);
		
		
		hitbox = new HitBox(this->position, getMaxX(), getMinX(), getMaxY(), getMinY(), getMaxZ(), getMinZ());
		
		setPosition(x, y, z);

		transformMatrix.resetMatrix();

		minX = maxX = minY = maxY = minZ = maxZ = 0.0;
		
	}

	Vector3d getStaticFront() {
		return staticFront;
	}

	inline unsigned int getTextureID(){
		return textID;
	}

	/*/// \brief obtenir la position du centre de masse
	Vector3d getCenterOfMass() {
		minX = vertices[0];
		maxX = minX;
		minY = vertices[1];
		maxY = minY;
		minZ = vertices[2];
		maxZ = minZ;
		for (unsigned int i = 0; i < verticeArrSize; i += 3) {
			if (vertices[i] < minX) minX = vertices[i];
			if (vertices[i] > maxX) maxX = vertices[i];
			if (vertices[i + 1] > maxY) maxY = vertices[i + 1];
			if (vertices[i + 1] < minY) minY = vertices[i + 1];
			if (vertices[i + 2] < minZ) minZ = vertices[i + 2];
			if (vertices[i + 2] > maxZ) maxZ = vertices[i + 2];
		}
		return ((minX+maxX)/2, (minY + maxY) / 2, (minZ + maxZ) / 2);
	}*/

	/// \brief obtenir la position du centre de masse
	Vector3d getCenterOfMass() {
		Vector3d COM;
		unsigned x = 0;
		for (unsigned int i = 0; i < verticeArrSize; i += 1) {
			COM = COM + vertices[i];
			x++;
		}
		COM = COM / x;
		return COM;
	}
	inline bool getVisible() {
		return visible;
	}
	/// \ controlle la visibiliter des mesh
	void setVisibility(bool option) {
		visible = option;
	}

	inline HitBox* getHitBox() {
		return hitbox;
	}

	inline list<HitBox*> getHitboxes() {
		if (!hitboxes.size()) {
			hitboxes.push_back(hitbox);
		}
		return hitboxes;
	}

	/// \ Permet d'obtenir les sommets du modèle 
	inline double* getVertices() {
		return vertices;
	}

	/// \ Permet d'obtenir les normales du modèle 
	inline double* getNormals() {
		return normals;
	}

	inline Vector3d* getFront() {
		return &front;
	}

	void setFront(Vector3d Front) {
		front = Front;
	}

	inline Vector3d getUp() {
		return up;
	}

	/// \brief obtenir la position du minimum X du modele
	double getMinX() {
		minX = vertices[0];
		for (unsigned int i = 0; i < verticeArrSize; i += 3) 
			if (vertices[i] < minX) minX = vertices[i];
		return minX;
	}

	/// \brief obtenir la position du maximum X du modele
	double getMaxX() {
		maxX = vertices[0];
		for (unsigned int i = 0; i < verticeArrSize; i += 3)
			if (vertices[i] > maxX) maxX = vertices[i];
		return maxX;
	}

	/// \brief obtenir la position du minimum Y du modele
	double getMinY() {
		minY = vertices[1];
		for (unsigned int i = 0; i < verticeArrSize; i += 3)
			if (vertices[i+1] < minY) minY = vertices[i+1];
		return minY;
	}

	/// \brief obtenir la position du maximum Y du modele
	double getMaxY() {
		maxY = vertices[1];
		for (unsigned int i = 0; i < verticeArrSize; i += 3)
			if (vertices[i + 1] > maxY) maxY = vertices[i + 1];
		return maxY;
	}

	/// \brief obtenir la position du minimum Z du modele
	double getMinZ() {
		minZ = vertices[2];
		for (unsigned int i = 0; i < verticeArrSize; i += 3)
			if (vertices[i + 2] < minZ) minZ = vertices[i + 2];
		return minZ;
	}

	/// \brief obtenir la position du maximum Z du modele
	double getMaxZ() {
		maxZ = vertices[2];
		for (unsigned int i = 0; i < verticeArrSize; i += 3)
			if (vertices[i + 2] > maxZ) maxZ = vertices[i + 2];
		return maxZ;
	}
	/// \brief change le pointeur de l'objet qui est en colision
	/// \param  collidedObject pointeur de l'objet en collision
	void getCollide(Mesh3a* collidedObject){
		collide= collidedObject;
	}

	double getRay() { // retourne la distance entre la position de l'objet et son point le plus loin
		getMaxX();
		getMaxY();
		getMaxZ();
		getMinX();
		getMinY();
		getMinZ();
		if (maxX >= maxY && maxX >= maxZ && maxX >= minX && maxX >= minY && maxX >= minZ) {
			return maxX;
		}
		else if (maxY >= maxX && maxY >= maxZ && maxY >= minY && maxY >= minX && maxY >= minZ) {
			return maxY;
		}
		else if (maxZ >= maxY && maxZ >= maxX && maxZ >= minZ && maxZ >= maxX && maxZ >= minY) {
			return maxZ;
		}
		else if (minX >= maxX && minX >= minZ && minX >= minY && minX >= maxY && minX >= maxZ) {
			return minX;
		}
		else if (minY >= maxY && minY >= minX && minY >= minZ && minY >= maxX && minY >= maxZ) {
			return minY;
		}
		else if (minZ >= maxZ && minZ >= minX && minZ >= minY && minZ >= maxX && minZ >= maxY) {
			return minZ;
		}

	}

	/// \brief Destructeur.
	~Mesh3a() {
		delete[] vertices;
		delete[] normals;
		delete[] coords;
		delete hitbox;
	}

	/// \brief Retourne un rectangles de la mesh corespondant a l'id fournie
	Triangle getTriangles(unsigned int id) {
		Vector3d point1 = Vector3d();
		Vector3d point2 = Vector3d();
		Vector3d point3 = Vector3d();
		Vector3d normal = Vector3d();
		
		point1.x = vertices[id];
		point1.y = vertices[id + 1];
		point1.z = vertices[id + 2];
			
		point2.x = vertices[id + 3];
		point2.y = vertices[id + 4];
		point2.z = vertices[id + 5];
			
		point3.x = vertices[id + 6];
		point3.y = vertices[id + 7];
		point3.z = vertices[id + 8];
			
		normal.x = normals[id];
		normal.y = normals[id + 1];
		normal.z = normals[id + 2];
		
		return Triangle(point1, point2, point3, normal);
	}

	/// \brief Permet de controller la visibilite des hitboxes 
	void toggleHitBoxes() {
		displayHitBoxes = !displayHitBoxes;
	}

	void toggleAxes() {
		displayAxes = !displayAxes;
	}

	void toggleFront() {
		displayFront = !displayFront;
	}


	/// \brief Enregistre la matrix
	void setTransformationMatrix(Matrix44d transformMatrix) {
		this->transformMatrix = transformMatrix;
		hitbox->setTransformationMatrix(transformMatrix);
		for (auto it : hitboxes) {
			it->setTransformationMatrix(transformMatrix);
		}
	}
	/// \brief retourne la matrix
	Matrix44d* getTransformationMatrix() {
		return &this->transformMatrix;
	}

	inline bool getSelected() {
		return selected;
	}

	inline Vector3d getPosition() {
		return position;
	}

	/// \brief change le booléen selected
	/// \param selected permet de choisir si le texture est selectionné
	inline void setSelected(bool selected) {
		this->selected = selected;
	}

	void translate(Vector3d translation = Vector3d(0.0, 0.0, 0.0)) {
		Vector3d newPosition = position + translation;
		setPosition(newPosition.x, newPosition.y, newPosition.z);
		hitbox->translate(translation);
		for (auto it : hitboxes) {
			it->translate(translation);
		}
        hitbox->setPosition(position.x, position.y, position.z);
        for (auto it : hitboxes) {
            it->setPosition(position.x, position.y, position.z);
        }
	}

	/// \brief Modifie la position du modèl
	void setPosition(const double& x, const double& y , const double& z){
		for (unsigned int i = 0; i < verticeArrSize; i += 3) {
			if(vertices[1])
			vertices[i] += (x - position.x);
			vertices[i + 1] += (y- position.y);
			vertices[i + 2] += (z - position.z);
		}

		position.x = x;
		position.y = y;
		position.z = z;

		hitbox->setPosition(x, y, z);
		for (auto it : hitboxes) {
			it->setPosition(x, y, z);
		}
	
	}

	void applyCurrentMatrix(){
		Vector3d vertex(0.0, 0.0, 0.0);
		Vector3d norm(0.0, 0.0, 0.0);
		for (unsigned int i = 0; i < verticeArrSize; i += 3) {
			vertex.x = vertices[i];
			vertex.y = vertices[i + 1];
			vertex.z = vertices[i + 2];
			norm.x = normals[i];
			norm.y = normals[i + 1];
			norm.z = normals[i + 2];

			vertex = this->transformMatrix * vertex;

			norm = this->transformMatrix * norm;

			vertices[i] = vertex.x;
			vertices[i + 1] = vertex.y;
			vertices[i + 2] = vertex.z;
			normals[i] = norm.x;
			normals[i + 1] = norm.y;
			normals[i + 2] = norm.z;
		}

		front = this->transformMatrix * front;
		front.normalize();

		position = this->transformMatrix * position;

		up = this->transformMatrix * up;
		up.normalize();

		hitbox->setTransformationMatrix(transformMatrix);
		for (auto it : hitboxes) {
			it->setTransformationMatrix(transformMatrix);
		}
		hitbox->applyCurrentMatrix();
		for (auto it : hitboxes) {
			it->applyCurrentMatrix();
		}
	}
	
	/// \brief Effectue la rotation autour d'un vecteur 3D fourni avec les trois double. l'axe est en fait la position et non l'axe de rotation
	void transform(double radius = 0.0, Vector3d rotationPosition = (0.0, 0.0, 0.0)) {
		Vector3d vertex(0.0, 0.0, 0.0);
		Vector3d norm(0.0, 0.0, 0.0);
		hitbox->setTransformationMatrix(this->transformMatrix);
		for (auto it : hitboxes) {
			it->setTransformationMatrix(this->transformMatrix);
		}
		hitbox->transform(radius, rotationPosition);
		for (auto it : hitboxes) {
			it->transform(radius, rotationPosition);
		}
		transformMatrix.setTranslation(position.x, position.y, position.z);
		if ((radius <-0.000000005) or (radius > 0.000000005)) {
			double positionNorm = position.getNorm();
			double xFraction = position.x / positionNorm;
			double yFraction = position.y / positionNorm;
			double zFraction = position.z / positionNorm;
			setPosition(xFraction*radius, yFraction*radius, zFraction*radius);
		}
		if ((rotationPosition.x > 0.002) or (rotationPosition.x < -0.002) or (rotationPosition.y > 0.002) or (rotationPosition.y < -0.002) or (rotationPosition.z > 0.002) or (rotationPosition.z < -0.002)) {
			setPosition(rotationPosition.x, rotationPosition.y, rotationPosition.z);
		}
		else{
			setPosition(0.0, 0.0, 0.0);
		}
		for (unsigned int i = 0; i < verticeArrSize; i += 3) {
			
			vertex.x = vertices[i];
			vertex.y = vertices[i + 1];
			vertex.z = vertices[i + 2];
			norm.x = normals[i];
			norm.y = normals[i + 1];
			norm.z = normals[i + 2];

			vertex = this->transformMatrix * vertex;

			norm = this->transformMatrix * norm;
			
			if (transformMatrix.matrix[3] && transformMatrix.matrix[7] && transformMatrix.matrix[11]) {
				vertex.x *= transformMatrix.matrix[3];
				vertex.y *= transformMatrix.matrix[7];
				vertex.z *= transformMatrix.matrix[11];
			}

			vertices[i] = vertex.x;
			vertices[i + 1] = vertex.y;
			vertices[i + 2] = vertex.z;
			normals[i] = norm.x;
			normals[i + 1] = norm.y;
			normals[i + 2] = norm.z;
		}

		front = this->transformMatrix * front;

		if (transformMatrix.matrix[3] && transformMatrix.matrix[7] && transformMatrix.matrix[11]) {
			front.x *= transformMatrix.matrix[3];
			front.y *= transformMatrix.matrix[7];
			front.z *= transformMatrix.matrix[11];
		}

		front.normalize();

		up = this->transformMatrix * up;

		if (transformMatrix.matrix[3] && transformMatrix.matrix[7] && transformMatrix.matrix[11]) {
			up.x *= transformMatrix.matrix[3];
			up.y *= transformMatrix.matrix[7];
			up.z *= transformMatrix.matrix[11];
		}

		up.normalize();

		setPosition(this->transformMatrix.matrix[12], this->transformMatrix.matrix[13], this->transformMatrix.matrix[14]);
		transformMatrix.setScale(0);

	}


	/// \brief Chargement du modèle.
	/// \param filename Nom du fichier contenant le modèle.
	void loadOBJ(const string& fileName) {
		string sBuffer; double dBuffer; unsigned int uiBuffer;
		
		vector<double> vert, textco, norms;
		vector<unsigned int> vertOrder, textcoOrder, normsOrder;
      
		std::ifstream ifs(fileName);
		while (!ifs.eof()) {
			sBuffer.clear(); ifs >> sBuffer;
			switch (sBuffer[0]) {
			case 'v':
				if (sBuffer[1] == 't') {
					ifs >> dBuffer; textco.push_back(dBuffer);
					ifs >> dBuffer; textco.push_back(1.0 - dBuffer);							
				}
				else if (sBuffer[1] == 'n'){
					ifs >> dBuffer; norms.push_back(dBuffer);
					ifs >> dBuffer; norms.push_back(dBuffer);
					ifs >> dBuffer; norms.push_back(dBuffer);
				}
				else {
					ifs >> dBuffer; vert.push_back(dBuffer);
					ifs >> dBuffer; vert.push_back(dBuffer);
					ifs >> dBuffer; vert.push_back(dBuffer);
				}
				break;

			case 'f':
				for (int x = 0; x < 3; x++) {
					ifs >> uiBuffer;  vertOrder.push_back(uiBuffer - 1); ifs.ignore();
					ifs >> uiBuffer;  textcoOrder.push_back(uiBuffer - 1); ifs.ignore();
					ifs >> uiBuffer; normsOrder.push_back(uiBuffer - 1); ifs.ignore();
				}
				break;

			default:
				ifs.ignore(30, 10);
				break;
			}			
		}
		ifs.close();

		verticeArrSize = (unsigned int)vertOrder.size() * 3;
		vertices = new double[verticeArrSize];
		normals = new double[verticeArrSize];
		coords = new double[textcoOrder.size() * 2];

		unsigned int ix2, ix3, vi, ni, ti;
		for (int i = 0; i < vertOrder.size(); i ++) {
			ix2 = i * 2; ix3 = i * 3;

			vi = vertOrder[i] * 3;
			vertices[ix3] = vert[vi];
			vertices[ix3 + 1] = vert[vi + 1];
			vertices[ix3 + 2] = vert[vi + 2];

			ni = normsOrder[i] * 3;
			normals[ix3] = norms[ni];
			normals[ix3 + 1] = norms[ni + 1];
			normals[ix3 + 2] = norms[ni + 2];

			ti = textcoOrder[i] * 2;
			coords[ix2] = textco[ti];
			coords[ix2 + 1] = textco[ti + 1];
		}
	}
	void setFront() {
		FrontVertices[0] = position.x;
		FrontVertices[1] = position.y;
		FrontVertices[2] = position.z;
		FrontVertices[3] = position.x + 3*front.x;
		FrontVertices[4] = position.y + 3*front.y;
		FrontVertices[5] = position.z + 3*front.z;
		FrontVertices[6] = position.x;
		FrontVertices[7] = position.y + 0.5;
		FrontVertices[8] = position.z;
	}

	void setVertices() {//aide visuelle
		axesVertices[0] = position.x;
		axesVertices[1] = position.y;
		axesVertices[2] = position.z;
		axesVertices[3] = position.x;
		axesVertices[4] = position.y;
		axesVertices[5] = position.z + .5;
		axesVertices[6] = position.x + 7;
		axesVertices[7] = position.y;
		axesVertices[8] = position.z;
		axesNormals[0] = 1;
		axesNormals[1] = 0;
		axesNormals[2] = 0;

		axesVertices[9] = position.x;
		axesVertices[10] = position.y;
		axesVertices[11] = position.z;
		axesVertices[12] = position.x + .5;
		axesVertices[13] = position.y;
		axesVertices[14] = position.z;
		axesVertices[15] = position.x;
		axesVertices[16] = position.y + 7;
		axesVertices[17] = position.z;
		axesNormals[3] = 0;
		axesNormals[4] = 1;
		axesNormals[5] = 0;

		axesVertices[18] = position.x;
		axesVertices[19] = position.y;
		axesVertices[20] = position.z;
		axesVertices[21] = position.x;
		axesVertices[22] = position.y + .5;
		axesVertices[23] = position.z;
		axesVertices[24] = position.x;
		axesVertices[25] = position.y;
		axesVertices[26] = position.z + 7;
		axesNormals[6] = 0;
		axesNormals[7] = 0;
		axesNormals[8] = 1;
	}

	/// \brief Affichage.
	virtual void draw() {
		if (visible) {
			(selected) ? glBindTexture(GL_TEXTURE_2D, selectedTextId) : glBindTexture(GL_TEXTURE_2D, textID);

			glEnableClientState(GL_NORMAL_ARRAY);
			glEnableClientState(GL_VERTEX_ARRAY);
			glEnableClientState(GL_TEXTURE_COORD_ARRAY);

			glNormalPointer(GL_DOUBLE, 0, normals);
			glVertexPointer(3, GL_DOUBLE, 0, vertices);
			glTexCoordPointer(2, GL_DOUBLE, 0, coords);

			glDrawArrays(GL_TRIANGLES, 0, (verticeArrSize / 3));

			/*if (displayAxes) {//aide visuelle
				setVertices();
				glVertexPointer(3, GL_DOUBLE, 0, axesNormals);
				glVertexPointer(3, GL_DOUBLE, 0, axesVertices);
				glDrawArrays(GL_TRIANGLES, 0, 9);
			}

			if (displayFront) {//aide visuelle
				setFront();
				glVertexPointer(3, GL_DOUBLE, 0, FrontVertices);
				glDrawArrays(GL_TRIANGLES, 0, 3);
			}*/

			glDisableClientState(GL_NORMAL_ARRAY);
			glDisableClientState(GL_VERTEX_ARRAY);
			glDisableClientState(GL_TEXTURE_COORD_ARRAY);

			if (displayHitBoxes) {//aide visuelle
				//hitbox->draw();
				for (auto it : hitboxes) {
					it->draw();
				}
			}

		}
	}
};

#endif
