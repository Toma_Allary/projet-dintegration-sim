/// \file Spring.h
/// \brief Spring

#ifndef PI_SPRING_H
#define PI_SPRING_H

#include "Controlable.h"
#include "Player.h"

/// \class Spring
/// \brief Spring
///
/// ressort
///
class Spring: public Controlable {
private:
    unsigned int conTextID;///< id des connceteurs non sélectioné
    unsigned int conSelTextID;///< id des connceteurs sélectioné
public:
    Spring(Material* material, const double& x, const double& y, const double& z, const unsigned int& selecId = 0) : Controlable(material, x, y, z,"Objets/spring.obj"){
        type = SPRING;
        name = "Spring";
        price = 300;
        mass = 150;
        maxHealth = 200 * material->getPriceMutltiplicator();
        health = 200 * material->getPriceMutltiplicator();
        energieCost = 1;
        Mesh3a::getTransformationMatrix()->setScale(3);
        Mesh3a::transform();

        textID =SDLGLContext::getInstance()->loadTexture("Textures/spring.png");
        conTextID = SDLGLContext::getInstance()->loadTexture("Textures/snapPoint.png");
        conSelTextID = SDLGLContext::getInstance()->loadTexture("Textures/snapPointSelect.png");

        connectors[0] = (new Mesh3a(conTextID, x, this->getMaxY(), z, "Objets/crate.obj", conSelTextID));
        connectors[0]->getTransformationMatrix()->setScale(0.3);
        connectors[0]->transform();

        connectors[1] = (new Mesh3a(conTextID, x, this->getMinY(), z, "Objets/crate.obj", conSelTextID));
        connectors[1]->getTransformationMatrix()->setScale(0.3);
        connectors[1]->transform();

        usingTime.reset();
    }

    Spring(Spring* spring) : Controlable(spring->getMaterial(), spring->position.x, spring->position.y, spring->position.z, spring->fileName){
        type = SPRING;
        name = "Spring";
        price = 300;
        mass = 150;
        health = 200 * material->getPriceMutltiplicator();
        maxHealth = 200 * material->getPriceMutltiplicator();
        energieCost = 0.1;

        Mesh3a::getTransformationMatrix()->setScale(3);
        Mesh3a::transform();

        textID =SDLGLContext::getInstance()->loadTexture("Textures/spring.png");
        conTextID = SDLGLContext::getInstance()->loadTexture("Textures/snapPoint.png");
        conSelTextID = SDLGLContext::getInstance()->loadTexture("Textures/snapPointSelect.png");

        connectors[0] = (new Mesh3a(conTextID, position.x, this->getMaxY(), position.z, "Objets/crate.obj", conSelTextID));
        connectors[0]->getTransformationMatrix()->setScale(0.3);
        connectors[0]->transform();

        connectors[1] = (new Mesh3a(conTextID, position.x, this->getMinY(), position.z, "Objets/crate.obj", conSelTextID));
        connectors[1]->getTransformationMatrix()->setScale(0.3);
        connectors[1]->transform();
    }

    /// \brief Activer l'effet de la composante
    void activate() {
        isUsedInstantly = true;
    }

    /// \brief Desactiver l'effet de la composante
    void disable(){
        isUsedInstantly = false;
    }

	inline bool isItActivated() {
		return isActivated;
	}

    void levelUp(){
        level++;
		maxHealth *= 1.5;
		health *= 1.5;
	}

    /// \brief Affichage
    void draw(){
        Mesh3a::draw();
        for (int i = 0; i < connectors.size(); i++) connectors[i]->draw();
        usingTime.getElapsedTime();
    }

    void notification(){

    }

    /// \brief Obtenir Description
    /// \return retourne la description
    list<string>* getDescrition(){
        list<string>* description = new list<string>;
        description->push_back("Descrition:");
        description->push_back("outil permettant de sauter");
		description->push_back("Niveau: " + to_string(level));
		description->push_back("Prix: " + to_string((int)(getPrice() + ((level - 1) * getPrice() / 2.0))));
        description->push_back("Masse: " + to_string((int)getMass()));
        description->push_back("Vie: " + to_string((int)getHealth()) + "/" + to_string((int)getMaxHealth()));
        return description;
    }
};

#endif
