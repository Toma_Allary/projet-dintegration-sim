/// \file Font.h
/// \brief Police de caractères

#ifndef FONT_H
#define FONT_H

#include <string>
#include "Resource.h"

using namespace std;

/// \class Font
/// \brief Police de caractères.
///
/// Police de caractères.
///
class Font : public Resource {
private:
	TTF_Font * font; ///< Police de caractères.

public:
	/// \brief Constructeur
	Font(const string& fontName, const unsigned int& size) {
		font = TTF_OpenFont(fontName.c_str(), size);
	}

	/// \brief Destructeur
	~Font() {
		TTF_CloseFont(font);
	}

	/// \brief Obtention de la police de caractères.
	/// \return Police de caractères.
	inline void* getResource() {
		return font;
	}
};

#endif
