/// \file LabelImageButton.h
/// \brief Bouton image étiquette

#ifndef LABELIMAGEBUTTON_H
#define LABELIMAGEBUTTON_H

#include "Label.h"
#include "ImageButton.h"

/// \class LabelImageButton
/// \brief Bouton image étiquette
///
/// Bouton image avec étiquette
///
class LabelImageButton : public ImageButton {
private:
	unsigned int textureId;
	Label* label;

public:
  /// \brief Constructeur
  /// /param textureId Identificateur de texture.
  /// \param x Position sur l'axe des x.
  /// \param y Position sur l'axe des y.
  /// \param h Hauteur.
  /// \param w Largeur.
 	/// \param optionMenu permet de savoir si c'est sur le menu option ou non
  LabelImageButton(const unsigned int& textureId, Font* font, const SDL_Color& fontColor, const string& title, const double& x = 0.0, const double& y = 0.0, const unsigned int& width = 0.0, const unsigned int& height = 0.0, const double& layer = 0.0, const unsigned int overTextureId = 0) : ImageButton(textureId, x, y, width, height, layer, overTextureId) {
    this->textureId = textureId;
    label = new Label(0.0, 0.0, font, title, fontColor, 0.2);
    label->setPosition(x + this->width / 2.0 - label->getWidth() / 2.0, y + this->height / 2.0 - label->getHeight() / 2.0);
  }

	~LabelImageButton() {
		delete this->label;
	}

	inline Label* getLabel(){
		return label;
	}

  /// \brief Affichage
  void draw() {
    if(visible){
      ImageButton::draw();
      label->draw();
    }
  }
    string getType(){
        return "LabelImageButton";
    }
};

#endif