/// \file ImageButton.h
/// \brief Bouton image

#ifndef IMAGEBUTTON_H
#define IMAGEBUTTON_H

#include "Button.h"

/// \class ImageButton
/// \brief Bouton image
///
/// Bouton image
///
class ImageButton : public Button {
private:
  unsigned int actualTextureId, textureId, overTextureId;

public:
    /// \brief Constructeur
    /// /param textureId Identificateur de texture.
    /// \param x Position sur l'axe des x.
    /// \param y Position sur l'axe des y.
    /// \param h Hauteur.
    /// \param w Largeur.
    ImageButton(const unsigned int& textureId, const double& x = 0.0, const double& y = 0.0, const unsigned int& width = 0.0, const unsigned int& height = 0.0, const double& layer = 0.0, const unsigned int& overTextureId = 0) : Button(x, y, width, height, layer) {
      this->actualTextureId = this->textureId = textureId;
      this->overTextureId = (overTextureId) ? overTextureId : this->textureId;
    }

    inline void changeTexture(unsigned int newTextureId){
        textureId = newTextureId;
        actualTextureId = textureId;
    }

    /// \brief Notification d'un événement
    void notification() {
      SDL_Event sdlEvent = SDLEvent::getInstance()->sdlEvent;
      SDL_Rect rect = { (int)position.x, (int)position.y, (int)width, (int)height };

      switch (sdlEvent.type) {
        case SDL_MOUSEBUTTONDOWN: {
          SDL_Point cursor = { sdlEvent.button.x, sdlEvent.button.y };
            if (SDL_PointInRect(&cursor, &rect))
              if (onClick){
                clicked = true;
                onClick();
                clicked = false;
              }
        }
        break;

        case SDL_MOUSEMOTION: {
          SDL_Point cursor = { sdlEvent.button.x, sdlEvent.button.y };
          actualTextureId = (SDL_PointInRect(&cursor, &rect)) ? overTextureId : textureId;
        }
        break;
      }
    }

    /// \brief Affichage
    virtual void draw() {
      if(visible){
        glBindTexture(GL_TEXTURE_2D, actualTextureId);

        glEnableClientState(GL_VERTEX_ARRAY);
        glEnableClientState(GL_TEXTURE_COORD_ARRAY);

        glVertexPointer(3, GL_DOUBLE, 0, vertices);
        glTexCoordPointer(2, GL_DOUBLE, 0, texCoords);
      
        glDrawArrays(GL_QUADS, 0, 4);

        glDisableClientState(GL_TEXTURE_COORD_ARRAY);

        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        glDrawArrays(GL_QUADS, 0, 4);
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

        glDisableClientState(GL_VERTEX_ARRAY);
      }
    }
    string getType(){
        return "ImageButton";
    }

    bool getMouseOn(){
        if(actualTextureId == overTextureId){
            return true;
        }
        else{
            return false;
        }
    }
};

#endif
