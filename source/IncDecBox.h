/// \file IncDecBox.h
/// \brief Classe permettant de cr�� des boites d'incr�mentation et de d�cr�mentation

#ifndef INCDECBOX_H
#define INCDECBOX_H

#include"VisualComponent.h"
#include"Label.h"
#include"LabelImageButton.h"

void increasePrototype();
void decreasePrototype();


/// \class IncDecBox
/// \brief Boîte d'incrémentation et de décrémentation.
///
/// Boîte d'incrémentation et de décrémentation.
///
class IncDecBox : public VisualComponent {
private:
	LabelImageButton* incButton;
	LabelImageButton* decButton;
	LabelImageButton* labelToDraw;
	int count = 0;

public:
	/// \brief Constructeur
	IncDecBox(double x, double y, double width, double height, const unsigned int& textureId, Font* font, const SDL_Color& fontColor) : VisualComponent(x, y, width, height) {
		this->labelToDraw = new LabelImageButton(textureId, font, fontColor, "0", x, y, (2 * (width / 3)), height);
		this->incButton = new LabelImageButton(textureId, font, fontColor, "+", (x + (2 * (width / 3))), y, (width / 3), (height / 2));
		this->decButton = new LabelImageButton(textureId, font, fontColor, "-", (x + (2 * (width / 3))), (y + (height / 2)), (width / 3), (height / 2));
		incButton->bindOnClick(increasePrototype);
	}

	~IncDecBox() {
		delete labelToDraw;
		delete incButton;
		delete decButton;
	}

	void increaseLabel() {
		/*int yes = stoi(labelToDraw->getLabel()->getText());
		yes++;
		string text = to_string(yes);
		labelToDraw->getLabel()->setText(text, {255, 0, 255, 255});*/
		count++;
		string text = to_string(count);
		labelToDraw->getLabel()->setText(text, { 150, 255, 0, 255 });
	}

	void decreaseLabel() {
		/*int yes = stoi(labelToDraw->getLabel()->getText());
		yes--;
		string text = to_string(yes);
		labelToDraw->getLabel()->setText(text, { 0, 0, 255, 255 });*/
		count--;
		string text = to_string(count);
		labelToDraw->getLabel()->setText(text, { 0, 0, 255, 255 });
	}

	/// \brief Affichage
	void draw() {
		labelToDraw->draw();
		incButton->draw();
		decButton->draw();
	}

	/// \brief Notification d'événement
	void notification() {
		SDL_Event sdlEvent = SDLEvent::getInstance()->sdlEvent;
		switch (sdlEvent.type) {
		case SDL_MOUSEBUTTONDOWN:
			SDL_Point cursor = { sdlEvent.button.x, sdlEvent.button.y };
			SDL_Rect rectAdd = { (int)(position.x + (2 * (width / 3))), (int)(position.y), (int)(width / 3), (int)(height / 2) };
			SDL_Rect rectReduce = { (int)(position.x + (2 * (width / 3))), (int)(position.y + (height / 2)), (int)(width / 3), (int)(height / 2) };

			if (SDL_PointInRect(&cursor, &rectAdd))
				//if(&increaseLabel)
					increaseLabel();
			if (SDL_PointInRect(&cursor, &rectReduce))
				//if (&decreaseLabel)
					decreaseLabel();
				
			break;
		}
	}
};

inline void increasePrototype() {
	((IncDecBox*)Drawable::getCurrentDrawable())->increaseLabel();
}

inline void decreasePrototype() {
	((IncDecBox*)Drawable::getCurrentDrawable())->decreaseLabel();
}

string getType(){
	return "IncDecBox";
}

#endif

//drawables["test"] = new IncDecBox(500, 500, 100, 100, buttonBackgroundId, fonts["buttonFont"], { 0, 255, 255, 255 });
