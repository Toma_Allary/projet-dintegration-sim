/// \file FPSCounter.h
/// \brief Compteur de fps

#ifndef FPSCOUNTER_H
#define FPSCOUNTER_H

#include "Singleton.h"
#include "Chrono.h"
#include "Label.h"
#include "Font.h"
#include "Option.h"

/// \class FPSCounter
/// \brief Compteur de FPS
///
/// permet de savoir combien d'images sont affichees � chaque seconde en appelant une fonction
///
class FPSCounter {
private:
	unsigned int fps;
	unsigned int numberOfFrames;///< Nombre d'images, s'incremente a chaque rafraichissement
	Chrono* chrono;///< Chronometre utilis� pour compter les secondes
	unsigned int tempo;///< Variable temporaire permettant de reinitialiser la variable numberOfFrames
	Label* fpsLabel;///< etiquette affichant les fps

public:
	/// \brief Construcuteur
	FPSCounter() {
		chrono = new Chrono();
		numberOfFrames = 0;
		fpsLabel = new Label(1150.0, 640.0, new Font("Fonts/button.ttf", 50), "0", { 0, 255, 0, 255 }, 0.4);
	}

	/// \brief Fonction principale
	unsigned int getFPS() {
		numberOfFrames++;
		if (chrono->getElapsedTime() > 1) {
			chrono->reset();
			tempo = numberOfFrames;
			numberOfFrames = 0;
			return tempo;
		}
		return 0; 
	}

	/// \brief affichage des FPS
	void draw() {
		fps = getFPS();
		if (fps) {
			fpsLabel->setText(to_string(fps), { 0, 255, 0, 255 });
		}
		fpsLabel->changeDimensions((double)(fpsLabel->getText().size()) * 50.0, 50.0);
		fpsLabel->draw();
	}

};

#endif