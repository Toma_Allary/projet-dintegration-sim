/// \file Observer.h
/// \brief Observateur.

#ifndef OBSERVER_H
#define OBSERVER_H

/// \class Observer
/// \brief Observateur.
///
/// Observateur.
///
class Observer {
public:
	/// \brief Notification d'observation.
	virtual void notification() = 0;
};

#endif
