/// \file libSDL2.h
/// \brief Inclusion de la librairie SDL2.

#ifndef LIBSDL2_H
#define LIBSDL2_H

#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "SDL2.lib")
#pragma comment(lib, "SDL2main.lib")
#pragma comment(lib, "SDL2_ttf.lib")
#pragma comment(lib, "SDL2_image.lib")

#pragma comment(linker, "/SUBSYSTEM:windows /entry:mainCRTStartup")

#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#ifdef __APPLE__
  #include <SDL2_image/SDL_image.h>
  #include <SDL2_ttf/SDL_ttf.h>
#else
	#include <SDL2/SDL_ttf.h>
	#include <SDL2/SDL_image.h>
#endif

#include "SDLEvent.h"
#include "SDLGLContext.h"

using namespace std;

/// \brief initialisation des librairies
void initSDL() {
	SDL_Init(SDL_INIT_EVERYTHING);
	IMG_Init(IMG_INIT_PNG);
	TTF_Init();
}

/// \brief fermeture des librairies
void quitSDL() {
	TTF_Quit();
	IMG_Quit();
	SDL_Quit();
}

#endif
