/// \file VisualComponent.h
/// \brief Contrôle visuel.

#ifndef VISUALCOMPONENT_H
#define VISUALCOMPONENT_H

#include"Vector3d.h"
#include "Drawable.h"

/// \class VisualComponent
/// \brief Contrôle visuel.
///
/// Contrôle visuel générique.
///
class VisualComponent : public Drawable {
protected:
	Vector3d position; ///< Position.
	double height; ///< Hauteur.
	double width; ///< Largeur.
	double layer; ///< Profondeur

	unsigned int textureId; ///< Identificateur de texture.
	double vertices[12]; ///< Sommets
	double texCoords[8]; ///< Coordonnées de texture.

public:
	/// \brief Constructeur.
	/// \param x Position sur l'axe des x.
	/// \param y Position sur l'axe des y.
	/// \param height Hauteur.
	/// \param width Largeur.
	VisualComponent(const double& x = 0.0, const double& y = 0.0, const double& width = 0.0, const double& height = 0.0, const double& layer = 0.0){
		this->height = height;
		this->width = width;
		this->layer = layer;

		setPosition(x, y);
		setLayer(layer);

		texCoords[0] = texCoords[1] = texCoords[3] = texCoords[6] = texCoords[7] = 0.0;
		texCoords[2] = texCoords[4] = texCoords[5] = texCoords[7] = 1.0;		
	}

	virtual ~VisualComponent() {}

	/// \brief Obtention de la hauteur.
	/// \return Hauteur.
	inline const double& getHeight(){
		return height;
	}

	/// \brief Obtention de la position.
	/// \return Positon.
	inline Vector3d& getPosition() {
		return position;
	}
	/// \brief Obtention de la largueur.
	/// \return Largueur.
	inline const double& getWidth(){
		return width;
	}

	/// \brief Détermination de la couche d'affichage.
	void setLayer(const double& layer) {
		vertices[2] = vertices[5] = vertices[8] = vertices[11] = layer;
	}

	/// \brief Affectation de la position.
	/// \param x Position sur l'axe des x.
	/// \param y Position sur l'axe des y.
	void setPosition(const double& x, const double& y) {
		position.x = x;
		position.y = y;

		vertices[0] = vertices[9] = x;
		vertices[1] = vertices[4] = y;
		vertices[3] = vertices[6] = x + width;
		vertices[7] = vertices[10] = y + height;
	}

	void changeDimensions(double width, double height){
        this->width=width;
        this->height = height;

        vertices[3] = vertices[6] = position.x + width;
        vertices[7] = vertices[10] = position.y + height;

	}

	/// \brief Affichage.
	virtual void draw() = 0;

	/// \brief Notification d'événement.
	virtual void notification() = 0;
};

#endif