
#ifndef ROBOT
#define ROBOT

#define PI 3.14159265358979324

#include"libSDL2.h"
#include "Catapult.h"
#include"Component.h"
#include "Locomotion.h"
#include"Matrix44d.h"
#include"Mesh3a.h"
#include"Chrono.h"
#include "Ntree.h"
#include<list>
#include<math.h>
#include <cmath>

class Robot {
private:
	double robotLength;///< Longueur du robot
	double robotWidth;///< Largueur du robot
	list<Component*>* robotComponents;
	Ntree<Component*>* componentsDispertion;
	Vector3d CenterOfMassPosition;
	double weight;
	double scale = 0;
	double rotationAngle = 0;
	double robotAngle = 0;
	Matrix44d rotationMatrix;
	Vector3d robotAxe = { 0.0, 1.0, 0.0 }; ///< axe de rotation
	Vector3d robotDirection = { 0.0, 0.0, 1.0 }; ///< angle de rotation du vehicule
	Chrono* time;
	string name;
	bool isDirectionSet = false,
		isGoingFoward = NULL,
		jumpOneTime = true,
		collided = false;
	//Vector3d front = {0.0, 0.0, -1.0};
	Vector3d initialPosition;
	Vector3d* robotFront;
	bool onGround;
    double mu;///< gerer l'adherence
	bool isOnOtherGround;

public:
	Robot(string name) {
		robotLength = 0.0;
		initialPosition.set(0, 0, -25);
		this->name = name;
		this->robotComponents = new list<Component*>();
		this->CenterOfMassPosition = (0.0, 0.0, 0.0);
		time = new Chrono();
		robotFront = nullptr;
		componentsDispertion = new Ntree<Component*>();
		onGround = false;
		time->reset();
        mu = 1.0;
		isOnOtherGround = true;
	}

	~Robot() {
		while (robotComponents->size()) {
			Component* todelete = robotComponents->back();
			robotComponents->pop_back();
			delete todelete;
		}
		delete componentsDispertion;
		delete robotComponents;

	}

    /// \brief change le mu
    /// \param newMu nouveau mu
    void changeMu (double newMu){
        mu = newMu;
		isOnOtherGround = !isOnOtherGround;
    }

	inline bool getIsOnOtherGround (){
		return isOnOtherGround;
	}

	/// \brief Obtenir la longueur du robot
	inline double getLength(){
		return this->robotLength;
	}

	double getLife() {
		double health = 0;
		if (robotComponents->size())
			for (auto it : *robotComponents) {
				health += it->getHealth();
			}
		return health;
	}

	double getMaxLife(){
		double maxHealth = 0;
		if (robotComponents->size())
			for (auto it : *robotComponents) {
				maxHealth += it->getMaxHealth();
			}
		return maxHealth;
	}

	bool getJumpBool() {
		return jumpOneTime;
	}

	void setJumpBool(bool yesNo) {
		jumpOneTime = yesNo;
	}

	/// \brief Definir la longueur du robot
	/// \param lenght Longueur actuelle du robot
	void updateLength(){
		double xPosition = robotComponents->back()->getMaxX(), xPosition2 = robotComponents->back()->getMinX();
		for (auto it : *robotComponents) {
			if (it->getMaxX() > xPosition)
				xPosition = it->getMaxX();
			if (it->getMinX() < xPosition2)
				xPosition2 = it->getMinX();
		}
		robotLength = xPosition - xPosition2;
	}

	/// \brief Obtenir la largueur du robot
	inline double getWidth(){
		return this->robotWidth;
	}

	/// \brief Definir la largueur du robot
	/// \param lenght largueur actuelle du robot
	inline void updateWidth(){
		double zPosition = robotComponents->back()->getMaxZ(), zPosition2 = robotComponents->back()->getMinZ();
		for (auto it : *robotComponents) {
			if (it->getMaxZ() > zPosition)
				zPosition = it->getMaxZ();
			if (it->getMinZ() < zPosition2)
				zPosition2 = it->getMinZ();
		}
		robotWidth = zPosition - zPosition2;
	}

	void setInitialPosition(Vector3d position) {
		initialPosition.set(position.x, position.y, position.z);
	}

	void changeOnGround(){
		onGround= !onGround;
        if(onGround){
            if (robotComponents->size())
                for (auto it : *robotComponents) {
                  it->setSpeed(it->getSpeed()->x,0, it->getSpeed()->z);
                }
	    }
    }

	bool getOnGround(){
        return onGround;
	}

	void disableAllComponent() {
		for (auto it : *robotComponents) {
			if ((it->getType() == SAW) or (it->getType() == CATAPULT) or (it->getType() == FLAMETHROWER))
				((Weapon*)it)->disable();
		}
	}

	void reposition() { // permet de replacer les robot des joueurs à leur emplacement d'origine
		for (list<Component*>::iterator it = getComponentList()->begin(); it != getComponentList()->end(); it++) {
			(*it)->setSpeed(0.0, 0.0, 0.0);

			if ((*it)->getName() == "Structures")
				robotFront = (*it)->getFront();
			if((*it)->getName() == "Roue standard"){
				((Locomotion*)(*it))->turnLocomotionForDirection(0.0);
			}
		}
		Vector3d translation = (*robotComponents->begin())->getPosition() * -1.0;
		translation.z -= 25;
		translateRobot(translation);
	}

	/// \brief Donne l'accest a l'arbre
	Ntree<Component*>* getComponentTree() {
		return componentsDispertion;
	}

	inline void changeDirectionBool() {
		isDirectionSet = !isDirectionSet;
	}

	double getWheelsAdherence() {
		double currentAdherence = 0.0;
		if (robotComponents->size())
			for (auto it : *robotComponents) {
				if ((it->getType() == STANDARDWHEEL) or (it->getType() == NAILEDWHEEL))
					currentAdherence += it->getAdherence();
			}
		return currentAdherence;
	}

	Vector3d getCenterOfMassPosition() {
		CenterOfMassPosition.x = 0.0;
		CenterOfMassPosition.y = 0.0;
		CenterOfMassPosition.z = 0.0;
		double totalMass = 0.0;
		if (robotComponents->size())
			for (auto it : *robotComponents) {
				CenterOfMassPosition = CenterOfMassPosition + ((it->getCenterOfMass()) * it->getMass());
				totalMass += it->getMass();
			}
		CenterOfMassPosition = CenterOfMassPosition / totalMass;
		return CenterOfMassPosition;
	}

	double getRobotMass() {
		double totalMass = 0.0;
		if (robotComponents->size())
			for (auto it : *robotComponents) {
				totalMass += it->getMass();
			}
		return totalMass;
	}

	Vector3d getFriction() {//aluminium-aluminium = 1.4, acier-glace = 0.10, air-metal = 0.01
		Vector3d speed = *getSpeed();
        if(((speed.y < 0.000002 or speed.y > -0.000002) and (speed.x > 0.000002 or speed.x < -0.000002)) or ((speed.y < 0.000002 or speed.y > -0.000002) and (speed.z > 0.000002 or speed.z < -0.000002))) {
            double speedNorm = speed.getNorm();
            double frictionNorm = getRobotMass() * 0.0000098 * mu;
            double xFraction = -speed.x / speedNorm;
            double yFraction = -speed.y / speedNorm;
            double zFraction = -speed.z / speedNorm;
            return Vector3d(xFraction * frictionNorm, yFraction * frictionNorm, zFraction * frictionNorm);
        }
        return {0.0, 0.0, 0.0};
	}

	inline double getCentrifugalForce(double lenght, double width, double wheelAngle) {
		double radiusLenght = (lenght / tan(wheelAngle) + (width / 2));
		double radius = sqrt(width * width + radiusLenght * radiusLenght);
		return getRobotMass() * getSpeed()->getNorm() * getSpeed()->getNorm() / radius;
	}

	double getInertia() {
		double xPosition = 0, yPosition = 0, zPosition = 0, xPosition2 = 0, yPosition2 = 0, zPosition2 = 0;
		weight = 0;
		if (robotComponents->size())
			for (auto it : *robotComponents) {
				weight += it->getMass() * 9.8;
				if (it->getMaxX() > xPosition)
					xPosition = it->getMaxX();
				if (it->getMaxY() > yPosition)
					yPosition = it->getMaxY();
				if (it->getMaxZ() > zPosition)
					zPosition = it->getMaxZ();

				if (it->getMinX() < xPosition2)
					xPosition2 = it->getMinX();
				if (it->getMinY() < yPosition2)
					yPosition2 = it->getMinY();
				if (it->getMinZ() < zPosition2)
					zPosition2 = it->getMinZ();
			}
		scale = xPosition + yPosition + zPosition;
		scale = scale - xPosition2 - yPosition2 - zPosition2;
		return weight + scale;
	}

	/*void setAngle(double angle) {
	   rotationAngle = angle;
	   front.x = cos(rotationAngle);
	   front.z = sin(rotationAngle);
   }*/

	inline double getAngle() {
		return rotationAngle;
	}

	inline Vector3d* getSpeed() {
		return (*robotComponents->begin())->getSpeed();
	}

	/*inline void setRotationAngle(bool leftRight) {
		(leftRight) ? rotationMatrix.loadRotation(-rotationAngle * 10000, robotAxe) : rotationMatrix.loadRotation(rotationAngle * 10000, robotAxe);
	}*/

	Component* getEnergyBlock(){
		for (auto it : *robotComponents)
			if (it->getType() == ENERGYBLOCK)
				return it;
		return nullptr;
	}

	void resetEnergyBlockHealth() {
		for (auto it : *robotComponents)
			if(it->getType() == ENERGYBLOCK)
				it->resetHealth();
	}

	void direction(bool leftRight, double lenght, double width, double wheelAngle, double deltaT) {
		//TODO
		//si aucune acc�l�ration/mouvement en meme temps, seulement les roues sont rotationn�es 
		//Toutes les composantes ont le meme centre de rotation;
		//Change selon les touches enfoncees
		double radiusLenght = (lenght / tan(wheelAngle) + (width / 2));
		double radius;
		if (leftRight)
			radius = -sqrt((width / 2) * (width/ 2) + radiusLenght * radiusLenght);
		else
			radius = sqrt((width / 2) * (width / 2) + radiusLenght * radiusLenght);

		Vector3d* currentSpeed = (*robotComponents->begin())->getSpeed();
		
		rotationAngle = wheelAngle * (1/deltaT) * currentSpeed->getNorm() * 0.005;

		if (leftRight) { //Virage a droite
			if (isGoingFoward)
				rotationMatrix.loadRotation(-rotationAngle, robotAxe);
			else{
				rotationMatrix.loadRotation(rotationAngle, robotAxe);
			}
		}
		else {
			if (isGoingFoward)
				rotationMatrix.loadRotation(rotationAngle, robotAxe);
			else{
				rotationMatrix.loadRotation(-rotationAngle, robotAxe);
			}
		}
		Vector3d upAxe = (*robotComponents->begin())->getUp();
		Vector3d radiusVector = upAxe % *robotFront;
		radiusVector.normalize();
		radiusVector = radiusVector * radius;
		Vector3d translation = (*robotComponents->begin())->getPosition() - radiusVector;
		translateRobot(translation * -1.0);

		if (robotComponents->size()) {
			if ((*robotComponents->begin())->getSpeed()->getNorm() > 0.00001) {//Si la speed du vehicule est plus grande que 0
				for (auto it : *robotComponents) {
					it->setTransformationMatrix(rotationMatrix);
					it->applyCurrentMatrix();

					if (it->getName() == "Structures")
						robotFront = it->getFront();
				}
			}

			if (leftRight)
				wheelAngle *= -1.0;
			for (auto it : *robotComponents) {//Juste tourner les roues sans mouvements
				if (it->getType() == STANDARDWHEEL or it->getType() == NAILEDWHEEL) {
					if(((Locomotion*)it)->getIsFrontLocomotion()){
						((Locomotion*)it)->turnLocomotionForDirection(wheelAngle);
					}
				}
			}
		}
		translateRobot(translation * 1.0);
	}

	void translateRobot(Vector3d translation = Vector3d(0.0, 0.0, 0.0)) {
		for (auto it : *robotComponents) {
			it->translate(translation);
		}
	}

	void setRobotSpeed(Vector3d speed) {
		for (auto it : *robotComponents) {
			it->setSpeed(speed.x, speed.y, speed.z);
		}
	}

	void applyGravityOnRobot(double deltaT, double adherence = 0.0) {
		for (auto it : *robotComponents) {
			it->setWindAndGravity();
			it->setSpeed(it->applyGravityAndWind(it->getSpeed(), deltaT, adherence)->x, it->applyGravityAndWind(it->getSpeed(), deltaT, 0.0)->y, it->applyGravityAndWind(it->getSpeed(), deltaT, adherence)->z);
			if(it->getPosition().y > 0)
				it->setPosition(it->getPosition().x , it->getPosition().y + it->getSpeed()->y, it->getPosition().z);
			//else
				//it->setPosition(it->getPosition().x + it->getSpeed()->x, it->getPosition().y + it->getSpeed()->y, it->getPosition().z + it->getSpeed()->z);
		}
	}

	void resetY() {
		for (auto it : *robotComponents) {
			it->setPosition(it->getPosition().x + it->getSpeed()->x, 0.0, it->getPosition().z + it->getSpeed()->z);
			it->setSpeed(it->getSpeed()->x, 0.0, it->getSpeed()->z);
		}
	}
	void setCollided(bool newCollided) {
		collided = newCollided;
	}
	string getCollided() {
		if (collided) {
			return " true";
		}
		return " false";
	}

	void free(double deltaT) {
		Vector3d* speed;
		Vector3d friction = getFriction()*deltaT;
        if (robotComponents->size())
			for (auto it : *robotComponents) {
				speed = it->getSpeed();
				if ((speed->x > 0.00035) or (speed->z > 0.00035) or (speed->x < -0.00035) or (speed->z < -0.00035) or (speed->y < -0.00035) or (speed->y > 0.00035)) {
					if (!collided) {
						if (isGoingFoward)
							it->setSpeed((sqrt(speed->x * speed->x + speed->z * speed->z)) * robotFront->x, speed->y, (sqrt(speed->x * speed->x + speed->z * speed->z)) * robotFront->z);
						else	
							it->setSpeed((sqrt(speed->x * speed->x + speed->z * speed->z)) * -(robotFront->x), speed->y, (sqrt(speed->x * speed->x + speed->z * speed->z)) * -(robotFront->z));
					}
					if ((speed->x > 0.00035) or (speed->z > 0.00035) or (speed->x < -0.00035) or (speed->z < -0.00035))
							it->setSpeed(speed->x - 1000 * deltaT * speed->x * 11 / getInertia() + friction.x, speed->y, speed->z - 1000 * deltaT * speed->z * 11 / getInertia() + friction.z);//remplacer les "11" par la valeur de ladh�rance (nbr de roues * adh�rence respective / inertie);
					else {
						it->setSpeed(0, speed->y, 0);
						collided = false;
					}
				}
				else {
					it->setSpeed(0, 0, 0);
					collided = false;
				}
				it->setPosition(it->getPosition().x + it->getSpeed()->x, it->getPosition().y, it->getPosition().z + it->getSpeed()->z);
			}

	}

	void acceleration(double deltaT, double amount = 1.0, double mapAngle = 0.0) {
		//TODO 
		//Faire acc�l�rer toutes les composantes en fonction des moteurs et des roues (quantit� + efficacit�)
		//double costeta = SDL_acos(robotAxe.y / robotAxe.getNorm());
		//double sinteta = SDL_asin(robotAxe.x / robotAxe.getNorm());
		//double costeta = cos(robotAngle);//*PI / 180.0);
		//double sinteta = sin(robotAngle);//*PI / 180.0);
		collided = false;
		Vector3d* speed;
        Vector3d friction = getFriction()*deltaT;
		if (robotComponents->size())
			for (auto it : *robotComponents) {
				speed = it->getSpeed();

				if (speed->x * robotFront->x > 0.0000000001)
					isGoingFoward = true;
				else if (speed->z * robotFront->z > 0.0000000001)
					isGoingFoward = true;
				else
					isGoingFoward = false;
				if (isGoingFoward)
					it->setSpeed((sqrt(speed->x * speed->x + speed->z * speed->z)) * robotFront->x, speed->y, (sqrt(speed->x * speed->x + speed->z * speed->z)) * robotFront->z);
				else {
					it->setSpeed((sqrt(speed->x * speed->x + speed->z * speed->z)) * -(robotFront->x), speed->y, (sqrt(speed->x * speed->x + speed->z * speed->z)) * -(robotFront->z));
					it->setSpeed(speed->x + 600 * amount * deltaT * (robotFront->x) * 11 / getInertia() + friction.x, speed->y, speed->z + 600 * amount * deltaT * (robotFront->z) * 11 / getInertia() + friction.z);//remplacer les "11" par la valeur de ladh�rance (nbr de roues * adh�rence respective / inertie);
				}
					if ((sqrt(speed->x * speed->x + speed->z * speed->z)) < 0.55) {
						it->setSpeed(speed->x + 100 * amount * deltaT * robotFront->x * 11 / getInertia() + friction.x, speed->y, speed->z + 100 * amount * deltaT * robotFront->z * 11 / getInertia() + friction.z);//remplacer les "11" par la valeur de ladh�rance (nbr de roues * adh�rence respective / inertie);
				    }

				it->setPosition(it->getPosition().x + it->getSpeed()->x, it->getPosition().y, it->getPosition().z + it->getSpeed()->z);
					/*if(it->getType() == STANDARDWHEEL or it->getType() == NAILEDWHEEL)
					    if (isGoingFoward) {
                            rotationMatrix.loadRotation(1 * deltaT * (*robotComponents->begin())->getSpeed()->getNorm() * 4.5, robotAxe);
                            it->setTransformationMatrix(rotationMatrix);
                            it->applyCurrentMatrix();
                        }*/
			}

	}

	void deceleration(double deltaT, double amount = 1.0) {
		//TODO 
		//Faire ralentir toutes les composantes en fonction des moteurs et des roues (quantit� + efficacit�)
		//S'effectue lorsqu'aucune touche de mouvement n'est enfonc�e
		//double costeta = cos(robotAngle);
		//double sinteta = sin(robotAngle);
		collided = false;
		Vector3d* speed;
		Vector3d friction = getFriction() * deltaT;
		if (robotComponents->size())
			for (auto it : *robotComponents) {
				speed = it->getSpeed();

				Vector2d speedNorm(speed->x, speed->z);
				speedNorm.normalize();
				isGoingFoward = ((speedNorm.x - robotFront->x) < 0.01 and (speedNorm.x - robotFront->x) > -0.01 and (speedNorm.y - robotFront->z) < 0.01 and (speedNorm.y - robotFront->z) > -0.01);

				if (isGoingFoward) {
					it->setSpeed((sqrt(speed->x * speed->x + speed->z * speed->z)) * robotFront->x, speed->y, (sqrt(speed->x * speed->x + speed->z * speed->z)) * robotFront->z);
					it->setSpeed(speed->x + 600 * amount * deltaT * -(robotFront->x) * 11 / getInertia() + friction.x, speed->y, speed->z + 600 * amount * deltaT * -(robotFront->z) * 11 / getInertia() + friction.z);//remplacer les "11" par la valeur de ladh�rance (nbr de roues * adh�rence respective / inertie);
				}
				else
					it->setSpeed((sqrt(speed->x * speed->x + speed->z * speed->z)) * -(robotFront->x), speed->y, (sqrt(speed->x * speed->x + speed->z * speed->z)) * -(robotFront->z));
				if ((sqrt(speed->x * speed->x + speed->z * speed->z)) < 0.4) {
					it->setSpeed(speed->x + 70 * amount * deltaT * -(robotFront->x) * 11 / getInertia() + friction.x, speed->y, speed->z + 70 * amount * deltaT * -(robotFront->z) * 11 / getInertia() + friction.z);//remplacer les "11" par la valeur de ladh�rance (nbr de roues * adh�rence respective / inertie);
				}

				it->setPosition(it->getPosition().x + it->getSpeed()->x, it->getPosition().y, it->getPosition().z + it->getSpeed()->z);
			}
	}

 	void jump(double deltaT) {
		if (robotComponents->size())
			for (auto it : *robotComponents) {
			    double upSpeed = 400 * deltaT;
			    if((upSpeed + it->getSpeed()->y) < 0 )
                    upSpeed = abs(getSpeed()->y) + 0.4;
				it->setSpeed(it->getSpeed()->x, it->getSpeed()->y + upSpeed, it->getSpeed()->z);
				it->setPosition(it->getPosition().x + it->getSpeed()->x, it->getPosition().y + it->getSpeed()->y, it->getPosition().z + it->getSpeed()->z);
			}
	}

	void fly(double deltaT) {
		for (auto it : *robotComponents) {
			it->setSpeed(it->getSpeed()->x, it->getSpeed()->y + 0.02, it->getSpeed()->z);
			it->setPosition(it->getPosition().x + it->getSpeed()->x, it->getPosition().y + it->getSpeed()->y, it->getPosition().z + it->getSpeed()->z);
			if(onGround)
			    Robot::changeOnGround();
		}
	}

	void applyGravityOnProjectile(double deltaT) {
		if (robotComponents->size())
			for (auto it : *robotComponents) {
				if (it->getType() == CATAPULT)
					NULL;
					//it->
			}
	}

	void addComponent(Component* newComponent) {
		robotComponents->push_back(newComponent);
		if (newComponent->getName() == "Structures" and robotFront == nullptr) {
			robotFront = newComponent->getFront();
		}
	}

	inline void deleteComponent(Component* toDeleteComponent) {
		robotComponents->remove(toDeleteComponent);
	}

	inline list<Component*>* getComponentList() {
		return robotComponents;
	}

	void draw() {
		time->reset();
		for (auto it : *robotComponents) {
				it->draw();
				if(it->getType()==SPRING){
				    if(((Controlable*)it)->isItUsedsInstantly()){
				        double a= time->getElapsedTime();
                        if(onGround)
				            Robot:: jump(time->getElapsedTime());
				    }
				}
				if(it->getType() == PROPELLER){
					if(((Controlable*)it)->isItUsedsInstantly()){
						double a= time->getElapsedTime();
							Robot:: fly(time->getElapsedTime());
					}
				}

		}

	}

};
#endif