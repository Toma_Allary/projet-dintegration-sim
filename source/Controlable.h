/// \file Controlable.h
/// \brief Controlable
#ifndef CONTROLABLE_H
#define CONTROLABLE_H

#include "Observer.h"
#include "Component.h"

/// \class Controlable
/// \brief Controlable
///
/// Composantes qui sont controlable
///
class Controlable : public Component, public Observer{
private:

protected:
	bool overTimeUse;
	bool isActivated;
	bool isUsedInstantly;
  Chrono usingTime;///< Temps d'utilisation
public:
    Controlable(Material* material, const double& x, const double& y, const double& z, string fileName, const unsigned int& selecId = 0) : Component(material, 0, x, y ,z, fileName){
		overTimeUse = false;
		isUsedInstantly = false;
		isActivated = false;
    }
    virtual ~Controlable(){

    }

	inline bool isItUsedsInstantly() {
		return isUsedInstantly;
	}

	virtual bool isItActivated() = 0;

  virtual void levelUp() = 0;

  /// \brief Activer l'effet de la composante
  virtual void activate() = 0;
  /// \brief Desactiver l'effet de la composante
  virtual void disable() = 0;
  /// \brief Affichage
  virtual void draw() = 0;
  /// \brief Notification
  virtual void notification() = 0;

};

#endif