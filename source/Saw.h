/// \file Saw.h
/// \brief Scie
#ifndef SAW_H
#define SAW_H

#include "Weapon.h"

/// \class Saw
/// \brief Scie
///
/// Scie
///
class Saw: public Weapon {
private:
	unsigned int conTextID;
    unsigned int conSelTextID;
public:
    Saw(Material* material, const double& x, const double& y, const double& z, const unsigned int& selecId = 0) : Weapon(material, x, y , z, "Objets/saw.obj"){
        textID = SDLGLContext::getInstance()->loadTexture("Textures/saw.png");
		conTextID = SDLGLContext::getInstance()->loadTexture("Textures/snapPoint.png");
        conSelTextID = SDLGLContext::getInstance()->loadTexture("Textures/snapPointSelect.png");
        type = SAW;
        name = "Scie";
		price = 350;
		mass = 200;
		health = 200 * material->getPriceMutltiplicator();
        maxHealth = 200 * material->getPriceMutltiplicator();
        energieCost = 1;
        damage = 1;

		connectors[0] = (new Mesh3a(conTextID, this->getMinX(), y, this->getMinZ(), "Objets/crate.obj", conSelTextID));
		connectors[0]->getTransformationMatrix()->setScale(0.3);
		connectors[0]->transform();
		hitboxes.push_back(new HitBox(this->position, getMaxX(), this->position.x, getMaxY(), getMinY(), getMaxZ(), this->position.z));
		hitboxes.push_back(new HitBox(this->position, this->position.x, getMinX(), getMaxY(), getMinY(), this->position.z, getMinZ()));
		hitboxes.push_back(new HitBox(this->position, this->position.x, getMinX(), getMaxY(), getMinY(), getMaxZ(), this->position.z));
		hitboxes.push_back(new HitBox(this->position, getMaxX(), this->position.x, getMaxY(), getMinY(), this->position.z, getMinZ()));
	}
    Saw(Saw* saw) : Weapon(saw->getMaterial(), saw->position.x, saw->position.y , saw->position.z, saw->fileName){
        textID = SDLGLContext::getInstance()->loadTexture("Textures/saw.png");
		conTextID = SDLGLContext::getInstance()->loadTexture("Textures/snapPoint.png");
        conSelTextID = SDLGLContext::getInstance()->loadTexture("Textures/snapPointSelect.png");
        type = SAW;
        name = "Scie";
		price = 350;
		mass = 200;
		health = 200 * material->getPriceMutltiplicator();
        maxHealth = 200 * material->getPriceMutltiplicator();
        energieCost = 1;
        damage = 1;

		connectors[0] = (new Mesh3a(conTextID, this->getMinX(), saw->position.y, this->getMinZ(), "Objets/crate.obj", conSelTextID));
		connectors[0]->getTransformationMatrix()->setScale(0.3);
		connectors[0]->transform();
		hitboxes.push_back(new HitBox(this->position, getMaxX() + 0.25, this->position.x, getMaxY() + 0.25, getMinY() - 0.25, getMaxZ() + 0.25, this->position.z));
		hitboxes.push_back(new HitBox(this->position, this->position.x, getMinX() - 0.25, getMaxY() + 0.25, getMinY() - 0.25, this->position.z, getMinZ() -0.25));
		hitboxes.push_back(new HitBox(this->position, this->position.x, getMinX() - 0.25, getMaxY() + 0.25, getMinY() - 0.25, getMaxZ() + 0.25, this->position.z));
		hitboxes.push_back(new HitBox(this->position, getMaxX() + 0.25, this->position.x, getMaxY() + 0.25, getMinY() - 0.25, this->position.z, getMinZ() - 0.25));
	}
    ~Saw(){

    }

	inline bool isItActivated() {
		return isActivated;
	}

	/// \brief Activer l'effet de la composante
    virtual void activate(){
		overTimeUse = true;
		isActivated = true;
	}
	/// \brief Desactiver l'effet de la composante
    virtual void disable(){
		overTimeUse = false;
		isActivated = false;
	}

	void levelUp(){
		level++;
        damage *= 1.5;
		maxHealth *= 1.5;
		health *= 1.5;
	}

    /// \brief Affichage
    void draw(){
		if(overTimeUse){
			Vector3d keepFront = front;
			double deltaTime = usingTime.getElapsedTime();
			if (deltaTime > 0.01) {
				usingTime.reset();
				rotationMatrix.loadRotation(deltaTime * 10, up);
				this->setTransformationMatrix(rotationMatrix);
				this->transform();
				front = keepFront;
			}
		}
        Mesh3a::draw();
		for (int i = 0; i < connectors.size(); i++) connectors[i]->draw();
    }

    /// \brief Notification
    void notification(){
        
    }

};

#endif