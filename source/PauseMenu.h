/// \file PauseMenu.h
/// \brief Menu de pause
#ifndef PAUSEMENU_H
#define PAUSEMENU_H

#include <list>
#include "Scene3d.h"
#include "LabelImageButton.h"
#include "Label.h"
#include "Image.h"
#include "ImageButton.h"

void quitMatch();
void returnToGameButton();
void settings();

/// \class PauseMenu
/// \brief Menu de pause
/// 
/// Menu de pause
///
using namespace std;
class PauseMenu : public Scene3d {
private:
	unsigned int buttonBackgroundId;
	unsigned int arrBackId;
	
public:
	/// \brief Construcuteur
	PauseMenu(){

		buttonBackgroundId = SDLGLContext::getInstance()->loadTexture("Textures/buttonBackground.png");
		arrBackId = SDLGLContext::getInstance()->loadTexture("Textures/StatsTab.png");

		drawables["zlines"] = new Image(arrBackId, 100, 150, 1000, 300);

		drawables["returnToGameButton"] = new LabelImageButton(buttonBackgroundId, ResourceManager::get<Font*>("font30"), { 255, 255, 255, 255 }, "Retour vers la partie", 200, 50, 800, 75);
		drawables["quitMatchButton"] = new LabelImageButton(buttonBackgroundId, ResourceManager::get<Font*>("font15"), { 255, 255, 255, 255 }, "Quitter la partie", 300, 500, 300, 75);
		drawables["settingsButton"] = new ImageButton(SDLGLContext::getInstance()->loadTexture("Textures/MenuPrincipal_Parametres.png"), 1000, 600, 75, 75);


		((Button*)drawables["returnToGameButton"])->bindOnClick(returnToGameButton);
		((Button*)drawables["quitMatchButton"])->bindOnClick(quitMatch);
		((Button*)drawables["settingsButton"])->bindOnClick(settings);

		drawables["damageI1L"] = new Label(300, 250, ResourceManager::get<Font*>("font30"), to_string((int)player1->getDamageDealt()), { 255, 255, 255, 255 });
		drawables["damageR1L"] = new Label(511, 250, ResourceManager::get<Font*>("font30"), to_string((int)(player1->getHealthBeforeGame() - player1->getRobot()->getLife())), { 255, 255, 255, 255 });
		drawables["scraps1L"] = new Label(723, 250, ResourceManager::get<Font*>("font30"), to_string((int)player1->pickedUpScrapValue), { 255, 255, 255, 255 });
		drawables["victories1L"] = new Label(988, 250, ResourceManager::get<Font*>("font30"), to_string(player1->getVictories()), { 255, 255, 255, 255 });
		drawables["damageI2L"] = new Label(300, 370, ResourceManager::get<Font*>("font30"), to_string((int)player2->getDamageDealt()), { 255, 255, 255, 255 });
		drawables["damageR2L"] = new Label(511, 370, ResourceManager::get<Font*>("font30"), to_string((int)(player2->getHealthBeforeGame() - player2->getRobot()->getLife())), { 255, 255, 255, 255 });
		drawables["scraps2L"] = new Label(723, 370, ResourceManager::get<Font*>("font30"), to_string((int)player2->pickedUpScrapValue), { 255, 255, 255, 255 });
		drawables["victories2L"] = new Label(988, 370, ResourceManager::get<Font*>("font30"), to_string(player2->getVictories()), { 255, 255, 255, 255 });


		currentDrawable = this;

		events[SDL_MOUSEBUTTONDOWN] = new Observable();
		events[SDL_MOUSEBUTTONDOWN]->subscribe(drawables["returnToGameButton"]);
		events[SDL_MOUSEBUTTONDOWN]->subscribe(drawables["quitMatchButton"]);
		events[SDL_MOUSEBUTTONDOWN]->subscribe(drawables["settingsButton"]);
	}

	~PauseMenu() {		
		for (auto it : events) delete it.second;
		for (auto it : drawables) delete it.second;

		glDeleteTextures(1, &buttonBackgroundId);
		glDeleteTextures(1, &arrBackId);
	}

	/// \brief Affichage
	void draw() {
		SDLGLContext::getInstance()->setPerspective(true);
		//((DoubleLabel*)drawables["damageR1L"])->setText(to_string((int)(staticHealthP1 - healthP1)), { 255,225,10,255 });
		//((DoubleLabel*)drawables["damageR2L"])->setText(to_string((int)(staticHealthP2 - healthP2)), { 255,225,10,255 });

		for (auto it : drawables)
			it.second->draw();
		Option::getInstance()->showFPS();
	}

	string getType(){
		return "PauseMenu";
	}

	/// \brief Notifications
	virtual void notification() {
		switch (SDLEvent::getInstance()->sdlEvent.type) {
		case SDL_MOUSEBUTTONDOWN:
			events[SDL_MOUSEBUTTONDOWN]->notify();
			break;
		}
	}
};
/// \brief fonction pour le bouton pour quitter le match
inline void quitMatch() {
	//Test de l'ecran de fin de partie
	Scene::setScene(ENDOFGAME);
	//Scene::setScene(MAINMENU);
}
/// \brief fonction pour le bouton de retour � la partie
inline void returnToGameButton() {
	Scene::setScene(INGAME);
}
/// \brief fonction pour le bouton des options
inline void settings() {
	Scene::setScene(OPTIONMENU);
}
#endif // !PAUSEMENU_H
