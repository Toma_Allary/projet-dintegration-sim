#ifndef SCENE3D_H
#define SCENE3D_H

#include "Camera3V.h"
#include "Player.h"
#include "Scene.h"
#include "Environment.h"
#include "Mesh3a.h"

class Scene3d : public Scene{
protected:
	Camera3v* camera;
	static Player* player1;
	static Player* player2;
	double healthP1, staticHealthP1;
	double healthP2, staticHealthP2;
	Environment* arena;
public:
	virtual ~Scene3d() {}

	/// \brief retourne le segment r�duit en fonction de la collision
	/// \param movingObject Le component qui se d�place
	/// \param targets Une liste des components pouvent entrer en collision avec notre movinobject
	/// \param segment Le vecteur d�placement
	static Vector3d totalCollisionSegment(Mesh3a* movingObject, list<Mesh3a*> allMeshes, Vector3d segment) {
		list<Mesh3a*> targets;
		double rayMovingObject = movingObject->getRay();
		Vector3d objectPosition = movingObject->getPosition();
		double incertitude = 0.01;
		bool boundingX = false, boundingY = false, boundingZ = false; //Booleen utilise pour s'assurer de pusher un mesh une seule fois dans la liste de target
		for (auto it : allMeshes) {//Verifie et ajout a la liste de target les mesh qui sont dans la direction du deplacement de l'objet
			if ((it) != movingObject) {
											
					if (segment.x > 0.0) {
						if ((it->getMinX() <= (movingObject->getMaxX() + segment.x) && it->getMaxX() >= movingObject->getMinX()) || (it->getMinX() >= movingObject->getMinX() && it->getMaxX() <= movingObject->getMaxX())) {
								boundingX = true;
						}
						
					}
					else if (it->getMaxX() >= (movingObject->getMinX() + segment.x) && it->getMinX() <= movingObject->getMaxX() || (it->getMinX() >= movingObject->getMinX() && it->getMaxX() <= movingObject->getMaxX())) {
							boundingX = true;
						
					}
				
					if (segment.y > 0.0) {
						if (it->getMinY() <= (movingObject->getMaxY() + segment.y) && it->getMaxY() >= movingObject->getMinY() || (it->getMinY() >= movingObject->getMinY() && it->getMaxY() <= movingObject->getMaxY())) {
							boundingY = true;
						}

					}
					else if (it->getMaxY() >= (movingObject->getMinY() + segment.y) && it->getMinY() <= movingObject->getMaxY() || (it->getMinY() >= movingObject->getMinY() && it->getMaxY() <= movingObject->getMaxY())) {
						boundingY = true;

					}
				
					if (segment.z > 0.0) {
						if (it->getMinZ() <= (movingObject->getMaxZ() + segment.z) && it->getMaxZ() >= movingObject->getMinZ() || (it->getMinZ() >= movingObject->getMinZ() && it->getMaxZ() <= movingObject->getMaxZ())) {
							boundingZ = true;
						}

					}
					else if (it->getMaxZ() >= (movingObject->getMinZ() + segment.z) && it->getMinZ() <= movingObject->getMaxZ() || (it->getMinZ() >= movingObject->getMinZ() && it->getMaxZ() <= movingObject->getMaxZ())) {
						boundingZ = true;

					}

					if (boundingX and boundingY and boundingZ) {
						targets.push_back(it);
					}

					boundingX = false;
					boundingY = false;
					boundingZ = false;
			}
			
		}

		
		
			Vector3d segmentTemp = segment;
			Vector3d verticeIt;
			double mulitplicator;
			for (auto it : targets) {//promenne dans la liste d'objet pouvant collisioner avec l'objet en deplacement
			
				for (auto movingIt : movingObject->getHitboxes()) { // se promene dans la liste des hitboxs du moving object

				
					for (int i = 0; i < 8; i++) {
					
						Vector3d vertice(movingIt->getVertice(0 + (i * 3)), movingIt->getVertice(1 + (i * 3)), movingIt->getVertice(2 + (i * 3)));
					

						for (auto itHitbox : it->getHitboxes()) {

							for (int j = 0; j < 12; j++) {//dans les face de la hitbox
								mulitplicator = PhysicalEntity::doesItCollide(vertice, segment, itHitbox->getTriangles(j));

								if (mulitplicator != 1.0)
									if(abs((segment * mulitplicator).getNorm()) < abs(segmentTemp.getNorm()))
										segmentTemp = segment * mulitplicator;
							}

							if ((segmentTemp.x == segment.x) && (segmentTemp.y == segment.y) && (segmentTemp.z == segment.z)) { // si il ny a pas eu de collision, v�rifie la collision inverse
								for (int k = 0; k < 8; k++) {
									verticeIt.set(itHitbox->getVertice(0 + (k * 3)), itHitbox->getVertice(1 + (k * 3)), itHitbox->getVertice(2 + (k * 3)));
									for (int m = 0; m < 12; m++) {
										mulitplicator = PhysicalEntity::doesItCollide(verticeIt, (segment * -1), movingIt->getTriangles(m));

										if (mulitplicator != 1.0)
											if (abs((segment * mulitplicator).getNorm()) < abs(segmentTemp.getNorm()))
												segmentTemp = segment * mulitplicator;
									}
								}
							}
							segment = segmentTemp;


						}


					}
				}
			}
		
	
		return segment;
	}

	static Mesh3a* ultimateDoesItCollide(Mesh3a* movingObject, list<Mesh3a*> allMeshes, Vector3d segment) {
		list<Mesh3a*> targets;
		double rayMovingObject = movingObject->getRay();
		Vector3d objectPosition = movingObject->getPosition();
		double incertitude = 0.01;
		bool boundingX = false, boundingY = false, boundingZ = false; //Booleen utilise pour s'assurer de pusher un mesh une seule fois dans la liste de target
		for (auto it : allMeshes) {//Verifie et ajout a la liste de target les mesh qui sont dans la direction du deplacement de l'objet
			if ((it) != movingObject) {

				if (segment.x > 0.0) {
					if ((it->getMinX() <= (movingObject->getMaxX() + segment.x) && it->getMaxX() >= movingObject->getMinX()) || (it->getMinX() >= movingObject->getMinX() && it->getMaxX() <= movingObject->getMaxX())) {
						boundingX = true;
					}

				}
				else if (it->getMaxX() >= (movingObject->getMinX() + segment.x) && it->getMinX() <= movingObject->getMaxX() || (it->getMinX() >= movingObject->getMinX() && it->getMaxX() <= movingObject->getMaxX())) {
					boundingX = true;

				}

				if (segment.y > 0.0) {
					if (it->getMinY() <= (movingObject->getMaxY() + segment.y) && it->getMaxY() >= movingObject->getMinY() || (it->getMinY() >= movingObject->getMinY() && it->getMaxY() <= movingObject->getMaxY())) {
						boundingY = true;
					}

				}
				else if (it->getMaxY() >= (movingObject->getMinY() + segment.y) && it->getMinY() <= movingObject->getMaxY() || (it->getMinY() >= movingObject->getMinY() && it->getMaxY() <= movingObject->getMaxY())) {
					boundingY = true;
				}

				if (segment.z > 0.0) {
					if (it->getMinZ() <= (movingObject->getMaxZ() + segment.z) && it->getMaxZ() >= movingObject->getMinZ() || (it->getMinZ() >= movingObject->getMinZ() && it->getMaxZ() <= movingObject->getMaxZ())) {
						boundingZ = true;
					}

				}
				else if (it->getMaxZ() >= (movingObject->getMinZ() + segment.z) && it->getMinZ() <= movingObject->getMaxZ() || (it->getMinZ() >= movingObject->getMinZ() && it->getMaxZ() <= movingObject->getMaxZ())) {
					boundingZ = true;
				}

				if (boundingX and boundingY and boundingZ) {
					targets.push_back(it);
				}

				boundingX = false;
				boundingY = false;
				boundingZ = false;
			}

		}
		//Vector3d segmentTemp = segment;
		Vector3d verticeIt;
		//double mulitplicator;
		for (auto it : targets) {//promenne dans la liste d'objet pouvant collisioner avec l'objet en deplacement

			for (auto movingIt : movingObject->getHitboxes()) { // se promene dans la liste des hitboxs du moving object

				for (int i = 0; i < 8; i++) {
					Vector3d vertice(movingIt->getVertice(0 + (i * 3)), movingIt->getVertice(1 + (i * 3)), movingIt->getVertice(2 + (i * 3)));

					for (auto itHitbox : it->getHitboxes()) {
						
						for (int j = 0; j < 12; j++) {//dans les face de la hitbox
							if(PhysicalEntity::doesItCollide(vertice, segment, itHitbox->getTriangles(j)) != 1){																
								return it;
							}
						}						
						for (int k = 0; k < 8; k++) {
							verticeIt.set(itHitbox->getVertice(0 + (k * 3)), itHitbox->getVertice(1 + (k * 3)), itHitbox->getVertice(2 + (k * 3)));
							for (int m = 0; m < 12; m++) {
								if (PhysicalEntity::doesItCollide(verticeIt, (segment * -1), movingIt->getTriangles(m)) != 1.0) {
									return it;
								}							
							}
						}

					}
				}
			}
		}
		return nullptr;
	}

	/// \brief Affichage.
	virtual void draw() = 0;

	/// \brief Notifications
	virtual void notification() = 0;

};

Player* Scene3d::player1 = new Player();
Player* Scene3d::player2 = new Player();

#endif