/// \file LaodingScene.h
/// \brief Écran de chargement

#ifndef LOADINGSCREEN_H
#define LOADINGSCREEN_H
using namespace std;

#include <list>

#include "Scene.h"
#include "Option.h"
#include "Font.h"

/// \class LoadingScreen
/// \brief Écran de chargement.
///
/// Écran permettant de charger les ressources nécessaires.
///
class LoadingScreen : public Scene {
public:
	/// \brief Construcuteur.
	LoadingScreen(const char* resourceFileName, const unsigned int& nextScene) : Scene() {
		if (ResourceManager::notSet("font12")) ResourceManager::set("font12", new Font("Fonts/button.ttf", 12));
		if (ResourceManager::notSet("font15")) ResourceManager::set("font15", new Font("Fonts/button.ttf", 15));
		if (ResourceManager::notSet("font20")) ResourceManager::set("font20", new Font("Fonts/button.ttf", 20));
		if (ResourceManager::notSet("font25")) ResourceManager::set("font25", new Font("Fonts/button.ttf", 25));
		if (ResourceManager::notSet("font30")) ResourceManager::set("font30", new Font("Fonts/button.ttf", 30));
		if (ResourceManager::notSet("font32")) ResourceManager::set("font32", new Font("Fonts/button.ttf", 32));
		if (ResourceManager::notSet("font60")) ResourceManager::set("font60", new Font("Fonts/button.ttf", 60));

		Scene::setScene(nextScene);
	}

	/// \brief Affichage.
	void draw() {		
		// 2D
		SDLGLContext::getInstance()->setPerspective(true);
		glDisable(GL_LIGHTING);
		// TODO: Draw 2D...
		glEnable(GL_LIGHTING);
		SDLGLContext::getInstance()->setPerspective(false);
	}

	/// \brief Notification d'événement.
	virtual void notification() {}

	/// \brief Obtention du type de scène.
	/// \return Titre de la scène.
	string getType() {
		return "LoadingScreen";
	}
};

#endif
