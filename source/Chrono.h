#ifndef CHRONO_H
#define CHRONO_H

#include <chrono>

class Chrono {
private:
	std::chrono::time_point<std::chrono::system_clock> start;

public:
	Chrono() {
		start = std::chrono::system_clock::now();
	}

	~Chrono() {

	}

	/// \brief Obtention du temps écoulé.
  /// \return Nombre de seconde écoulé.
	inline double getElapsedTime() {
		std::chrono::duration<double> elapsedTime = std::chrono::system_clock::now() - start;
		return elapsedTime.count();
	}

	inline void reset() {
		start = std::chrono::system_clock::now();
	}
};
#endif
