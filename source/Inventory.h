/// \file Inventory.h
/// \brief inventaire
#ifndef INVENTORY_H
#define INVENTORY_H

#include <string>
#include "VisualComponent.h"
#include "Scene.h"
#include "Image.h"
#include "Label.h"
#include "Component.h"
#include "FrameBloc.h"
#include "Shop.h"
#include "EnergyBlock.h"
#include "YSnap.h"
#include "HintBox.h"
#include "ComponentButton.h"

/// \class Inventory
/// \brief inventaire
///
/// inventaire
///
class Inventory : public Scene {
private:
    list<Component*> componentsBought;///< Map des composantes acheter
    unsigned int inventoryBackgroundTextureId;///< Texture de l'arriere plan
	unsigned int overGarageBackgroundId;///< Texture pour le mouse over
    Shop* shop;///< pointeur pouvant mener à la boutique du garage
    int servicesPrice;///< Prix comprenant les reparations et les levelUp acheter
public:
    Inventory(){
        servicesPrice = 0;
				inventoryBackgroundTextureId = SDLGLContext::getInstance()->loadTexture("Textures/panelBackground.png");
				overGarageBackgroundId = SDLGLContext::getInstance()->loadTexture("Textures/_buttonBackground.png");

				drawables["totalCost"] = new Label(920, 0, ResourceManager::get<Font*>("font60"), to_string(getTotalCost()) + " LL", { 0, 0, 255, 255 }, 0.5);
        drawables["background"] = new Image(inventoryBackgroundTextureId, 905, 0, 300, 700);        
        drawables["upButton"] = new ImageButton(SDLGLContext::getInstance()->loadTexture("Textures/UpArrow.png"), 1165, 130, 25, 25, 0.2);
        drawables["downButton"] = new ImageButton(SDLGLContext::getInstance()->loadTexture("Textures/DownArrow.png"), 1165, 665, 25, 25, 0.2);

        list<string>* stringDescriptionBox = new list<string>;
        stringDescriptionBox->push_back("Description:");
        drawables["DescritionBox"] = new HintBox(SDLGLContext::getInstance()->loadTexture("Textures/buttonBackground.png"), stringDescriptionBox,{255, 255, 255, 255}, 1200.0, 700.0, 1.0, 1.0, 0.2);
        ((HintBox*)drawables["DescritionBox"])->setPosition(1200.0- ((HintBox*)drawables["DescritionBox"])->getWidth(), 700.0 - ((HintBox*)drawables["DescritionBox"])->getHeight());
    }

    ~Inventory() {
        glDeleteTextures(1, &inventoryBackgroundTextureId);        

        for (auto it : drawables)
					delete it.second;        
		}

	void setShop(Shop* shop) {
		this->shop = shop;
	}

    /// \brief Calcul du cout total
    double getTotalCost(){
        double cost = 0;
        for(list<Component*>::iterator it = componentsBought.begin(); it != componentsBought.end(); it++){
            if(!(*it)->isAlreadyBought)
                cost += (*it)->getPrice();
        }
        cost += servicesPrice;
        if(cost < 0.0)
            cost = 0.0;
        return cost;
    }

    void removeBoughtItemsCost(){
        servicesPrice = 0.0;
        
        for(list<Component*>::iterator it = componentsBought.begin(); it != componentsBought.end(); it++){
            (*it)->isAlreadyBought = true;
        }
        ((Label*)drawables["totalCost"])->setText(to_string(getTotalCost()) + " LL", { 0, 0, 255, 255 });
    }

    /// \brief Obtenir un drawable
    /// \param name Nom du drawable
    /// \return Retourn le drawable
    inline Drawable* getDrawable(string name){
        return drawables[name];
    }

    inline list<Component*>* getComponentList(){
        return &componentsBought;
    }

    /// \brief Obtenir une composante
    /// \param index Position dans la liste des composantes
    /// \return Retourne la composante
    Component* getComponent(int index){
        list<Component*>::iterator it = componentsBought.begin();
       for(int i = 0; i < index; i++){
           it++;
       }
       return *it;
    }

    /// \brief Obtenir le nombre de composantes acheter
    /// \return Retourne le nombre de composantes dans l'inventaire
    inline unsigned int getNumberOfComponentInInventory(){
        return (unsigned int)(componentsBought.size());
    }

    /// \brief ajouter une composante a l'inventaire
    /// \param component composante a ajouter
    void addComponent(Component* component, double priceWithoutAddingComponent = 0.0){
        if(component){
            Component* toAdd = nullptr;
            switch(component->getType()){
                case FRAMEBLOC:
                    toAdd = new FrameBloc(((FrameBloc*)component));
                    break;
                case FLAMETHROWER:
                    toAdd = new FlameThrower(((FlameThrower*)component));
                    break;
                case CATAPULT:
                    toAdd = new Catapult(((Catapult*)component));
                    break;
                case SAW:
                    toAdd = new Saw(((Saw*)component));
                    break;
                case STANDARDWHEEL:
                    toAdd = new StandardWheel(((StandardWheel*)component));
                    break;
                case NAILEDWHEEL:
                    toAdd = new NailedWheel(((NailedWheel*)component));
                    break;
                case ENERGYBLOCK:
                    toAdd = new EnergyBlock(((EnergyBlock*)component));
                    break;
                case SPRING:
                    toAdd = new Spring((Spring*)component);
                    break;
                case PROPELLER:
                    toAdd = new Propeller((Propeller*)component);
                    break;
                case YSNAP:
                    toAdd = new YSnap((YSnap*)component);
                    break;
                
            }
            if(shop->getMoney() >= toAdd->getPrice()){
                drawables[to_string(componentsBought.size()) + "xButton"] = new ImageButton(SDLGLContext::getInstance()->loadTexture("Textures/xButton.png"), 1132.0, (95.0 + (((double)(componentsBought.size()) + 1.0) * 30.0)), 16, 16, 0.2);
                drawables[to_string(componentsBought.size())] = new ComponentButton(toAdd, SDLGLContext::getInstance()->loadTexture("Textures/buttonBackground.png"), ResourceManager::get<Font*>("font20"), {255, 255, 255, 255}, component->getName(), 945.0, (100.0 + (((double)(componentsBought.size()) + 1.0) * 30.0)), 200, 25, 0.1, overGarageBackgroundId);
                if(((LabelImageButton*)drawables[to_string(componentsBought.size())])->getPosition().y > 665){
                    drawables[to_string(componentsBought.size())]->setVisible(false);
                    drawables[to_string(componentsBought.size()) + "xButton"]->setVisible(false);
                }
                componentsBought.push_back(toAdd);

                ((Label*)drawables["totalCost"])->setText(to_string(getTotalCost()) + " LL", { 0, 0, 255, 255 });
                shop->chargeBoughtItem(toAdd->getPrice());
            }
        }else{
            servicesPrice += priceWithoutAddingComponent;
            ((Label*)drawables["totalCost"])->setText(to_string(getTotalCost()) + " LL", { 0, 0, 255, 255 });
            shop->chargeBoughtItem(priceWithoutAddingComponent);
        }
    }

    /// \brief Retire une composante de l'inventaire
    /// \param index Position dans la liste de composante de la composante a retirer
    void removeComponent(unsigned int index){
        Component* toRemove = ((ComponentButton*)drawables[to_string(index)])->getComponent();
        drawables.erase(to_string(index) + "xButton");
		drawables.erase(to_string(index));

        //scroll des boutons en dessous s'il y a
		for(int j = index + 1; j < componentsBought.size(); j++){
			LabelImageButton* toChange  = ((LabelImageButton*)drawables[to_string(j)]);
            toChange->setPosition(945, toChange->getPosition().y - 30);
            toChange->getLabel()->setPosition(toChange->getLabel()->getPosition().x, toChange->getLabel()->getPosition().y - 30);
            ((ImageButton*)drawables[to_string(j) + "xButton"])->setPosition(1132, ((ImageButton*)drawables[to_string(j) + "xButton"])->getPosition().y - 30);

            if(toChange->getPosition().y < 130){
                toChange->setVisible(false);
                drawables[to_string(j) + "xButton"]->setVisible(false);
            }
            if(toChange->getPosition().y < 665 and toChange->getPosition().y > 130){
                toChange->setVisible(true);
                drawables[to_string(j) + "xButton"]->setVisible(true);
            }

            //decrementation des noms des boutons
            drawables[to_string(j - 1)] = drawables[to_string(j)];
            drawables.erase(to_string(j));
            drawables[to_string(j - 1) + "xButton"] = drawables[to_string(j) + "xButton"];
            drawables.erase(to_string(j) + "xButton");
		}
        //Changer le cout total
        shop->chargeBoughtItem(-(toRemove->getPrice() / 2 * (toRemove->getLevel() + 1)));
        componentsBought.remove(toRemove);
        servicesPrice -= toRemove->getPrice() / 2 * (toRemove->getLevel() - 1);
        ((Label*)drawables["totalCost"])->setText(to_string(getTotalCost()) + " LL", { 0, 0, 255, 255 });
    }

    /// \brief Abonner les buttons de la boutique general
    /// \param event Evenement a s'abonner
    void unsubscribeComponentButtons(Observable* event){
        for(int i = 0; i < componentsBought.size(); i++){
            event->unsubscribe(drawables[to_string(i)]);
            event->unsubscribe(drawables[to_string(i) + "xButton"]);
        }
        event->unsubscribe(drawables["upButton"]);
        event->unsubscribe(drawables["downButton"]);

    }

    /// \brief Abonner les buttons de la boutique general
    /// \param event Evenement a s'abonner
    void subscribeComponentButtons(Observable* event){
        for(int i = 0; i < componentsBought.size(); i++){
            event->subscribe(drawables[to_string(i)]);
            event->subscribe(drawables[to_string(i) + "xButton"]);
        }
        event->subscribe(drawables["upButton"]);
        event->subscribe(drawables["downButton"]);
    }

    /// \brief Defiler les boutons de l'inventaire
    /// \param up Si le defilement permet de voir les boutons plus haut
    void scroll(bool up){
        if(up){
            if(drawables["0"]){
                if(!drawables["0"]->getVisible()){
                    for(int i = 0; i < componentsBought.size(); i++){
                        LabelImageButton* toChange  = ((LabelImageButton*)drawables[to_string(i)]);
                        toChange->setPosition(945, toChange->getPosition().y + 30);
                        toChange->getLabel()->setPosition(toChange->getLabel()->getPosition().x, toChange->getLabel()->getPosition().y + 30);
                        ((ImageButton*)drawables[to_string(i) + "xButton"])->setPosition(1132, ((ImageButton*)drawables[to_string(i) + "xButton"])->getPosition().y + 30);

                        if(toChange->getPosition().y > 665){
                            toChange->setVisible(false);
                            drawables[to_string(i) + "xButton"]->setVisible(false);
                        }
                        if(toChange->getPosition().y > 100 and toChange->getPosition().y < 665){
                            toChange->setVisible(true);
                            drawables[to_string(i) + "xButton"]->setVisible(true);
                        }
                    }
                }
            }else
                drawables.erase("0");
        }else{
            if(drawables[to_string(componentsBought.size() - 1)]){
                if(!drawables[to_string(componentsBought.size() - 1)]->getVisible()){
                    for(int i = 0; i < componentsBought.size(); i++){
                        LabelImageButton* toChange  = ((LabelImageButton*)drawables[to_string(i)]);
                        toChange->setPosition(945, toChange->getPosition().y - 30);
                        toChange->getLabel()->setPosition(toChange->getLabel()->getPosition().x, toChange->getLabel()->getPosition().y - 30);
                        ((ImageButton*)drawables[to_string(i) + "xButton"])->setPosition(1132, ((ImageButton*)drawables[to_string(i) + "xButton"])->getPosition().y - 30);

                        if(toChange->getPosition().y < 130){
                            toChange->setVisible(false);
                            drawables[to_string(i) + "xButton"]->setVisible(false);
                        }
                        if(toChange->getPosition().y < 665 and toChange->getPosition().y > 130){
                            toChange->setVisible(true);
                            drawables[to_string(i) + "xButton"]->setVisible(true);
                        }
                    }
                }
            }else
                drawables.erase(to_string(componentsBought.size()));
        }
    }

    /// \brief Affichage
    void draw(){
		// 2D
		SDLGLContext::getInstance()->setPerspective(true);
        glDisable(GL_LIGHTING);
		for(auto it : drawables){
            it.second->draw();
            if(it.second->getType()=="ComponentButton") {
                if (((ComponentButton *) it.second)->getMouseOn()) {
                    ((HintBox*)drawables["DescritionBox"])->changeText(((ComponentButton*)it.second)->getDescription());
                    ((HintBox*)drawables["DescritionBox"])->setPosition(905 , 700.0 - ((HintBox*)drawables["DescritionBox"])->getHeight());
                }
            }
        }
		glDisable(GL_LIGHTING);
		
		glEnable(GL_LIGHTING);
    }

    /// \brief notifier
    void notification(){
        
    }

    string getType(){
        return "Inventaire";
    }

};



#endif