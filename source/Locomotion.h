/// \file Locomotion.h
/// \brief locomotion
#ifndef LOCOMOTION_H
#define LOCOMOTION_H

#include "Controlable.h"

/// \class Locomotion
/// \brief objets de locomotion
///
/// objets de locomotion
///
class Locomotion : public Controlable{
protected:
    double adherence; ///< adherence 
    bool isFrontLocomotion;///< Savoir si elle doit tourner lors d'un virage
    double turningAngle;///< Angle (en rad) pour tourner. **seulement si isFrontLocomotion est a true**
public:
    Locomotion(Material* material, const double& x, const double& y, const double& z, string fileName, const unsigned int& selecId = 0) : Controlable(material, x, y ,z, fileName){
        isFrontLocomotion = false;
        turningAngle = 0.0;
    }
    
    ~Locomotion(){
    }
    /// \brief Tourner la composante
    void turnLocomotionForDirection(double angle){
        double angleToAdd = angle - turningAngle;
        turningAngle += angleToAdd;
        Matrix44d rotation;
        rotation.loadRotation(angleToAdd, Vector3d(0.0, 1.0, 0.0));
        setTransformationMatrix(rotation);
		transform();
    }

    /// \breif Savoir s'il est en avant
    /// \return S'il est en avant ou non
    bool getIsFrontLocomotion(){
        return isFrontLocomotion;
    }
    /// \brief Initialiser s'il est en avant
    void setIsFrontLocomotion(bool isFrontLocomotion){
        this->isFrontLocomotion = isFrontLocomotion;
    }

    void levelUp(){
		maxHealth *= 1.5;
		health *= 1.5;
        adherence += 0.5;
        level++;
	}

	/// \brief Obtenir Description
    /// \return retourne la description
	list<string>* getDescrition(){
		list<string>* description = new list<string>;
		description->push_back("Descrition:");
		description->push_back("Roue standard");
		description->push_back("Adherence: " + to_string(getAdherence()));
		description->push_back("Niveau: " + to_string(level));
		description->push_back("Prix: " + to_string((int)(getPrice() + ((level - 1) * getPrice() / 2.0))));
        description->push_back("Masse: " + to_string((int)getMass()));
        description->push_back("Vie: " + to_string((int)getHealth()) + "/" + to_string((int)getMaxHealth()));

		return description;
	}

    /// \brief Activer l'effet de la composante
    virtual void activate(){
        
    }
    /// \brief Desactiver l'effet de la composante
    virtual void disable(){

    }
    /// \brief Affichage
    virtual void draw() = 0;
    /// \brief Notification
    virtual void notification() = 0;
    /// \brief getAdherence
    virtual double getAdherence() = 0;

};


#endif