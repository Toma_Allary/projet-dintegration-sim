/// \file Garage.h
/// \brief Garage

#ifndef GARAGE_H
#define GARAGE_H

#define NO_COMPONENT_REMOVED 1000

#include <string>
#include <iterator>
#include <list>
#include <cmath>
#include <iostream>
#include "Scene3d.h"
#include "LabelImageButton.h"
#include "Inventory.h"
#include "Shop.h"
#include "Mesh3a.h"
#include "Orbital.h"
#include "HintBox.h"
#include "FrameBloc.h"
#include "Robot.h"
#include "Component.h"
#include "EnergyBlock.h"
#include "Material.h"
#include "Chrono.h"

void mainMenuPrototype();
void tryPrototype();
void buyPiecesPrototype();
void piecesBoughtPrototype();
void preSetPrototype();
void readyPrototype();
void frameBlocPrototype();
void locomotionsPrototype();
void weaponsPrototype();
void toolPrototype();
void goBackTitlePrototype();
void specializedShopButtonsPrototype();
void inventoryButtonsPrototype();
void inventoryDownPrototype();
void inventoryUpPrototype();
void removeComponentFromInventoryPrototype();
void changeMaterialPrototype();
void buyRobotPrototype();
void fixPrototype();

#define GARAGE_TEXTURE_ATMOSPHERE 0
#define GARAGE_TEXTURE_MENU_BACKGROUND 1
#define GARAGE_TEXTURE_BUTTON_BACKGROUND 2
#define GARAGE_TEXTURE_OVERBUTTON_BACKGROUND 3
#define GARAGE_TEXTURE_USED_METAL 4
#define GARAGE_TEXTURE_CHASSIS 5
#define GARAGE_TEXTURE_ENERGY_BLOCK 6

using namespace std;

/// \class Garage
/// \brief Garage
///
/// Garage/boutique
///
class Garage : public Scene3d {
private:
	//Vector3d segmentToDraw;///< aide visuelle
	//double* segmentVertex;///< aide visuelle
	list<Component*>* robotComponents;
	map<string, Material*> usableMaterial;
	list<Component*>::iterator  meshIt;
	unsigned int texturesId[7];
	Mesh3a* atmoSphere;
  Inventory* inventory;///< inventaire (piecesBought)
  Shop* shop;///< boutique (buyPieces)
	unsigned int componentButtonToUnsubscribeLaterId;///< Index du bouton de la composante retirer
	bool unsubscribeFixComponentButtons;
	bool rightMouseButtonDown;///< vérifie si le bouton doirte de la souris est toujours enfoncé
	bool leftMouseButtonDown;///< vérifie si le bouton doirte de la souris est toujours enfoncé
	//bool segmentVisibility;///< aide visuelle
  bool goToSpecializedShop;///< Si la boutique doit changer pour la sous boutique
  bool goToShop;///< Si la sous boutique doit changer pour la boutique
	bool rotationMode;
	bool xLocked;
	bool yLocked;
	bool zLocked;
	list<Controlable*> waitingKeyForBinding;///< Prochain evenement de gamepad sera assigner a la composante
	unsigned int waitingKeyForLocomotionBinding;///< 4 prochains evenements de gamepad sera assigner a la locomotion
  list<string>* description;
	Chrono chronoDeltaT;
	Chrono bindingChrono;
	Chrono chronoMoving;
	double mouseWheelAcceleration;
	static Player* actualPlayer;
    string gamepadCheckBox;

public:
    Garage() {
			//initialisation des manettes
			const unsigned int gamePadCount = SDL_NumJoysticks();
			for (unsigned int i = 0; i < gamePadCount or i < 2; i++)
				SDL_GameControllerOpen(i);

			player1->setGamePadId(0);
			player2->setGamePadId(1);

			//segmentVisibility = false;//aide visuelle
			//segmentToDraw = Vector3d(0.0, 0.0, 0.0);//aide visuelle
			//segmentVertex = new double[9];//aide visuelle
			xLocked = false;
			yLocked = false;
			zLocked = false;
			rotationMode = false;
			waitingKeyForLocomotionBinding = 0;
			goToSpecializedShop = goToShop = unsubscribeFixComponentButtons = false;
			componentButtonToUnsubscribeLaterId = NO_COMPONENT_REMOVED;
			actualPlayer = player1;
			gamepadCheckBox = "player1GamePadCheckBox";

		
			texturesId[GARAGE_TEXTURE_ATMOSPHERE] = SDLGLContext::getInstance()->loadTexture("Textures/garageAtmoSphere.png");
			texturesId[GARAGE_TEXTURE_MENU_BACKGROUND] = SDLGLContext::getInstance()->loadTexture("Textures/menuBackground.png");
			texturesId[GARAGE_TEXTURE_BUTTON_BACKGROUND] = SDLGLContext::getInstance()->loadTexture("Textures/buttonBackground.png");
			texturesId[GARAGE_TEXTURE_OVERBUTTON_BACKGROUND] = SDLGLContext::getInstance()->loadTexture("Textures/_buttonBackground.png");
			texturesId[GARAGE_TEXTURE_USED_METAL] = SDLGLContext::getInstance()->loadTexture("Textures/usedMetal.png");
			texturesId[GARAGE_TEXTURE_CHASSIS] = SDLGLContext::getInstance()->loadTexture("Textures/brushedMetal.png");
			texturesId[GARAGE_TEXTURE_ENERGY_BLOCK] = SDLGLContext::getInstance()->loadTexture("Textures/energyBlock.png");
		
			atmoSphere = new Mesh3a(texturesId[GARAGE_TEXTURE_ATMOSPHERE], 0.0, 0.0, -25.0, "Objets/garageAtmoSphere.obj");

			if (actualPlayer->getRobot()->getComponentList()->size() == 0) {
				actualPlayer->getRobot()->addComponent(new FrameBloc(new Material(texturesId[GARAGE_TEXTURE_USED_METAL], 2.00, 2.00, 2.00, 2.00, 2.00, 2.00, 2.00), 0.0, 0.0, -25.0));
				actualPlayer->getRobot()->getComponentTree()->add(actualPlayer->getRobot()->getComponentList()->front(), actualPlayer->getRobot()->getComponentList()->front()->getConnectors().size(),nullptr, 0, 0);
			}
			robotComponents = actualPlayer->getRobot()->getComponentList();
				
			shop = new Shop(actualPlayer->getMoney());
			if (!actualPlayer->getInventory()) {
				actualPlayer->addInventory();
			}
			inventory = actualPlayer->getInventory();
			inventory->setShop(shop);
			((LabelImageButton*)inventory->getDrawable("upButton"))->bindOnClick(inventoryUpPrototype);
			((LabelImageButton*)inventory->getDrawable("downButton"))->bindOnClick(inventoryDownPrototype);

			drawables["shop"] = shop;

			rightMouseButtonDown = false; // IMPORTANT sinon le mouvement de caméra avec la souris de fonctionne pas
			leftMouseButtonDown = false; // IMPORTANT sinon le mouvement de caméra avec la souris de fonctionne pas

			camera = new Orbital(Vector3d(0.0, 5.0, -15.0), Vector3d(0.0, 0.0, -25.0), Vector3d(0.0, 1.0, 0.0));
		
			meshIt = robotComponents->end();

			drawables["mainMenuButton"] = new LabelImageButton(texturesId[GARAGE_TEXTURE_BUTTON_BACKGROUND], ResourceManager::get<Font*>("font12"), { 255, 255, 255, 255 }, "Menu Principal", 1, 0, 149, 30, 0.1, texturesId[GARAGE_TEXTURE_OVERBUTTON_BACKGROUND]);
			drawables["tryButton"] = new LabelImageButton(texturesId[GARAGE_TEXTURE_BUTTON_BACKGROUND], ResourceManager::get<Font*>("font12"), { 255, 255, 255, 255 }, "Essayer", 152, 0, 149, 30, 0.1, texturesId[GARAGE_TEXTURE_OVERBUTTON_BACKGROUND]);
			drawables["buyPiecesButton"] = new LabelImageButton(texturesId[GARAGE_TEXTURE_BUTTON_BACKGROUND], ResourceManager::get<Font*>("font12"), { 255, 255, 255, 255 }, "Boutique", 303, 0, 149, 30, 0.1, texturesId[GARAGE_TEXTURE_OVERBUTTON_BACKGROUND]);
			drawables["piecesBoughtButton"] = new LabelImageButton(texturesId[GARAGE_TEXTURE_BUTTON_BACKGROUND], ResourceManager::get<Font*>("font12"), { 255, 255, 255, 255 }, "Inventaire", 454, 0, 149, 30, 0.1, texturesId[GARAGE_TEXTURE_OVERBUTTON_BACKGROUND]);
			drawables["preSetButton"] = new LabelImageButton(texturesId[GARAGE_TEXTURE_BUTTON_BACKGROUND], ResourceManager::get<Font*>("font12"), { 255, 255, 255, 255 }, "Véhicules", 605, 0, 149, 30, 0.1, texturesId[GARAGE_TEXTURE_OVERBUTTON_BACKGROUND]);
			drawables["readyButton"] = new LabelImageButton(texturesId[GARAGE_TEXTURE_BUTTON_BACKGROUND], ResourceManager::get<Font*>("font12"), { 255, 255, 255, 255 }, "Prêt", 756, 0, 149, 30, 0.1, texturesId[GARAGE_TEXTURE_OVERBUTTON_BACKGROUND]);
			drawables["hints"] = new DoubleLabel(170, 635.0, ResourceManager::get<Font*>("font12"), "Conseil: ", { 0, 0, 255, 255 }, 0.2, 2.0);
			drawables["hints1"] = new DoubleLabel(1.0, 660.0, ResourceManager::get<Font*>("font12"), "Appuyez sur TAB à répétition pour changer de modèle à sélectionner, ", { 0, 0, 255, 255 }, 0.2, 2.0);
			drawables["hints2"] = new DoubleLabel(1.0, 675.0, ResourceManager::get<Font*>("font12"), "puis gardez le clique gauche de la souris appuyé pour le déplacer.", { 0, 0, 255, 255 }, 0.2, 2.0);
			drawables["R"] = new DoubleLabel(1, 55.0, ResourceManager::get<Font*>("font12"), "*Mode rotation activé*", { 0, 0, 255, 255 }, 0.2, 2.0);
			drawables["movingMode"] = new DoubleLabel(1, 55.0, ResourceManager::get<Font*>("font12"), "*Mode déplacement activé*", { 0, 0, 255, 255 }, 0.2, 2.0);
			drawables["X"] = new DoubleLabel(1, 70.0, ResourceManager::get<Font*>("font12"), "*L'axe X est bloqué*", { 0, 0, 255, 255 }, 0.2, 2.0);
			drawables["Y"] = new DoubleLabel(1, 70.0, ResourceManager::get<Font*>("font12"), "*L'axe Y est bloqué*", { 0, 0, 255, 255 }, 0.2, 2.0);
			drawables["Z"] = new DoubleLabel(1, 70.0, ResourceManager::get<Font*>("font12"), "*L'axe Z est bloqué*", { 0, 0, 255, 255 }, 0.2, 2.0);
			drawables["noAxe"] = new DoubleLabel(1, 70.0, ResourceManager::get<Font*>("font12"), "*Aucun axe n'est bloqué, la rotation ne fonctionnera pas.*", { 0, 0, 255, 255 }, 0.2, 2.0);
			drawables["noAxe1"] = new DoubleLabel(1, 85.0, ResourceManager::get<Font*>("font12"), "*Appuyez sur X, Y ou Z pour bloquer un axe puis cliquez sur le bouton", { 0, 0, 255, 255 }, 0.2, 2.0);
			drawables["noAxe2"] = new DoubleLabel(1, 100.0, ResourceManager::get<Font*>("font12"), "gauche de la souris pour faire tourner l'objet sélectionné*", { 0, 0, 255, 255 }, 0.2, 2.0);
			drawables["noSelected"] = new DoubleLabel(1, 40.0, ResourceManager::get<Font*>("font12"), "*Aucun Objet est sélectionné.*", { 0, 0, 255, 255 }, 0.2, 2.0);


			drawables["R"]->setVisible(false);
			drawables["X"]->setVisible(false);
			drawables["Y"]->setVisible(false);
			drawables["Z"]->setVisible(false);
			drawables["noAxe"]->setVisible(false);
			drawables["noAxe1"]->setVisible(false);
			drawables["noAxe2"]->setVisible(false);

			((Button*)drawables["mainMenuButton"])->bindOnClick(mainMenuPrototype);
			((Button*)drawables["tryButton"])->bindOnClick(tryPrototype);
			((Button*)drawables["buyPiecesButton"])->bindOnClick(buyPiecesPrototype);
			((Button*)drawables["piecesBoughtButton"])->bindOnClick(piecesBoughtPrototype);
			((Button*)drawables["preSetButton"])->bindOnClick(preSetPrototype);
			((Button*)drawables["readyButton"])->bindOnClick(readyPrototype);

			events[SDL_MOUSEMOTION] = new Observable();
			events[SDL_MOUSEBUTTONUP] = new Observable();
			events[SDL_MOUSEBUTTONDOWN] = new Observable();
			events[SDL_MOUSEBUTTONDOWN]->subscribe(drawables["mainMenuButton"]);
			events[SDL_MOUSEBUTTONDOWN]->subscribe(drawables["tryButton"]);
			events[SDL_MOUSEBUTTONDOWN]->subscribe(drawables["buyPiecesButton"]);
			events[SDL_MOUSEBUTTONDOWN]->subscribe(drawables["piecesBoughtButton"]);
			events[SDL_MOUSEBUTTONDOWN]->subscribe(drawables["preSetButton"]);
			events[SDL_MOUSEBUTTONDOWN]->subscribe(drawables["readyButton"]);

			events[SDL_MOUSEMOTION]->subscribe(drawables["mainMenuButton"]);
			events[SDL_MOUSEMOTION]->subscribe(drawables["tryButton"]);
			events[SDL_MOUSEMOTION]->subscribe(drawables["buyPiecesButton"]);
			events[SDL_MOUSEMOTION]->subscribe(drawables["piecesBoughtButton"]);
			events[SDL_MOUSEMOTION]->subscribe(drawables["preSetButton"]);
			events[SDL_MOUSEMOTION]->subscribe(drawables["readyButton"]);

			shop->subscribeComponentButtons(events[SDL_MOUSEBUTTONDOWN], true);
			shop->subscribeComponentButtons(events[SDL_MOUSEMOTION], true);
		
			((LabelImageButton*)shop->getDrawable("StructuresButton"))->bindOnClick(frameBlocPrototype);
			((LabelImageButton*)shop->getDrawable("LocomotionsButton"))->bindOnClick(locomotionsPrototype);
			((LabelImageButton*)shop->getDrawable("ArmesButton"))->bindOnClick(weaponsPrototype);
			((LabelImageButton*)shop->getDrawable("OutilsButton"))->bindOnClick(toolPrototype);
            ((LabelImageButton*)shop->getDrawable("Robot CompletButton"))->bindOnClick(buyRobotPrototype);
			((LabelImageButton*)shop->getDrawable("Materiaux"))->bindOnClick(changeMaterialPrototype);
			((LabelImageButton*)shop->getDrawable("Robot CompletButton"))->bindOnClick(buyRobotPrototype);
			((LabelImageButton*)shop->getDrawable("FixButton"))->bindOnClick(fixPrototype);

			if(ResourceManager::notSet("font20"))
				ResourceManager::set("font20", new Font("button.ttf", 20));

			currentDrawable = this;
			chronoDeltaT.reset();
			bindingChrono.reset();
			mouseWheelAcceleration = 0.0;

			for(auto it : *player1->getRobot()->getComponentList()){
				if(it->getHealth() > 0.0){
					it->setConnectorsVisibilty(true);
					it->setVisibility(true);
				}
			}
			for(auto it : *player2->getRobot()->getComponentList()){
				if(it->getHealth() > 0.0){
					it->setConnectorsVisibilty(true);
					it->setVisibility(true);
				}		
			}
    }

    /// \brief Destructeur
    ~Garage() {
		if (meshIt != robotComponents->end()) {
			(*meshIt)->setSelected(false);
		}

			delete atmoSphere;

			glDeleteTextures(7, texturesId);

			drawables["shop"] = shop;
			drawables["inventory"] = nullptr;

			delete camera;

			for (auto it : events)
				delete it.second;

			for (auto it : drawables) 
				delete it.second;

			usableMaterial.erase(usableMaterial.begin(), usableMaterial.end());
    }

    /// \brief Defiler l'inventaire
    /// \param up Si le defilement permet de voir les boutons plus haut
    void scrollInventory(bool up){
        inventory->scroll(up);
    }

    /// \brief changer le matériau des composantes
    inline void changeMaterial(){
        shop->changeMaterial();
    }

    /// \brief Ajout d'une composante a l'inventaire selon le bouton cliquer
    void addComponentToInventory(){
        Component* toAdd = nullptr;
        string generalComponentName = shop->getSpecializedShopTitle();

        for(unsigned int componentIt = 1; componentIt < shop->getGeneralComponentList(generalComponentName)->size() + 1; componentIt++){
            if(((LabelImageButton*)shop->getDrawable(generalComponentName + to_string(componentIt)))->getClicked()){
                //trouver la composante dans la liste de la boutique avec la position trouver plus haut
                list<Component*>::iterator it = shop->getGeneralComponentList(generalComponentName)->begin();
                for(unsigned int i = 0; i < componentIt- 1; i++){
                    it++;
                }

                if(shop->getSpecializedShopTitle() != "Fix"){
					//ajout de la composante dans la liste
					(*it)->changeMaterial(shop->getCurrentMaterial());
					inventory->addComponent(*it);
					actualPlayer->setMoney(actualPlayer->getMoney() - (*it)->getPrice());
					//association du nouveau bouton creer depuis la nouvelle composante
					((LabelImageButton*)inventory->getDrawable(to_string(inventory->getNumberOfComponentInInventory() - 1)))->bindOnClick(inventoryButtonsPrototype);
					((LabelImageButton*)inventory->getDrawable(to_string(inventory->getNumberOfComponentInInventory() - 1) + "xButton"))->bindOnClick(removeComponentFromInventoryPrototype);
				}else{
					double fixingPrice = (*it)->getPrice() * ((*it)->getMaxHealth() - (*it)->getHealth()) / (*it)->getMaxHealth();
					if(actualPlayer->getMoney() - fixingPrice >= 0.0){
						actualPlayer->setMoney(actualPlayer->getMoney() - fixingPrice);

						(*it)->setConnectorsVisibilty(true);
						(*it)->setVisibility(true);

						(*it)->setHealth((*it)->getMaxHealth());
						unsubscribeFixComponentButtons = true;

						inventory->addComponent(nullptr, fixingPrice);//seulement pour les label
					}
				}
            }
        }
    }

	/// \brief Retire une composante de l'inventaire selon le bouton cliquer
	void removeComponentFromInventory(){
		for(unsigned int i = 0; i < inventory->getNumberOfComponentInInventory(); i++){
			if(((Button*)inventory->getDrawable(to_string(i) + "xButton"))->getClicked()){
				componentButtonToUnsubscribeLaterId = i;
				
				for (list<Component*>::iterator it = actualPlayer->getRobot()->getComponentList()->begin(); it != actualPlayer->getRobot()->getComponentList()->end(); it++) {
					for (int z = 0; z < (*it)->getConnectors().size(); z++) {//auto connectorsIt : (*it)->getConnectors()
						for (unsigned int j = 0; j < inventory->getComponent(i)->getConnectors().size(); j++) {
							double distance = abs(((*(*it)->getConnectors()[z]).getPosition() - (*(inventory->getComponent(i)->getConnectors()[j])).getPosition()).getNorm());
							double distanceRequiredForSnap = 0.6;
							
							if (inventory->getComponent(i)->getConnectors()[j]->getSelected() and (distance < distanceRequiredForSnap)) {
								//remttre les connecteur à faux
								inventory->getComponent(i)->getConnectors()[j]->setSelected(false);
								(*it)->getConnectors()[z]->setSelected(false);
							}
						}
					}
				}
				//Code pour clear les nodes de l'arbre va ici			
				actualPlayer->getRobot()->getComponentTree()->remove(inventory->getComponent(i));
				robotComponents->remove(inventory->getComponent(i));
				
				double componentPriceValue = ((inventory->getComponent(i)->getPrice() / 2) * (inventory->getComponent(i)->getLevel() + 1));
				actualPlayer->setMoney(actualPlayer->getMoney() + componentPriceValue);
			}
		}
	}

	void addCompleteRobotPart(string robotShopName) {
		list<Component*>::iterator it = robotComponents->begin();
		while (it != robotComponents->end()){
			//retrait de l'arbre du robot
			if (*it == actualPlayer->getRobot()->getComponentTree()->getRootData()) {
				actualPlayer->getRobot()->getComponentTree()->remove(*it);
				robotComponents->remove(*it);
				it = robotComponents->begin();
			}
			if(it != robotComponents->end())
				it++;
		}
		it = shop->getGeneralComponentList(robotShopName)->begin();
		unsigned int i = 0;
		unsigned int frameID = 0;
		while (it != shop->getGeneralComponentList(robotShopName)->end()) {
			(*it)->changeMaterial(shop->getCurrentMaterial());
			inventory->addComponent(*it);
			robotComponents->push_back(inventory->getComponent(inventory->getNumberOfComponentInInventory() - 1));
			//placer les objets au bon endroit
			if ((*it)->getName() != "Structures") {
				//faire rotater les roues dans le bon angle( pi 3 2 = 3.14/2 = 1.57/2)
				if (((*it)->getName() == "Roue standard") or ((*it)->getName() == "Roue clouter")) {
					if (inventory->getComponent(frameID)->getConnectors()[(i - 1)]->getPosition().z != inventory->getComponent(frameID)->getPosition().z) {
						if (inventory->getComponent(frameID)->getConnectors()[(i - 1)]->getPosition().z < inventory->getComponent(frameID)->getPosition().z) {
							robotComponents->back()->getTransformationMatrix()->loadXRotation(-1.57);
						}
						else
							robotComponents->back()->getTransformationMatrix()->loadXRotation(1.57);

							robotComponents->back()->transform();
					}
			
					robotComponents->back()->setPosition(inventory->getComponent(frameID)->getConnectors()[(i - 1)]->getPosition().x, inventory->getComponent(frameID)->getConnectors()[(i - 1)]->getPosition().y, inventory->getComponent(frameID)->getConnectors()[(i - 1)]->getPosition().z);
					if(robotComponents->back()->getPosition().x < 0.0)
						((Locomotion*)robotComponents->back())->setIsFrontLocomotion(true);

					if (waitingKeyForLocomotionBinding != 5) {//Pour savoir si la locomotion a deja ete assigner
						if(drawables.find("keyBindingPopUp") == drawables.end())
							drawables.erase("keyBindingPopUp");
						waitingKeyForLocomotionBinding = 1;
						drawables["keyBindingPopUp"] = new DoubleLabel(550.0, 340.0, ResourceManager::get<Font*>("font25"), "Appuyez sur RT", { 0, 0, 255, 255 }, 0.2, 2.0);
					}
				}
				else {
					double newX = (inventory->getComponent(frameID)->getConnectors()[(i - 1)]->getPosition().x + ((*it)->getPosition().x - (*it)->getConnectors()[0]->getPosition().x)), newY = (inventory->getComponent(frameID)->getConnectors()[(i - 1)]->getPosition().y + ((*it)->getPosition().y) - (*it)->getConnectors()[0]->getPosition().y), newZ = (inventory->getComponent(frameID)->getConnectors()[(i - 1)]->getPosition().z + ((*it)->getPosition().z - (*it)->getConnectors()[0]->getPosition().z));

					robotComponents->back()->setPosition(newX, newY, newZ);
					if((*it)->getName() != "bloc d'energie"){
						waitingKeyForBinding.push_back((Controlable*)robotComponents->back());
						if(drawables.find("keyBindingPopUp") == drawables.end())
							drawables["keyBindingPopUp"] = new DoubleLabel(300.0, 340.0, ResourceManager::get<Font*>("font25"), "Assignez une touche a cette composante: " + waitingKeyForBinding.back()->getName(), { 0, 0, 255, 255 }, 0.2, 2.0);
					}
					actualPlayer->getRobot()->updateLength();
					actualPlayer->getRobot()->updateWidth();

				}
			}
			else {
				frameID = inventory->getNumberOfComponentInInventory() - 1;
			}
			//refaire l'arbre du robot
			if (it == shop->getGeneralComponentList(robotShopName)->begin()) {
				actualPlayer->getRobot()->getComponentTree()->add(robotComponents->back(), robotComponents->back()->getConnectors().size(), nullptr, 0, 0);
			}
			else{
				actualPlayer->getRobot()->getComponentTree()->add(robotComponents->back(), robotComponents->back()->getConnectors().size() - 1, actualPlayer->getRobot()->getComponentTree()->getRootData(), i - 1, 0);
				robotComponents->back()->getConnectors()[0]->setSelected(true);
				inventory->getComponent(frameID)->getConnectors()[(i - 1)]->setSelected(true);
			}
	
			actualPlayer->setMoney(actualPlayer->getMoney() - (*it)->getPrice());

			//association du nouveau bouton creer depuis la nouvelle composante
			((LabelImageButton*)inventory->getDrawable(to_string(inventory->getNumberOfComponentInInventory() - 1)))->bindOnClick(inventoryButtonsPrototype);
			((LabelImageButton*)inventory->getDrawable(to_string(inventory->getNumberOfComponentInInventory() - 1) + "xButton"))->bindOnClick(removeComponentFromInventoryPrototype);
			it++;
			i++;
		}
				
	}

	/// \brief Ajout ou retrait d'une composante dans la liste de composante selon le bouton cliquer
	void addOrRemoveMeshWithButton(){
		for(unsigned int i = 0; i < inventory->getNumberOfComponentInInventory(); i++){
			if(((LabelImageButton*)inventory->getDrawable(to_string(i)))->getClicked()){
				Component* toAddOrRemove = inventory->getComponent(i);
				bool add = true;
				for(list<Component*>::iterator it = robotComponents->begin(); it != robotComponents->end(); it++){
					if((*it) == toAddOrRemove)
						add = false;
				}
				if(add){
					robotComponents->push_back(inventory->getComponent(i));
				}else{
					meshIt = robotComponents->end();
					robotComponents->remove(toAddOrRemove);
				}
			}
		}
	}

    /// \brief Fonction pour ouvrir une sous-boutique
    /// \param index Nombre assigner a chaque composante generale
    void openSpecializedShop(int index){
        switch (index){
            case 1://moteurs
                shop->changePanelState("Structures", true);
				break;
            case 2://locomotions
				shop->changePanelState("Locomotions", true);
				break;
			case 3://armes
				shop->changePanelState("Armes", true);
				break;
        	case 4://outils
        		shop->changePanelState("Outils",true);
        		break;
        	case 5://fix
        		shop->changePanelState("Fix", true, actualPlayer->getRobot()->getComponentList());
        	break;
		}
		((LabelImageButton*)shop->getDrawable("SpecializedShopTitle"))->bindOnClick(goBackTitlePrototype);
		goToSpecializedShop = true;
	}

	/// \brief Pour ouvrir la boutique depuis la sous boutique
	inline void openShopFromSpecializedShop() {
		goToShop = true;
	}

	/// \brief ouvre ou ferme l'inventaire
	void changeInventoryState() {
		if (!drawables["inventory"]) {
			//sert a eviter qu'un inventaire s'ouvre par dessus une boutique
			drawables["shop"] = nullptr;
			drawables.erase("shop");

			if (shop->getSpecializedShopTitle() != "") {
				shop->unsubscribeComponentButtons(events[SDL_MOUSEBUTTONDOWN], false);
				shop->unsubscribeComponentButtons(events[SDL_MOUSEMOTION], false);
			}
			else {
				shop->unsubscribeComponentButtons(events[SDL_MOUSEBUTTONDOWN], true);
				shop->unsubscribeComponentButtons(events[SDL_MOUSEMOTION], true);
			}

			drawables["inventory"] = inventory;

			inventory->subscribeComponentButtons(events[SDL_MOUSEBUTTONDOWN]);
			inventory->subscribeComponentButtons(events[SDL_MOUSEMOTION]);
		}
		else {
			drawables.erase("inventory");

			inventory->unsubscribeComponentButtons(events[SDL_MOUSEBUTTONDOWN]);
			inventory->unsubscribeComponentButtons(events[SDL_MOUSEMOTION]);
		}
	}
	/// \brief ouvre ou ferme la boutique
	void changeShopState() {
		if (!drawables["shop"]) {
			//sert a eviter qu'une boutique s'ouvre par dessus un inventaire
			drawables["inventory"] = nullptr;
			drawables.erase("inventory");

			events[SDL_MOUSEBUTTONDOWN]->unsubscribe(inventory);
			inventory->unsubscribeComponentButtons(events[SDL_MOUSEBUTTONDOWN]);
			inventory->unsubscribeComponentButtons(events[SDL_MOUSEMOTION]);

			drawables["shop"] = shop;

			if (shop->getSpecializedShopTitle() != "") {
				shop->subscribeComponentButtons(events[SDL_MOUSEBUTTONDOWN], false);
				shop->subscribeComponentButtons(events[SDL_MOUSEMOTION], false);
			}
			else {
				shop->subscribeComponentButtons(events[SDL_MOUSEBUTTONDOWN], true);
				shop->subscribeComponentButtons(events[SDL_MOUSEMOTION], true);

			}
		}
		else {
			drawables.erase("shop");

			if (shop->getSpecializedShopTitle() != "") {
				shop->unsubscribeComponentButtons(events[SDL_MOUSEBUTTONDOWN], false);
				shop->unsubscribeComponentButtons(events[SDL_MOUSEMOTION], false);
			}
			else {
				shop->unsubscribeComponentButtons(events[SDL_MOUSEBUTTONDOWN], true);
				shop->unsubscribeComponentButtons(events[SDL_MOUSEMOTION], true);

			}
		}
	}

	void changePlayer() {

        Component* blockExist = nullptr;
        Component* wheelExist = nullptr;
        for(auto it: *actualPlayer->getRobot()->getComponentList()) {
            if (it->getType() == ENERGYBLOCK)
                blockExist = it;
            if(it->getType() == STANDARDWHEEL)
                wheelExist = it;
        }
        if(blockExist and wheelExist){
            if (meshIt != robotComponents->end()) {
                (*meshIt)->setSelected(false);
            }
            meshIt = robotComponents->end();
            if (actualPlayer == player1) {
                addOrRemoveMeshWithButton();
                actualPlayer = player2;
                gamepadCheckBox = "player2GamePadCheckBox";
                robotComponents = actualPlayer->getRobot()->getComponentList();
                if (actualPlayer->getRobot()->getComponentList()->size() == 0) {
                    actualPlayer->getRobot()->addComponent(new FrameBloc(new Material(texturesId[GARAGE_TEXTURE_USED_METAL], 2.00, 2.00, 2.00, 2.00, 2.00, 2.00, 2.00), 0.0, 0.0, -25.0));
                    actualPlayer->getRobot()->getComponentTree()->add(actualPlayer->getRobot()->getComponentList()->front(), actualPlayer->getRobot()->getComponentList()->front()->getConnectors().size(),nullptr, 0, 0);
                }
                meshIt = robotComponents->end();
            }
            else{
                player1->getRobot()->setInitialPosition((*player1->getRobot()->getComponentList()->begin())->getPosition()); // position initiale du robot
                player2->getRobot()->setInitialPosition((*player2->getRobot()->getComponentList()->begin())->getPosition()); // position initiale du robot
                Scene::setScene(INGAME);
                player1->getInventory()->removeBoughtItemsCost();
                player2->getInventory()->removeBoughtItemsCost();
            }
            if (!actualPlayer->getInventory()) {
                actualPlayer->addInventory();
            }
            inventory = actualPlayer->getInventory();
            shop->setMoney(actualPlayer->getMoney());
            inventory->setShop(shop);
            ((LabelImageButton*)inventory->getDrawable("upButton"))->bindOnClick(inventoryUpPrototype);
            ((LabelImageButton*)inventory->getDrawable("downButton"))->bindOnClick(inventoryDownPrototype);

            if(drawables["inventory"])
                drawables["inventory"] = inventory;
            else
                drawables.erase("inventory");

            waitingKeyForLocomotionBinding = 0;
        }
	}

	/// \brief Affichage
	
	void draw() {

		// 3D
		glEnable(GL_LIGHTING);

		SDLGLContext::getInstance()->setPerspective(false);
		double deltaTRotationt= chronoDeltaT.getElapsedTime();
		if (chronoMoving.getElapsedTime() > 0.01) {
			chronoMoving.reset();

		if (SDLEvent::getInstance()->sdlEvent.type == SDL_MOUSEMOTION && rightMouseButtonDown) { // vérifie si la sourie bouge ET qu'un bouton de la sourie est appuyé
            ((Orbital*)camera)->rotateAroundTarget2(-(SDLEvent::getInstance()->sdlEvent.motion.xrel) * 0.01/*(deltaTRotationt)*/, SDLEvent::getInstance()->sdlEvent.motion.yrel * 0.01/*(deltaTRotationt)*/);

		}
		else if (SDLEvent::getInstance()->sdlEvent.type == SDL_MOUSEMOTION && leftMouseButtonDown) {
			
			if (meshIt != robotComponents->end()) {
				if ((*meshIt)->getSelected()) {
					if (!rotationMode) { // si on n'est pas en mode rotation
					
						Vector3d segment = (camera->getSide() * SDLEvent::getInstance()->sdlEvent.motion.xrel * -0.01 + (camera->getUp() * SDLEvent::getInstance()->sdlEvent.motion.yrel * 0.01)); // Le segment par lequel la position sera additionné
					
						//Detection temporairement dans le garage pour tester
						list<Mesh3a*> componentList;
						for(list<Component*>::iterator it = actualPlayer->getRobot()->getComponentList()->begin(); it != actualPlayer->getRobot()->getComponentList()->end(); it++){
							componentList.push_back((Mesh3a*)(*it));
						}
						Vector3d position = (*meshIt)->getPosition() + segment;
						/*if (segmentVisibility) {//aide visuelle
							segmentToDraw = Scene3d::totalCollisionSegment((*meshIt), componentList, segment);
							segmentToDraw = segmentToDraw * 20;
							segmentVertex[0] = position.x;
							segmentVertex[1] = position.y;
							segmentVertex[2] = position.z;

							segmentVertex[3] = position.x + segmentToDraw.x;
							segmentVertex[4] = position.y + segmentToDraw.y;
							segmentVertex[5] = position.z + segmentToDraw.z;

							segmentVertex[6] = position.x;
							segmentVertex[7] = position.y + 0.5;
							segmentVertex[8] = position.z;
						}*/
						
						//verifie si deplacement trop grand en x, y et z sur deux direction par axe 
						/*if ((position.x - (*meshIt)->getPosition().x) < -0.015) {
							position.x = (*meshIt)->getPosition().x - 0.015;
						}
						else if ((position.x - (*meshIt)->getPosition().x) > 0.015) {
							position.x = (*meshIt)->getPosition().x + 0.015;
						}

						if ((position.y - (*meshIt)->getPosition().y) < -0.015) {
							position.y = (*meshIt)->getPosition().y - 0.015;
						}
						else if ((position.y - (*meshIt)->getPosition().y) > 0.015) {
							position.y = (*meshIt)->getPosition().y + 0.015;
						}

						if ((position.z - (*meshIt)->getPosition().z) < -0.015) {
							position.z = (*meshIt)->getPosition().z - 0.015;
						}
						else if ((position.z - (*meshIt)->getPosition().z) > 0.015) {
							position.z = (*meshIt)->getPosition().z + 0.015;
						}*/

						(*meshIt)->setPosition(position.x, position.y, position.z);
					}
					else { // si on est en mode rotation
						if (xLocked) {
							(*meshIt)->getTransformationMatrix()->loadXRotation((SDLEvent::getInstance()->sdlEvent.motion.yrel + SDLEvent::getInstance()->sdlEvent.motion.xrel) * 0.01);
						}
						else if (yLocked) {
							(*meshIt)->getTransformationMatrix()->loadYRotation((SDLEvent::getInstance()->sdlEvent.motion.yrel + SDLEvent::getInstance()->sdlEvent.motion.xrel) * 0.01);
						}
						else if (zLocked) {
							(*meshIt)->getTransformationMatrix()->loadZRotation((SDLEvent::getInstance()->sdlEvent.motion.yrel + SDLEvent::getInstance()->sdlEvent.motion.xrel) * 0.01);
						}
						//(*meshIt)->getTransformationMatrix()->loadRotation((SDLEvent::getInstance()->sdlEvent.motion.yrel + SDLEvent::getInstance()->sdlEvent.motion.xrel) * 0.001, (camera->getFront() + (*meshIt)->getPosition()));
						(*meshIt)->transform();
						(*meshIt)->getTransformationMatrix()->loadIdentity();
					}

					//regarder s'il y a connection ou deconnection
					for (list<Component*>::iterator it = actualPlayer->getRobot()->getComponentList()->begin(); it != actualPlayer->getRobot()->getComponentList()->end(); it++) {
						for (int i = 0; i < (*it)->getConnectors().size(); i++) {//auto connectorsIt : (*it)->getConnectors()
							for (int j = 0; j < (*meshIt)->getConnectors().size(); j++) {
								double distance = abs(((*(*it)->getConnectors()[i]).getPosition() - (*(*meshIt)->getConnectors()[j]).getPosition()).getNorm());
								double distanceRequiredForSnap = 0.6;
								//connection
								if (distance < distanceRequiredForSnap and meshIt != it and !(*it)->getConnectors()[i]->getSelected()) {
									actualPlayer->getRobot()->getComponentTree()->add((Component*)(*meshIt), 1, (Component*)(*it), i, j);
									(*meshIt)->getConnectors()[j]->setSelected(true);
									(*it)->getConnectors()[i]->setSelected(true);

									//preparer l'assignement de touche
									if((*meshIt)->getType() != ENERGYBLOCK and (*meshIt)->getType() != YSNAP){
										if ((*meshIt)->getType() != NAILEDWHEEL and (*meshIt)->getType() != STANDARDWHEEL) {//tous les type de locomotion
											waitingKeyForBinding.push_back((Controlable*)(*meshIt));
											drawables["keyBindingPopUp"] = new DoubleLabel(300.0, 340.0, ResourceManager::get<Font*>("font25"), "Assignez une touche a cette composante: " + waitingKeyForBinding.back()->getName(), { 0, 0, 255, 255 }, 0.2, 2.0);
										}
										else {
											if((*meshIt)->getPosition().x < 0.0)
												((Locomotion*)(*meshIt))->setIsFrontLocomotion(true);
											
											if (waitingKeyForLocomotionBinding != 5) {//Pour savoir si la locomotion a deja ete assigner
												waitingKeyForLocomotionBinding = 1;
												drawables["keyBindingPopUp"] = new DoubleLabel(550.0, 340.0, ResourceManager::get<Font*>("font25"), "Appuyez sur RT", { 0, 0, 255, 255 }, 0.2, 2.0);
											}
										}
									}
									actualPlayer->getRobot()->updateLength();
									actualPlayer->getRobot()->updateWidth();
								}
								else
									if (actualPlayer->getRobot()->getComponentTree()->searchRecursive((*meshIt)) and (*meshIt != actualPlayer->getRobot()->getComponentTree()->getRootData())) {
										distance = abs(((*actualPlayer->getRobot()->getComponentTree()->searchRecursive((*meshIt))->parent->data->getConnectors()[actualPlayer->getRobot()->getComponentTree()->searchRecursive((*meshIt))->parentId]).getPosition() - (*(*meshIt)->getConnectors()[actualPlayer->getRobot()->getComponentTree()->searchRecursive((*meshIt))->idToParent]).getPosition()).getNorm());
										if (distance > distanceRequiredForSnap) {

											(*meshIt)->getConnectors()[actualPlayer->getRobot()->getComponentTree()->searchRecursive((*meshIt))->idToParent]->setSelected(false);
											actualPlayer->getRobot()->getComponentTree()->searchRecursive((*meshIt))->parent->data->getConnectors()[actualPlayer->getRobot()->getComponentTree()->searchRecursive((*meshIt))->parentId]->setSelected(false);


											actualPlayer->getRobot()->getComponentTree()->remove((Component*)(*meshIt));


											//enlever le pop up
											drawables.erase("keyBindingPopUp");
											//todo : regarder si un unbind est a faire...

											if ((*meshIt)->getType() == NAILEDWHEEL or (*meshIt)->getType() == STANDARDWHEEL) {
												if (waitingKeyForLocomotionBinding > 1) {
													//unbind
												}
												waitingKeyForLocomotionBinding = 0;
											}
										}
									}
							}

						}
					}

				}

			}
		}
		}


     ((Orbital*)camera)->zoom(mouseWheelAcceleration);
        mouseWheelAcceleration/=1.03;


		camera->applyView();

		atmoSphere->draw();

		actualPlayer->getRobot()->draw();

		/*if (segmentVisibility) {//aide visuelle
			glEnableClientState(GL_VERTEX_ARRAY);
			glVertexPointer(3, GL_DOUBLE, 0, segmentVertex);
			glDrawArrays(GL_TRIANGLES, 0, 3);

			glDisableClientState(GL_VERTEX_ARRAY);
		}*/

		/*for (list<Component*>::iterator it = robotComponents->begin(); it != robotComponents->end(); it++) {
			(*it)->draw();
		}*/

		// 2D
		SDLGLContext::getInstance()->setPerspective(true);
		glDisable(GL_LIGHTING);

		for (auto it : drawables) {
			if (it.second->getVisible()) {
				it.second->draw();
			}
		}
		Option::getInstance()->showFPS();

		glEnable(GL_LIGHTING);

        chronoDeltaT.reset();
    }

    /// \brief Notifications
	virtual void notification(){
		switch (SDLEvent::getInstance()->sdlEvent.type) {
			case SDL_CONTROLLERBUTTONDOWN:
				if(waitingKeyForBinding.size() > 0 and (waitingKeyForLocomotionBinding == 0  or waitingKeyForLocomotionBinding == 5)){
					actualPlayer->keyBind(SDLEvent::getInstance()->sdlEvent.cbutton.button, waitingKeyForBinding.back());
					waitingKeyForBinding.pop_back();

					drawables.erase("keyBindingPopUp");
					if(waitingKeyForBinding.size() > 0)
							drawables["keyBindingPopUp"] = new DoubleLabel(300.0, 340.0, ResourceManager::get<Font*>("font25"), "Assignez une touche a cette composante: " + waitingKeyForBinding.back()->getName(), { 0, 0, 255, 255 }, 0.2, 2.0);
				}
				break;
			case SDL_CONTROLLERAXISMOTION:
			    if(Option::getInstance()->getCheckBoxChecked(gamepadCheckBox)) {
					switch(waitingKeyForLocomotionBinding){
						case 1:
							//rt
							actualPlayer->keyBind(SDLEvent::getInstance()->sdlEvent.caxis.axis, nullptr, "goFoward");
							waitingKeyForLocomotionBinding++;
							((DoubleLabel*)drawables["keyBindingPopUp"])->setText("Appuyez sur LT", {0, 0, 255, 255});
							bindingChrono.reset();
							break;
						case 2:
							//lt
							if(bindingChrono.getElapsedTime()>0.4) {
								actualPlayer->keyBind(SDLEvent::getInstance()->sdlEvent.caxis.axis, nullptr, "goBackward");
								waitingKeyForLocomotionBinding++;
								((DoubleLabel *) drawables["keyBindingPopUp"])->setText("Utilisez votre joystick",{0, 0, 255, 255});
								bindingChrono.reset();
							}
							break;
						case 3:
							//axis
							if(bindingChrono.getElapsedTime()>0.4) {
								actualPlayer->keyBind(SDLEvent::getInstance()->sdlEvent.caxis.axis, nullptr, "turn");
								waitingKeyForLocomotionBinding = 5;//Valeur pour laquel les controles de la locomotion ont deja ete assigner
								if(waitingKeyForBinding.size() < 1)
									drawables.erase("keyBindingPopUp");
								else
									drawables["keyBindingPopUp"] = new DoubleLabel(300.0, 340.0, ResourceManager::get<Font*>("font25"), "Assignez une touche a cette composante: " + waitingKeyForBinding.back()->getName(), { 0, 0, 255, 255 }, 0.2, 2.0);
								bindingChrono.reset();
							}
							break;
					}
                }
				break;
			case SDL_MOUSEBUTTONDOWN:
				events[SDL_MOUSEBUTTONDOWN]->notify();
				if (SDLEvent::getInstance()->sdlEvent.button.button == SDL_BUTTON_RIGHT) {
					rightMouseButtonDown = true; // IMPORTANT sinon le mouvement de caméra avec la souris de fonctionne pas
				}
				else {
					leftMouseButtonDown = true;
				}
				break;
			case SDL_MOUSEBUTTONUP:
				if (SDLEvent::getInstance()->sdlEvent.button.button == SDL_BUTTON_RIGHT) {
					rightMouseButtonDown = false; // IMPORTANT sinon le mouvement de caméra avec la souris de fonctionne pas
				}
				else {
					leftMouseButtonDown = false;
				}
				break;
				case SDL_MOUSEWHEEL:
					mouseWheelAcceleration = SDLEvent::getInstance()->sdlEvent.wheel.y  * (chronoDeltaT.getElapsedTime()*50);
					chronoDeltaT.reset();	
					break;



			case SDL_KEYDOWN:
				if(waitingKeyForBinding.size() > 0 and (waitingKeyForLocomotionBinding == 0  or waitingKeyForLocomotionBinding == 5)){//binding pour clavier
					actualPlayer->keyBind(SDLEvent::getInstance()->sdlEvent.key.keysym.scancode, waitingKeyForBinding.back());
					waitingKeyForBinding.pop_back();
					if(waitingKeyForBinding.size() < 1)
						drawables.erase("keyBindingPopUp");
				}
                if(!Option::getInstance()->getCheckBoxChecked(gamepadCheckBox)) {
                    switch (waitingKeyForLocomotionBinding) {
                        case 1:
                            //rt
                            actualPlayer->keyBind(SDLEvent::getInstance()->sdlEvent.key.keysym.scancode, nullptr,
                                                  "goFoward");
                            waitingKeyForLocomotionBinding++;
                            ((DoubleLabel *) drawables["keyBindingPopUp"])->setText("Appuyez sur LT", {0, 0, 255, 255});
                            bindingChrono.reset();
                            break;
                        case 2:
                            //lt
                                actualPlayer->keyBind(SDLEvent::getInstance()->sdlEvent.key.keysym.scancode, nullptr,
                                                      "goBackward");
                                waitingKeyForLocomotionBinding++;
                                ((DoubleLabel *) drawables["keyBindingPopUp"])->setText("Utilisez votre bouton droite",
                                                                                        {0, 0, 255, 255});
                                bindingChrono.reset();
                            break;
                        case 3:
                            //axis
                                actualPlayer->keyBind(SDLEvent::getInstance()->sdlEvent.key.keysym.scancode, nullptr,
                                                      "turn");
                                waitingKeyForLocomotionBinding++;
                                ((DoubleLabel *) drawables["keyBindingPopUp"])->setText("Utilisez votre bouton gauche",
                                                                                        {0, 0, 255, 255});
                                bindingChrono.reset();
                            break;
                        case 4:
                                actualPlayer->keyBind(SDLEvent::getInstance()->sdlEvent.key.keysym.scancode, nullptr,
                                                      "turnOpposite");
                                waitingKeyForLocomotionBinding = 5;//Valeur pour laquel les controles de la locomotion ont deja ete assigner
                                drawables.erase("keyBindingPopUp");
                                if (waitingKeyForBinding.size() > 0)
                                    drawables["keyBindingPopUp"] = new DoubleLabel(300.0, 340.0, ResourceManager::get<Font*>("font25"),
                                                                                   "Assignez une touche a cette composante: " +
                                                                                   waitingKeyForBinding.back()->getName(),
                                                                                   {0, 0, 255, 255}, 0.2, 2.0);
                                bindingChrono.reset();
                            break;
                    }
                }//binding pour clavier

				if (SDLEvent::getInstance()->sdlEvent.key.keysym.scancode == SDL_SCANCODE_H) { // Sert a controller la visibilite des hitbox
					for (list<Component*>::iterator it = actualPlayer->getRobot()->getComponentList()->begin(); it != actualPlayer->getRobot()->getComponentList()->end(); it++) {
						(*it)->toggleHitBoxes();
					}
				}
				if (SDLEvent::getInstance()->sdlEvent.key.keysym.scancode == SDL_SCANCODE_A) { // Sert a controller la visibilite des axes
					for (list<Component*>::iterator it = actualPlayer->getRobot()->getComponentList()->begin(); it != actualPlayer->getRobot()->getComponentList()->end(); it++) {
						(*it)->toggleAxes();
					}
				}
				if (SDLEvent::getInstance()->sdlEvent.key.keysym.scancode == SDL_SCANCODE_F) { // Sert a controller la visibilite des axes
					for (list<Component*>::iterator it = actualPlayer->getRobot()->getComponentList()->begin(); it != actualPlayer->getRobot()->getComponentList()->end(); it++) {
						(*it)->toggleFront();
					}
				}
				if (SDLEvent::getInstance()->sdlEvent.key.keysym.scancode == SDL_SCANCODE_C) { // Sert a controller la visibilite du segment
					//segmentVisibility = !segmentVisibility;
				}
				if (SDLEvent::getInstance()->sdlEvent.key.keysym.scancode == SDL_SCANCODE_TAB) {
					if (meshIt != robotComponents->end()) {
						//events[SDL_KEYDOWN]->unsubscribe((Drawable*)(*meshIt));
						if ((*meshIt)->getSelected()) {
							(*meshIt)->setSelected(false);
						}
					}
					else {
						meshIt = robotComponents->begin();
						drawables["noSelected"]->setVisible(false);
					}
					meshIt++;
					if (meshIt != robotComponents->end()) {
						//events[SDL_KEYDOWN]->subscribe((Drawable*)(*meshIt));
						(*meshIt)->setSelected(true);
						
					}
					if (meshIt == robotComponents->end()) {
						drawables["noSelected"]->setVisible(true);
					}
				}
				if (SDLEvent::getInstance()->sdlEvent.key.keysym.scancode == SDL_SCANCODE_R) { // Sert à activer le mode rotation des objets
					rotationMode = !rotationMode;
					drawables["R"]->setVisible(!drawables["R"]->getVisible());
					drawables["movingMode"]->setVisible(!drawables["movingMode"]->getVisible());
					if (!yLocked && !zLocked && !xLocked && rotationMode) { // Sert à faire apparaître les conseils sur le bloquage des axes
						drawables["noAxe"]->setVisible(true);
						drawables["noAxe1"]->setVisible(true);
						drawables["noAxe2"]->setVisible(true);
					}
					else {
						drawables["noAxe"]->setVisible(false);
						drawables["noAxe1"]->setVisible(false);
						drawables["noAxe2"]->setVisible(false);
					}
				}
				if (SDLEvent::getInstance()->sdlEvent.key.keysym.scancode == SDL_SCANCODE_X) { // Sert à bloquer l'axe des X lors de la rotation
					if (!yLocked && !zLocked) {
						xLocked = !xLocked;
						drawables["X"]->setVisible(!drawables["X"]->getVisible());
						if (!yLocked && !zLocked && !xLocked) { // Sert à faire apparaître les conseils sur le bloquage des axes
							if (rotationMode) {
								drawables["noAxe"]->setVisible(true);
								drawables["noAxe1"]->setVisible(true);
								drawables["noAxe2"]->setVisible(true);
							}
						}
						else {
							drawables["noAxe"]->setVisible(false);
							drawables["noAxe1"]->setVisible(false);
							drawables["noAxe2"]->setVisible(false);
						}
					}
				}
				if (SDLEvent::getInstance()->sdlEvent.key.keysym.scancode == SDL_SCANCODE_Y) { // Sert à bloquer l'axe des Y lors de la rotation
					if (!xLocked && !zLocked) {
						yLocked = !yLocked;
						drawables["Y"]->setVisible(!drawables["Y"]->getVisible());
						if (!yLocked && !zLocked && !xLocked) { // Sert à faire apparaître les conseils sur le bloquage des axes
							if (rotationMode) {
								drawables["noAxe"]->setVisible(true);
								drawables["noAxe1"]->setVisible(true);
								drawables["noAxe2"]->setVisible(true);
							}
						}
						else {
							drawables["noAxe"]->setVisible(false);
							drawables["noAxe1"]->setVisible(false);
							drawables["noAxe2"]->setVisible(false);
						}
					}
				}
				if (SDLEvent::getInstance()->sdlEvent.key.keysym.scancode == SDL_SCANCODE_Z) { // Sert à bloquer l'axe des Z lors de la rotation
					if (!xLocked && !yLocked) {
						zLocked = !zLocked;
						drawables["Z"]->setVisible(!drawables["Z"]->getVisible());
						if (!yLocked && !zLocked && !xLocked) { // Sert à faire apparaître les conseils sur le bloquage des axes
							if (rotationMode) {
								drawables["noAxe"]->setVisible(true);
								drawables["noAxe1"]->setVisible(true);
								drawables["noAxe2"]->setVisible(true);
							}
						}
						else {
							drawables["noAxe"]->setVisible(false);
							drawables["noAxe1"]->setVisible(false);
							drawables["noAxe2"]->setVisible(false);
						}
					}
				}
				if (SDLEvent::getInstance()->sdlEvent.key.keysym.scancode == SDL_SCANCODE_U) { 
					if(drawables["update"]){
						for(int i = 0; i < inventory->getNumberOfComponentInInventory(); i++){
							if(((ImageButton*)inventory->getDrawable(to_string(i)))->getMouseOn()){
								ComponentButton* toUpdate = ((ComponentButton*)inventory->getDrawable(to_string(i)));
								double levelUpPrice = (toUpdate->getComponent()->getPrice() / 2.0);
								if(actualPlayer->getMoney() - levelUpPrice >= 0.0){
									toUpdate->getComponent()->levelUp();
									actualPlayer->setMoney(actualPlayer->getMoney() - levelUpPrice);
									inventory->addComponent(nullptr, levelUpPrice);//seulement pour les label
								}
							}
						}
					}else{
						drawables.erase("update");
					}
				}


				break;
			case SDL_MOUSEMOTION:
				events[SDL_MOUSEMOTION]->notify();
				ComponentButton* createUpdateLabel = nullptr;
				if(drawables["inventory"]){
					for(int i = 0; i < inventory->getNumberOfComponentInInventory(); i++){
						if(((ImageButton*)inventory->getDrawable(to_string(i)))->getMouseOn()){
							if(((ComponentButton*)inventory->getDrawable(to_string(i)))->getComponent()->getName() != "Structures" and ((ComponentButton*)inventory->getDrawable(to_string(i)))->getComponent()->getName() != "y snap")
								createUpdateLabel = ((ComponentButton*)inventory->getDrawable(to_string(i)));
						}
					}
				}else{
					drawables.erase("inventory");
				}

				if(createUpdateLabel){
			 		drawables["update"] = new DoubleLabel(740, 640, ResourceManager::get<Font*>("font20"), "(U)pdate: " + to_string((int)(createUpdateLabel->getComponent()->getPrice() / 2.0)) + " LL", { 0, 0, 255, 255 }, 0.2, 4.0);
				}else{
					drawables["update"];
					drawables.erase("update");
				}
				break;
		}

		if(componentButtonToUnsubscribeLaterId != NO_COMPONENT_REMOVED){
			events[SDL_MOUSEBUTTONDOWN]->unsubscribe(inventory->getDrawable(to_string(componentButtonToUnsubscribeLaterId)));
			events[SDL_MOUSEMOTION]->unsubscribe(inventory->getDrawable(to_string(componentButtonToUnsubscribeLaterId)));

			events[SDL_MOUSEBUTTONDOWN]->unsubscribe(inventory->getDrawable(to_string(componentButtonToUnsubscribeLaterId) + "xButton"));
			inventory->removeComponent(componentButtonToUnsubscribeLaterId);
			meshIt = robotComponents->end(); //pour eviter que le programme plante au prochain changment de piece
			componentButtonToUnsubscribeLaterId = NO_COMPONENT_REMOVED;
		}

        if(goToSpecializedShop){
            shop->unsubscribeComponentButtons(events[SDL_MOUSEBUTTONDOWN], true);
            shop->unsubscribeComponentButtons(events[SDL_MOUSEMOTION], true);

            goToSpecializedShop = false;
            events[SDL_MOUSEBUTTONDOWN]->subscribe(shop->getDrawable("SpecializedShopTitle"));
            events[SDL_MOUSEMOTION]->subscribe(shop->getDrawable("SpecializedShopTitle"));

            shop->subscribeComponentButtons(events[SDL_MOUSEBUTTONDOWN], false);
            shop->subscribeComponentButtons(events[SDL_MOUSEMOTION], false);

            string generalComponentName = shop->getSpecializedShopTitle();
            list<Component*>* components = shop->getGeneralComponentList(generalComponentName);
            unsigned int componentIt = 1;
            for(list<Component*>::iterator it = components->begin(); it != components->end(); it++){
                ((LabelImageButton*)shop->getDrawable(generalComponentName + to_string(componentIt)))->bindOnClick(specializedShopButtonsPrototype);
                componentIt++;
            }
        }
        if(goToShop){
            events[SDL_MOUSEBUTTONDOWN]->unsubscribe(shop->getDrawable("SpecializedShopTitle"));
            shop->unsubscribeComponentButtons(events[SDL_MOUSEBUTTONDOWN], false);
            shop->unsubscribeComponentButtons(events[SDL_MOUSEMOTION], false);

            shop->changePanelState(shop->getSpecializedShopTitle(), false);
            shop->subscribeComponentButtons(events[SDL_MOUSEBUTTONDOWN], true);
            shop->subscribeComponentButtons(events[SDL_MOUSEMOTION], true);

            goToShop = false;
        }
		if(unsubscribeFixComponentButtons){
			int i = 1;
			for(auto it : *shop->getGeneralComponentList("Fix")){
				events[SDL_MOUSEBUTTONDOWN]->unsubscribe(shop->getDrawable("Fix" + to_string(i)));
				events[SDL_MOUSEMOTION]->unsubscribe(shop->getDrawable("Fix" + to_string(i)));
				i++;
			}
			shop->refreshFixableComponents(actualPlayer->getInventory()->getComponentList());

			i = 1;
			for(auto it : *shop->getGeneralComponentList("Fix")){
				events[SDL_MOUSEBUTTONDOWN]->subscribe(shop->getDrawable("Fix" + to_string(i)));
				events[SDL_MOUSEMOTION]->subscribe(shop->getDrawable("Fix" + to_string(i)));
				((Button*)shop->getDrawable("Fix" + to_string(i)))->bindOnClick(specializedShopButtonsPrototype);
				i++;
			}
			unsubscribeFixComponentButtons = false;
		}
    }

     string getType(){
	    return "garage";
	}
};
/// \brief change de scene pour aller vers le mainMenu
void mainMenuPrototype(){
    Scene::setScene(MAINMENU);
}
/// \brief change de scene pour tester son vehicule
void tryPrototype(){
	Scene::setScene(ROBOTTESTINGSPACE);
}
/// \brief ouvre le magasin
inline void buyPiecesPrototype(){
    ((Garage*)Drawable::getCurrentDrawable())->changeShopState();
}
/// \brief ouvre l'inventaire
inline void piecesBoughtPrototype(){
    ((Garage*)Drawable::getCurrentDrawable())->changeInventoryState();
}
/// \brief ouvre l'inventaire de vehicules pre-fabriquer
void preSetPrototype(){
	Scene::setScene(CAMERATESTINGSPACE);
}
/// \brief changer de scene pour la suite du jeu
void readyPrototype(){
	((Garage*)Drawable::getCurrentDrawable())->changePlayer();
}
/// \brief Ouvrir la sous boutique des moteurs
inline void frameBlocPrototype(){
    ((Garage*)Drawable::getCurrentDrawable())->openSpecializedShop(1);
}
/// \brief Ouvrir la sous boutique des locomotions
inline void locomotionsPrototype(){
    ((Garage*)Drawable::getCurrentDrawable())->openSpecializedShop(2);    
}
/// \brief Ouvrir la sous boutique des armes
inline void weaponsPrototype(){
    ((Garage*)Drawable::getCurrentDrawable())->openSpecializedShop(3);
}
/// \brief Ouvrir la sous boutique des outils
inline void toolPrototype(){
	((Garage*)Drawable::getCurrentDrawable())->openSpecializedShop(4);
}
/// \brief Ouvrir le menu de reparation
inline void fixPrototype(){
	((Garage*)Drawable::getCurrentDrawable())->openSpecializedShop(5);
}
/// \brief Revenir a la boutique depuis la sous boutique
inline void goBackTitlePrototype(){
    ((Garage*)Drawable::getCurrentDrawable())->openShopFromSpecializedShop();
}
/// \brief Ajouter la composante selon le bouton cliquer
void specializedShopButtonsPrototype(){
    ((Garage*)Drawable::getCurrentDrawable())->addComponentToInventory();
}
/// \brief Ajouter la composante dans la liste de mesh
void inventoryButtonsPrototype(){
    ((Garage*)Drawable::getCurrentDrawable())->addOrRemoveMeshWithButton();    
}
/// \brief Defiler l'inventaire vers le bas
void inventoryDownPrototype(){
    ((Garage*)Drawable::getCurrentDrawable())->scrollInventory(false);
}
/// \brief Defiler l'inventaire vers le haut
void inventoryUpPrototype(){
    ((Garage*)Drawable::getCurrentDrawable())->scrollInventory(true);
}
/// \brief Retire une composante de l'inventaire
void removeComponentFromInventoryPrototype(){
    ((Garage*)Drawable::getCurrentDrawable())->removeComponentFromInventory();
}

/// \brief Change le materiau des composantes
void changeMaterialPrototype(){
	((Garage*)Drawable::getCurrentDrawable())->changeMaterial();
}

void buyRobotPrototype() {
	((Garage*)Drawable::getCurrentDrawable())->addCompleteRobotPart("Robot Complet");
}

Player* Garage::actualPlayer = Scene3d::player2;

#endif


