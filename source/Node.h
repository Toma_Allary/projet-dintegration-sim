/// \file Node.h
/// \brief Node qui est utiliser dans Ntree.h
#ifndef NODE_H
#define NODE_H

#include <map>
using namespace std;

template <typename T>
/// \class Node
/// \brief Node de l'arbre Ntree
///
/// Node
///
class Node {
public:
	T data;
	Node<T>* parent;
	map<int, Node<T>* > next;
	unsigned int linksCount;
	unsigned int idToParent;
	unsigned int parentId;


	/// \Constructeur
	Node(T d, unsigned int size, Node<T>* parent, unsigned parentId, unsigned idToParent) {
		this->data = d;
		this->parent = parent;
		for (int i = 0; i < size; i++) {
			next[i] = nullptr;
		}
		linksCount = size;
		this->idToParent = idToParent;
		this->parentId = parentId;
	}

	~Node() {
		for (int i = 0; i < linksCount; i++) {
			if (next[i]) {
				delete next[i];
				next[i] = nullptr;
			}
		}
		if (parent) {
			delete parent;
			parent = nullptr;
		}
	}
};
#endif