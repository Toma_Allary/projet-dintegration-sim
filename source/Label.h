/// \file Label.h
/// \brief Étiquette

#ifndef LABEL_H
#define LABEL_H

#include <string>
#include "VisualComponent.h"
#include "Font.h"

using namespace std;

/// \class Label
/// \brief Étiquette
///
/// Étiquettes.
///
class Label : public VisualComponent {
private:
	Font* font; ///< Police de caractères.
	string text;///< Texte de base.

public:
	/// \brief Affectation du texte.
	void setText(const string& text, const SDL_Color& color) {
		if (font->getResource()) {
			glDeleteTextures(1, &textureId);
			glGenTextures(1, &textureId);

			glBindTexture(GL_TEXTURE_2D, textureId);

			SDL_Surface* surface = TTF_RenderUTF8_Blended((TTF_Font*)font->getResource(), text.c_str(), color);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, surface->w, surface->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, surface->pixels);
			width = surface->w;	height = surface->h;
			SDL_FreeSurface(surface);

			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		}
	}

	/// \brief Constructeur.
	/// \param x Position sur l'axe des x.
	/// \param y Position sur l'axe des y.
	/// \param width Largeur.
	/// \param height Hauteur.
	/// \param font Police de caractères.
	/// \param text Texte.
	/// \param color Couleur.
	Label(const double& x, const double& y, Font* font, const string& text, const SDL_Color& color, const double& layer = 0.0) : VisualComponent(x, y, 0.0, 0.0, layer) {
		this->font = font;
		this->text = text;
		setText(text, color);
		setPosition(x, y);
	}

	/// \brief Destructeur.
	~Label() {
		glDeleteTextures(1, &textureId);
	}

	const string& getText() {
		return text;
	}

	/// \brief Affichage.
	void draw() {
		glBindTexture(GL_TEXTURE_2D, textureId);

		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);

		glVertexPointer(3, GL_DOUBLE, 0, vertices);
		glTexCoordPointer(2, GL_DOUBLE, 0, texCoords);
		
		glDrawArrays(GL_QUADS, 0, 4);

		glDisableClientState(GL_VERTEX_ARRAY);
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	}

	void notification() {}

	string getType(){
		return "Label";
	}
};

#endif