/// \file Image.h
/// \brief Image
#ifndef IMAGE_H
#define IMAGE_H

#include "VisualComponent.h"

/// \class Image
/// \brief Image
///
/// Contrôle visuel image.
///
class Image : public VisualComponent{
private:
  unsigned int textureId; ///< Identificateur de texture.
  double vertices[12]; ///< Sommets du rectangle.
	double texCoords[8]; ///< Coordonnees de texture.

public:
    /// \brief Constructeur
    /// /param textureId Identificateur de texture.
    /// \param x Position sur l'axe des x.
    /// \param y Position sur l'axe des y.
    /// \param height Hauteur.
    /// \param width Largeur.
    Image(const unsigned int& textureId = 0, const double& x = 0.0, const double& y = 0.0, const unsigned int& width = 0.0, const unsigned int& height = 0.0, const double& layer = 0.0) : VisualComponent(x, y, width, height, layer) {
      this->textureId = textureId;

		  vertices[0] = vertices[9] = x;
		  vertices[1] = vertices[4] = y;
		  vertices[3] = vertices[6] = x + width;
		  vertices[7] = vertices[10] = y + height;
		  vertices[2] = vertices[5] = vertices[8] = vertices[11] = 0.0;

		  texCoords[0] = texCoords[1] = texCoords[3] = texCoords[6] = texCoords[7] = 0.0;
		  texCoords[2] = texCoords[4] = texCoords[5] = texCoords[7] = 1.0;
    }

    /// \brief Affichage
    void draw(){
		glBindTexture(GL_TEXTURE_2D, textureId);

		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);

		glVertexPointer(3, GL_DOUBLE, 0, vertices);
		glTexCoordPointer(2, GL_DOUBLE, 0, texCoords);
		
		glDrawArrays(GL_QUADS, 0, 4);

		glDisableClientState(GL_VERTEX_ARRAY);
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    }

    void notification() {

    }

    inline void setRepeat(const double& n) {
      texCoords[2] = texCoords[4] = texCoords[5] = texCoords[7] = n;
    }

	string getType(){
		return "Image";
	}
};

#endif