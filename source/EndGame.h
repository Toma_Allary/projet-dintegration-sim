/// \file EndGame.h
/// \brief fin de partie
#ifndef ENDGAME_H
#define ENDGAME_H

#include <list>
#include "Scene.h"
#include "LabelImageButton.h"
#include "Label.h"
#include "Image.h"
#include "ImageButton.h"

void quitGame();
void toSettings();
void toGarage();
void outOfProgram();

/// \class End Game
/// \brief Fin de partie
/// 
/// Pour partir une nouvelle manche ou retoruner au garage
///
using namespace std;
class EndGame : public Scene3d {
private:
	bool* isRunning;///< Programme en ex�cution
	unsigned int buttonBackgroundId;
	unsigned int arrBackId;
public:
	/// \brief Construcuteur
	EndGame(bool* isRunning) {
		buttonBackgroundId = SDLGLContext::getInstance()->loadTexture("Textures/buttonBackground.png");
		arrBackId = SDLGLContext::getInstance()->loadTexture("Textures/StatsTab.png");

		drawables["zlines"] = new Image(arrBackId, 100, 150, 1000, 300);

		drawables["returnToGarageButton"] = new LabelImageButton(buttonBackgroundId, ResourceManager::get<Font*>("font30"), { 255, 255, 255, 255 }, "Vers le garage", 800, 500, 300, 75);
		drawables["quitMatchButton"] = new LabelImageButton(buttonBackgroundId, ResourceManager::get<Font*>("font15"), { 255, 255, 255, 255 }, "Retour au menu principal", 475, 500, 300, 75);
		drawables["quitGameButton"] = new LabelImageButton(buttonBackgroundId, ResourceManager::get<Font*>("font15"), { 255, 255, 255, 255 }, "Quitter le jeu", 150, 500, 300, 75);
		drawables["settingsButton"] = new ImageButton(SDLGLContext::getInstance()->loadTexture("Textures/MenuPrincipal_Parametres.png"), 1100, 600, 75, 75);

		drawables["Title"] = new Label(470, 50, ResourceManager::get<Font*>("font30"), "Fin de la manche ", { 150, 23, 69, 255 });
		drawables["Winner"] = new Label(300, 90, ResourceManager::get<Font*>("font30"), "Le joueur " + to_string(Scene::lastWinner) + " viens de gagner la derniere manche", { 0, 0, 100, 255 });
		
		((Button*)drawables["returnToGarageButton"])->bindOnClick(toGarage);
		((Button*)drawables["quitMatchButton"])->bindOnClick(quitGame);
		((Button*)drawables["settingsButton"])->bindOnClick(toSettings);
		((Button*)drawables["quitGameButton"])->bindOnClick(outOfProgram);

		double a = player1->getHealthBeforeGame();
		double b = player1->getRobot()->getLife();
		drawables["damageI1L"] = new Label(300, 250, ResourceManager::get<Font*>("font30"), to_string((int)player1->getDamageDealt()), { 255, 255, 255, 255 });
		drawables["damageR1L"] = new Label(511, 250, ResourceManager::get<Font*>("font30"), to_string((int)(player1->getHealthBeforeGame() - player1->getRobot()->getLife())), { 255, 255, 255, 255 });
		drawables["scraps1L"] = new Label(723, 250, ResourceManager::get<Font*>("font30"), to_string((int)player1->pickedUpScrapValue), { 255, 255, 255, 255 });
		drawables["victories1L"] = new Label(988, 250, ResourceManager::get<Font*>("font30"), to_string(player1->getVictories()), { 255, 255, 255, 255 });
		drawables["damageI2L"] = new Label(300, 370, ResourceManager::get<Font*>("font30"), to_string((int)player2->getDamageDealt()), { 255, 255, 255, 255 });
		drawables["damageR2L"] = new Label(511, 370, ResourceManager::get<Font*>("font30"), to_string((int)(player2->getHealthBeforeGame() - player2->getRobot()->getLife())), { 255, 255, 255, 255 });
		drawables["scraps2L"] = new Label(723, 370, ResourceManager::get<Font*>("font30"), to_string((int)player2->pickedUpScrapValue), { 255, 255, 255, 255 });
		drawables["victories2L"] = new Label(988, 370, ResourceManager::get<Font*>("font30"), to_string(player2->getVictories()), { 255, 255, 255, 255 });
		currentDrawable = this;

		events[SDL_MOUSEBUTTONDOWN] = new Observable();
		events[SDL_MOUSEBUTTONDOWN]->subscribe(drawables["returnToGarageButton"]);
		events[SDL_MOUSEBUTTONDOWN]->subscribe(drawables["quitMatchButton"]);
		events[SDL_MOUSEBUTTONDOWN]->subscribe(drawables["settingsButton"]);
		events[SDL_MOUSEBUTTONDOWN]->subscribe(drawables["quitGameButton"]);
		this->isRunning = isRunning;

		//reset des statistiques de partie et de l'energie
		((EnergyBlock*)player1->getRobot()->getEnergyBlock())->refillEnergy();		
		((EnergyBlock*)player2->getRobot()->getEnergyBlock())->refillEnergy();
		player1->getRobot()->disableAllComponent();
		player2->getRobot()->disableAllComponent();
		player1->getRobot()->resetEnergyBlockHealth();
		player2->getRobot()->resetEnergyBlockHealth();

		player1->resetDamageDealt();
		player2->resetDamageDealt();
	}
	

	~EndGame() {
		for (auto it : events) delete it.second;
		for (auto it : drawables) delete it.second;

		glDeleteTextures(1, &buttonBackgroundId);
		glDeleteTextures(1, &arrBackId);
	}
		

	/// \brief Affichage
	void draw(){
		SDLGLContext::getInstance()->setPerspective(true);
		for (auto it : drawables)
			it.second->draw();
		Option::getInstance()->showFPS();
	}

	string getType() {
		return "End of Game";
	}

	/// \brief Notifications
	virtual void notification() {
		switch (SDLEvent::getInstance()->sdlEvent.type) {
		case SDL_MOUSEBUTTONDOWN:
			events[SDL_MOUSEBUTTONDOWN]->notify();
			break;
		}
	}

	/// \brief Quitter la fenetre
	void quit() {
		*isRunning = false;
	}
};
inline void quitGame() {
	Scene::setScene(MAINMENU);
}

inline void toSettings(){
	Scene::setScene(OPTIONMENU);
}

inline void toGarage() {
	Scene::setScene(GARAGE);
}

void outOfProgram() {
	EndGame* endgame = (EndGame*)Drawable::getCurrentDrawable();
	endgame->quit();
}

#endif 
