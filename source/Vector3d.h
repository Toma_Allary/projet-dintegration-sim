/// \file Vector3d.h
/// \brief contient la classe Vector3d
#ifndef VECTOR3D_H
#define VECTOR3D_H
#include<cmath>

/// \class Vector3d
/// \brief Vecteur à 3 composantes en réels.
///
/// Vecteur à 3 composantes réels.
///
class Vector3d {
public:
	double x, y, z; ///< Composantes.

	/// \brief Constructeur.
	/// \param x Composante x.
	/// \param y Composante y.
	/// \param z Composante z.
	Vector3d(const double& x = 0.0, const double& y = 0.0, const double& z = 0.0) {
		this->x = x;
		this->y = y;
		this->z = z;
	}

	/// \brief Constructeur de copie.
	Vector3d(const Vector3d& v) {
		x = v.x;
		y = v.y;
		z = v.z;
	}

	/// \brief Obtention de la norme.
	/// \return Norme.
	inline double getNorm() const {
		return sqrt(x * x + y * y + z * z);
	}

	void set(const double& x, const double & y, const double & z) {
		this->x = x;
		this->y = y;
		this->z = z;
	}

	/// \brief Rendre le vecteur unitaire.
	void normalize() {
	  double norm;
	  if (norm = sqrt(x * x + y * y + z * z)) {
      x = x / norm;
      y = y / norm;
      z = z / norm;
    }
	}

	/// \brief Soustraction.
	/// \param v Vecteur.
	/// \return Vecteur résultant.
	Vector3d operator-(const Vector3d& v) const {
		return Vector3d(x - v.x, y - v.y, z - v.z);
	}

	/// \brief Addition.
	/// \param v Vecteur.
	/// \return Vecteur résultant.
	Vector3d operator+(const Vector3d& v) const {
		return Vector3d(x + v.x, y + v.y, z + v.z);
	}

	/// \brief Multiplication par un scalaire.
	/// \param s Scalaire.
	/// \return Vecteur résultant.
	Vector3d operator*(const double& s) const {
		return Vector3d(x * s, y * s, z * s);
	}

	/// \brief Division par un scalaire.
	/// \param s Scalaire.
	/// \return Vecteur résultant.
	Vector3d operator/(const double& s) const {
		return Vector3d(x / s, y / s, z / s);
	}

	/// \brief Produit vectoriel.
	/// \param v Vecteur.
	/// \return Vecteur résultant.
	Vector3d operator%(const Vector3d& v) const {
		return Vector3d((y * v.z) - (z * v.y), (z * v.x) - (x * v.z), (x * v.y) - (y * v.x));
	}
	/// \brief Produit scalaire.
	/// \param v Vecteur.
	/// \return scalaire résultant.
	double dotProduct(const Vector3d& v) const {
		return (x * v.x) + (y * v.y) + (z * v.z);
	}
	 /// \brief Distance entre deux points
	/// \param v Vecteur
	/// \return Distance entre les deux points
	double distanceBetween(const Vector3d& v) const {
		return sqrt(((x - v.x) * (x - v.x)) + ((y - v.y) * (y - v.y)) + ((z - v.z) * (z - v.z)));
	}
};

#endif
