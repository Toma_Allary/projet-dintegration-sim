/// \file CameraTestingSpace.h
/// \brief test de camerA
#ifndef CAMERATESTINGSPACE_H
#define CAMERATESTINGSPACE_H

#define GAMEPAD_MAX 2
#define GAMEPAD_BUTTON_A 0
#define GAMEPAD_BUTTON_B 1
#define GAMEPAD_BUTTON_X 2
#define GAMEPAD_BUTTON_Y 3
#define GAMEPAD_BUTTON_LS 4
#define GAMEPAD_BUTTON_RS 5
#define GAMEPAD_BUTTON_START 6
#define GAMEPAD_LAXIS 7
#define GAMEPAD_LT 8
#define GAMEPAD_RT 9
#define GAMEPAD_LTDOWN 10
#define GAMEPAD_RTDOWN 11

#include "Scene.h"
#include "Image.h"
#include "DoubleLabel.h"
#include "LabelImageButton.h"
#include "libSDL2.h"
#include"Component.h"
#include"FrameBloc.h"
#include"Robot.h"
#include "DynamicCamera.h"
#include"Chrono.h"
#include"Observable.h"
#include "Scene3d.h"

/// \class CameraTestingSpace
/// \brief ecran de test de v�hicule avant de commencer
///
/// ecran de test de v�hicule avant de commencer
///
class CameraTestingSpace : public Scene3d {
private:
	unsigned int buttonBackgroundId,
	menuBackgroundId,
	overGarageBackgroundId;
	FrameBloc* frame;
    FrameBloc* frame2;
	Robot* robot1;
    Robot* robot2;
	DynamicCamera* camera;
	bool rightMouseButtonDown;
	bool upkey, downkey, rightkey, leftkey;
	Chrono* chrono;
	bool a;
	unsigned int atmoSphereId;
	Mesh3a* atmoSphere;

public:

	CameraTestingSpace() {
		a = false;
		buttonBackgroundId = SDLGLContext::getInstance()->loadTexture("Textures/buttonBackground.png");
		overGarageBackgroundId = SDLGLContext::getInstance()->loadTexture("Textures/_buttonBackground.png");

        Material* materiel = new Material(SDLGLContext::getInstance()->loadTexture("Textures/usedMetal.png"), 2.00, 2.00, 2.00, 2.00, 2.00, 2.00, 2.00);
		
		/*robot1 = new Robot("Paul");
		 frame = new FrameBloc(materiel, 3.0, 0.0, -25.0);
		 robot1->addComponent(frame);*/

        robot2 = new Robot("pas Paul");
		 frame2 = new FrameBloc(materiel, 10.0, 0.0, -25.0);
		 robot2->addComponent(frame2);

		 rightMouseButtonDown = rightkey=leftkey=downkey=upkey = false;

		 drawables["garageButton"] = new LabelImageButton(buttonBackgroundId, ResourceManager::get<Font*>("font12"), { 255, 255, 255, 255 }, "Retour au Garage", 0, 0, 149, 30, 0.1, overGarageBackgroundId);
		 ((Button*)drawables["garageButton"])->bindOnClick(garagePrototype);
		 events[SDL_MOUSEBUTTONDOWN] = new Observable();
		 events[SDL_MOUSEBUTTONDOWN]->subscribe(drawables["garageButton"]);
		 events[SDL_MOUSEMOTION] = new Observable();
		 events[SDL_MOUSEMOTION]->subscribe(drawables["garageButton"]);

		 chrono = new Chrono();

		camera = new DynamicCamera(Vector3d(3.0, 10.0, -50.0), Vector3d(-10.0, 5.0, -25.0), Vector3d(0.0, 1.0, 0.0), player1->getRobot(), robot2);

		atmoSphereId = SDLGLContext::getInstance()->loadTexture("Textures/garageAtmoSphere.png");
	
		atmoSphere = new Mesh3a(atmoSphereId, 0.0, 0.0, -25.0, "Objets/garageAtmoSphere.obj");
		atmoSphere->getTransformationMatrix()->setScale(5);
		atmoSphere->transform();
	}

	~CameraTestingSpace() {
		glDeleteTextures(1, &buttonBackgroundId);
		glDeleteTextures(1, &menuBackgroundId);
		glDeleteTextures(1, &overGarageBackgroundId);
		delete camera;

		while (meshes.size()) {
			Mesh3a* toDelete = meshes.back();
			meshes.pop_back();
			delete toDelete;
		}

		for (auto it : events)
			delete it.second;

		for (auto it : drawables)
			delete it.second;
	}

	void draw() {
		// 3D
		glEnable(GL_LIGHTING);

		SDLGLContext::getInstance()->setPerspective(false);

        camera->applyDynamicView();

		atmoSphere->draw();

		const unsigned int gamePadNum = SDL_NumJoysticks();
		if (gamePadNum) {
			SDL_GameController* gamePads[GAMEPAD_MAX];

			short gamePadInput[] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

			for (unsigned int i = 0; i < gamePadNum; i++)
				gamePads[i] = SDL_GameControllerOpen(i);
		}
		
		switch (SDLEvent::getInstance()->sdlEvent.type) {
		case SDL_KEYDOWN:
			switch (SDLEvent::getInstance()->sdlEvent.key.keysym.sym) {
			case SDLK_UP:
				upkey = true;
				break;

			case SDLK_DOWN:
				downkey = true;
				break;

			case SDLK_RIGHT:
				rightkey = true;
				a = true;
				break;
			case SDLK_LEFT:
				leftkey = true;
				a = true;
				break;
			}
			break;

		case SDL_KEYUP:
			switch (SDLEvent::getInstance()->sdlEvent.key.keysym.sym) {
			case SDLK_UP:
				upkey = false;
				break;

			case SDLK_DOWN:
				downkey = false;
				break;

			case SDLK_RIGHT:
				rightkey = false;
				a = false;
				//robot1->changeDirectionBool();
				break;

			case SDLK_LEFT:
				leftkey = false;
				a = false;
				//robot1->changeDirectionBool();
				break;
				//robot1->free(chrono->getElapsedTime());
			}
			
			break;
		case SDL_CONTROLLERAXISMOTION:
			short gamePadAxisValue = SDLEvent::getInstance()->sdlEvent.caxis.value;
			switch (SDLEvent::getInstance()->sdlEvent.caxis.value) {
				case SDL_CONTROLLER_AXIS_LEFTX:
					//TODO
					break;
				case SDL_CONTROLLER_AXIS_LEFTY:
					//TODO
					break;

				case SDL_CONTROLLER_AXIS_TRIGGERLEFT:
					//TODO
					break;

				case SDL_CONTROLLER_AXIS_TRIGGERRIGHT:
					//TODO
					break;
			}
			break;
		}
		chrono->reset();
		if(upkey){
			Vector3d robotsPosit = (*robot2->getComponentList()->begin())->getPosition();
			(*robot2->getComponentList()->begin())->setPosition(robotsPosit.x, robotsPosit.y, robotsPosit.z + 0.1);
			//robotsPosit = (*robot1->getComponentList()->begin())->getPosition();
			//(*robot1->getComponentList()->begin())->setPosition(robotsPosit.x - 0.1, robotsPosit.y, robotsPosit.z);
		}
		if (downkey){
			Vector3d robotsPosit = (*robot2->getComponentList()->begin())->getPosition();
			(*robot2->getComponentList()->begin())->setPosition(robotsPosit.x, robotsPosit.y, robotsPosit.z - 0.1);
			//robotsPosit = (*robot1->getComponentList()->begin())->getPosition();
			//(*robot1->getComponentList()->begin())->setPosition(robotsPosit.x + 0.1, robotsPosit.y, robotsPosit.z);
		}
		if (rightkey){
			Vector3d robotsPosit = (*robot2->getComponentList()->begin())->getPosition();
			(*robot2->getComponentList()->begin())->setPosition(robotsPosit.x - 0.1, robotsPosit.y, robotsPosit.z);
			//robotsPosit = (*robot1->getComponentList()->begin())->getPosition();
			//(*robot1->getComponentList()->begin())->setPosition(robotsPosit.x, robotsPosit.y, robotsPosit.z - 0.1);
		}
		else
		if (leftkey){
			Vector3d robotsPosit = (*robot2->getComponentList()->begin())->getPosition();
			(*robot2->getComponentList()->begin())->setPosition(robotsPosit.x + 0.1, robotsPosit.y, robotsPosit.z);
			//robotsPosit = (*robot1->getComponentList()->begin())->getPosition();
			//(*robot1->getComponentList()->begin())->setPosition(robotsPosit.x, robotsPosit.y, robotsPosit.z + 0.1);
		}
		if (!upkey and !downkey)
			robot2->free(chrono->getElapsedTime());
		player1->getRobot()->draw();
        robot2->draw();

		// 2D
		SDLGLContext::getInstance()->setPerspective(true);
		glDisable(GL_LIGHTING);

		for (auto it : drawables)
			it.second->draw();

		glEnable(GL_LIGHTING);
	}

	void notification(){
		switch (SDLEvent::getInstance()->sdlEvent.type) {
		case SDL_MOUSEBUTTONDOWN:
			events[SDL_MOUSEBUTTONDOWN]->notify();
			if (SDLEvent::getInstance()->sdlEvent.button.button == SDL_BUTTON_RIGHT) {
				rightMouseButtonDown = true; // IMPORTANT sinon le mouvement de cam�ra avec la souris de fonctionne pas
			}
			break;
		case SDL_MOUSEBUTTONUP:
			if (SDLEvent::getInstance()->sdlEvent.button.button == SDL_BUTTON_RIGHT) {
				rightMouseButtonDown = false; // IMPORTANT sinon le mouvement de cam�ra avec la souris de fonctionne pas
			}
	
			break;
		case SDL_MOUSEMOTION:
			events[SDL_MOUSEMOTION]->notify();
			break;
		
		}


	}

	string getType() {
		return "RobotTestingSpace";
		
	}

	};
#endif