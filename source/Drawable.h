/// \file Drawable.h
/// \brief Affichable
#ifndef DRAWABLE_H
#define DRAWABLE_H

#include "Observer.h"

/// \class Drawable
/// \brief Affichable
///
/// Generalisation de tout ce qui est affichable.
///
class Drawable : public Observer {
protected:
    bool visible;///< savoir s'il doit etre afficher
    static Drawable* currentDrawable;///< pointeur de l'affichable actuelle
public:
    /// \brief Constructeur
    Drawable(){
        visible = true;
    }

    /// \brief Destructeur
    virtual ~Drawable() {}

    /// \brief Affichage
    virtual void draw() = 0;
    /// \brief Notification
    virtual void notification() = 0;

    /// \brief Obtention d'un pointeur de l'affichable actuelle
	/// \return Pointeur de l'affichable
	static Drawable* getCurrentDrawable(){
		return currentDrawable;
	}
    /// \brief Changer la visibiliter
    void setVisible(bool visible){
        this->visible = visible;
    }

    /// \brief Obtenir la visibilite
    inline bool getVisible(){
        return visible;
    }
    virtual string getType() =0;
};

Drawable* Drawable::currentDrawable = nullptr;

#endif