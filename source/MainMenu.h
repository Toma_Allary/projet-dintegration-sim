/// \file MainMenu.h
/// \brief Menu principal
#ifndef MAINMENU_H
#define MAINMENU_H

#include "Scene.h"
#include "Image.h"
#include "DoubleLabel.h"
#include "LabelImageButton.h"

#define MAINMENU_TEXTURE_BACKGROUND 1
#define MAINMENU_TEXTURE_BUTTON_BACKGROUND 2
#define MAINMENU_TEXTURE_OVERBUTTON_BACKGROUND 3

void quitPrototype();
void startGamePrototype();
void optionPrototype();

/// \class MainMenu
/// \brief Menu principal.
///
/// Menu principal.
///
class MainMenu : public Scene {
private:
  bool* isRunning;///< Programme en exécution
  unsigned int texturesId[3];

public:
  /// \brief Construcuteur
  /// \param isRunning Programme en exécution
  MainMenu(bool* isRunning) {
		texturesId[MAINMENU_TEXTURE_BACKGROUND] = SDLGLContext::getInstance()->loadTexture("Textures/menuBackground.png");
		texturesId[MAINMENU_TEXTURE_BUTTON_BACKGROUND] = SDLGLContext::getInstance()->loadTexture("Textures/buttonBackground.png");
		texturesId[MAINMENU_TEXTURE_OVERBUTTON_BACKGROUND] = SDLGLContext::getInstance()->loadTexture("Textures/_buttonBackground.png");

		drawables["background"] = new Image(texturesId[MAINMENU_TEXTURE_BACKGROUND], 0.0, 0.0, 1200, 700);
		drawables["title"] = new DoubleLabel(200.0, 50.0, ResourceManager::get<Font*>("font60"), "ULT1M4TE R0B0T K0MB4T", { 0, 0, 255, 255 }, 0.2, 5.0);
    drawables["startButton"] = new LabelImageButton(texturesId[MAINMENU_TEXTURE_BUTTON_BACKGROUND], ResourceManager::get<Font*>("font32"), { 255, 255, 255, 255 }, "Commencer", 450, 190, 300, 100, 0.1, texturesId[MAINMENU_TEXTURE_OVERBUTTON_BACKGROUND]);
    drawables["optionButton"] = new LabelImageButton(texturesId[MAINMENU_TEXTURE_BUTTON_BACKGROUND], ResourceManager::get<Font*>("font32"), { 255, 255, 255, 255 }, "Options", 450, 300, 300, 100, 0.1, texturesId[MAINMENU_TEXTURE_OVERBUTTON_BACKGROUND]);
    drawables["quitButton"] = new LabelImageButton(texturesId[MAINMENU_TEXTURE_BUTTON_BACKGROUND], ResourceManager::get<Font*>("font32"), { 255, 255, 255, 255 }, "Quitter", 450, 410, 300, 100, 0.1, texturesId[MAINMENU_TEXTURE_OVERBUTTON_BACKGROUND]);

    ((Button*)drawables["startButton"])->bindOnClick(startGamePrototype);
    ((Button*)drawables["optionButton"])->bindOnClick(optionPrototype);
    ((Button*)drawables["quitButton"])->bindOnClick(quitPrototype);

		events[SDL_MOUSEMOTION] = new Observable();
		events[SDL_MOUSEBUTTONDOWN] = new Observable();
    events[SDL_MOUSEBUTTONDOWN]->subscribe(drawables["quitButton"]);
    events[SDL_MOUSEBUTTONDOWN]->subscribe(drawables["optionButton"]);
    events[SDL_MOUSEBUTTONDOWN]->subscribe(drawables["startButton"]);

    events[SDL_MOUSEMOTION]->subscribe(drawables["quitButton"]);
    events[SDL_MOUSEMOTION]->subscribe(drawables["optionButton"]);
    events[SDL_MOUSEMOTION]->subscribe(drawables["startButton"]);

    currentDrawable = this;
    this->isRunning = isRunning;
  }

  ~MainMenu() {
    for (auto it : events) delete it.second;
    for (auto it : drawables) delete it.second;

    glDeleteTextures(3, texturesId);
  }
    
  /// \brief Quitter la fenetre
  void quit() {
    *isRunning = false;
  }

  inline string getType() {
    return "MainMenu";
  }

  /// \brief Affichage
  void draw() {
    // 3D
		SDLGLContext::getInstance()->setPerspective(false);
		for (auto it : meshes)
			it->draw();

		// 2D
		SDLGLContext::getInstance()->setPerspective(true);
		glDisable(GL_LIGHTING);

		for(auto it : drawables)
      it.second->draw();

		Option::getInstance()->showFPS();
		glEnable(GL_LIGHTING);
  }

  /// \brief Notifications
	virtual void notification(){
	  if (events[SDLEvent::getInstance()->sdlEvent.type])
		  events[SDLEvent::getInstance()->sdlEvent.type]->notify();
	}
};

/// \brief Prototype de fonction pour le bouton quitter
void quitPrototype() {
  MainMenu* mainMenu = (MainMenu*)Drawable::getCurrentDrawable();
  mainMenu->quit();
}

/// \brief Prototype de fonction pour le bouton commencer
inline void startGamePrototype() {
  Scene::setScene(GARAGE);
}

/// \brief Prototype de fonction pour le bouton option
inline void optionPrototype(){
	Scene::setScene(OPTIONMENU);
}

#endif