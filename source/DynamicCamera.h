/// \file DynamicCamera.h
/// \brief Camera dynamique
#ifndef DYNAMICCAMERA_H
#define DYNAMICCAMERA_H

#include<list>
#include <cmath>
#include "Robot.h"
#include "Camera3V.h"
#include "Vector3d.h"

/// \class DynamicCamera
/// \brief Camera dynamique
///
/// Camera dynamique
///
class DynamicCamera : public Camera3v {
protected:
    Vector3d distanceBetweenTargetAndCam;///< Vecteur entre la camera et sa cible
    Robot* robot1;///< Robot 1 en prendre en compte
    Robot* robot2;///< Robot 2 en prendre en compte
    double xDistanceBetweenRobots;///< Distance en x entre les robots
    Vector3d robot1Position;
    Vector3d robot2Position;

public:
    DynamicCamera(Vector3d position, Vector3d target, Vector3d up, Robot* robot1, Robot* robot2) : Camera3v(position, target, up){
        this->robot1 = robot1;
        this->robot2 = robot2;
		if(robot1)
			robot1Position = (*robot1->getComponentList()->begin())->getPosition(); //robot1->getCenterOfMassPosition();
		
		if(robot2)
			robot2Position = (*robot2->getComponentList()->begin())->getPosition(); //robot2->getCenterOfMassPosition();

        distanceBetweenTargetAndCam = position - target;
		if (robot1 and robot2)
			xDistanceBetweenRobots = abs((*robot2->getComponentList()->begin())->getPosition().x - (*robot1->getComponentList()->begin())->getPosition().x);
		else
			xDistanceBetweenRobots = 0;
    }

    /// \brief Approcher ou eloigner la camera de son centre
    /// \param amount Scalaire multipliant la norme ou s'ajoutant a la norme
    /// \param normAddingOrScaleNorm Multiplier la norme par l'amount ou ajouter l'amount a la norme.
    void zoom(double amount, bool normAdding = false) {//amount < 1 => s'approche. amount > 1 => s'eloigne
        if(normAdding)
            amount = (distanceBetweenTargetAndCam.getNorm() + amount) / distanceBetweenTargetAndCam.getNorm();

        distanceBetweenTargetAndCam = distanceBetweenTargetAndCam * amount;
        position.z = target.z + distanceBetweenTargetAndCam.z;
        position.y = target.y + distanceBetweenTargetAndCam.y;
	}

    /// \brief Replace la camera selon la position et le mouvement des robots
    void applyDynamicView(){

        if(robot1Position.z < robot2Position.z){
            double translation = (*robot1->getComponentList()->begin())->getPosition().z - robot1Position.z;
            target.z += translation;
            position.z += translation;
        }else{
            double translation = (*robot2->getComponentList()->begin())->getPosition().z - robot2Position.z;
            target.z += translation;
            position.z += translation;
        }

		robot1Position = (*robot1->getComponentList()->begin())->getPosition(); //robot1->getCenterOfMassPosition();
		robot2Position = (*robot2->getComponentList()->begin())->getPosition(); //robot2->getCenterOfMassPosition();

        double xDistanceToCompare = xDistanceBetweenRobots;
        xDistanceBetweenRobots = abs(robot2Position.x - robot1Position.x);

        //centrer la camera sur les robots
        if(robot1){
            double xCenter = robot1Position.x;
            if(robot2){
                xCenter += robot2Position.x;
                xCenter /= 2;
            }

            target.x = xCenter;
        }
        position.x = target.x;

        if((xDistanceBetweenRobots / 2) > 10)
            zoom(xDistanceBetweenRobots / xDistanceToCompare);

        applyView();
    }

    /// \brief Notification
    void notification(){

    }

};

#endif