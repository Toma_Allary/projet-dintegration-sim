/// \file Material.h
/// \brief Materiel
#ifndef MATERIAL_H
#define MATERIAL_H

/// \class Material
/// \brief Materiel
///
/// Materiel changeant les statistiques d'une composante
///
class Material {
private:
    unsigned int textureId;///< Texture de la composante
    double priceMultiplicator;///< Multiplicateur de prix
    double massMultiplicator;///< Mutliplicateur de masse
    double fireResistance;///< Resistance au degat de feu
    double iceResistance;///< Resistance au degat de glace
    double impactResistance;///< Resistance au degat d'impact
    double explosionResistance;///< Resistance au degat d'explosion
    double cutResistance;///< Resistance au degat de coupure

public:
    Material(const unsigned int& textureId ,double priceMultiplicator, double massMultiplicator, double fireResistance, double iceResistance, double impactResistance, double explosionResistance, double cutResistance){
        this->textureId = textureId;
        this->priceMultiplicator = priceMultiplicator;
        this->massMultiplicator = massMultiplicator;
        this->fireResistance = fireResistance;
        this->iceResistance = iceResistance;
        this->impactResistance = impactResistance;
        this->explosionResistance = explosionResistance;
        this->cutResistance = cutResistance;
    }

    ~Material(){
        glDeleteTextures(1, &textureId);
    }

    /// \brief Obtenir la texture
    /// \return texture
    inline unsigned int getTexture(){
        return textureId;
    }

    /// \brief Obtenir le Multiplicateur de prix
    /// \return Multiplicateur de prix
    inline double getPriceMutltiplicator(){
        return priceMultiplicator;
    }
    /// \brief Obtenir le Multiplicateur de masse
    /// \return Multiplicateur de masse
    inline double getMassMultiplicator(){
        return massMultiplicator;
    }
    /// \brief Obtenir la resistance au degat de feu
    /// \return Resistance au degat de feu
    inline double getFireResistance(){
        return fireResistance;
    }
    /// \brief Obtenir la resistance au degat de glace
    /// \return Resistance au degat de glace
    inline double getIceResistance(){
        return iceResistance;
    }
    /// \brief Obtenir la resistance au degat d'impact
    /// \return Resistance au degat d'impact
    inline double getImpactResistance(){
        return impactResistance;
    }
    /// \brief Obtenir la resistance au degat d'explosion'
    /// \return Resistance au degat d'explosion
    inline double getExplosionResistance(){
        return explosionResistance;
    }
    /// \brief Obtenir la resistance au degat de coupure
    /// \return Resistance au degat de coupure
    inline double getCutResistance(){
        return cutResistance;
    }

};

#endif