/// \file Shop.h
/// \brief boutique
#ifndef SHOP_H
#define SHOP_H

#define SHOP_TEXTURE_BUTTON_BACKGROUND 0
#define SHOP_TEXTURE_OVERBUTTON_BACKGROUND 1
#define SHOP_TEXTURE_SHOP_BACKGROUND 2

#define METAL 1
#define WOOD 2

#include <list>
#include "Scene.h"
#include "Image.h"
#include "Label.h"
#include "FrameBloc.h"
#include "FlameThrower.h"
#include "Saw.h"
#include "Catapult.h"
#include "StandardWheel.h"
#include "NailedWheel.h"
#include "Inventory.h"
#include "HintBox.h"
#include "ComponentButton.h"
#include "EnergyBlock.h"
#include "YSnap.h"
#include "Material.h"
#include "Spring.h"
#include "Propeller.h"

using namespace std;

/// \class Shop
/// \brief Boutique
///
/// Boutique
///
class Shop : public Scene {
private:
  map<unsigned int, Material*> materials; ///< Materiaux possibles
  unsigned int currentMaterial; ///< Materiau actuel
  Material* currentMaterialPointer;
  int money; ///< Monnaie du joueur
  map<string, list<Component*>*> generalComponent; ///< Map de liste pour chaque type de composante
  unsigned int texturesId[3];

public:
  /// \brief Constructeur
  /// \param money Argent de base du joueur
  /// \param inventory Pointeur de l'inventaire du garage
  Shop(const unsigned int& money) {
    this->money = money;

    texturesId[0] = SDLGLContext::getInstance()->loadTexture("Textures/buttonBackground.png");
    texturesId[1] = SDLGLContext::getInstance()->loadTexture("Textures/_buttonBackground.png");
    texturesId[2] = SDLGLContext::getInstance()->loadTexture("Textures/panelBackground.png");

    materials[METAL] = new Material(SDLGLContext::getInstance()->loadTexture("Textures/usedMetal.png"), 2.00, 2.00, 2.00, 2.00, 2.00, 2.00, 2.00);
    materials[WOOD] = new Material(SDLGLContext::getInstance()->loadTexture("Textures/wood.png"), 1.50, 1.50, 1.50, 1.50, 1.50, 1.50, 1.50);
    currentMaterialPointer = materials[METAL];
		currentMaterial = 0;

    drawables["background"] = new Image(texturesId[SHOP_TEXTURE_SHOP_BACKGROUND], 905, 0, 300, 700);

		drawables["moneyLeft"] = new Label(945, 0, ResourceManager::get<Font*>("font60"), to_string(money) + " LL", { 0, 0, 255, 255 }, 0.5);

    drawables["Materiaux"] = new LabelImageButton(texturesId[SHOP_TEXTURE_BUTTON_BACKGROUND], ResourceManager::get<Font*>("font20"), {255, 255, 255, 255}, "Materiaux", 919, 100, 270, 50, 0.1, texturesId[SHOP_TEXTURE_OVERBUTTON_BACKGROUND]);

    generalComponent["Structures"] = new list<Component*>();
    generalComponent["Locomotions"] = new list<Component*>();
    generalComponent["Armes"] = new list<Component*>();
    generalComponent["Outils"] = new list<Component*>();
		generalComponent["Robot Complet"] = new list<Component*>();
    generalComponent["Fix"] = new list<Component*>();
    generalComponent["Locomotions"]->push_back(new StandardWheel(currentMaterialPointer, 0, 2, -25.0));
    //generalComponent["Locomotions"]->push_back(new NailedWheel(currentMaterialPointer, 0, 2, -25.0));
    generalComponent["Armes"]->push_back(new FlameThrower(currentMaterialPointer, 0, 2, -25.0));
    generalComponent["Armes"]->push_back(new Saw(currentMaterialPointer, 0, 2, -25.0));
    generalComponent["Armes"]->push_back(new Catapult(currentMaterialPointer, 0, 2, -25.0));
    generalComponent["Outils"]->push_back(new Spring(currentMaterialPointer, 0.0, 2.0, -25.0));
    generalComponent["Outils"]->push_back(new Propeller(currentMaterialPointer, 0.0, 2.0,-25.0));
    generalComponent["Outils"]->push_back(new YSnap(currentMaterialPointer, 0.0, 2.0,-25.0));

    generalComponent["Structures"]->push_back(new FrameBloc(currentMaterialPointer, 0, 2, -25.0));
		generalComponent["Structures"]->push_back(new EnergyBlock(0.0, 2.0, -25.0));
		//pour l'ordre d'un robot complet 1- Frame 2- Bloc d'�nergie 3- Arme/Outil 4- Locomotions
		generalComponent["Robot Complet"]->push_back(new FrameBloc(currentMaterialPointer, 0, 2, -25.0));
		generalComponent["Robot Complet"]->push_back(new EnergyBlock(0.0, 2.0, -25.0));
		generalComponent["Robot Complet"]->push_back(new Saw(currentMaterialPointer, 0, 2, -25.0));
		generalComponent["Robot Complet"]->push_back(new StandardWheel(currentMaterialPointer, 0, 2, -25.0));
		generalComponent["Robot Complet"]->push_back(new StandardWheel(currentMaterialPointer, 0, 2, -25.0));
		generalComponent["Robot Complet"]->push_back(new StandardWheel(currentMaterialPointer, 0, 2, -25.0));
		generalComponent["Robot Complet"]->push_back(new StandardWheel(currentMaterialPointer, 0, 2, -25.0));

    double positionIt = 0.0;
    for(auto it : generalComponent) {
      drawables[it.first + "Button"] = new LabelImageButton(texturesId[SHOP_TEXTURE_BUTTON_BACKGROUND], ResourceManager::get<Font*>("font20"), {255, 255, 255, 255}, it.first, 919.0, 155.0 + positionIt++ * 55.0, 270, 50, 0.1, texturesId[SHOP_TEXTURE_OVERBUTTON_BACKGROUND]);
      ((LabelImageButton*)drawables[it.first + "Button"])->changeTexture(materials[METAL]->getTexture());
    }

    list<string>* stringDescriptionBox = new list<string>;
    stringDescriptionBox->push_back("Description:");
    drawables["DescritionBox"] = new HintBox(texturesId[SHOP_TEXTURE_BUTTON_BACKGROUND], stringDescriptionBox,{255, 255, 255, 255}, 1200.0, 700.0, 1.0, 1.0, 0.2);
    ((HintBox*)drawables["DescritionBox"])->setPosition(1200.0- ((HintBox*)drawables["DescritionBox"])->getWidth(), 700.0 - ((HintBox*)drawables["DescritionBox"])->getHeight());
  }

  ~Shop(){
   // for (auto it : materials) delete it.second;
    for (auto it : drawables) delete it.second;
        
		for(auto it : generalComponent)
      for(auto it2 : *it.second)
        delete it2;

    glDeleteTextures(3, texturesId);
  }

  /// \brief Changer la materiau
  void changeMaterial() {
    currentMaterial++;
    for(auto it : generalComponent){
      ((LabelImageButton*)drawables[it.first + "Button"])->changeTexture(materials[currentMaterial % materials.size() + 1]->getTexture());
    }
    currentMaterialPointer = materials[currentMaterial % materials.size() + 1];
  }

  /// \brief obtenir le materiau du garage
  /// \return Materiau
  inline Material* getCurrentMaterial() {
    return currentMaterialPointer;
  }

  /// \brief Obtenir un drawable
  /// \param name Nom du drawable
  /// \return Retourn le drawable
  inline Drawable* getDrawable(const string& name) {
    return drawables[name];
  }

  /// \brief obtenir l'argent restant
  /// \return retourne l'argent restant
  inline int getMoney(){
    return money;
  }

	inline void setMoney(const int& money) {
	  this->money = money;
        ((Label*)drawables["moneyLeft"])->setText(to_string(money) + " LL", { 0, 0, 255, 255 });
	}

  /// \brief Obtenir la liste d'un type de composante
  /// \param generalComponent Type de composante
  /// \return Retourne la liste du type de composante
  inline list<Component*>* getGeneralComponentList(const string& generalComponentName){
    return generalComponent[generalComponentName];
  }

  void refreshFixableComponents(list<Component*>* actualPlayerBoughtComponentList){
    if(actualPlayerBoughtComponentList){
      for(int i = 1; generalComponent["Fix"]->size(); i++){
        generalComponent["Fix"]->pop_back();
        drawables.erase("Fix" + to_string(i));
      }

      unsigned int componentIt = 1;
      for(auto it : *actualPlayerBoughtComponentList){        
        if(it->getHealth() < it->getMaxHealth()){
          generalComponent["Fix"]->push_back(it);
          drawables["Fix" + to_string(componentIt)] = new ComponentButton(it, it->getMaterial()->getTexture(), ResourceManager::get<Font *>("font20"),{255, 255, 255, 255}, it->getName(), 945,(100 +(componentIt * 30)),200, 25, 0.1,texturesId[SHOP_TEXTURE_OVERBUTTON_BACKGROUND]);
          componentIt++;
        }
      }
    }
  }

  /// \brief Passe de la boutique a la sous boutique ou inversement selon le booleen
  /// \param generalComponentName Nom de type de composante
  /// \param panelState true pour une sous boutique et false pour la boutique
  void changePanelState(const string& generalComponentName, const bool& panelState, list<Component*>* actualPlayerBoughtComponentList = nullptr) {
    if(panelState){
        for (auto it : generalComponent)
            drawables[it.first + "Button"]->setVisible(false);
        drawables["Materiaux"]->setVisible(false);

        drawables["SpecializedShopTitle"] = new LabelImageButton(texturesId[SHOP_TEXTURE_BUTTON_BACKGROUND],ResourceManager::get<Font *>("font20"),{124, 56, 223, 255}, generalComponentName, 910,100, 270, 25, 0.1,texturesId[SHOP_TEXTURE_OVERBUTTON_BACKGROUND]);
        
        if(generalComponentName == "Fix") {
          unsigned int componentIt = 1;
          for(auto it : *actualPlayerBoughtComponentList){
            drawables["Fix" + to_string(componentIt)];//car mis dans un if, creer pareil
            drawables.erase("Fix" + to_string(componentIt));//todo: gerer le unsubscribe du bouton //todo: voir comment delete le pointeur de drawables
            
            if(it->getHealth() < it->getMaxHealth()){
              generalComponent["Fix"]->push_back(it);
              componentIt++;

            }
          }
        }
        unsigned int componentIt = 1;
        for (list<Component*>::iterator it = generalComponent[generalComponentName]->begin(); it != generalComponent[generalComponentName]->end(); it++) {
          drawables[generalComponentName + to_string(componentIt)] = new ComponentButton(*it, materials[currentMaterial % materials.size() + 1]->getTexture(), ResourceManager::get<Font *>("font20"),{255, 255, 255, 255},(*it)->getName(), 945,(100 +(componentIt * 30)),200, 25, 0.1,texturesId[SHOP_TEXTURE_OVERBUTTON_BACKGROUND]);
          componentIt++;
        }
    }
    else {
      for (auto it : generalComponent)
        drawables[it.first + "Button"]->setVisible(true);

      drawables["Materiaux"]->setVisible(true);
      drawables.erase("SpecializedShopTitle");
      unsigned int componentIt = 1;
      for(list<Component*>::iterator it = generalComponent[generalComponentName]->begin(); it != generalComponent[generalComponentName]->end(); it++){
        drawables.erase(generalComponentName + to_string(componentIt));
        componentIt++;
      }
      if(generalComponentName == "Fix") {
        while(generalComponent["Fix"]->size())
          generalComponent["Fix"]->pop_back();
      }
    }
  }

    /// \brief Obtenir le titre de la sous boutique
    /// \return Retourne le titre de la sous boutique
    string getSpecializedShopTitle() {
        if(drawables["SpecializedShopTitle"]){
            return ((LabelImageButton*)drawables["SpecializedShopTitle"])->getLabel()->getText();            
        }else{
            drawables.erase("SpecializedShopTitle");
            return "";
        }
    }

    /// \brief Desabonner les buttons de la boutique general
    /// \param event Evenement a se desabonner
    /// \param generalShop Si on est dans la boutique general sinon c'est la speciliser
    void unsubscribeComponentButtons(Observable* event, bool generalShop) {
        if (generalShop) {
            for (auto it : generalComponent){
                   event->unsubscribe(drawables[it.first + "Button"]);
            }
            event->unsubscribe(drawables["Materiaux"]);
        }
				else {
            string generalComponentName = getSpecializedShopTitle();
            unsigned int componentIt = 1;
            for (list<Component*>::iterator it = generalComponent[generalComponentName]->begin(); it != generalComponent[generalComponentName]->end(); it++){
                event->unsubscribe(drawables[generalComponentName + to_string(componentIt)]);
                componentIt++;
            }
        }
    }

    /// \brief Abonner les buttons de la boutique general
    /// \param event Evenement a s'abonner
    /// \param generalShop Si on est dans la boutique general sinon c'est la speciliser
    void subscribeComponentButtons(Observable* event, bool generalShop) {
        if (generalShop){
            for(auto it : generalComponent){
                   event->subscribe(drawables[it.first + "Button"]);
            }
            event->subscribe(drawables["Materiaux"]);
        }
				else {
            string generalComponentName = getSpecializedShopTitle();
            unsigned int componentIt = 1;
            for(list<Component*>::iterator it = generalComponent[generalComponentName]->begin(); it != generalComponent[generalComponentName]->end(); it++){
                event->subscribe(drawables[generalComponentName + to_string(componentIt)]);
                componentIt++;
            }
        }
    }

    /// \brief Charger le prix d'un item sur le porte feuille
    /// \param boughtItemPrice Prix a deduire
    void chargeBoughtItem(const int& boughtItemPrice) {
        money -= boughtItemPrice;
        ((Label*)drawables["moneyLeft"])->setText(to_string(money) + " LL", { 0, 0, 255, 255 });
    }

    /// \brief Affichage
    void draw(){
		// 2D
				SDLGLContext::getInstance()->setPerspective(true);
        glDisable(GL_LIGHTING);
				for(auto it : drawables){
            it.second->draw();
            if(it.second->getType()=="ComponentButton"){
                if(((ComponentButton*)it.second)->getMouseOn()){
										((HintBox*)drawables["DescritionBox"])->changeText(((ComponentButton*)it.second)->getDescription());
										((HintBox*)drawables["DescritionBox"])->setPosition(1200.0 - ((HintBox*)drawables["DescritionBox"])->getWidth(), 700.0 - ((HintBox*)drawables["DescritionBox"])->getHeight());
                }
            }
        }
				glEnable(GL_LIGHTING);
    }

    /// \brief Notificattion
    void notification() {}

    inline string getType() {
      return "Shop";
    }
};

#endif
