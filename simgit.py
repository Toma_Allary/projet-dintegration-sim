import os

print("SIM Git v2.11\n--------------------------")
choixType = input("Type : ")
commit = ''

#Choix du type d'action qui sera effectué
if choixType in ['A', 'a', 'Ajout', 'ajout', 'Ajouter', 'ajouter', 'Ajout', 'ajouté', 'Add', 'add']:
	commit += 'Ajout'
elif choixType in ['M', 'm', 'Modifier', 'modifier', 'Modification', 'modification', 'Modifié', 'modifié', 'Modif', 'modif', 'Edit', 'edit', 'E', 'e']:
	commit += 'Modification'
elif choixType in ['S', 's', 'Supression', 'supression', 'Supprimer', 'supprimer', 'Supprimé', 'supprimé', 'Delete', 'delete']:
	commit += 'Supression'
elif choixType in ['R', 'r', 'Resolution', 'resolution', 'C', 'c', 'Conflit', 'conflit', 'Résolution', 'résolution', 'Résolution de conflit' , 'résolution de coflit', 'Resolution de conflit' , 'resolution de coflit']:
	commit += 'Resolution'

if commit:
	#utilisateur entre le path du fichier à ajouter ou enlever
	if commit == 'Ajout':
		path = input("Entrer le chemin vers le fichier qui est cree: ")
		if path:
			os.system("Git add " + path)
		else:
			print('Path invalide : NomFicher/NomFicher...')
		
	
	if commit == 'Supression':
		path = input("Entrer le chemin vers le fichier a supprimer: ")
		if path:
			os.system("Git rm " + path)
		else:
			print('Path invalide : NomFicher/NomFicher...')
	
	
#Utilisateur remplit le formulaire pour l'uniformation du commit

	fichiers = input("Nom du fichier : ")
	if fichiers:
		commit += ' : ' + fichiers
		commentaire = ''
		commentaire =  input("Commentaire : ")
		if commentaire:
			credit = input("Autres personnes a crediter pour ce commit? : ")
			initiales = ''
			if credit in ['o', 'O', 'y', 'Y', 'Oui', 'oui', 'Yes', 'yes']:
				initiales = input("Initiales des personnes a crediter : ")
				print('git commit -am "' + commit + ' - ' + commentaire + ' (avec la participation de ' + initiales + ')"')
			else:
				print('git commit -am "' + commit + ' - ' + commentaire + '"')
			confirmation = input("Est-ce que le commit est valide?: ")
			if confirmation in ['o', 'O', 'y', 'Y', 'Oui', 'oui', 'Yes', 'yes']:
				if initiales:
					os.system('git commit -am "' + commit + ' - ' + commentaire + '-' + ' (avec la participation de ' + initiales + ')' + '"')
				else:
					os.system('git commit -am "' + commit + ' - ' + commentaire + '"')
				os.system("git pull origin master")
				confirmation = input("Y a t-il des conflits?: ")
				if confirmation in ['n', 'N', 'Non', 'non', 'No', 'no']:
					os.system("git push origin master")
					print('Code a ete pusher')
				else:
					print('Veulier resoudre les conflits avant de commit de nouveau.')
			
		else:
			print('Commentaire invalide : Commentaire...')
	
	else:
		print('Nom de fichier invalide : NomFichier.ext')
		
else:#code d'érreur type d'action
	print('Type invalide : (A)jout | (M)odification | (S)upression | (R)esolution de conflit' )
