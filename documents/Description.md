# Concept g�n�ral #

Combat de robots en ar�ne(s) en mode joueur vs joueur en trois dimensions. Les robots seront assembl�s et am�lior�s avec des pi�ces pr�d�termin�es (ex : scies rotatives, lance-flammes) avant chaque ronde. Les pi�ces en question pourront �tre achet�es � l�aide d�argent gagn� lors des combats. Les mat�riaux utilis�s pour la construction des robots auront tous leurs caract�ristiques propres et r�agiront diff�remment selon les attaques re�ues. Les ar�nes contiendront des armes int�gr�es pouvant endommager les robots combattants, les joueurs devront donc adapter leur style � l�ar�ne et �viter les dangers. Les robots contiendront tous une pi�ce d�alimentation � remplir avant chaque ronde. Les armes et outils utiliseront et videront cette pi�ce, qui est essentielle au fonctionnement du v�hicule. Un combattant perdra donc le combat si sa source d�alimentation est d�truite. Des sources d��nergie seront pr�sentes dans les ar�nes pour pouvoir faire le plein d��nergie en plein combat. Les ar�nes contiendront des incitatifs � combattre, qui �viteront les temps morts dans les combats.

# Sc�nes #
* Menu principal
* Chargements
* Options
* �diteur de v�hicule, Inventaire et Magasin (une sc�ne)
* Banc d'essaie
* Choix d�ar�ne
* Ar�ne
* Pause
* R�sultats

# Gestion des fonctionnalit�s #
* Activation / D�sactivation des outils
* Cam�ra dynamique et �lastique
* Pi�ces de base, attaque, d�fense, mobile, �nerg�tique.
* �dition d'un v�hicule.
* �v�nements d'ar�nes 
	* Climatiques
	* Structures
	* Physique

# Composantes et assemblage #

* R�sistance : modification du nombre de d�gats re�us d'un type de d�gat pr�cis
	* Feu 
	* Glace
	* Impacts
	* Explosions
	* Scies
* Masse : influe sur les forces que le v�hivule subit (moment d'inertie, gravit�)
* Co�t
* Consommation d'�nergie : influe sur le nombre d'activation d'un bloc/outil
* Mat�riau : influe sur la masse et les r�sistances (ex: bois et m�tal)
* Blocs d��nergie
	*ne contient pas d'option de mat�riau car automatiquement en m�tal
	*peuvent �tre reg�n�r�s par des pi�ces ext�rieures
* Structures: d�fini par sa masse, sa grosseur et son mat�riau
* Armes: provoque diff�rents types de d�gat
	*type de d�g�ts (feu, glace, collision, explosion)
	*Valeur de d�g�ts
* Outils: effets vari�s sur le v�hicule
	*type
	*Vitesse d'op�ration
* D�fense: augmente la r�sistance
* Outils de locomotion: influe sur les capacit�es de d�placement du v�hicule
	*Adh�rence
	*Puissance
	
# Jouabilit� et exemple d�taill� d'une partie compl�te #

En appuyant sur l'application, un �cran de chargement est pr�sent�. Ensuite, on a acc�s au menu o� on a le choix de commencer une partie, de quitter ou d'acc�der aux options, principalement d'affichage.

On acc�de alors au garage o� chaque joueurs � un montant de 100 Lapis Lazulis (LL) qu'il peut investir dans son inventaire. Le premier choisit un mod�le robuste � quatres roues avec un cadre en bois et quatre connecteurs libres utilisables. Le second en choisit un mod�le a�rien plus petit en bois avec le m�me nombre de connecteurs. Ils ont ensuite acc�s � un banc d'essai pour tester leur v�hicule avant de comfirmer leurs choix.

Les scies co�tent 30 LL, les pneus clout�s 30LL, les catapultes co�tent 20 LL, les lance-flammes co�tent 25 LL, les blocs de bois co�tent 10 LL par unit� cube et le m�tal 15 LL par unit� cube. Le premier choisis d'ajouter un lance-flammes et il d�cide d'attribuer la touche du lance-flammes sur le <<x>> de sa manette, une scie � laquelle il attribu la touche <<y>> de sa manette et quelques bouts de bois pour fortifier, il se garde de l'argent pour les prochaines rondes. Il appuie sur le bouton "Pr�t", laissant � son adversaire l'acc�s au garage. Le second s'installe un morceau de m�tal sur le c�t� et une catapulte en lui assignant la touche <<y>> de sa manette, augmentant son nombre de connecteurs de basees. Il garde le reste pour une autre partie. Il appuie sur Pr�t.


�cran de chargement

Les ar�nes et les �v�nements sont d�termin�es au hasard et pr�sent�s aux joueurs et la partie se d�roule selon un syst�me de manche qui fait que le premier qui gagne trois manches gagne.. Ceux-ci appuient sur "Pr�t" pour d�marrer la partie.

�cran de chargement

La partie d�marre. Les joueurs sont � leur emplacement dans l'ar�ne de glace et se font face autour du centre. Le joueur 1 avance vers le joueur deux avec ses pneux clout�s qui permettent une meilleure adh�rence, lui pointant son lance-flamme, affectant particuli�rement sa structure de bois qui est fragile au feu. Le joueur 2 commence � s'envoler et active sa catapulte, essayant d'atteindre le joueur 1 avec sa catapulte. Le joueur 1 est atteint et d�truit son lance flame dont les d�tritus sont projet�s dans l'ar�ne. Le joueur 1 en r�cup�re 2 tandis que le joueur 2 en r�cup�re 1, leur donnant un bonus de LL lors de leur prochaine visite au garage.

Le joueur 1 utilise une de ses 2 pauses disponibles pour mettre le jeu en pause.  Dans ce menu, quelques statisques sont pr�sent�s ainsi que le choix de quitter ou de continuer la partie. Le joueur 1 appuie sur "Continuer". Les deux joueurs continuent le combat s'�changeant coup jusqu'a la fin de la premeire manche. La partie se finie soit quand l'un des deux joueur n'a plus d'�nergie, soit quand le bloc d'�nergie n'a plus de vie, soit quand le temps impartie est �coul�.

Un �cran de r�sultats de partie s'affiche indiquant les d�g�ts, les bonis et le joueur qui a remport� la premiere manche. Les deux joueurs retournent au garage pour modifier leur v�hicule et pour se ravitailler.Les pi�ces qui ont �t� endommag�es mais non d�truites se r�parent automatiquement et il est possible de les am�liorer selon un niveau(1 � 3). Le joueur 2 choisi alors d'am�liorer sa catapulte pour que ses statistiques soient plus puissantes. Sa catapulte est alors une catapulte de niveau 2. Quand les deux joueurs sont pr�ts, la deuxi�me manche commence.


Au d�but de la deuxi�me manche, le joueur 1, ayant chang� ses roues afin de s'acheter un nouveau lance-flamme lors de l'entre manche, se d�place afin d'aller attaquer le joueur 2, mais d�rape sur une plaque de glace et va finir sa course contre une scie qui est le long d'un mur. La scie affecte une de ses roues sans la d�truire. Pendant ce temps, le joueur 2 essaie d'atteindre le joueur 1 avec sa catapulte mais rate son coup � plusieurs reprises. De ce fait, il commence � manquer d'�nergie et il va chercher un des bonus d'�nergie disponible dans l'ar�ne. Pour le reste de la manche, les deux joueurs se livrent un duel d�fensif, en esquivant les coups de chaque adversaire, la deuxi�me manche se termine.

L'�cran de r�sultat de partie s'affiche de nouveau, indiquant une �qualit� dans le nombre de victoire de manche. Les joueurs retournent au garage et lorsqu'ils sont pr�ts, la troisi�me et derni�re manche commence.

Au cours de l'ultime manche, le joueur 1 atteint une des h�lices du joueur 2 avec son lance-flamme, la d�truisant et la propulsant dans l'ar�ne. Ainsi, le joueur 2 se raproche un peu plus du sol pusiqu'il a perdu de la puissance de propulsion. Le joueur 1 atteint l'autre h�lice du joueur 2 et la d�truit, ce qui envoie l'h�lice dans l'ar�ne. Consid�rant le fait que le joueur 2 n'ait plus de moyen de locomotion, il est clou� au sol et ne peut plus se d�placer. Le joueur 1 en profite pour atteindre le bloc d'�nergie du joueur 2, ce qui le d�truit et vient mettre le joueur 2 K.O.. Lanche se termine. La partie se termine, affichant un �cran de r�sultat de fin de partie.

Cet �cran indique une victoire de 2 manche � 1 pour le joueur 1,le nombre de d�gats inflig� par chaque joueur lors de chaque manche ainsi que le nombre de LL remport� par chaque joueur au cours de la partie.

Les joueurs retournent au garage, pr�ts � d�buter une nouvelle partie.  
