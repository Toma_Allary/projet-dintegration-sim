# Tempête d'idées #

Tempête d'idées de projets d'intégration.

### ~~Matériel~~ ###

* ~~Lance projectile / attrapeur automatisé~~
* ~~Robot projectile à rebond~~
* ~~Robot à orientation automatisé~~
* ~~Robot combattant~~
* ~~Robot athlétique~~
* ~~Robot chasseur de trésor~~
* ~~Robot manipulateur~~


### Logiciel ###

* Robot combatant
* Éléments de RPG
* Éléments de gestion
* ~~Jeux de combat 1v1 (Mortal Kombat, Street Fighter, etc.)~~
* ~~Jeux de combat de plateforme (Smash Bros, etc.)~~
* ~~Parcours de projectile~~
* ~~Meteo sand box~~
* ~~Sport individuel~~
* ~~Simulateur de vol~~
* ~~Course de véhicule (Mario Kart)~~
* ~~Drag race~~
* ~~Lancement de projectile~~
* ~~Jeux de gestion~~
* ~~Tir spatial~~
* ~~Capture the flag~~
* ~~Style Genital Joustik,dans un autre concept.~~
* ~~Shapeshifter hunt~~
* ~~Lethal League / Ping Pong~~
* ~~Course de vol rapproché en combinaison ailée~~